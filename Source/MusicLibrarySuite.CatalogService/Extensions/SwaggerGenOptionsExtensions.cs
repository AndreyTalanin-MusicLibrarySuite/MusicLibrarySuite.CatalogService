using System;
using System.IO;

using Microsoft.Extensions.DependencyInjection;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace MusicLibrarySuite.CatalogService.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="SwaggerGenOptions" /> interface.
/// </summary>
public static class SwaggerGenOptionsExtensions
{
    private const string c_aspNetCoreXmlDocsFileName = "MusicLibrarySuite.AspNetCore.Mvc.xml";
    private const string c_catalogServiceXmlDocsFileName = "MusicLibrarySuite.CatalogService.xml";
    private const string c_catalogServiceContractsXmlDocsFileName = "MusicLibrarySuite.CatalogService.Contracts.xml";

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for built-in ASP.NET Core types.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeAspNetCoreXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        swaggerGenOptions.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, c_aspNetCoreXmlDocsFileName));
    }

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for catalog service API controllers.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeCatalogServiceXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        string applicationXmlDocsFileName = Path.Combine(AppContext.BaseDirectory, c_catalogServiceXmlDocsFileName);
        swaggerGenOptions.IncludeXmlComments(applicationXmlDocsFileName, includeControllerXmlComments: true);
    }

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for catalog service API models and service interfaces.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeCatalogServiceContractsXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        string contractsXmlDocsFileName = Path.Combine(AppContext.BaseDirectory, c_catalogServiceContractsXmlDocsFileName);
        swaggerGenOptions.IncludeXmlComments(contractsXmlDocsFileName);
    }
}
