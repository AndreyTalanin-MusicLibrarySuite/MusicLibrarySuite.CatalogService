using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.AspNetCore.Hosting;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Services;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Services;
using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Sqlite.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Services;
using MusicLibrarySuite.CatalogService.Data.Visitors;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Extensions;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

// Disable the IDE0079 (Remove unnecessary suppression) notification due to false positive alerts.
#pragma warning disable IDE0079

namespace MusicLibrarySuite.CatalogService.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    private const string c_sqliteSqlProvider = "Sqlite";
    private const string c_sqlServerSqlProvider = "SqlServer";
    private const string c_sqlProviderEnvironmentVariableName = "MUSICLIBRARYSUITE_SQLPROVIDER";

    /// <summary>
    /// Registers the <see cref="CatalogServiceDbContext" /> database context,
    /// the <see cref="IDbContextFactory{TContext}" /> database context factory,
    /// the <see cref="IDbContextScope{TContext}" /> database context scope,
    /// the <see cref="IDbContextScopeFactory{TContext}" /> database context scope factory,
    /// and the <see cref="IDbContextProvider{TContext}" /> database context provider
    /// as services in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <param name="startup">The <see cref="IStartupEx" /> object to get the application configuration from.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">When the <c>MUSICLIBRARYSUITE_SQLPROVIDER</c> environment variable is not set or is set to an unknown SQL provider.</exception>
    public static IServiceCollection AddDbContext(this IServiceCollection services, IStartupEx startup)
    {
        IConfiguration configuration = startup.Configuration;

        string? sqlProvider = Environment.GetEnvironmentVariable(c_sqlProviderEnvironmentVariableName);

        if (sqlProvider is null)
            throw new InvalidOperationException("The MUSICLIBRARYSUITE_SQLPROVIDER environment variable is not set.");
        else if (sqlProvider?.Equals(c_sqliteSqlProvider, StringComparison.InvariantCultureIgnoreCase) ?? false)
            services.AddSqliteDbContext(configuration);
        else if (sqlProvider?.Equals(c_sqlServerSqlProvider, StringComparison.InvariantCultureIgnoreCase) ?? false)
            services.AddSqlServerDbContext(configuration);
        else
            throw new InvalidOperationException("The MUSICLIBRARYSUITE_SQLPROVIDER environment variable is set to an unknown SQL provider.");

        services.AddDbContextScopeFactory<CatalogServiceDbContext>();

        return services;
    }

    /// <summary>
    /// Registers additional database services in the specified <see cref="IServiceCollection" /> collection.
    /// <para>The list of registered services:</para>
    /// <list type="bullet">
    /// <item>The <see cref="IPrimaryKeyValueProvider" /> primary key <see cref="Guid" /> value provider.</item>
    /// </list>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddDbServices(this IServiceCollection services)
    {
        string? sqlProvider = Environment.GetEnvironmentVariable(c_sqlProviderEnvironmentVariableName);

        services.AddTransient<IPrimaryKeyValueProvider, DefaultPrimaryKeyValueProvider>();
        services.AddTransient<IPrimaryKeyValueProvider, EntityFrameworkPrimaryKeyValueProvider>();

        if (sqlProvider?.Equals(c_sqlServerSqlProvider, StringComparison.InvariantCultureIgnoreCase) ?? false)
        {
            services.AddTransient<IPrimaryKeyValueProvider, SqlServerPrimaryKeyValueProvider>((serviceProvider) =>
            {
                IDbContextProvider<CatalogServiceDbContext> contextProvider =
                    serviceProvider.GetRequiredService<IDbContextProvider<CatalogServiceDbContext>>();

                return new SqlServerPrimaryKeyValueProvider(contextProvider);
            });
        }

        services.AddSingleton<ISetPrimaryKeyVisitor<Catalog, CatalogId>, CatalogSetPrimaryKeyVisitor>();
        services.AddSingleton<ISetNavigationPropertyForeignKeysVisitor<Catalog>, CatalogSetNavigationPropertyForeignKeysVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<Catalog>, CatalogCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISortNavigationPropertiesVisitor<Catalog>, CatalogSortNavigationPropertiesVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogRelationship>, CatalogRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogRelationship>, CatalogRelationshipSetDisplayOrderVisitor>();

        services.AddSingleton<ISetPrimaryKeyVisitor<CatalogNode, CatalogNodeId>, CatalogNodeSetPrimaryKeyVisitor>();
        services.AddSingleton<ISetNavigationPropertyForeignKeysVisitor<CatalogNode>, CatalogNodeSetNavigationPropertyForeignKeysVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogNode>, CatalogNodeCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISortNavigationPropertiesVisitor<CatalogNode>, CatalogNodeSortNavigationPropertiesVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship>, CatalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship>, CatalogNodeToCatalogRelationshipSetDisplayOrderVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship>, CatalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship>, CatalogNodeHierarchicalRelationshipSetDisplayOrderVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship>, CatalogNodeRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogNodeRelationship>, CatalogNodeRelationshipSetDisplayOrderVisitor>();

        services.AddSingleton<ISetPrimaryKeyVisitor<CatalogEntry, CatalogEntryId>, CatalogEntrySetPrimaryKeyVisitor>();
        services.AddSingleton<ISetNavigationPropertyForeignKeysVisitor<CatalogEntry>, CatalogEntrySetNavigationPropertyForeignKeysVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogEntry>, CatalogEntryCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISortNavigationPropertiesVisitor<CatalogEntry>, CatalogEntrySortNavigationPropertiesVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship>, CatalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship>, CatalogEntryToCatalogRelationshipSetDisplayOrderVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship>, CatalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship>, CatalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship>, CatalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship>, CatalogEntryHierarchicalRelationshipSetDisplayOrderVisitor>();
        services.AddSingleton<ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship>, CatalogEntryRelationshipCleanUpNavigationPropertiesVisitor>();
        services.AddSingleton<ISetDisplayOrderVisitor<CatalogEntryRelationship>, CatalogEntryRelationshipSetDisplayOrderVisitor>();

        return services;
    }

    /// <summary>
    /// Registers the CMS (Content Management System) repositories in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">When the <c>MUSICLIBRARYSUITE_SQLPROVIDER</c> environment variable is not set or is set to an unknown SQL provider.</exception>
    public static IServiceCollection AddCmsRepositories(this IServiceCollection services)
    {
        string? sqlProvider = Environment.GetEnvironmentVariable(c_sqlProviderEnvironmentVariableName);

        if (sqlProvider is null)
            throw new InvalidOperationException("The MUSICLIBRARYSUITE_SQLPROVIDER environment variable is not set.");
        else if (sqlProvider?.Equals(c_sqliteSqlProvider, StringComparison.InvariantCultureIgnoreCase) ?? false)
            services.AddSqliteCmsRepositories();
        else if (sqlProvider?.Equals(c_sqlServerSqlProvider, StringComparison.InvariantCultureIgnoreCase) ?? false)
            services.AddSqlServerCmsRepositories();
        else
            throw new InvalidOperationException("The MUSICLIBRARYSUITE_SQLPROVIDER environment variable is set to an unknown SQL provider.");

        return services;
    }

    /// <summary>
    /// Registers the CMS (Content Management System) services in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddCmsServices(this IServiceCollection services)
    {
#pragma warning disable IDE0001
        services.AddTransient<ICatalogService, MusicLibrarySuite.CatalogService.Core.Services.CatalogService>();
        services.AddTransient<ICatalogRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogRelationshipServiceExtension>();
        services.AddTransient<ICatalogServiceFacade, CatalogServiceFacade>();

        services.AddTransient<ICatalogNodeService, MusicLibrarySuite.CatalogService.Core.Services.CatalogNodeService>();
        services.AddTransient<ICatalogNodeToCatalogRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogNodeToCatalogRelationshipServiceExtension>();
        services.AddTransient<ICatalogNodeHierarchicalRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogNodeHierarchicalRelationshipServiceExtension>();
        services.AddTransient<ICatalogNodeRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogNodeRelationshipServiceExtension>();
        services.AddTransient<ICatalogNodeServiceFacade, CatalogNodeServiceFacade>();

        services.AddTransient<ICatalogEntryService, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryService>();
        services.AddTransient<ICatalogEntryToCatalogRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryToCatalogRelationshipServiceExtension>();
        services.AddTransient<ICatalogEntryToCatalogNodeRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryToCatalogNodeRelationshipServiceExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryHierarchicalRelationshipServiceExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipTypeServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryHierarchicalRelationshipTypeServiceExtension>();
        services.AddTransient<ICatalogEntryRelationshipServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryRelationshipServiceExtension>();
        services.AddTransient<ICatalogEntryRelationshipTypeServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryRelationshipTypeServiceExtension>();
        services.AddTransient<ICatalogEntryTypeServiceExtension, MusicLibrarySuite.CatalogService.Core.Services.CatalogEntryTypeServiceExtension>();
        services.AddTransient<ICatalogEntryServiceFacade, CatalogEntryServiceFacade>();
#pragma warning restore IDE0001

        return services;
    }
}
