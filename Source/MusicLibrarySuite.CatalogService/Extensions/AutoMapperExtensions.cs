using AutoMapper;

using MusicLibrarySuite.CatalogService.Core.AutoMapper;

namespace MusicLibrarySuite.CatalogService.Extensions;

/// <summary>
/// Provides a set of extension methods for AutoMapper configuration interfaces.
/// </summary>
public static class AutoMapperExtensions
{
    /// <summary>
    /// Adds the CMS (Content Management System) profile types. Profiles will be instantiated and added to the configuration.
    /// </summary>
    /// <param name="mapperConfigurationExpression">The <see cref="IMapperConfigurationExpression" /> instance to use.</param>
    public static void AddCmsProfiles(this IMapperConfigurationExpression mapperConfigurationExpression)
    {
        mapperConfigurationExpression.AddProfile<CatalogProfile>();
        mapperConfigurationExpression.AddProfile<CatalogNodeProfile>();
        mapperConfigurationExpression.AddProfile<CatalogEntryProfile>();
    }
}
