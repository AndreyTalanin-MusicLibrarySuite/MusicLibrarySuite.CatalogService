using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using MusicLibrarySuite.AspNetCore.Diagnostics.Services.Abstractions;
using MusicLibrarySuite.AspNetCore.Mvc.Controllers.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Controllers;

/// <summary>
/// Represents an API controller for the methods provided by the <see cref="ICatalogNodeService" /> service.
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class CatalogNodeController : MusicLibrarySuiteControllerBase
{
    private readonly ICatalogNodeServiceFacade m_catalogNodeServiceFacade;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeController" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeServiceFacade">The catalog node service facade.</param>
    /// <param name="exceptionHandlerFeatureSource">The <see cref="IExceptionHandlerFeatureSource" /> instance to manually set the <see cref="IExceptionHandlerFeature" /> feature of <see cref="HttpContext" /> HTTP context.</param>
    public CatalogNodeController(ICatalogNodeServiceFacade catalogNodeServiceFacade, IExceptionHandlerFeatureSource exceptionHandlerFeatureSource)
        : base(exceptionHandlerFeatureSource)
    {
        m_catalogNodeServiceFacade = catalogNodeServiceFacade;
    }

    #region ICatalogNodeService Service Core Methods

    /// <summary>
    /// Asynchronously gets a catalog node by its unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog node is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogNodeModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogNodeModel>> GetCatalogNodeAsync([Required][FromQuery] Guid catalogNodeId, CancellationToken cancellationToken)
    {
        CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
        CatalogNodeModel? catalogNodeModel = await m_catalogNodeServiceFacade.GetCatalogNodeAsync(catalogNodeIdStronglyTyped, cancellationToken);
        return catalogNodeModel is not null
            ? (ActionResult<CatalogNodeModel>)Ok(catalogNodeModel)
            : (ActionResult<CatalogNodeModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalog nodes by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogNodeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogNodeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogNodeModel[]>> GetCatalogNodesAsync([Required][FromQuery] Guid[] catalogNodeIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogNodeId> catalogNodeIdsStronglyTyped = catalogNodeIds.Select(catalogNodeId => new CatalogNodeId(catalogNodeId));
        CatalogNodeModel[] catalogNodeModels = await m_catalogNodeServiceFacade.GetCatalogNodesAsync(catalogNodeIdsStronglyTyped, cancellationToken);
        return Ok(catalogNodeModels);
    }

    /// <summary>
    /// Asynchronously gets all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogNodeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogNodeModel[]>> GetAllCatalogNodesAsync(CancellationToken cancellationToken)
    {
        CatalogNodeModel[] catalogNodeModels = await m_catalogNodeServiceFacade.GetCatalogNodesAsync(cancellationToken);
        return Ok(catalogNodeModels);
    }

    /// <summary>
    /// Asynchronously counts all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with a total number of catalog nodes written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<int>> CountAllCatalogNodesAsync(CancellationToken cancellationToken)
    {
        int catalogNodesCount = await m_catalogNodeServiceFacade.CountCatalogNodesAsync(cancellationToken);
        return Ok(catalogNodesCount);
    }

    /// <summary>
    /// Asynchronously adds a new catalog node.
    /// </summary>
    /// <param name="catalogNodeModel">The catalog node to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog node is added successfully, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the updated <see cref="CatalogNodeModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// <para>
    /// If the catalog node is added successfully,
    /// the updated <see cref="CatalogNodeModel" /> model will have its <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogNodeModel.Id" /> set.
    /// </para>
    /// </returns>
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<CatalogNodeModel>> AddCatalogNodeAsync([Required][FromBody] CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken)
    {
        try
        {
            CatalogNodeModel addedCatalogNodeModel = await m_catalogNodeServiceFacade.AddCatalogNodeAsync(catalogNodeModel, cancellationToken);
            return Ok(addedCatalogNodeModel);
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously updates an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeModel">The catalog node to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog node is found and updated, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog node is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> UpdateCatalogNodeAsync([Required][FromBody] CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken)
    {
        try
        {
            await m_catalogNodeServiceFacade.UpdateCatalogNodeAsync(catalogNodeModel, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously removes an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog node is found and removed, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog node is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> RemoveCatalogNodeAsync([Required][FromQuery] Guid catalogNodeId, CancellationToken cancellationToken)
    {
        try
        {
            CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
            await m_catalogNodeServiceFacade.RemoveCatalogNodeAsync(catalogNodeIdStronglyTyped, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogNodeToCatalogRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog-node-to-catalog relationships by a catalog node's or a catalog's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog-node-to-catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogNodeToCatalogRelationshipModel.Catalog" />, otherwise where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeToCatalogRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogNodeToCatalogRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogNodeToCatalogRelationshipModel[]>> GetCatalogNodeToCatalogRelationshipsAsync([FromQuery] Guid catalogNodeId, [FromQuery] Guid catalogId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
        CatalogId catalogIdStronglyTyped = new(catalogId);
        CatalogNodeToCatalogRelationshipModel[] catalogNodeToCatalogRelationshipModels = !byTarget
            ? await m_catalogNodeServiceFacade.GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeIdStronglyTyped, cancellationToken)
            : await m_catalogNodeServiceFacade.GetCatalogNodeToCatalogRelationshipsAsync(catalogIdStronglyTyped, cancellationToken);
        return Ok(catalogNodeToCatalogRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog-node-to-catalog relationships for an existing catalog node or an existing catalog.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog-node-to-catalog relationships for.</param>
    /// <param name="catalogId">The unique identifier of the catalog to reorder catalog-node-to-catalog relationships for.</param>
    /// <param name="catalogNodeToCatalogRelationshipModels">The collection of catalog-node-to-catalog relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog-node-to-catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogNodeToCatalogRelationshipModel.Catalog" />, otherwise where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeToCatalogRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog-node-to-catalog relationships are reordered
    /// or the original collection of catalog-node-to-catalog relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogNodeToCatalogRelationshipsAsync([FromQuery] Guid catalogNodeId, [FromQuery] Guid catalogId, [Required][FromBody] CatalogNodeToCatalogRelationshipModel[] catalogNodeToCatalogRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
            CatalogId catalogIdStronglyTyped = new(catalogId);
            if (!byTarget)
                await m_catalogNodeServiceFacade.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeIdStronglyTyped, catalogNodeToCatalogRelationshipModels, cancellationToken);
            else
                await m_catalogNodeServiceFacade.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogIdStronglyTyped, catalogNodeToCatalogRelationshipModels, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogNodeHierarchicalRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog node hierarchical relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationshipModel.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogNodeHierarchicalRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogNodeHierarchicalRelationshipModel[]>> GetCatalogNodeHierarchicalRelationshipsAsync([Required][FromQuery] Guid catalogNodeId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
        CatalogNodeHierarchicalRelationshipModel[] catalogNodeHierarchicalRelationshipModels = await m_catalogNodeServiceFacade.GetCatalogNodeHierarchicalRelationshipsAsync(catalogNodeIdStronglyTyped, byTarget, cancellationToken);
        return Ok(catalogNodeHierarchicalRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog node hierarchical relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog node hierarchical relationships for.</param>
    /// <param name="catalogNodeHierarchicalRelationshipModels">The collection of catalog node hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationshipModel.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog node hierarchical relationships are reordered
    /// or the original collection of catalog node hierarchical relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogNodeHierarchicalRelationshipsAsync([Required][FromQuery] Guid catalogNodeId, [Required][FromBody] CatalogNodeHierarchicalRelationshipModel[] catalogNodeHierarchicalRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
            await m_catalogNodeServiceFacade.ReorderCatalogNodeHierarchicalRelationshipsAsync(catalogNodeIdStronglyTyped, catalogNodeHierarchicalRelationshipModels, byTarget, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogNodeRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog node relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeRelationshipModel.DependentCatalogNode" /> instead of <see cref="CatalogNodeRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogNodeRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogNodeRelationshipModel[]>> GetCatalogNodeRelationshipsAsync([Required][FromQuery] Guid catalogNodeId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
        CatalogNodeRelationshipModel[] catalogNodeRelationshipModels = await m_catalogNodeServiceFacade.GetCatalogNodeRelationshipsAsync(catalogNodeIdStronglyTyped, byTarget, cancellationToken);
        return Ok(catalogNodeRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog node relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog node relationships for.</param>
    /// <param name="catalogNodeRelationshipModels">The collection of catalog node relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeRelationshipModel.DependentCatalogNode" /> instead of <see cref="CatalogNodeRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog node relationships are reordered
    /// or the original collection of catalog node relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogNodeRelationshipsAsync([Required][FromQuery] Guid catalogNodeId, [Required][FromBody] CatalogNodeRelationshipModel[] catalogNodeRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
            await m_catalogNodeServiceFacade.ReorderCatalogNodeRelationshipsAsync(catalogNodeIdStronglyTyped, catalogNodeRelationshipModels, byTarget, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion
}
