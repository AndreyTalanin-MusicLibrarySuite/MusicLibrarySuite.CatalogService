using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using MusicLibrarySuite.AspNetCore.Diagnostics.Services.Abstractions;
using MusicLibrarySuite.AspNetCore.Mvc.Controllers.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Controllers;

/// <summary>
/// Represents an API controller for the methods provided by the <see cref="ICatalogEntryService" /> service.
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class CatalogEntryController : MusicLibrarySuiteControllerBase
{
    private readonly ICatalogEntryServiceFacade m_catalogEntryServiceFacade;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryController" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryServiceFacade">The catalog entry service facade.</param>
    /// <param name="exceptionHandlerFeatureSource">The <see cref="IExceptionHandlerFeatureSource" /> instance to manually set the <see cref="IExceptionHandlerFeature" /> feature of <see cref="HttpContext" /> HTTP context.</param>
    public CatalogEntryController(ICatalogEntryServiceFacade catalogEntryServiceFacade, IExceptionHandlerFeatureSource exceptionHandlerFeatureSource)
        : base(exceptionHandlerFeatureSource)
    {
        m_catalogEntryServiceFacade = catalogEntryServiceFacade;
    }

    #region ICatalogEntryService Service Core Methods

    /// <summary>
    /// Asynchronously gets a catalog entry by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog entry is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogEntryModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogEntryModel>> GetCatalogEntryAsync([Required][FromQuery] Guid catalogEntryId, CancellationToken cancellationToken)
    {
        CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
        CatalogEntryModel? catalogEntryModel = await m_catalogEntryServiceFacade.GetCatalogEntryAsync(catalogEntryIdStronglyTyped, cancellationToken);
        return catalogEntryModel is not null
            ? (ActionResult<CatalogEntryModel>)Ok(catalogEntryModel)
            : (ActionResult<CatalogEntryModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalog entries by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogEntryModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryModel[]>> GetCatalogEntriesAsync([Required][FromQuery] Guid[] catalogEntryIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogEntryId> catalogEntryIdsStronglyTyped = catalogEntryIds.Select(catalogEntryId => new CatalogEntryId(catalogEntryId));
        CatalogEntryModel[] catalogEntryModels = await m_catalogEntryServiceFacade.GetCatalogEntriesAsync(catalogEntryIdsStronglyTyped, cancellationToken);
        return Ok(catalogEntryModels);
    }

    /// <summary>
    /// Asynchronously gets all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogEntryModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryModel[]>> GetAllCatalogEntriesAsync(CancellationToken cancellationToken)
    {
        CatalogEntryModel[] catalogEntryModels = await m_catalogEntryServiceFacade.GetCatalogEntriesAsync(cancellationToken);
        return Ok(catalogEntryModels);
    }

    /// <summary>
    /// Asynchronously counts all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with a total number of catalog entries written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<int>> CountAllCatalogEntriesAsync(CancellationToken cancellationToken)
    {
        int catalogEntriesCount = await m_catalogEntryServiceFacade.CountCatalogEntriesAsync(cancellationToken);
        return Ok(catalogEntriesCount);
    }

    /// <summary>
    /// Asynchronously adds a new catalog entry.
    /// </summary>
    /// <param name="catalogEntryModel">The catalog entry to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog entry is added successfully, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the updated <see cref="CatalogEntryModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// <para>
    /// If the catalog entry is added successfully,
    /// the updated <see cref="CatalogEntryModel" /> model will have its <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogEntryModel.Id" /> set.
    /// </para>
    /// </returns>
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<CatalogEntryModel>> AddCatalogEntryAsync([Required][FromBody] CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryModel addedCatalogEntryModel = await m_catalogEntryServiceFacade.AddCatalogEntryAsync(catalogEntryModel, cancellationToken);
            return Ok(addedCatalogEntryModel);
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously updates an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryModel">The catalog entry to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog entry is found and updated, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog entry is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> UpdateCatalogEntryAsync([Required][FromBody] CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken)
    {
        try
        {
            await m_catalogEntryServiceFacade.UpdateCatalogEntryAsync(catalogEntryModel, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously removes an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog entry is found and removed, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog entry is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> RemoveCatalogEntryAsync([Required][FromQuery] Guid catalogEntryId, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
            await m_catalogEntryServiceFacade.RemoveCatalogEntryAsync(catalogEntryIdStronglyTyped, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogEntryToCatalogRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog relationships by a catalog entry's or a catalog's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog-entry-to-catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogEntryToCatalogRelationshipModel.Catalog" />, otherwise where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryToCatalogRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogEntryToCatalogRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryToCatalogRelationshipModel[]>> GetCatalogEntryToCatalogRelationshipsAsync([FromQuery] Guid catalogEntryId, [FromQuery] Guid catalogId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
        CatalogId catalogIdStronglyTyped = new(catalogId);
        CatalogEntryToCatalogRelationshipModel[] catalogEntryToCatalogRelationshipModels = !byTarget
            ? await m_catalogEntryServiceFacade.GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryIdStronglyTyped, cancellationToken)
            : await m_catalogEntryServiceFacade.GetCatalogEntryToCatalogRelationshipsAsync(catalogIdStronglyTyped, cancellationToken);
        return Ok(catalogEntryToCatalogRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog relationships for an existing catalog entry or an existing catalog.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog-entry-to-catalog relationships for.</param>
    /// <param name="catalogId">The unique identifier of the catalog to reorder catalog-entry-to-catalog relationships for.</param>
    /// <param name="catalogEntryToCatalogRelationshipModels">The collection of catalog-entry-to-catalog relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog-entry-to-catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogEntryToCatalogRelationshipModel.Catalog" />, otherwise where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryToCatalogRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog-entry-to-catalog relationships are reordered
    /// or the original collection of catalog-entry-to-catalog relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogEntryToCatalogRelationshipsAsync([FromQuery] Guid catalogEntryId, [FromQuery] Guid catalogId, [Required][FromBody] CatalogEntryToCatalogRelationshipModel[] catalogEntryToCatalogRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
            CatalogId catalogIdStronglyTyped = new(catalogId);
            if (!byTarget)
                await m_catalogEntryServiceFacade.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryToCatalogRelationshipModels, cancellationToken);
            else
                await m_catalogEntryServiceFacade.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogIdStronglyTyped, catalogEntryToCatalogRelationshipModels, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogEntryToCatalogNodeRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog-node relationships by a catalog entry's or a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog-entry-to-catalog-node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogEntryToCatalogNodeRelationshipModel.CatalogNode" />, otherwise where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryToCatalogNodeRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogEntryToCatalogNodeRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryToCatalogNodeRelationshipModel[]>> GetCatalogEntryToCatalogNodeRelationshipsAsync([FromQuery] Guid catalogEntryId, [FromQuery] Guid catalogNodeId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
        CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
        CatalogEntryToCatalogNodeRelationshipModel[] catalogEntryToCatalogNodeRelationshipModels = !byTarget
            ? await m_catalogEntryServiceFacade.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryIdStronglyTyped, cancellationToken)
            : await m_catalogEntryServiceFacade.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeIdStronglyTyped, cancellationToken);
        return Ok(catalogEntryToCatalogNodeRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog-node relationships for an existing catalog entry or an existing catalog node.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog-entry-to-catalog-node relationships for.</param>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog-entry-to-catalog-node relationships for.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipModels">The collection of catalog-entry-to-catalog-node relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog-entry-to-catalog-node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogEntryToCatalogNodeRelationshipModel.CatalogNode" />, otherwise where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryToCatalogNodeRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog-entry-to-catalog-node relationships are reordered
    /// or the original collection of catalog-entry-to-catalog-node relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogEntryToCatalogNodeRelationshipsAsync([FromQuery] Guid catalogEntryId, [FromQuery] Guid catalogNodeId, [Required][FromBody] CatalogEntryToCatalogNodeRelationshipModel[] catalogEntryToCatalogNodeRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
            CatalogNodeId catalogNodeIdStronglyTyped = new(catalogNodeId);
            if (!byTarget)
                await m_catalogEntryServiceFacade.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryToCatalogNodeRelationshipModels, cancellationToken);
            else
                await m_catalogEntryServiceFacade.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeIdStronglyTyped, catalogEntryToCatalogNodeRelationshipModels, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogEntryHierarchicalRelationshipTypeId">The catalog entry hierarchical relationship type's unique identifier. May be <see langword="null" />.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationshipModel.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogEntryHierarchicalRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryHierarchicalRelationshipModel[]>> GetCatalogEntryHierarchicalRelationshipsAsync([Required][FromQuery] Guid catalogEntryId, [FromQuery] Guid? catalogEntryHierarchicalRelationshipTypeId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
        CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeIdStronglyTyped =
            catalogEntryHierarchicalRelationshipTypeId is not null ? new CatalogEntryHierarchicalRelationshipTypeId((Guid)catalogEntryHierarchicalRelationshipTypeId) : null;
        CatalogEntryHierarchicalRelationshipModel[] catalogEntryHierarchicalRelationshipModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryHierarchicalRelationshipTypeIdStronglyTyped, byTarget, cancellationToken);
        return Ok(catalogEntryHierarchicalRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog entry hierarchical relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog entry hierarchical relationships for.</param>
    /// <param name="catalogEntryHierarchicalRelationshipModels">The collection of catalog entry hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationshipModel.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog entry hierarchical relationships are reordered
    /// or the original collection of catalog entry hierarchical relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogEntryHierarchicalRelationshipsAsync([Required][FromQuery] Guid catalogEntryId, [Required][FromBody] CatalogEntryHierarchicalRelationshipModel[] catalogEntryHierarchicalRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
            await m_catalogEntryServiceFacade.ReorderCatalogEntryHierarchicalRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryHierarchicalRelationshipModels, byTarget, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipTypeServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets a catalog entry hierarchical relationship type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypeId">The catalog entry hierarchical relationship type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog entry hierarchical relationship type is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogEntryHierarchicalRelationshipTypeModel>> GetCatalogEntryHierarchicalRelationshipTypeAsync([Required][FromQuery] Guid catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeIdStronglyTyped = new(catalogEntryHierarchicalRelationshipTypeId);
        CatalogEntryHierarchicalRelationshipTypeModel? catalogEntryHierarchicalRelationshipTypeModel =
            await m_catalogEntryServiceFacade.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeIdStronglyTyped, cancellationToken);
        return catalogEntryHierarchicalRelationshipTypeModel is not null
            ? (ActionResult<CatalogEntryHierarchicalRelationshipTypeModel>)Ok(catalogEntryHierarchicalRelationshipTypeModel)
            : (ActionResult<CatalogEntryHierarchicalRelationshipTypeModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationship types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryHierarchicalRelationshipTypeModel[]>> GetCatalogEntryHierarchicalRelationshipTypesAsync([Required][FromQuery] Guid[] catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIdsStronglyTyped =
            catalogEntryHierarchicalRelationshipTypeIds.Select(catalogEntryHierarchicalRelationshipTypeId => new CatalogEntryHierarchicalRelationshipTypeId(catalogEntryHierarchicalRelationshipTypeId));
        CatalogEntryHierarchicalRelationshipTypeModel[] catalogEntryHierarchicalRelationshipTypeModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIdsStronglyTyped, cancellationToken);
        return Ok(catalogEntryHierarchicalRelationshipTypeModels);
    }

    /// <summary>
    /// Asynchronously gets all catalog entry hierarchical relationship types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryHierarchicalRelationshipTypeModel[]>> GetAllCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipTypeModel[] catalogEntryHierarchicalRelationshipTypeModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryHierarchicalRelationshipTypesAsync(cancellationToken);
        return Ok(catalogEntryHierarchicalRelationshipTypeModels);
    }

    #endregion

    #region ICatalogEntryRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog entry relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogEntryRelationshipTypeId">The catalog entry relationship type's unique identifier. May be <see langword="null" />.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryRelationshipModel.DependentCatalogEntry" /> instead of <see cref="CatalogEntryRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogEntryRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryRelationshipModel[]>> GetCatalogEntryRelationshipsAsync([Required][FromQuery] Guid catalogEntryId, [FromQuery] Guid? catalogEntryRelationshipTypeId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
        CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeIdStronglyTyped =
            catalogEntryRelationshipTypeId is not null ? new CatalogEntryRelationshipTypeId((Guid)catalogEntryRelationshipTypeId) : null;
        CatalogEntryRelationshipModel[] catalogEntryRelationshipModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryRelationshipTypeIdStronglyTyped, byTarget, cancellationToken);
        return Ok(catalogEntryRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog entry relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog entry relationships for.</param>
    /// <param name="catalogEntryRelationshipModels">The collection of catalog entry relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog entry relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryRelationshipModel.DependentCatalogEntry" /> instead of <see cref="CatalogEntryRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog entry relationships are reordered
    /// or the original collection of catalog entry relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogEntryRelationshipsAsync([Required][FromQuery] Guid catalogEntryId, [Required][FromBody] CatalogEntryRelationshipModel[] catalogEntryRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogEntryId catalogEntryIdStronglyTyped = new(catalogEntryId);
            await m_catalogEntryServiceFacade.ReorderCatalogEntryRelationshipsAsync(catalogEntryIdStronglyTyped, catalogEntryRelationshipModels, byTarget, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogEntryRelationshipTypeServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets a catalog entry relationship type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeId">The catalog entry relationship type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog entry relationship type is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogEntryRelationshipTypeModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogEntryRelationshipTypeModel>> GetCatalogEntryRelationshipTypeAsync([Required][FromQuery] Guid catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeIdStronglyTyped = new(catalogEntryRelationshipTypeId);
        CatalogEntryRelationshipTypeModel? catalogEntryRelationshipTypeModel =
            await m_catalogEntryServiceFacade.GetCatalogEntryRelationshipTypeAsync(catalogEntryRelationshipTypeIdStronglyTyped, cancellationToken);
        return catalogEntryRelationshipTypeModel is not null
            ? (ActionResult<CatalogEntryRelationshipTypeModel>)Ok(catalogEntryRelationshipTypeModel)
            : (ActionResult<CatalogEntryRelationshipTypeModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalog entry relationship types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogEntryRelationshipTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryRelationshipTypeModel[]>> GetCatalogEntryRelationshipTypesAsync([Required][FromQuery] Guid[] catalogEntryRelationshipTypeIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIdsStronglyTyped =
            catalogEntryRelationshipTypeIds.Select(catalogEntryRelationshipTypeId => new CatalogEntryRelationshipTypeId(catalogEntryRelationshipTypeId));
        CatalogEntryRelationshipTypeModel[] catalogEntryRelationshipTypeModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeIdsStronglyTyped, cancellationToken);
        return Ok(catalogEntryRelationshipTypeModels);
    }

    /// <summary>
    /// Asynchronously gets all catalog entry relationship types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogEntryRelationshipTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryRelationshipTypeModel[]>> GetAllCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipTypeModel[] catalogEntryRelationshipTypeModels =
            await m_catalogEntryServiceFacade.GetCatalogEntryRelationshipTypesAsync(cancellationToken);
        return Ok(catalogEntryRelationshipTypeModels);
    }

    #endregion

    #region ICatalogEntryTypeServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets a catalog entry type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryTypeId">The catalog entry type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog entry type is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogEntryTypeModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogEntryTypeModel>> GetCatalogEntryTypeAsync([Required][FromQuery] Guid catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryTypeId catalogEntryTypeIdStronglyTyped = new(catalogEntryTypeId);
        CatalogEntryTypeModel? catalogEntryTypeModel = await m_catalogEntryServiceFacade.GetCatalogEntryTypeAsync(catalogEntryTypeIdStronglyTyped, cancellationToken);
        return catalogEntryTypeModel is not null
            ? (ActionResult<CatalogEntryTypeModel>)Ok(catalogEntryTypeModel)
            : (ActionResult<CatalogEntryTypeModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalog entry types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogEntryTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryTypeModel[]>> GetCatalogEntryTypesAsync([Required][FromQuery] Guid[] catalogEntryTypeIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogEntryTypeId> catalogEntryTypeIdsStronglyTyped = catalogEntryTypeIds.Select(catalogEntryTypeId => new CatalogEntryTypeId(catalogEntryTypeId));
        CatalogEntryTypeModel[] catalogEntryTypeModels = await m_catalogEntryServiceFacade.GetCatalogEntryTypesAsync(catalogEntryTypeIdsStronglyTyped, cancellationToken);
        return Ok(catalogEntryTypeModels);
    }

    /// <summary>
    /// Asynchronously gets all catalog entry types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogEntryTypeModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogEntryTypeModel[]>> GetAllCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryTypeModel[] catalogEntryTypeModels = await m_catalogEntryServiceFacade.GetCatalogEntryTypesAsync(cancellationToken);
        return Ok(catalogEntryTypeModels);
    }

    #endregion
}
