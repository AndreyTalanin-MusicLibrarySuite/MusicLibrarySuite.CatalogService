using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using MusicLibrarySuite.AspNetCore.Diagnostics.Services.Abstractions;
using MusicLibrarySuite.AspNetCore.Mvc.Controllers.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Controllers;

/// <summary>
/// Represents an API controller for the methods provided by the <see cref="ICatalogService" /> service.
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class CatalogController : MusicLibrarySuiteControllerBase
{
    private readonly ICatalogServiceFacade m_catalogServiceFacade;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogController" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceFacade">The catalog service facade.</param>
    /// <param name="exceptionHandlerFeatureSource">The <see cref="IExceptionHandlerFeatureSource" /> instance to manually set the <see cref="IExceptionHandlerFeature" /> feature of <see cref="HttpContext" /> HTTP context.</param>
    public CatalogController(ICatalogServiceFacade catalogServiceFacade, IExceptionHandlerFeatureSource exceptionHandlerFeatureSource)
        : base(exceptionHandlerFeatureSource)
    {
        m_catalogServiceFacade = catalogServiceFacade;
    }

    #region ICatalogService Service Core Methods

    /// <summary>
    /// Asynchronously gets a catalog by its unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// If the catalog is found, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the <see cref="CatalogModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> instead.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CatalogModel>> GetCatalogAsync([Required][FromQuery] Guid catalogId, CancellationToken cancellationToken)
    {
        CatalogId catalogIdStronglyTyped = new(catalogId);
        CatalogModel? catalogModel = await m_catalogServiceFacade.GetCatalogAsync(catalogIdStronglyTyped, cancellationToken);
        return catalogModel is not null
            ? (ActionResult<CatalogModel>)Ok(catalogModel)
            : (ActionResult<CatalogModel>)NotFound();
    }

    /// <summary>
    /// Asynchronously gets catalogs by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all found <see cref="CatalogModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogModel[]>> GetCatalogsAsync([Required][FromQuery] Guid[] catalogIds, CancellationToken cancellationToken)
    {
        IEnumerable<CatalogId> catalogIdsStronglyTyped = catalogIds.Select(catalogId => new CatalogId(catalogId));
        CatalogModel[] catalogModels = await m_catalogServiceFacade.GetCatalogsAsync(catalogIdsStronglyTyped, cancellationToken);
        return Ok(catalogModels);
    }

    /// <summary>
    /// Asynchronously gets all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all <see cref="CatalogModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogModel[]>> GetAllCatalogsAsync(CancellationToken cancellationToken)
    {
        CatalogModel[] catalogModels = await m_catalogServiceFacade.GetCatalogsAsync(cancellationToken);
        return Ok(catalogModels);
    }

    /// <summary>
    /// Asynchronously counts all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with a total number of catalogs written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<int>> CountAllCatalogsAsync(CancellationToken cancellationToken)
    {
        int catalogsCount = await m_catalogServiceFacade.CountCatalogsAsync(cancellationToken);
        return Ok(catalogsCount);
    }

    /// <summary>
    /// Asynchronously adds a new catalog.
    /// </summary>
    /// <param name="catalogModel">The catalog to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog is added successfully, the task's result will be the <see cref="OkObjectResult" /> object
    /// with the updated <see cref="CatalogModel" /> model written to the response,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// <para>
    /// If the catalog is added successfully,
    /// the updated <see cref="CatalogModel" /> model will have its <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogModel.Id" /> set.
    /// </para>
    /// </returns>
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<CatalogModel>> AddCatalogAsync([Required][FromBody] CatalogModel catalogModel, CancellationToken cancellationToken)
    {
        try
        {
            CatalogModel addedCatalogModel = await m_catalogServiceFacade.AddCatalogAsync(catalogModel, cancellationToken);
            return Ok(addedCatalogModel);
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously updates an existing catalog.
    /// </summary>
    /// <param name="catalogModel">The catalog to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog is found and updated, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> UpdateCatalogAsync([Required][FromBody] CatalogModel catalogModel, CancellationToken cancellationToken)
    {
        try
        {
            await m_catalogServiceFacade.UpdateCatalogAsync(catalogModel, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    /// <summary>
    /// Asynchronously removes an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If the catalog is found and removed, the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="NotFoundResult" /> if the catalog is not found
    /// or <see cref="ConflictResult" /> if the operation is unsuccessful.
    /// </returns>
    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> RemoveCatalogAsync([Required][FromQuery] Guid catalogId, CancellationToken cancellationToken)
    {
        try
        {
            CatalogId catalogIdStronglyTyped = new(catalogId);
            await m_catalogServiceFacade.RemoveCatalogAsync(catalogIdStronglyTyped, cancellationToken);
            return Ok();
        }
        catch (EntityNotFoundException exception)
        {
            return EnrichClientErrorActionResult(NotFound(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion

    #region ICatalogRelationshipServiceExtension Service Extension Methods

    /// <summary>
    /// Asynchronously gets catalog relationships by a catalog's unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogRelationshipModel.DependentCatalog" /> instead of <see cref="CatalogRelationshipModel.Catalog" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="OkObjectResult" /> object
    /// with an array of all filtered <see cref="CatalogRelationshipModel" /> models written to the response.
    /// </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<CatalogRelationshipModel[]>> GetCatalogRelationshipsAsync([Required][FromQuery] Guid catalogId, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        CatalogId catalogIdStronglyTyped = new(catalogId);
        CatalogRelationshipModel[] catalogRelationshipModels = await m_catalogServiceFacade.GetCatalogRelationshipsAsync(catalogIdStronglyTyped, byTarget, cancellationToken);
        return Ok(catalogRelationshipModels);
    }

    /// <summary>
    /// Asynchronously reorders catalog relationships for an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to reorder catalog relationships for.</param>
    /// <param name="catalogRelationshipModels">The collection of catalog relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogRelationshipModel.DependentCatalog" /> instead of <see cref="CatalogRelationshipModel.Catalog" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// If all catalog relationships are reordered
    /// or the original collection of catalog relationships is empty,
    /// the task's result will be the <see cref="OkResult" /> object,
    /// otherwise the task's result will be <see cref="ConflictResult" /> instead.
    /// </returns>
    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> ReorderCatalogRelationshipsAsync([Required][FromQuery] Guid catalogId, [Required][FromBody] CatalogRelationshipModel[] catalogRelationshipModels, [Required][FromQuery] bool byTarget, CancellationToken cancellationToken)
    {
        try
        {
            CatalogId catalogIdStronglyTyped = new(catalogId);
            await m_catalogServiceFacade.ReorderCatalogRelationshipsAsync(catalogIdStronglyTyped, catalogRelationshipModels, byTarget, cancellationToken);
            return Ok();
        }
        catch (EntityConflictException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
        catch (EntityConstraintViolationException exception)
        {
            return EnrichClientErrorActionResult(Conflict(), exception);
        }
    }

    #endregion
}
