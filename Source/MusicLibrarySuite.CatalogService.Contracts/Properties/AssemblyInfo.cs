using MusicLibrarySuite.Infrastructure.Microservices.Attributes;

[assembly: AssemblyMicroserviceName("catalog-service", "MusicLibrarySuite.CatalogService")]
