using System;

namespace MusicLibrarySuite.CatalogService.Contracts.Identifiers;

/// <summary>
/// Represents a catalog node's unique identifier.
/// </summary>
public readonly record struct CatalogNodeId
{
    /// <summary>
    /// Gets the underlying <see cref="Guid" /> value.
    /// </summary>
    public Guid Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeId" /> type using the specified <see cref="Guid" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="Guid" /> value.</param>
    public CatalogNodeId(Guid value)
    {
        Value = value;
    }
}
