using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.CatalogService.Contracts.Identifiers;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to the types in the namespace.
/// </summary>
/// <remarks>
/// <para>
/// List of available CMS (Content Management System) service models' unique identifiers:
/// <list type="bullet">
/// <item><see cref="CatalogId" /></item>
/// <item><see cref="CatalogNodeId" /></item>
/// <item><see cref="CatalogEntryId" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipTypeId" /></item>
/// <item><see cref="CatalogEntryRelationshipTypeId" /></item>
/// <item><see cref="CatalogEntryTypeId" /></item>
/// </list>
/// </para>
/// </remarks>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __AboutNamespace class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __AboutNamespace
{
}
