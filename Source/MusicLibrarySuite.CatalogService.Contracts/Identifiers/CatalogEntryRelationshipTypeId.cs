using System;

namespace MusicLibrarySuite.CatalogService.Contracts.Identifiers;

/// <summary>
/// Represents a catalog entry relationship type's unique identifier.
/// </summary>
public readonly record struct CatalogEntryRelationshipTypeId
{
    /// <summary>
    /// Gets the underlying <see cref="Guid" /> value.
    /// </summary>
    public Guid Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRelationshipTypeId" /> type using the specified <see cref="Guid" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="Guid" /> value.</param>
    public CatalogEntryRelationshipTypeId(Guid value)
    {
        Value = value;
    }
}
