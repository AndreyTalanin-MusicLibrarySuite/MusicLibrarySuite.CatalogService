using System;

namespace MusicLibrarySuite.CatalogService.Contracts.Identifiers;

/// <summary>
/// Represents a catalog entry type's unique identifier.
/// </summary>
public readonly record struct CatalogEntryTypeId
{
    /// <summary>
    /// Gets the underlying <see cref="Guid" /> value.
    /// </summary>
    public Guid Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryTypeId" /> type using the specified <see cref="Guid" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="Guid" /> value.</param>
    public CatalogEntryTypeId(Guid value)
    {
        Value = value;
    }
}
