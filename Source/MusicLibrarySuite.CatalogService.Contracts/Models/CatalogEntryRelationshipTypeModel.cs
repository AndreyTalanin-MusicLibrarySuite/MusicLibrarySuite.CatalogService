using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog entry relationship type.
/// </summary>
public class CatalogEntryRelationshipTypeModel
{
    /// <summary>
    /// Gets or sets the catalog entry relationship type's unique identifier.
    /// </summary>
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship owner.
    /// </summary>
    [Required]
    public Guid OwnerCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship target.
    /// </summary>
    [Required]
    public Guid TargetCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship type's name.
    /// </summary>
    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry relationship type's code.
    /// </summary>
    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Code { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be annotated.
    /// </summary>
    [Required]
    public bool Annotated { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be unique for the catalog entry acting as a relationship owner.
    /// </summary>
    [Required]
    public bool OwnerUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be unique for the catalog entry acting as a relationship target.
    /// </summary>
    [Required]
    public bool TargetUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship type is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry relationship type was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry relationship type was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship owner.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryTypeModel? OwnerCatalogEntryType { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship target.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryTypeModel? TargetCatalogEntryType { get; set; }
}
