using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog node hierarchical relationship.
/// </summary>
public class CatalogNodeHierarchicalRelationshipModel
{
    /// <summary>
    /// Gets or sets the child (principal) catalog node's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog node's unique identifier.
    /// </summary>
    [Required]
    public Guid ParentCatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the child (principal) catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNodeModel? CatalogNode { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNodeModel? ParentCatalogNode { get; set; }
}
