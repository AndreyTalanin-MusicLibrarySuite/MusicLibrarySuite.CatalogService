using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to the types in the namespace.
/// </summary>
/// <remarks>
/// <para>
/// List of available CMS (Content Management System) service models:
/// <list type="bullet">
/// <item><see cref="CatalogModel" /></item>
/// <item><see cref="CatalogRelationshipModel" /></item>
/// <item><see cref="CatalogNodeModel" /></item>
/// <item><see cref="CatalogNodeToCatalogRelationshipModel" /></item>
/// <item><see cref="CatalogNodeHierarchicalRelationshipModel" /></item>
/// <item><see cref="CatalogNodeRelationshipModel" /></item>
/// <item><see cref="CatalogEntryModel" /></item>
/// <item><see cref="CatalogEntryToCatalogRelationshipModel" /></item>
/// <item><see cref="CatalogEntryToCatalogNodeRelationshipModel" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipModel" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipAnnotationModel" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipTypeModel" /></item>
/// <item><see cref="CatalogEntryRelationshipModel" /></item>
/// <item><see cref="CatalogEntryRelationshipAnnotationModel" /></item>
/// <item><see cref="CatalogEntryRelationshipTypeModel" /></item>
/// <item><see cref="CatalogEntryTypeModel" /></item>
/// </list>
/// </para>
/// </remarks>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __AboutNamespace class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __AboutNamespace
{
}
