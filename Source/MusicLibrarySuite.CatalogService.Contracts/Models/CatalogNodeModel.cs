using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog node.
/// </summary>
public class CatalogNodeModel
{
    /// <summary>
    /// Gets or sets the catalog node's unique identifier.
    /// </summary>
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog node's name.
    /// </summary>
    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog node's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog node is system-protected.
    /// </summary>
    [Required]
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog node is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog node was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog node was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog-node-to-catalog relationships where the current catalog node is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogNodeToCatalogRelationshipModel> CatalogNodeToCatalogRelationships { get; set; } = new List<CatalogNodeToCatalogRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog node hierarchical relationships where the current catalog node is the child (principal) entity.
    /// </summary>
    /// <remarks>
    /// This property is currently representing a one-to-one relationship. Each catalog node can have at most one parent.
    /// Use the <see cref="Enumerable.SingleOrDefault{TSource}(IEnumerable{TSource})" /> method to work with this property.
    /// </remarks>
    [Required]
    public ICollection<CatalogNodeHierarchicalRelationshipModel> CatalogNodeHierarchicalRelationships { get; set; } = new List<CatalogNodeHierarchicalRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog node hierarchical relationships where the current catalog node is the parent (dependent) entity.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public ICollection<CatalogNodeHierarchicalRelationshipModel> ChildCatalogNodeHierarchicalRelationships { get; set; } = new List<CatalogNodeHierarchicalRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog node relationships where the current catalog node is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogNodeRelationshipModel> CatalogNodeRelationships { get; set; } = new List<CatalogNodeRelationshipModel>();
}
