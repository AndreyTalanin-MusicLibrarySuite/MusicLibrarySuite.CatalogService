using System.ComponentModel.DataAnnotations;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog entry relationship annotation.
/// </summary>
public class CatalogEntryRelationshipAnnotationModel
{
    /// <summary>
    /// Gets or sets the catalog entry relationship's name.
    /// </summary>
    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry relationship's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }
}
