using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog entry.
/// </summary>
public class CatalogEntryModel
{
    /// <summary>
    /// Gets or sets the catalog entry's unique identifier.
    /// </summary>
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry is system-protected.
    /// </summary>
    [Required]
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog-entry-to-catalog relationships where the current catalog entry is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogEntryToCatalogRelationshipModel> CatalogEntryToCatalogRelationships { get; set; } = new List<CatalogEntryToCatalogRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog-entry-to-catalog-node relationships where the current catalog entry is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogEntryToCatalogNodeRelationshipModel> CatalogEntryToCatalogNodeRelationships { get; set; } = new List<CatalogEntryToCatalogNodeRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog entry hierarchical relationships where the current catalog entry is the child (principal) entity.
    /// </summary>
    [Required]
    public ICollection<CatalogEntryHierarchicalRelationshipModel> CatalogEntryHierarchicalRelationships { get; set; } = new List<CatalogEntryHierarchicalRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog entry hierarchical relationships where the current catalog entry is the parent (dependent) entity.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public ICollection<CatalogEntryHierarchicalRelationshipModel> ChildCatalogEntryHierarchicalRelationships { get; set; } = new List<CatalogEntryHierarchicalRelationshipModel>();

    /// <summary>
    /// Gets or sets a collection of catalog entry relationships where the current catalog entry is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogEntryRelationshipModel> CatalogEntryRelationships { get; set; } = new List<CatalogEntryRelationshipModel>();

    /// <summary>
    /// Gets or sets the catalog entry type.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryTypeModel? CatalogEntryType { get; set; }
}
