using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog entry hierarchical relationship.
/// </summary>
public class CatalogEntryHierarchicalRelationshipModel
{
    /// <summary>
    /// Gets or sets the child (principal) catalog entry's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog entry's unique identifier.
    /// </summary>
    [Required]
    public Guid ParentCatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogEntryHierarchicalRelationshipTypeId { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the relationship's annotation.
    /// </summary>
    public CatalogEntryHierarchicalRelationshipAnnotationModel? CatalogEntryHierarchicalRelationshipAnnotation { get; set; }

    /// <summary>
    /// Gets or sets the child (principal) catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryModel? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryModel? ParentCatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryHierarchicalRelationshipTypeModel? CatalogEntryHierarchicalRelationshipType { get; set; }
}
