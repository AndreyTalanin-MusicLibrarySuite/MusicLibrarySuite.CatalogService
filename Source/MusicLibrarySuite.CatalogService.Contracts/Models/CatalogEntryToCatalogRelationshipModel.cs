using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog-entry-to-catalog relationship.
/// </summary>
public class CatalogEntryToCatalogRelationshipModel
{
    /// <summary>
    /// Gets or sets the principal catalog entry's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog's unique identifier.
    /// </summary>
    [Required]
    public Guid CatalogId { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryModel? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogModel? Catalog { get; set; }
}
