using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrarySuite.CatalogService.Contracts.Models;

/// <summary>
/// Represents a service model for a catalog.
/// </summary>
public class CatalogModel
{
    /// <summary>
    /// Gets or sets the catalog's unique identifier.
    /// </summary>
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog's name.
    /// </summary>
    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog is system-protected.
    /// </summary>
    [Required]
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog is enabled.
    /// </summary>
    [Required]
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog relationships where the current catalog is the principal entity.
    /// </summary>
    [Required]
    public ICollection<CatalogRelationshipModel> CatalogRelationships { get; set; } = new List<CatalogRelationshipModel>();
}
