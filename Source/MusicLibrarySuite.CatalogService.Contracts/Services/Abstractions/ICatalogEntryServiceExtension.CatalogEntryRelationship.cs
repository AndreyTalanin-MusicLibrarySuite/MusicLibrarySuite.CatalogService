using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryRelationshipModel" /> service model.
/// </summary>
public interface ICatalogEntryRelationshipServiceExtension
{
    /// <summary>
    /// Asynchronously gets catalog entry relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryRelationshipModel.DependentCatalogEntry" /> instead of <see cref="CatalogEntryRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry relationships.
    /// </returns>
    public Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry relationships by a catalog entry's unique identifier and possibly a catalog entry relationship type's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogEntryRelationshipTypeId">The catalog entry relationship type's unique identifier. May be <see langword="null" />.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryRelationshipModel.DependentCatalogEntry" /> instead of <see cref="CatalogEntryRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry relationships.
    /// </returns>
    public Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog entry relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog entry relationships for.</param>
    /// <param name="catalogEntryRelationshipModels">The collection of catalog entry relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog entry relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryRelationshipModel.DependentCatalogEntry" /> instead of <see cref="CatalogEntryRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog entry relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationshipModel> catalogEntryRelationshipModels, bool byTarget, CancellationToken cancellationToken = default);
}
