using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> service model.
/// </summary>
public interface ICatalogEntryHierarchicalRelationshipTypeServiceExtension
{
    /// <summary>
    /// Asynchronously gets a catalog entry hierarchical relationship type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypeId">The catalog entry hierarchical relationship type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry hierarchical relationship type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationshipTypeModel?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationship types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry hierarchical relationship types.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entry hierarchical relationship types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entry hierarchical relationship types.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default);
}
