using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service for the <see cref="CatalogNodeModel" /> service model.
/// </summary>
public interface ICatalogNodeService
{
    /// <summary>
    /// Asynchronously gets a catalog node by its unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog node found or <see langword="null" />.
    /// </returns>
    public Task<CatalogNodeModel?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog nodes by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogNodeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog nodes.
    /// </returns>
    public Task<CatalogNodeModel[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog nodes.
    /// </returns>
    public Task<CatalogNodeModel[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalog nodes.
    /// </returns>
    public Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog node.
    /// </summary>
    /// <param name="catalogNodeModel">The catalog node to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog node with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogNodeModel.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog node with the given <see cref="CatalogNodeModel.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<CatalogNodeModel> AddCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeModel">The catalog node to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog node with the given <see cref="CatalogNodeModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog node with the given <see cref="CatalogNodeModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default);
}
