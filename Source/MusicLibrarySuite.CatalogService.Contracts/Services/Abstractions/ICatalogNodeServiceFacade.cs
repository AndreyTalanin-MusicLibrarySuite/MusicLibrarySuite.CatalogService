using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service facade for the <see cref="CatalogNodeModel" /> service model.
/// </summary>
public interface ICatalogNodeServiceFacade :
    ICatalogNodeService,
    ICatalogNodeToCatalogRelationshipServiceExtension,
    ICatalogNodeHierarchicalRelationshipServiceExtension,
    ICatalogNodeRelationshipServiceExtension
{
}
