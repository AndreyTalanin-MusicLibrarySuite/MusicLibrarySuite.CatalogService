using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service facade for the <see cref="CatalogEntryModel" /> service model.
/// </summary>
public interface ICatalogEntryServiceFacade :
    ICatalogEntryService,
    ICatalogEntryToCatalogRelationshipServiceExtension,
    ICatalogEntryToCatalogNodeRelationshipServiceExtension,
    ICatalogEntryHierarchicalRelationshipServiceExtension,
    ICatalogEntryHierarchicalRelationshipTypeServiceExtension,
    ICatalogEntryRelationshipServiceExtension,
    ICatalogEntryRelationshipTypeServiceExtension,
    ICatalogEntryTypeServiceExtension
{
}
