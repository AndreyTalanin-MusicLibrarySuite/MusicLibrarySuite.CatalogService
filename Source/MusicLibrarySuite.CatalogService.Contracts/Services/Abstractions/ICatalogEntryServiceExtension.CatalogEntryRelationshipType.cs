using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryRelationshipTypeModel" /> service model.
/// </summary>
public interface ICatalogEntryRelationshipTypeServiceExtension
{
    /// <summary>
    /// Asynchronously gets a catalog entry relationship type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeId">The catalog entry relationship type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry relationship type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryRelationshipTypeModel?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry relationship types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry relationship types.
    /// </returns>
    public Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entry relationship types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entry relationship types.
    /// </returns>
    public Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default);
}
