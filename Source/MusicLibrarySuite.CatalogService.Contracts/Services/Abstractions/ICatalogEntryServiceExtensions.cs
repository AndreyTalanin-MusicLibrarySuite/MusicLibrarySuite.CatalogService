using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to service extensions for the <see cref="CatalogEntryModel" /> service model.
/// <para>
/// List of available service extensions:
/// <list type="bullet">
/// <item><see cref="ICatalogEntryToCatalogRelationshipServiceExtension" /></item>
/// <item><see cref="ICatalogEntryToCatalogNodeRelationshipServiceExtension" /></item>
/// <item><see cref="ICatalogEntryHierarchicalRelationshipServiceExtension" /></item>
/// <item><see cref="ICatalogEntryHierarchicalRelationshipTypeServiceExtension" /></item>
/// <item><see cref="ICatalogEntryRelationshipServiceExtension" /></item>
/// <item><see cref="ICatalogEntryRelationshipTypeServiceExtension" /></item>
/// <item><see cref="ICatalogEntryTypeServiceExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __CatalogEntryServiceExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __CatalogEntryServiceExtensions
{
}
