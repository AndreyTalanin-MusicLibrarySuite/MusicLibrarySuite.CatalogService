using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogNodeRelationshipModel" /> service model.
/// </summary>
public interface ICatalogNodeRelationshipServiceExtension
{
    /// <summary>
    /// Asynchronously gets catalog node relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeRelationshipModel.DependentCatalogNode" /> instead of <see cref="CatalogNodeRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog node relationships.
    /// </returns>
    public Task<CatalogNodeRelationshipModel[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog node relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog node relationships for.</param>
    /// <param name="catalogNodeRelationshipModels">The collection of catalog node relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog node relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeRelationshipModel.DependentCatalogNode" /> instead of <see cref="CatalogNodeRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog node relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationshipModel> catalogNodeRelationshipModels, bool byTarget, CancellationToken cancellationToken = default);
}
