using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service for the <see cref="CatalogModel" /> service model.
/// </summary>
public interface ICatalogService
{
    /// <summary>
    /// Asynchronously gets a catalog by its unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog found or <see langword="null" />.
    /// </returns>
    public Task<CatalogModel?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalogs by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalogs.
    /// </returns>
    public Task<CatalogModel[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalogs.
    /// </returns>
    public Task<CatalogModel[]> GetCatalogsAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalogs.
    /// </returns>
    public Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog.
    /// </summary>
    /// <param name="catalogModel">The catalog to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogModel.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog with the given <see cref="CatalogModel.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<CatalogModel> AddCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog.
    /// </summary>
    /// <param name="catalogModel">The catalog to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog with the given <see cref="CatalogModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog with the given <see cref="CatalogModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default);
}
