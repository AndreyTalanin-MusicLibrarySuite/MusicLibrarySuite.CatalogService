using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service facade for the <see cref="CatalogModel" /> service model.
/// </summary>
public interface ICatalogServiceFacade :
    ICatalogService,
    ICatalogRelationshipServiceExtension
{
}
