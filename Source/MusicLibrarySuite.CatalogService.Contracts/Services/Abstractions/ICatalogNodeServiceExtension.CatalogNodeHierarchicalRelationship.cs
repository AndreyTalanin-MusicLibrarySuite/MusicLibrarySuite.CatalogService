using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogNodeHierarchicalRelationshipModel" /> service model.
/// </summary>
public interface ICatalogNodeHierarchicalRelationshipServiceExtension
{
    /// <summary>
    /// Asynchronously gets catalog node hierarchical relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationshipModel.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog node hierarchical relationships.
    /// </returns>
    public Task<CatalogNodeHierarchicalRelationshipModel[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog node hierarchical relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog node hierarchical relationships for.</param>
    /// <param name="catalogNodeHierarchicalRelationshipModels">The collection of catalog node hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationshipModel.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationshipModel.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog node hierarchical relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationshipModel> catalogNodeHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default);
}
