using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryToCatalogRelationshipModel" /> service model.
/// </summary>
public interface ICatalogEntryToCatalogRelationshipServiceExtension
{
    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog-entry-to-catalog relationships.
    /// </returns>
    public Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog relationships by a catalog's unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog-entry-to-catalog relationships.
    /// </returns>
    public Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog-entry-to-catalog relationships for.</param>
    /// <param name="catalogEntryToCatalogRelationshipModels">The collection of catalog-entry-to-catalog relationships.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog-entry-to-catalog relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog relationships for an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to reorder catalog-entry-to-catalog relationships for.</param>
    /// <param name="catalogEntryToCatalogRelationshipModels">The collection of catalog-entry-to-catalog relationships.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog-entry-to-catalog relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default);
}
