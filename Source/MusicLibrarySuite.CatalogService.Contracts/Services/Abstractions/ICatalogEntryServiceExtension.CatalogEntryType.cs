using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryTypeModel" /> service model.
/// </summary>
public interface ICatalogEntryTypeServiceExtension
{
    /// <summary>
    /// Asynchronously gets a catalog entry type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryTypeId">The catalog entry type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryTypeModel?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry types.
    /// </returns>
    public Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entry types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entry types.
    /// </returns>
    public Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default);
}
