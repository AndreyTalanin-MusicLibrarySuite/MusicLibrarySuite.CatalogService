using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service extension for the <see cref="CatalogEntryHierarchicalRelationshipModel" /> service model.
/// </summary>
public interface ICatalogEntryHierarchicalRelationshipServiceExtension
{
    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationshipModel.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry hierarchical relationships.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationships by a catalog entry's unique identifier and possibly a catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogEntryHierarchicalRelationshipTypeId">The catalog entry hierarchical relationship type's unique identifier. May be <see langword="null" />.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationshipModel.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry hierarchical relationships.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog entry hierarchical relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog entry hierarchical relationships for.</param>
    /// <param name="catalogEntryHierarchicalRelationshipModels">The collection of catalog entry hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationshipModel.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationshipModel.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog entry hierarchical relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default);
}
