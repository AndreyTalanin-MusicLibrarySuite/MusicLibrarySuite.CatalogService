using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

/// <summary>
/// Describes a service for the <see cref="CatalogEntryModel" /> service model.
/// </summary>
public interface ICatalogEntryService
{
    /// <summary>
    /// Asynchronously gets a catalog entry by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryModel?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entries by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entries.
    /// </returns>
    public Task<CatalogEntryModel[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entries.
    /// </returns>
    public Task<CatalogEntryModel[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalog entries.
    /// </returns>
    public Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog entry.
    /// </summary>
    /// <param name="catalogEntryModel">The catalog entry to add.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog entry with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogEntryModel.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog entry with the given <see cref="CatalogEntryModel.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<CatalogEntryModel> AddCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryModel">The catalog entry to update.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog entry with the given <see cref="CatalogEntryModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to remove.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog entry with the given <see cref="CatalogEntryModel.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);
}
