using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services;

/// <summary>
/// Represents the service facade for the <see cref="CatalogModel" /> service model.
/// </summary>
public class CatalogServiceFacade : ICatalogServiceFacade
{
    private readonly ICatalogService m_catalogService;
    private readonly ICatalogRelationshipServiceExtension m_catalogRelationshipServiceExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogServiceFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogService">The service for the <see cref="CatalogModel" /> service model.</param>
    /// <param name="catalogRelationshipServiceExtension">The service extension for the <see cref="CatalogRelationshipModel" /> service model.</param>
    public CatalogServiceFacade(
        ICatalogService catalogService,
        ICatalogRelationshipServiceExtension catalogRelationshipServiceExtension)
    {
        m_catalogService = catalogService;
        m_catalogRelationshipServiceExtension = catalogRelationshipServiceExtension;
    }

    #region ICatalogService Service Core Methods

    /// <inheritdoc />
    public async Task<CatalogModel?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogService.GetCatalogAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogModel[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogService.GetCatalogsAsync(catalogIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogModel[]> GetCatalogsAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogService.GetCatalogsAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogService.CountCatalogsAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogModel> AddCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default)
    {
        return await m_catalogService.AddCatalogAsync(catalogModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default)
    {
        await m_catalogService.UpdateCatalogAsync(catalogModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        await m_catalogService.RemoveCatalogAsync(catalogId, cancellationToken);
    }

    #endregion

    #region ICatalogRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogRelationshipModel[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogRelationshipServiceExtension.GetCatalogRelationshipsAsync(catalogId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationshipModel> catalogRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogRelationshipServiceExtension.ReorderCatalogRelationshipsAsync(catalogId, catalogRelationshipModels, byTarget, cancellationToken);
    }

    #endregion
}
