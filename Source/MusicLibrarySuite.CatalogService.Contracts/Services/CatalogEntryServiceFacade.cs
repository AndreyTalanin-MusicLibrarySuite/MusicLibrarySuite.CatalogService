using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services;

/// <summary>
/// Represents the service facade for the <see cref="CatalogEntryModel" /> service model.
/// </summary>
public class CatalogEntryServiceFacade : ICatalogEntryServiceFacade
{
    private readonly ICatalogEntryService m_catalogEntryService;
    private readonly ICatalogEntryToCatalogRelationshipServiceExtension m_catalogEntryToCatalogRelationshipServiceExtension;
    private readonly ICatalogEntryToCatalogNodeRelationshipServiceExtension m_catalogEntryToCatalogNodeRelationshipServiceExtension;
    private readonly ICatalogEntryHierarchicalRelationshipServiceExtension m_catalogEntryHierarchicalRelationshipServiceExtension;
    private readonly ICatalogEntryHierarchicalRelationshipTypeServiceExtension m_catalogEntryHierarchicalRelationshipTypeServiceExtension;
    private readonly ICatalogEntryRelationshipServiceExtension m_catalogEntryRelationshipServiceExtension;
    private readonly ICatalogEntryRelationshipTypeServiceExtension m_catalogEntryRelationshipTypeServiceExtension;
    private readonly ICatalogEntryTypeServiceExtension m_catalogEntryTypeServiceExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryServiceFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryService">The service for the <see cref="CatalogEntryModel" /> service model.</param>
    /// <param name="catalogEntryToCatalogRelationshipServiceExtension">The service extension for the <see cref="CatalogEntryToCatalogRelationshipModel" /> service model.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipServiceExtension">The service extension for the <see cref="CatalogEntryToCatalogNodeRelationshipModel" /> service model.</param>
    /// <param name="catalogEntryHierarchicalRelationshipServiceExtension">The service extension for the <see cref="CatalogEntryHierarchicalRelationshipModel" /> service model.</param>
    /// <param name="catalogEntryHierarchicalRelationshipTypeServiceExtension">The service extension for the <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> service model.</param>
    /// <param name="catalogEntryRelationshipServiceExtension">The service extension for the <see cref="CatalogEntryRelationshipModel" /> service model.</param>
    /// <param name="catalogEntryRelationshipTypeServiceExtension">The service extension for the <see cref="CatalogEntryRelationshipTypeModel" /> service model.</param>
    /// <param name="catalogEntryTypeServiceExtension">The service extension for the <see cref="CatalogEntryTypeModel" /> service model.</param>
    public CatalogEntryServiceFacade(
        ICatalogEntryService catalogEntryService,
        ICatalogEntryToCatalogRelationshipServiceExtension catalogEntryToCatalogRelationshipServiceExtension,
        ICatalogEntryToCatalogNodeRelationshipServiceExtension catalogEntryToCatalogNodeRelationshipServiceExtension,
        ICatalogEntryHierarchicalRelationshipServiceExtension catalogEntryHierarchicalRelationshipServiceExtension,
        ICatalogEntryHierarchicalRelationshipTypeServiceExtension catalogEntryHierarchicalRelationshipTypeServiceExtension,
        ICatalogEntryRelationshipServiceExtension catalogEntryRelationshipServiceExtension,
        ICatalogEntryRelationshipTypeServiceExtension catalogEntryRelationshipTypeServiceExtension,
        ICatalogEntryTypeServiceExtension catalogEntryTypeServiceExtension)
    {
        m_catalogEntryService = catalogEntryService;
        m_catalogEntryToCatalogRelationshipServiceExtension = catalogEntryToCatalogRelationshipServiceExtension;
        m_catalogEntryToCatalogNodeRelationshipServiceExtension = catalogEntryToCatalogNodeRelationshipServiceExtension;
        m_catalogEntryHierarchicalRelationshipServiceExtension = catalogEntryHierarchicalRelationshipServiceExtension;
        m_catalogEntryHierarchicalRelationshipTypeServiceExtension = catalogEntryHierarchicalRelationshipTypeServiceExtension;
        m_catalogEntryRelationshipServiceExtension = catalogEntryRelationshipServiceExtension;
        m_catalogEntryRelationshipTypeServiceExtension = catalogEntryRelationshipTypeServiceExtension;
        m_catalogEntryTypeServiceExtension = catalogEntryTypeServiceExtension;
    }

    #region ICatalogEntryService Service Core Methods

    /// <inheritdoc />
    public async Task<CatalogEntryModel?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryService.GetCatalogEntryAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryService.GetCatalogEntriesAsync(catalogEntryIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryService.GetCatalogEntriesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryService.CountCatalogEntriesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel> AddCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryService.AddCatalogEntryAsync(catalogEntryModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryService.UpdateCatalogEntryAsync(catalogEntryModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryService.RemoveCatalogEntryAsync(catalogEntryId, cancellationToken);
    }

    #endregion

    #region ICatalogEntryToCatalogRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogRelationshipServiceExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogRelationshipServiceExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogRelationshipServiceExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, catalogEntryToCatalogRelationshipModels, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogRelationshipServiceExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogId, catalogEntryToCatalogRelationshipModels, cancellationToken);
    }

    #endregion

    #region ICatalogEntryToCatalogNodeRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationshipModel[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogNodeRelationshipServiceExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationshipModel[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogNodeRelationshipServiceExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationshipModel> catalogEntryToCatalogNodeRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogNodeRelationshipServiceExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, catalogEntryToCatalogNodeRelationshipModels, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationshipModel> catalogEntryToCatalogNodeRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogNodeRelationshipServiceExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, catalogEntryToCatalogNodeRelationshipModels, cancellationToken);
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipServiceExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipServiceExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, catalogEntryHierarchicalRelationshipTypeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryHierarchicalRelationshipServiceExtension.ReorderCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, catalogEntryHierarchicalRelationshipModels, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipTypeServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeServiceExtension.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeServiceExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeServiceExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(cancellationToken);
    }

    #endregion

    #region ICatalogEntryRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipServiceExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipServiceExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, catalogEntryRelationshipTypeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationshipModel> catalogEntryRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryRelationshipServiceExtension.ReorderCatalogEntryRelationshipsAsync(catalogEntryId, catalogEntryRelationshipModels, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogEntryRelationshipTypeServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeServiceExtension.GetCatalogEntryRelationshipTypeAsync(catalogEntryRelationshipTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeServiceExtension.GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeServiceExtension.GetCatalogEntryRelationshipTypesAsync(cancellationToken);
    }

    #endregion

    #region ICatalogEntryTypeServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeServiceExtension.GetCatalogEntryTypeAsync(catalogEntryTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeServiceExtension.GetCatalogEntryTypesAsync(catalogEntryTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeServiceExtension.GetCatalogEntryTypesAsync(cancellationToken);
    }

    #endregion
}
