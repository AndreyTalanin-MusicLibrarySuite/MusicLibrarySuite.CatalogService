using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Contracts.Services;

/// <summary>
/// Represents the service facade for the <see cref="CatalogNodeModel" /> service model.
/// </summary>
public class CatalogNodeServiceFacade : ICatalogNodeServiceFacade
{
    private readonly ICatalogNodeService m_catalogNodeService;
    private readonly ICatalogNodeToCatalogRelationshipServiceExtension m_catalogNodeToCatalogRelationshipServiceExtension;
    private readonly ICatalogNodeHierarchicalRelationshipServiceExtension m_catalogNodeHierarchicalRelationshipServiceExtension;
    private readonly ICatalogNodeRelationshipServiceExtension m_catalogNodeRelationshipServiceExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeServiceFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeService">The service for the <see cref="CatalogNodeModel" /> service model.</param>
    /// <param name="catalogNodeToCatalogRelationshipServiceExtension">The service extension for the <see cref="CatalogNodeToCatalogRelationshipModel" /> service model.</param>
    /// <param name="catalogNodeHierarchicalRelationshipServiceExtension">The service extension for the <see cref="CatalogNodeHierarchicalRelationshipModel" /> service model.</param>
    /// <param name="catalogNodeRelationshipServiceExtension">The service extension for the <see cref="CatalogNodeRelationshipModel" /> service model.</param>
    public CatalogNodeServiceFacade(
        ICatalogNodeService catalogNodeService,
        ICatalogNodeToCatalogRelationshipServiceExtension catalogNodeToCatalogRelationshipServiceExtension,
        ICatalogNodeHierarchicalRelationshipServiceExtension catalogNodeHierarchicalRelationshipServiceExtension,
        ICatalogNodeRelationshipServiceExtension catalogNodeRelationshipServiceExtension)
    {
        m_catalogNodeService = catalogNodeService;
        m_catalogNodeToCatalogRelationshipServiceExtension = catalogNodeToCatalogRelationshipServiceExtension;
        m_catalogNodeHierarchicalRelationshipServiceExtension = catalogNodeHierarchicalRelationshipServiceExtension;
        m_catalogNodeRelationshipServiceExtension = catalogNodeRelationshipServiceExtension;
    }

    #region ICatalogNodeService Service Core Methods

    /// <inheritdoc />
    public async Task<CatalogNodeModel?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeService.GetCatalogNodeAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeService.GetCatalogNodesAsync(catalogNodeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeService.GetCatalogNodesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeService.CountCatalogNodesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel> AddCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeService.AddCatalogNodeAsync(catalogNodeModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeService.UpdateCatalogNodeAsync(catalogNodeModel, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeService.RemoveCatalogNodeAsync(catalogNodeId, cancellationToken);
    }

    #endregion

    #region ICatalogNodeToCatalogRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationshipModel[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeToCatalogRelationshipServiceExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationshipModel[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeToCatalogRelationshipServiceExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeToCatalogRelationshipModel> catalogNodeToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeToCatalogRelationshipServiceExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, catalogNodeToCatalogRelationshipModels, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogNodeToCatalogRelationshipModel> catalogNodeToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeToCatalogRelationshipServiceExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogId, catalogNodeToCatalogRelationshipModels, cancellationToken);
    }

    #endregion

    #region ICatalogNodeHierarchicalRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeHierarchicalRelationshipModel[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeHierarchicalRelationshipServiceExtension.GetCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationshipModel> catalogNodeHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeHierarchicalRelationshipServiceExtension.ReorderCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, catalogNodeHierarchicalRelationshipModels, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogNodeRelationshipServiceExtension Service Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeRelationshipModel[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRelationshipServiceExtension.GetCatalogNodeRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationshipModel> catalogNodeRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeRelationshipServiceExtension.ReorderCatalogNodeRelationshipsAsync(catalogNodeId, catalogNodeRelationshipModels, byTarget, cancellationToken);
    }

    #endregion
}
