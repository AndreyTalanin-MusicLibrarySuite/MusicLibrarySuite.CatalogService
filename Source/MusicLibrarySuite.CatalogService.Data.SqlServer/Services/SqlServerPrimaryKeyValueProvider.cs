using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Dapper;

using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Data.Services;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Data.Extensions;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

// Disable the IDE0301 (Simplify collection initialization) notification for better readability.
#pragma warning disable IDE0301

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Services;

/// <summary>
/// Represents the SQL-Server-specific implementation of a primary key <see cref="Guid" /> value provider.
/// </summary>
public class SqlServerPrimaryKeyValueProvider : DefaultPrimaryKeyValueProvider
{
    private const int c_maxBatchSize = 4096;

    private readonly IDbContextProvider<DbContext> m_contextProvider;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerPrimaryKeyValueProvider" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    public SqlServerPrimaryKeyValueProvider(IDbContextProvider<DbContext> contextProvider)
    {
        m_contextProvider = contextProvider;
    }

    /// <inheritdoc />
    public override Guid GetSequentialGuidValue()
    {
        int count = 1;
        Guid sequentialGuidValue = GetSequentialGuidValues(count).Single();
        return sequentialGuidValue;
    }

    /// <inheritdoc />
    public override async Task<Guid> GetSequentialGuidValueAsync(CancellationToken cancellationToken = default)
    {
        int count = 1;
        Guid sequentialGuidValue = (await GetSequentialGuidValuesAsync(count, cancellationToken)).Single();
        return sequentialGuidValue;
    }

    /// <inheritdoc />
    public override Guid[] GetSequentialGuidValues(int count)
    {
        IEnumerable<Guid> guidValueBatches = Enumerable.Empty<Guid>();
        using (DbConnectionHandle connectionHandle = m_contextProvider.Context.Database.GetDbConnectionHandle())
        {
            DbConnection connection = connectionHandle.Connection;

            int offset = 0;
            for (int currentBatchSize = GetNextBatchSize(count, offset); currentBatchSize > 0; currentBatchSize = GetNextBatchSize(count, offset))
            {
                DynamicParameters dynamicParameters = new();
                dynamicParameters.Add("@Count", currentBatchSize);

                string commandText = "[utils].[sp_SelectSequentialGuids]";
                CommandDefinition commandDefinition = new(commandText, dynamicParameters, commandType: CommandType.StoredProcedure);

                Guid[] guidValueBatch = connection.Query<Guid>(commandDefinition).ToArray();

                guidValueBatches = guidValueBatches.Concat(guidValueBatch);
                offset += currentBatchSize;
            }
        }

        return guidValueBatches.ToArray();
    }

    /// <inheritdoc />
    public override async Task<Guid[]> GetSequentialGuidValuesAsync(int count, CancellationToken cancellationToken = default)
    {
        IEnumerable<Guid> guidValueBatches = Enumerable.Empty<Guid>();
        await using (DbConnectionHandle connectionHandle = await m_contextProvider.Context.Database.GetDbConnectionHandleAsync(CancellationToken.None))
        {
            DbConnection connection = connectionHandle.Connection;

            int offset = 0;
            for (int currentBatchSize = GetNextBatchSize(count, offset); currentBatchSize > 0; currentBatchSize = GetNextBatchSize(count, offset))
            {
                DynamicParameters dynamicParameters = new();
                dynamicParameters.Add("@Count", currentBatchSize);

                string commandText = "[utils].[sp_SelectSequentialGuids]";
                CommandDefinition commandDefinition = new(commandText, dynamicParameters, commandType: CommandType.StoredProcedure, cancellationToken: cancellationToken);

                Guid[] guidValueBatch = (await connection.QueryAsync<Guid>(commandDefinition)).ToArray();

                guidValueBatches = guidValueBatches.Concat(guidValueBatch);
                offset += currentBatchSize;
            }
        }

        return guidValueBatches.ToArray();
    }

    private static int GetNextBatchSize(int count, int offset)
    {
        return Math.Min(c_maxBatchSize, count - offset);
    }
}
