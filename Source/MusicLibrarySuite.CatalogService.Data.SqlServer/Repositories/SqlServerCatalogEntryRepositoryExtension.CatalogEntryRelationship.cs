using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogEntryRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship> m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryRelationship> m_catalogEntryRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogEntryRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog entry relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog entry relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogEntryRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship> catalogEntryRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogEntryRelationship> catalogEntryRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryRelationshipSetDisplayOrderVisitor = catalogEntryRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogEntryRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryRelationshipTypeId: null,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryRelationshipTypeId: (CatalogEntryRelationshipTypeId?)catalogEntryRelationshipTypeId,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationship> catalogEntryRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        IEnumerable<IGrouping<Guid, CatalogEntryRelationship>> catalogEntryRelationshipsGroupings =
            catalogEntryRelationships.GroupBy(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipTypeId);
        foreach (IGrouping<Guid, CatalogEntryRelationship> catalogEntryRelationshipsGrouping in catalogEntryRelationshipsGroupings)
        {
            m_catalogEntryRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntryRelationships, byTarget);

            using DataTable catalogEntryRelationshipsDataTable = catalogEntryRelationships.ToDataTable();

            SqlParameter resultUpdatedRowsParameter;
            SqlParameter[] parameters = new SqlParameter[]
            {
                new("CatalogEntryId", DbValueConvert.ToDbValue(catalogEntryId.Value)),
                new("DependentCatalogEntryId", DbValueConvert.ToDbValue(catalogEntryId.Value)),
                new("CatalogEntryRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryRelationship]", Value = catalogEntryRelationshipsDataTable },
                new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
                resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
            };

            string query = @$"
                EXEC [dbo].[sp_ReorderCatalogEntryRelationships]
                    @{"CatalogEntryId"},
                    @{"DependentCatalogEntryId"},
                    @{"CatalogEntryRelationships"},
                    @{"ByTarget"},
                    @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
                cancellationToken);

            if (catalogEntryRelationshipsGrouping.Any() && getUpdatedRowsCount() != catalogEntryRelationshipsGrouping.Count())
            {
                throw new EntityConflictException("Unable to reorder catalog entry relationships because the provided set is different from the database state.");
            }
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    private async Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntryRelationship> catalogEntryRelationshipsQueryable = Context.CatalogEntryRelationships;

        catalogEntryRelationshipsQueryable = catalogEntryRelationshipsQueryable
            .Include(catalogEntryRelationship => catalogEntryRelationship.CatalogEntry!.CatalogEntryType)
            .Include(catalogEntryRelationship => catalogEntryRelationship.DependentCatalogEntry!.CatalogEntryType)
            .Include(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipType!.OwnerCatalogEntryType)
            .Include(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipType!.TargetCatalogEntryType)
            .Include(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation)
            .AsNoTracking();

        if (catalogEntryRelationshipTypeId is not null)
        {
            Guid catalogEntryRelationshipTypeIdValue = ((CatalogEntryRelationshipTypeId)catalogEntryRelationshipTypeId).Value;
            catalogEntryRelationshipsQueryable = catalogEntryRelationshipsQueryable.Where(catalogEntryRelationship =>
                catalogEntryRelationship.CatalogEntryRelationshipTypeId == catalogEntryRelationshipTypeIdValue);
        }

        catalogEntryRelationshipsQueryable = byTarget
            ? catalogEntryRelationshipsQueryable
                .Where(catalogEntryRelationship => catalogEntryRelationship.DependentCatalogEntryId == catalogEntryId.Value)
                .OrderBy(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipTypeId)
                .ThenBy(catalogEntryRelationship => catalogEntryRelationship.ReferenceOrder)
        : catalogEntryRelationshipsQueryable
                .Where(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryId == catalogEntryId.Value)
                .OrderBy(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipTypeId)
                .ThenBy(catalogEntryRelationship => catalogEntryRelationship.Order);

        CatalogEntryRelationship[] catalogEntryRelationships = await catalogEntryRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogEntryRelationships);

        return catalogEntryRelationships;
    }
}
