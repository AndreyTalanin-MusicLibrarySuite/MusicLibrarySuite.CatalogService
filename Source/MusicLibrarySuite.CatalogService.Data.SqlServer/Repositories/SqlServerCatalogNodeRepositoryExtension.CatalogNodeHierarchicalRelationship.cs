using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogNodeHierarchicalRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogNodeHierarchicalRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogNodeHierarchicalRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship> m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship> m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogNodeHierarchicalRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog node hierarchical relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog node hierarchical relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogNodeHierarchicalRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor = catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogNodeRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeHierarchicalRelationship[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationshipsQueryable = Context.CatalogNodeHierarchicalRelationships;

        catalogNodeHierarchicalRelationshipsQueryable = catalogNodeHierarchicalRelationshipsQueryable
            .Include(catalogNodeRelationship => catalogNodeRelationship.CatalogNode)
            .Include(catalogNodeRelationship => catalogNodeRelationship.ParentCatalogNode)
            .AsNoTracking();

        catalogNodeHierarchicalRelationshipsQueryable = byTarget
            ? catalogNodeHierarchicalRelationshipsQueryable
                .Where(catalogNodeRelationship => catalogNodeRelationship.ParentCatalogNodeId == catalogNodeId.Value)
                .OrderBy(catalogNodeRelationship => catalogNodeRelationship.ReferenceOrder)
        : catalogNodeHierarchicalRelationshipsQueryable
                .Where(catalogNodeRelationship => catalogNodeRelationship.CatalogNodeId == catalogNodeId.Value)
                .OrderBy(catalogNodeRelationship => catalogNodeRelationship.Order);

        CatalogNodeHierarchicalRelationship[] catalogNodeHierarchicalRelationships = await catalogNodeHierarchicalRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogNodeHierarchicalRelationships);

        return catalogNodeHierarchicalRelationships;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNodeHierarchicalRelationships, byTarget);

        using DataTable catalogNodeHierarchicalRelationshipsDataTable = catalogNodeHierarchicalRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNodeId", DbValueConvert.ToDbValue(catalogNodeId.Value)),
            new("ParentCatalogNodeId", DbValueConvert.ToDbValue(catalogNodeId.Value)),
            new("CatalogNodeHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeHierarchicalRelationship]", Value = catalogNodeHierarchicalRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogNodeHierarchicalRelationships]
                @{"CatalogNodeId"},
                @{"ParentCatalogNodeId"},
                @{"CatalogNodeHierarchicalRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogNodeHierarchicalRelationships.Any() && getUpdatedRowsCount() != catalogNodeHierarchicalRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog node hierarchical relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
