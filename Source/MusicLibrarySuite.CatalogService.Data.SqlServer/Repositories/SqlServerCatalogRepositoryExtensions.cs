using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to repository extensions for the <see cref="Catalog" /> database entity.
/// <para>
/// List of available repository extensions:
/// <list type="bullet">
/// <item><see cref="SqlServerCatalogRelationshipRepositoryExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __SqlServerCatalogRepositoryExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __SqlServerCatalogRepositoryExtensions
{
}
