using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents the SQL-Server-specific implementation of a repository extension for the <see cref="CatalogRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogRelationship> m_catalogRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogRelationship> m_catalogRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogRelationship> catalogRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogRelationship> catalogRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogRelationshipCleanUpNavigationPropertiesVisitor = catalogRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogRelationshipSetDisplayOrderVisitor = catalogRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogRelationship[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogRelationship> catalogRelationshipsQueryable = Context.CatalogRelationships;

        catalogRelationshipsQueryable = catalogRelationshipsQueryable
            .Include(catalogRelationship => catalogRelationship.Catalog)
            .Include(catalogRelationship => catalogRelationship.DependentCatalog)
            .AsNoTracking();

        catalogRelationshipsQueryable = byTarget
            ? catalogRelationshipsQueryable
                .Where(catalogRelationship => catalogRelationship.DependentCatalogId == catalogId.Value)
        .OrderBy(catalogRelationship => catalogRelationship.ReferenceOrder)
            : catalogRelationshipsQueryable
                .Where(catalogRelationship => catalogRelationship.CatalogId == catalogId.Value)
                .OrderBy(catalogRelationship => catalogRelationship.Order);

        CatalogRelationship[] catalogRelationships = await catalogRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogRelationships);

        return catalogRelationships;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationship> catalogRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        m_catalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogRelationships, byTarget);

        using DataTable catalogRelationshipsDataTable = catalogRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogId", DbValueConvert.ToDbValue(catalogId.Value)),
            new("DependentCatalogId", DbValueConvert.ToDbValue(catalogId.Value)),
            new("CatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogRelationship]", Value = catalogRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogRelationships]
                @{"CatalogId"},
                @{"DependentCatalogId"},
                @{"CatalogRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogRelationships.Any() && getUpdatedRowsCount() != catalogRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
