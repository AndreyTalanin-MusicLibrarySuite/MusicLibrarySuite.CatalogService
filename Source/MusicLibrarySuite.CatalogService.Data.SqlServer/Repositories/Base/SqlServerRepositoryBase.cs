using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;

/// <summary>
/// Represents the base class for all SQL-Server-specific repositories.
/// </summary>
public abstract class SqlServerRepositoryBase
{
    private readonly IDbContextProvider<CatalogServiceDbContext> m_contextProvider;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerRepositoryBase" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    protected SqlServerRepositoryBase(IDbContextProvider<CatalogServiceDbContext> contextProvider)
    {
        m_contextProvider = contextProvider;
    }

    /// <summary>
    /// Gets the default <see cref="CatalogServiceDbContext" /> database context or one provided by the current <see cref="IDbContextScope{TContext}" /> if in effect.
    /// </summary>
    protected CatalogServiceDbContext Context
    {
        get
        {
            return m_contextProvider.Context;
        }
    }
}
