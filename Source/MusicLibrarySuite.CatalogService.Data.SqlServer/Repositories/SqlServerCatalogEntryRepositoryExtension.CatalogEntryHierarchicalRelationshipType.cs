using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryHierarchicalRelationshipType" /> database entity.
/// </summary>
public class SqlServerCatalogEntryHierarchicalRelationshipTypeRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryHierarchicalRelationshipTypeRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    public SqlServerCatalogEntryHierarchicalRelationshipTypeRepositoryExtension(IDbContextProvider<CatalogServiceDbContext> contextProvider)
        : base(contextProvider)
    {
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds = catalogEntryHierarchicalRelationshipTypeId.Yield();

        CatalogEntryHierarchicalRelationshipType? catalogEntryHierarchicalRelationshipType =
            (await GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIds, cancellationToken)).SingleOrDefault();

        return catalogEntryHierarchicalRelationshipType;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeCode catalogEntryHierarchicalRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryHierarchicalRelationshipTypeCode> catalogEntryHierarchicalRelationshipTypeCodes = catalogEntryHierarchicalRelationshipTypeCode.Yield();

        CatalogEntryHierarchicalRelationshipType? catalogEntryHierarchicalRelationshipType =
            (await GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeCodes, cancellationToken)).SingleOrDefault();

        return catalogEntryHierarchicalRelationshipType;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipTypes = await GetCatalogEntryHierarchicalRelationshipTypesAsync(
            catalogEntryHierarchicalRelationshipTypeIds: catalogEntryHierarchicalRelationshipTypeIds,
            catalogEntryHierarchicalRelationshipTypeCodes: Enumerable.Empty<CatalogEntryHierarchicalRelationshipTypeCode>(),
            cancellationToken);

        return catalogEntryHierarchicalRelationshipTypes;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeCode> catalogEntryHierarchicalRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipTypes = await GetCatalogEntryHierarchicalRelationshipTypesAsync(
            catalogEntryHierarchicalRelationshipTypeIds: Enumerable.Empty<CatalogEntryHierarchicalRelationshipTypeId>(),
            catalogEntryHierarchicalRelationshipTypeCodes: catalogEntryHierarchicalRelationshipTypeCodes,
            cancellationToken);

        return catalogEntryHierarchicalRelationshipTypes;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntryHierarchicalRelationshipType> catalogEntryHierarchicalRelationshipTypesQueryable = Context.CatalogEntryHierarchicalRelationshipTypes;
        catalogEntryHierarchicalRelationshipTypesQueryable = catalogEntryHierarchicalRelationshipTypesQueryable.AsNoTracking();

        catalogEntryHierarchicalRelationshipTypesQueryable = catalogEntryHierarchicalRelationshipTypesQueryable
            .Include(catalogEntryHierarchicalRelationshipType => catalogEntryHierarchicalRelationshipType.OwnerCatalogEntryType)
            .Include(catalogEntryHierarchicalRelationshipType => catalogEntryHierarchicalRelationshipType.TargetCatalogEntryType);

        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipTypes = await catalogEntryHierarchicalRelationshipTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryHierarchicalRelationshipTypes;
    }

    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(
        IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds,
        IEnumerable<CatalogEntryHierarchicalRelationshipTypeCode> catalogEntryHierarchicalRelationshipTypeCodes,
        CancellationToken cancellationToken = default)
    {
        using DataTable catalogEntryHierarchicalRelationshipTypeIdsDataTable = catalogEntryHierarchicalRelationshipTypeIds
            .Select(catalogEntryHierarchicalRelationshipTypeId => catalogEntryHierarchicalRelationshipTypeId.Value)
            .ToDataTable();
        using DataTable catalogEntryHierarchicalRelationshipTypeCodesDataTable = catalogEntryHierarchicalRelationshipTypeCodes
            .Select(catalogEntryHierarchicalRelationshipTypeCode => catalogEntryHierarchicalRelationshipTypeCode.Value)
            .ToDataTable();

        SqlParameter catalogEntryHierarchicalRelationshipTypeIdsParameter;
        SqlParameter catalogEntryHierarchicalRelationshipTypeCodesParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            catalogEntryHierarchicalRelationshipTypeIdsParameter =
                new("CatalogEntryHierarchicalRelationshipTypeIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogEntryHierarchicalRelationshipTypeIdsDataTable },
            catalogEntryHierarchicalRelationshipTypeCodesParameter =
                new("CatalogEntryHierarchicalRelationshipTypeCodes", SqlDbType.Structured) { TypeName = "[ctt].[String256UnorderedSet]", Value = catalogEntryHierarchicalRelationshipTypeCodesDataTable },
        };

        string query = $@"
            SELECT * FROM [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypes]
            (
                @{catalogEntryHierarchicalRelationshipTypeIdsParameter.ParameterName},
                @{catalogEntryHierarchicalRelationshipTypeCodesParameter.ParameterName}
            )";

        IQueryable<CatalogEntryHierarchicalRelationshipType> catalogEntryHierarchicalRelationshipTypesQueryable = Context.CatalogEntryHierarchicalRelationshipTypes.FromSqlRaw(query, parameters);
        catalogEntryHierarchicalRelationshipTypesQueryable = catalogEntryHierarchicalRelationshipTypesQueryable.AsNoTracking();

        catalogEntryHierarchicalRelationshipTypesQueryable = catalogEntryHierarchicalRelationshipTypesQueryable
            .Include(catalogEntryHierarchicalRelationshipType => catalogEntryHierarchicalRelationshipType.OwnerCatalogEntryType)
            .Include(catalogEntryHierarchicalRelationshipType => catalogEntryHierarchicalRelationshipType.TargetCatalogEntryType);

        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipTypes = await catalogEntryHierarchicalRelationshipTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryHierarchicalRelationshipTypes;
    }
}
