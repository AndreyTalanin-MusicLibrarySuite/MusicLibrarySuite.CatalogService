using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents the SQL-Server-specific implementation of a repository extension for the <see cref="CatalogNodeToCatalogRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogNodeToCatalogRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogNodeToCatalogRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship> m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship> m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogNodeToCatalogRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-node-to-catalog relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeToCatalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-node-to-catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogNodeToCatalogRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor = catalogNodeToCatalogRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogNodeRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeId: catalogNodeId, catalogId: null, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeId: null, catalogId: catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeId: catalogNodeId, catalogId: null, catalogNodeToCatalogRelationships, false, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeId: null, catalogId: catalogId, catalogNodeToCatalogRelationships, true, cancellationToken);
    }

    private async Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId? catalogNodeId, CatalogId? catalogId, CancellationToken cancellationToken = default)
    {
        if (catalogNodeId is null && catalogId is null)
            throw new ArgumentException($"Either {nameof(catalogNodeId)} or {nameof(catalogId)} should be not null.");

        IQueryable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationshipsQueryable = Context.CatalogNodeToCatalogRelationships;

        catalogNodeToCatalogRelationshipsQueryable = catalogNodeToCatalogRelationshipsQueryable
            .Include(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.CatalogNode)
            .Include(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.Catalog)
            .AsNoTracking();

        if (catalogNodeId is not null)
        {
            Guid catalogNodeIdValue = ((CatalogNodeId)catalogNodeId).Value;
            catalogNodeToCatalogRelationshipsQueryable = catalogNodeToCatalogRelationshipsQueryable
                .Where(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.CatalogNodeId == catalogNodeIdValue)
                .OrderBy(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.Order);
        }
        if (catalogId is not null)
        {
            Guid catalogIdValue = ((CatalogId)catalogId).Value;
            catalogNodeToCatalogRelationshipsQueryable = catalogNodeToCatalogRelationshipsQueryable
                .Where(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.CatalogId == catalogIdValue);

            if (catalogNodeId is null)
            {
                catalogNodeToCatalogRelationshipsQueryable = catalogNodeToCatalogRelationshipsQueryable
                    .OrderBy(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.ReferenceOrder);
            }
        }

        CatalogNodeToCatalogRelationship[] catalogNodeToCatalogRelationships = await catalogNodeToCatalogRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogNodeToCatalogRelationships);

        return catalogNodeToCatalogRelationships;
    }

    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId? catalogNodeId, CatalogId? catalogId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        if (byTarget && catalogId is null)
            throw new ArgumentNullException(nameof(catalogId));
        else if (!byTarget && catalogNodeId is null)
            throw new ArgumentNullException(nameof(catalogNodeId));

        m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNodeToCatalogRelationships, byTarget);

        using DataTable catalogNodeToCatalogRelationshipsDataTable = catalogNodeToCatalogRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNodeId", DbValueConvert.ToDbValue((catalogNodeId ?? new CatalogNodeId()).Value)),
            new("CatalogId", DbValueConvert.ToDbValue((catalogId ?? new CatalogId()).Value)),
            new("CatalogNodeToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeToCatalogRelationship]", Value = catalogNodeToCatalogRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogNodeToCatalogRelationships]
                @{"CatalogNodeId"},
                @{"CatalogId"},
                @{"CatalogNodeToCatalogRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogNodeToCatalogRelationships.Any() && getUpdatedRowsCount() != catalogNodeToCatalogRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog-node-to-catalog relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
