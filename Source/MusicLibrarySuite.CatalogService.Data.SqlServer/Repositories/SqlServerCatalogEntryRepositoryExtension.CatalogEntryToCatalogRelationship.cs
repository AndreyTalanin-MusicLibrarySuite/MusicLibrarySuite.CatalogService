using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryToCatalogRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogEntryToCatalogRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryToCatalogRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship> m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship> m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryToCatalogRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-entry-to-catalog relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryToCatalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-entry-to-catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogEntryToCatalogRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor = catalogEntryToCatalogRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogEntryRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationship[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryId: catalogEntryId, catalogId: null, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationship[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryId: null, catalogId: catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryId: catalogEntryId, catalogId: null, catalogEntryToCatalogRelationships, false, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryId: null, catalogId: catalogId, catalogEntryToCatalogRelationships, true, cancellationToken);
    }

    private async Task<CatalogEntryToCatalogRelationship[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId? catalogEntryId, CatalogId? catalogId, CancellationToken cancellationToken = default)
    {
        if (catalogEntryId is null && catalogId is null)
            throw new ArgumentException($"Either {nameof(catalogEntryId)} or {nameof(catalogId)} should be not null.");

        IQueryable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationshipsQueryable = Context.CatalogEntryToCatalogRelationships;

        catalogEntryToCatalogRelationshipsQueryable = catalogEntryToCatalogRelationshipsQueryable
            .Include(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.CatalogEntry)
            .Include(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.Catalog)
            .AsNoTracking();

        if (catalogEntryId is not null)
        {
            Guid catalogEntryIdValue = ((CatalogEntryId)catalogEntryId).Value;
            catalogEntryToCatalogRelationshipsQueryable = catalogEntryToCatalogRelationshipsQueryable
                .Where(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.CatalogEntryId == catalogEntryIdValue)
                .OrderBy(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.Order);
        }
        if (catalogId is not null)
        {
            Guid catalogIdValue = ((CatalogId)catalogId).Value;
            catalogEntryToCatalogRelationshipsQueryable = catalogEntryToCatalogRelationshipsQueryable
                .Where(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.CatalogId == catalogIdValue);

            if (catalogEntryId is null)
            {
                catalogEntryToCatalogRelationshipsQueryable = catalogEntryToCatalogRelationshipsQueryable
                    .OrderBy(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.ReferenceOrder);
            }
        }

        CatalogEntryToCatalogRelationship[] catalogEntryToCatalogRelationships = await catalogEntryToCatalogRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogEntryToCatalogRelationships);

        return catalogEntryToCatalogRelationships;
    }

    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId? catalogEntryId, CatalogId? catalogId, IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        if (byTarget && catalogId is null)
            throw new ArgumentNullException(nameof(catalogId));
        else if (!byTarget && catalogEntryId is null)
            throw new ArgumentNullException(nameof(catalogEntryId));

        m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntryToCatalogRelationships, byTarget);

        using DataTable catalogEntryToCatalogRelationshipsDataTable = catalogEntryToCatalogRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogEntryId", DbValueConvert.ToDbValue((catalogEntryId ?? new CatalogEntryId()).Value)),
            new("CatalogId", DbValueConvert.ToDbValue((catalogId ?? new CatalogId()).Value)),
            new("CatalogEntryToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogRelationship]", Value = catalogEntryToCatalogRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogEntryToCatalogRelationships]
                @{"CatalogEntryId"},
                @{"CatalogId"},
                @{"CatalogEntryToCatalogRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogEntryToCatalogRelationships.Any() && getUpdatedRowsCount() != catalogEntryToCatalogRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog-entry-to-catalog relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
