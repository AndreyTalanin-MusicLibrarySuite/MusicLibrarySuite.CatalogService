using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.SqlServer.Extensions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository for the <see cref="CatalogNode" /> database entity.
/// </summary>
public class SqlServerCatalogNodeRepository : SqlServerRepositoryBase, ICatalogNodeRepository
{
    private readonly IPrimaryKeyValueProvider m_primaryKeyValueProvider;
    private readonly ISetPrimaryKeyVisitor<CatalogNode, CatalogNodeId> m_catalogNodeSetPrimaryKeyVisitor;
    private readonly ISetNavigationPropertyForeignKeysVisitor<CatalogNode> m_catalogNodeSetNavigationPropertyForeignKeysVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNode> m_catalogNodeCleanUpNavigationPropertiesVisitor;
    private readonly ISortNavigationPropertiesVisitor<CatalogNode> m_catalogNodeSortNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship> m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship> m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeRelationship> m_catalogNodeRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogNodeRepository" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="primaryKeyValueProvider">The primary key <see cref="Guid" /> value provider.</param>
    /// <param name="catalogNodeSetPrimaryKeyVisitor">The visitor that sets the primary key of a catalog node so dependent entities can reference the main one via foreign keys.</param>
    /// <param name="catalogNodeSetNavigationPropertyForeignKeysVisitor">The visitor that sets foreign keys of navigation properties of a catalog node so dependent entities are referencing the main one.</param>
    /// <param name="catalogNodeCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog node so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeSortNavigationPropertiesVisitor">The visitor that sorts collection-represented navigation properties of a catalog node so their order is correct.</param>
    /// <param name="catalogNodeToCatalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-node-to-catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog node hierarchical relationships so they can be inserted in the database as is.</param>
    /// <param name="catalogNodeRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog node relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogNodeRepository(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        IPrimaryKeyValueProvider primaryKeyValueProvider,
        ISetPrimaryKeyVisitor<CatalogNode, CatalogNodeId> catalogNodeSetPrimaryKeyVisitor,
        ISetNavigationPropertyForeignKeysVisitor<CatalogNode> catalogNodeSetNavigationPropertyForeignKeysVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogNode> catalogNodeCleanUpNavigationPropertiesVisitor,
        ISortNavigationPropertiesVisitor<CatalogNode> catalogNodeSortNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationshipSetDisplayOrderVisitor,
        ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor,
        ISetDisplayOrderVisitor<CatalogNodeRelationship> catalogNodeRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_primaryKeyValueProvider = primaryKeyValueProvider;
        m_catalogNodeSetPrimaryKeyVisitor = catalogNodeSetPrimaryKeyVisitor;
        m_catalogNodeSetNavigationPropertyForeignKeysVisitor = catalogNodeSetNavigationPropertyForeignKeysVisitor;
        m_catalogNodeCleanUpNavigationPropertiesVisitor = catalogNodeCleanUpNavigationPropertiesVisitor;
        m_catalogNodeSortNavigationPropertiesVisitor = catalogNodeSortNavigationPropertiesVisitor;
        m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor = catalogNodeToCatalogRelationshipSetDisplayOrderVisitor;
        m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor = catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor;
        m_catalogNodeRelationshipSetDisplayOrderVisitor = catalogNodeRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogNode?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogNodeId> catalogNodeIds = catalogNodeId.Yield();

        CatalogNode? catalogNode = (await GetCatalogNodesAsync(catalogNodeIds, cancellationToken)).SingleOrDefault();

        return catalogNode;
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        catalogNodeIds = catalogNodeIds.Distinct();
        using DataTable catalogNodeIdsDataTable = catalogNodeIds
            .Select(catalogNodeId => catalogNodeId.Value)
            .ToDataTable();

        SqlParameter catalogNodeIdsParameter = new("CatalogNodeIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogNodeIdsDataTable };

        string query = $"SELECT * FROM [dbo].[ufn_GetCatalogNodes] (@{catalogNodeIdsParameter.ParameterName})";

        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes.FromSqlRaw(query, catalogNodeIdsParameter);

        catalogNodesQueryable = catalogNodesQueryable
            .Include(catalogNode => catalogNode.CatalogNodeToCatalogRelationships)
            .ThenInclude(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.Catalog)
            .Include(catalogNode => catalogNode.CatalogNodeHierarchicalRelationships)
            .ThenInclude(catalogNodeHierarchicalRelationship => catalogNodeHierarchicalRelationship.ParentCatalogNode)
            .Include(catalogNode => catalogNode.ChildCatalogNodeHierarchicalRelationships)
            .ThenInclude(catalogNodeHierarchicalRelationship => catalogNodeHierarchicalRelationship.CatalogNode)
            .Include(catalogNode => catalogNode.CatalogNodeRelationships)
            .ThenInclude(catalogNodeRelationship => catalogNodeRelationship.DependentCatalogNode);

        catalogNodesQueryable = catalogNodesQueryable
            .AsNoTracking()
            .AsSplitQuery();

        CatalogNode[] catalogNodes = await catalogNodesQueryable.ToArrayAsync(cancellationToken);

        m_catalogNodeCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogNodes);
        m_catalogNodeSortNavigationPropertiesVisitor.SortNavigationProperties(catalogNodes);

        return catalogNodes;
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;
        catalogNodesQueryable = catalogNodesQueryable.Filter(filter)
            .AsNoTracking();

        CatalogNode[] catalogNodes = await catalogNodesQueryable.ToArrayAsync(cancellationToken);

        return catalogNodes;
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;
        catalogNodesQueryable = (await catalogNodesQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        CatalogNode[] catalogNodes = await catalogNodesQueryable.ToArrayAsync(cancellationToken);

        return catalogNodes;
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;
        catalogNodesQueryable = catalogNodesQueryable.AsNoTracking();

        CatalogNode[] catalogNodes = await catalogNodesQueryable.ToArrayAsync(cancellationToken);

        return catalogNodes;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;
        catalogNodesQueryable = catalogNodesQueryable.Filter(filter)
            .AsNoTracking();

        int catalogNodesCount = await catalogNodesQueryable.CountAsync(cancellationToken);

        return catalogNodesCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;
        catalogNodesQueryable = (await catalogNodesQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        int catalogNodesCount = await catalogNodesQueryable.CountAsync(cancellationToken);

        return catalogNodesCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNode> catalogNodesQueryable = Context.CatalogNodes;

        int catalogNodesCount = await catalogNodesQueryable.CountAsync(cancellationToken);

        return catalogNodesCount;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task<CatalogNode> AddCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        Guid catalogNodeId = catalogNode.Id == Guid.Empty
            ? await m_primaryKeyValueProvider.GetSequentialGuidValueAsync(cancellationToken)
            : Guid.Empty;

        m_catalogNodeSetPrimaryKeyVisitor.SetPrimaryKeyIfEmpty(catalogNode, new CatalogNodeId(catalogNodeId));
        m_catalogNodeSetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalogNode);
        m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeToCatalogRelationships, false);
        m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeHierarchicalRelationships, false);
        m_catalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeRelationships, false);

        using DataTable catalogNodeDataTable = catalogNode.Yield()
            .ToDataTable();

        using DataTable catalogNodeToCatalogRelationshipsDataTable = catalogNode.CatalogNodeToCatalogRelationships.ToDataTable();
        using DataTable catalogNodeHierarchicalRelationshipsDataTable = catalogNode.CatalogNodeHierarchicalRelationships.ToDataTable();
        using DataTable catalogNodeRelationshipsDataTable = catalogNode.CatalogNodeRelationships.ToDataTable();

        SqlParameter resultCreatedOnParameter;
        SqlParameter resultUpdatedOnParameter;
        SqlParameter resultAddedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNode", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNode]", Value = catalogNodeDataTable, },
            new("CatalogNodeToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeToCatalogRelationship]", Value = catalogNodeToCatalogRelationshipsDataTable },
            new("CatalogNodeHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeHierarchicalRelationship]", Value = catalogNodeHierarchicalRelationshipsDataTable },
            new("CatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeRelationship]", Value = catalogNodeRelationshipsDataTable },
            resultCreatedOnParameter = CreateResultSqlParameter("CreatedOn", SqlDbType.DateTimeOffset),
            resultUpdatedOnParameter = CreateResultSqlParameter("UpdatedOn", SqlDbType.DateTimeOffset),
            resultAddedRowsParameter = CreateResultAddedRowsSqlParameter(out GetRowsCountDelegate getAddedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_AddCatalogNode]
                @{"CatalogNode"},
                @{"CatalogNodeToCatalogRelationships"},
                @{"CatalogNodeHierarchicalRelationships"},
                @{"CatalogNodeRelationships"},
                @{resultCreatedOnParameter.ParameterName} OUTPUT,
                @{resultUpdatedOnParameter.ParameterName} OUTPUT,
                @{resultAddedRowsParameter.ParameterName} OUTPUT;";

        using (IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken))
        {
            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
                cancellationToken);

            if (getAddedRowsCount() == 0)
            {
                throw new EntityConflictException("Unable to add a catalog node because it already exists.");
            }

            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
                cancellationToken);
        }

        catalogNode.CreatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultCreatedOnParameter.Value);
        catalogNode.UpdatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultUpdatedOnParameter.Value);

        return catalogNode;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task UpdateCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        m_catalogNodeSetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalogNode);
        m_catalogNodeToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeToCatalogRelationships, false);
        m_catalogNodeHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeHierarchicalRelationships, false);
        m_catalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNode.CatalogNodeRelationships, false);

        using DataTable catalogNodeDataTable = catalogNode.Yield()
            .ToDataTable();

        using DataTable catalogNodeToCatalogRelationshipsDataTable = catalogNode.CatalogNodeToCatalogRelationships.ToDataTable();
        using DataTable catalogNodeHierarchicalRelationshipsDataTable = catalogNode.CatalogNodeHierarchicalRelationships.ToDataTable();
        using DataTable catalogNodeRelationshipsDataTable = catalogNode.CatalogNodeRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNode", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNode]", Value = catalogNodeDataTable, },
            new("CatalogNodeToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeToCatalogRelationship]", Value = catalogNodeToCatalogRelationshipsDataTable },
            new("CatalogNodeHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeHierarchicalRelationship]", Value = catalogNodeHierarchicalRelationshipsDataTable },
            new("CatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeRelationship]", Value = catalogNodeRelationshipsDataTable },
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_UpdateCatalogNode]
                @{"CatalogNode"},
                @{"CatalogNodeToCatalogRelationships"},
                @{"CatalogNodeHierarchicalRelationships"},
                @{"CatalogNodeRelationships"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getUpdatedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to update a catalog node because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        SqlParameter resultRemovedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNodeId", DbValueConvert.ToDbValue(catalogNodeId.Value)),
            resultRemovedRowsParameter = CreateResultRemovedRowsSqlParameter(out GetRowsCountDelegate getRemovedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_RemoveCatalogNode]
                @{"CatalogNodeId"},
                @{resultRemovedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getRemovedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to remove a catalog node because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <summary>
    /// Configures exception mapping rules for the <see cref="CatalogNode" /> database entity.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance to configure.</param>
    internal static void ConfigureExceptionMapper(IExceptionMapperBuilder exceptionMapperBuilder)
    {
        // dbo.CatalogNode - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNode_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNode_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNode_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node's description can not be longer than 32768 byte pairs.", sqlException));

        // dbo.CatalogNodeToCatalogRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-node-to-catalog relationship must reference an existing catalog node as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-node-to-catalog relationship must reference an existing catalog as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeToCatalogRelationship_CatalogNodeId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-node-to-catalog relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeToCatalogRelationship_CatalogId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-node-to-catalog relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // dbo.CatalogNodeHierarchicalRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship must reference an existing catalog node as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship must reference an existing catalog node as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeHierarchicalRelationship_Order_EqualsZero", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship must be unique on the relationship owner side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeHierarchicalRelationship_CatalogNodeIds_NotEqual", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship can not reference the same catalog node as both relationship owner and target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeHierarchicalRelationship_CatalogNodeId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node hierarchical relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // dbo.CatalogNodeRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship must reference an existing catalog node as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship must reference an existing catalog node as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeRelationship_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeRelationship_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeRelationship_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship's description can not be longer than 32768 byte pairs.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogNodeRelationship_CatalogNodeIds_NotEqual", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship can not reference the same catalog node as both relationship owner and target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeRelationship_CatalogNodeId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogNodeRelationship_DependentCatalogNodeId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog node relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // Generic SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 2628,
            sqlErrorPredicate: sqlError => true,
            factoryMethod: sqlException => new EntityConstraintViolationException("String or binary data would be truncated, max length was exceeded.", sqlException));
    }
}
