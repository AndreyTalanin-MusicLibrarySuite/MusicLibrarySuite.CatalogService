using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryToCatalogNodeRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogEntryToCatalogNodeRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryToCatalogNodeRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship> m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship> m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryToCatalogNodeRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-entry-to-catalog-node relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-entry-to-catalog-node relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogEntryToCatalogNodeRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor = catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogEntryRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId: catalogEntryId, catalogNodeId: null, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId: null, catalogNodeId: catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId: catalogEntryId, catalogNodeId: null, catalogEntryToCatalogNodeRelationships, false, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        await ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId: null, catalogNodeId: catalogNodeId, catalogEntryToCatalogNodeRelationships, true, cancellationToken);
    }

    private async Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId? catalogEntryId, CatalogNodeId? catalogNodeId, CancellationToken cancellationToken = default)
    {
        if (catalogEntryId is null && catalogNodeId is null)
            throw new ArgumentException($"Either {nameof(catalogEntryId)} or {nameof(catalogNodeId)} should be not null.");

        IQueryable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationshipsQueryable = Context.CatalogEntryToCatalogNodeRelationships;

        catalogEntryToCatalogNodeRelationshipsQueryable = catalogEntryToCatalogNodeRelationshipsQueryable
            .Include(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.CatalogEntry)
            .Include(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.CatalogNode)
            .AsNoTracking();

        if (catalogEntryId is not null)
        {
            Guid catalogEntryIdValue = ((CatalogEntryId)catalogEntryId).Value;
            catalogEntryToCatalogNodeRelationshipsQueryable = catalogEntryToCatalogNodeRelationshipsQueryable
                .Where(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.CatalogEntryId == catalogEntryIdValue)
                .OrderBy(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.Order);
        }
        if (catalogNodeId is not null)
        {
            Guid catalogNodeIdValue = ((CatalogNodeId)catalogNodeId).Value;
            catalogEntryToCatalogNodeRelationshipsQueryable = catalogEntryToCatalogNodeRelationshipsQueryable
                .Where(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.CatalogNodeId == catalogNodeIdValue);

            if (catalogEntryId is null)
            {
                catalogEntryToCatalogNodeRelationshipsQueryable = catalogEntryToCatalogNodeRelationshipsQueryable
                    .OrderBy(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.ReferenceOrder);
            }
        }

        CatalogEntryToCatalogNodeRelationship[] catalogEntryToCatalogNodeRelationships = await catalogEntryToCatalogNodeRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogEntryToCatalogNodeRelationships);

        return catalogEntryToCatalogNodeRelationships;
    }

    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId? catalogEntryId, CatalogNodeId? catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        if (byTarget && catalogNodeId is null)
            throw new ArgumentNullException(nameof(catalogNodeId));
        else if (!byTarget && catalogEntryId is null)
            throw new ArgumentNullException(nameof(catalogEntryId));

        m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntryToCatalogNodeRelationships, byTarget);

        using DataTable catalogEntryToCatalogNodeRelationshipsDataTable = catalogEntryToCatalogNodeRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogEntryId", DbValueConvert.ToDbValue((catalogEntryId ?? new CatalogEntryId()).Value)),
            new("CatalogNodeId", DbValueConvert.ToDbValue((catalogNodeId ?? new CatalogNodeId()).Value)),
            new("CatalogEntryToCatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogNodeRelationship]", Value = catalogEntryToCatalogNodeRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogEntryToCatalogNodeRelationships]
                @{"CatalogEntryId"},
                @{"CatalogNodeId"},
                @{"CatalogEntryToCatalogNodeRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogEntryToCatalogNodeRelationships.Any() && getUpdatedRowsCount() != catalogEntryToCatalogNodeRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog-entry-to-catalog-node relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
