using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryRelationshipType" /> database entity.
/// </summary>
public class SqlServerCatalogEntryRelationshipTypeRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryRelationshipTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryRelationshipTypeRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    public SqlServerCatalogEntryRelationshipTypeRepositoryExtension(IDbContextProvider<CatalogServiceDbContext> contextProvider)
        : base(contextProvider)
    {
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds = catalogEntryRelationshipTypeId.Yield();

        CatalogEntryRelationshipType? catalogEntryRelationshipType =
            (await GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeIds, cancellationToken)).SingleOrDefault();

        return catalogEntryRelationshipType;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeCode catalogEntryRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes = catalogEntryRelationshipTypeCode.Yield();

        CatalogEntryRelationshipType? catalogEntryRelationshipType =
            (await GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeCodes, cancellationToken)).SingleOrDefault();

        return catalogEntryRelationshipType;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipType[] catalogEntryRelationshipTypes = await GetCatalogEntryRelationshipTypesAsync(
            catalogEntryRelationshipTypeIds: catalogEntryRelationshipTypeIds,
            catalogEntryRelationshipTypeCodes: Enumerable.Empty<CatalogEntryRelationshipTypeCode>(),
            cancellationToken);

        return catalogEntryRelationshipTypes;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipType[] catalogEntryRelationshipTypes = await GetCatalogEntryRelationshipTypesAsync(
            catalogEntryRelationshipTypeIds: Enumerable.Empty<CatalogEntryRelationshipTypeId>(),
            catalogEntryRelationshipTypeCodes: catalogEntryRelationshipTypeCodes,
            cancellationToken);

        return catalogEntryRelationshipTypes;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntryRelationshipType> catalogEntryRelationshipTypesQueryable = Context.CatalogEntryRelationshipTypes;
        catalogEntryRelationshipTypesQueryable = catalogEntryRelationshipTypesQueryable.AsNoTracking();

        catalogEntryRelationshipTypesQueryable = catalogEntryRelationshipTypesQueryable
            .Include(catalogEntryRelationshipType => catalogEntryRelationshipType.OwnerCatalogEntryType)
            .Include(catalogEntryRelationshipType => catalogEntryRelationshipType.TargetCatalogEntryType);

        CatalogEntryRelationshipType[] catalogEntryRelationshipTypes = await catalogEntryRelationshipTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryRelationshipTypes;
    }

    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(
        IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds,
        IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes,
        CancellationToken cancellationToken = default)
    {
        using DataTable catalogEntryRelationshipTypeIdsDataTable = catalogEntryRelationshipTypeIds
            .Select(catalogEntryRelationshipTypeId => catalogEntryRelationshipTypeId.Value)
            .ToDataTable();
        using DataTable catalogEntryRelationshipTypeCodesDataTable = catalogEntryRelationshipTypeCodes
            .Select(catalogEntryRelationshipTypeCode => catalogEntryRelationshipTypeCode.Value)
            .ToDataTable();

        SqlParameter catalogEntryRelationshipTypeIdsParameter;
        SqlParameter catalogEntryRelationshipTypeCodesParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            catalogEntryRelationshipTypeIdsParameter =
                new("CatalogEntryRelationshipTypeIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogEntryRelationshipTypeIdsDataTable },
            catalogEntryRelationshipTypeCodesParameter =
                new("CatalogEntryRelationshipTypeCodes", SqlDbType.Structured) { TypeName = "[ctt].[String256UnorderedSet]", Value = catalogEntryRelationshipTypeCodesDataTable },
        };

        string query = $@"
            SELECT * FROM [dbo].[ufn_GetCatalogEntryRelationshipTypes]
            (
                @{catalogEntryRelationshipTypeIdsParameter.ParameterName},
                @{catalogEntryRelationshipTypeCodesParameter.ParameterName}
            )";

        IQueryable<CatalogEntryRelationshipType> catalogEntryRelationshipTypesQueryable = Context.CatalogEntryRelationshipTypes.FromSqlRaw(query, parameters);
        catalogEntryRelationshipTypesQueryable = catalogEntryRelationshipTypesQueryable.AsNoTracking();

        catalogEntryRelationshipTypesQueryable = catalogEntryRelationshipTypesQueryable
            .Include(catalogEntryRelationshipType => catalogEntryRelationshipType.OwnerCatalogEntryType)
            .Include(catalogEntryRelationshipType => catalogEntryRelationshipType.TargetCatalogEntryType);

        CatalogEntryRelationshipType[] catalogEntryRelationshipTypes = await catalogEntryRelationshipTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryRelationshipTypes;
    }
}
