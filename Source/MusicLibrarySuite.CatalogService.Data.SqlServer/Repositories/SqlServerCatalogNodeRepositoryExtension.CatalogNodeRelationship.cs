using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogNodeRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogNodeRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogNodeRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship> m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogNodeRelationship> m_catalogNodeRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogNodeRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogNodeRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog node relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog node relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogNodeRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship> catalogNodeRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogNodeRelationship> catalogNodeRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogNodeRelationshipSetDisplayOrderVisitor = catalogNodeRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogNodeRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeRelationship[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogNodeRelationship> catalogNodeRelationshipsQueryable = Context.CatalogNodeRelationships;

        catalogNodeRelationshipsQueryable = catalogNodeRelationshipsQueryable
            .Include(catalogNodeRelationship => catalogNodeRelationship.CatalogNode)
            .Include(catalogNodeRelationship => catalogNodeRelationship.DependentCatalogNode)
            .AsNoTracking();

        catalogNodeRelationshipsQueryable = byTarget
            ? catalogNodeRelationshipsQueryable
                .Where(catalogNodeRelationship => catalogNodeRelationship.DependentCatalogNodeId == catalogNodeId.Value)
                .OrderBy(catalogNodeRelationship => catalogNodeRelationship.ReferenceOrder)
        : catalogNodeRelationshipsQueryable
                .Where(catalogNodeRelationship => catalogNodeRelationship.CatalogNodeId == catalogNodeId.Value)
                .OrderBy(catalogNodeRelationship => catalogNodeRelationship.Order);

        CatalogNodeRelationship[] catalogNodeRelationships = await catalogNodeRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogNodeRelationships);

        return catalogNodeRelationships;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationship> catalogNodeRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        m_catalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogNodeRelationships, byTarget);

        using DataTable catalogNodeRelationshipsDataTable = catalogNodeRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogNodeId", DbValueConvert.ToDbValue(catalogNodeId.Value)),
            new("DependentCatalogNodeId", DbValueConvert.ToDbValue(catalogNodeId.Value)),
            new("CatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogNodeRelationship]", Value = catalogNodeRelationshipsDataTable },
            new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_ReorderCatalogNodeRelationships]
                @{"CatalogNodeId"},
                @{"DependentCatalogNodeId"},
                @{"CatalogNodeRelationships"},
                @{"ByTarget"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (catalogNodeRelationships.Any() && getUpdatedRowsCount() != catalogNodeRelationships.Count())
        {
            throw new EntityConflictException("Unable to reorder catalog node relationships because the provided set is different from the database state.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }
}
