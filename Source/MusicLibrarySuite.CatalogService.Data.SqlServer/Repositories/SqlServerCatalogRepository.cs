using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.SqlServer.Extensions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents the SQL-Server-specific implementation of a repository for the <see cref="Catalog" /> database entity.
/// </summary>
public class SqlServerCatalogRepository : SqlServerRepositoryBase, ICatalogRepository
{
    private readonly IPrimaryKeyValueProvider m_primaryKeyValueProvider;
    private readonly ISetPrimaryKeyVisitor<Catalog, CatalogId> m_catalogSetPrimaryKeyVisitor;
    private readonly ISetNavigationPropertyForeignKeysVisitor<Catalog> m_catalogSetNavigationPropertyForeignKeysVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<Catalog> m_catalogCleanUpNavigationPropertiesVisitor;
    private readonly ISortNavigationPropertiesVisitor<Catalog> m_catalogSortNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogRelationship> m_catalogRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogRepository" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="primaryKeyValueProvider">The primary key <see cref="Guid" /> value provider.</param>
    /// <param name="catalogSetPrimaryKeyVisitor">The visitor that sets the primary key of a catalog so dependent entities can reference the main one via foreign keys.</param>
    /// <param name="catalogSetNavigationPropertyForeignKeysVisitor">The visitor that sets foreign keys of navigation properties of a catalog so dependent entities are referencing the main one.</param>
    /// <param name="catalogCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog so there are no reference cycles in the object graph.</param>
    /// <param name="catalogSortNavigationPropertiesVisitor">The visitor that sorts collection-represented navigation properties of a catalog so their order is correct.</param>
    /// <param name="catalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogRepository(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        IPrimaryKeyValueProvider primaryKeyValueProvider,
        ISetPrimaryKeyVisitor<Catalog, CatalogId> catalogSetPrimaryKeyVisitor,
        ISetNavigationPropertyForeignKeysVisitor<Catalog> catalogSetNavigationPropertyForeignKeysVisitor,
        ICleanUpNavigationPropertiesVisitor<Catalog> catalogCleanUpNavigationPropertiesVisitor,
        ISortNavigationPropertiesVisitor<Catalog> catalogSortNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogRelationship> catalogRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_primaryKeyValueProvider = primaryKeyValueProvider;
        m_catalogSetPrimaryKeyVisitor = catalogSetPrimaryKeyVisitor;
        m_catalogSetNavigationPropertyForeignKeysVisitor = catalogSetNavigationPropertyForeignKeysVisitor;
        m_catalogCleanUpNavigationPropertiesVisitor = catalogCleanUpNavigationPropertiesVisitor;
        m_catalogSortNavigationPropertiesVisitor = catalogSortNavigationPropertiesVisitor;
        m_catalogRelationshipSetDisplayOrderVisitor = catalogRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<Catalog?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogId> catalogIds = catalogId.Yield();

        Catalog? catalog = (await GetCatalogsAsync(catalogIds, cancellationToken)).SingleOrDefault();

        return catalog;
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default)
    {
        catalogIds = catalogIds.Distinct();
        using DataTable catalogIdsDataTable = catalogIds
            .Select(catalogId => catalogId.Value)
            .ToDataTable();

        SqlParameter catalogIdsParameter = new("CatalogIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogIdsDataTable };

        string query = $"SELECT * FROM [dbo].[ufn_GetCatalogs] (@{catalogIdsParameter.ParameterName})";

        IQueryable<Catalog> catalogsQueryable = Context.Catalogs.FromSqlRaw(query, catalogIdsParameter);

        catalogsQueryable = catalogsQueryable
            .Include(catalog => catalog.CatalogRelationships)
            .ThenInclude(catalogRelationship => catalogRelationship.DependentCatalog);

        catalogsQueryable = catalogsQueryable
            .AsNoTracking()
            .AsSplitQuery();

        Catalog[] catalogs = await catalogsQueryable.ToArrayAsync(cancellationToken);

        m_catalogCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogs);
        m_catalogSortNavigationPropertiesVisitor.SortNavigationProperties(catalogs);

        return catalogs;
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;
        catalogsQueryable = catalogsQueryable.Filter(filter)
            .AsNoTracking();

        Catalog[] catalogs = await catalogsQueryable.ToArrayAsync(cancellationToken);

        return catalogs;
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;
        catalogsQueryable = (await catalogsQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        Catalog[] catalogs = await catalogsQueryable.ToArrayAsync(cancellationToken);

        return catalogs;
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;
        catalogsQueryable = catalogsQueryable.AsNoTracking();

        Catalog[] catalogs = await catalogsQueryable.ToArrayAsync(cancellationToken);

        return catalogs;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;
        catalogsQueryable = catalogsQueryable.Filter(filter)
            .AsNoTracking();

        int catalogsCount = await catalogsQueryable.CountAsync(cancellationToken);

        return catalogsCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;
        catalogsQueryable = (await catalogsQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        int catalogsCount = await catalogsQueryable.CountAsync(cancellationToken);

        return catalogsCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<Catalog> catalogsQueryable = Context.Catalogs;

        int catalogsCount = await catalogsQueryable.CountAsync(cancellationToken);

        return catalogsCount;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task<Catalog> AddCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        Guid catalogId = catalog.Id == Guid.Empty
            ? await m_primaryKeyValueProvider.GetSequentialGuidValueAsync(cancellationToken)
            : Guid.Empty;

        m_catalogSetPrimaryKeyVisitor.SetPrimaryKeyIfEmpty(catalog, new CatalogId(catalogId));
        m_catalogSetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalog);
        m_catalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalog.CatalogRelationships, false);

        using DataTable catalogDataTable = catalog.Yield()
            .ToDataTable();

        using DataTable catalogRelationshipsDataTable = catalog.CatalogRelationships.ToDataTable();

        SqlParameter resultCreatedOnParameter;
        SqlParameter resultUpdatedOnParameter;
        SqlParameter resultAddedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("Catalog", SqlDbType.Structured) { TypeName = "[dbo].[Catalog]", Value = catalogDataTable, },
            new("CatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogRelationship]", Value = catalogRelationshipsDataTable },
            resultCreatedOnParameter = CreateResultSqlParameter("CreatedOn", SqlDbType.DateTimeOffset),
            resultUpdatedOnParameter = CreateResultSqlParameter("UpdatedOn", SqlDbType.DateTimeOffset),
            resultAddedRowsParameter = CreateResultAddedRowsSqlParameter(out GetRowsCountDelegate getAddedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_AddCatalog]
                @{"Catalog"},
                @{"CatalogRelationships"},
                @{resultCreatedOnParameter.ParameterName} OUTPUT,
                @{resultUpdatedOnParameter.ParameterName} OUTPUT,
                @{resultAddedRowsParameter.ParameterName} OUTPUT;";

        using (IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken))
        {
            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
                cancellationToken);

            if (getAddedRowsCount() == 0)
            {
                throw new EntityConflictException("Unable to add a catalog because it already exists.");
            }

            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
                cancellationToken);
        }

        catalog.CreatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultCreatedOnParameter.Value);
        catalog.UpdatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultUpdatedOnParameter.Value);

        return catalog;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task UpdateCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        m_catalogSetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalog);
        m_catalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalog.CatalogRelationships, false);

        using DataTable catalogDataTable = catalog.Yield()
            .ToDataTable();

        using DataTable catalogRelationshipsDataTable = catalog.CatalogRelationships.ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("Catalog", SqlDbType.Structured) { TypeName = "[dbo].[Catalog]", Value = catalogDataTable, },
            new("CatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogRelationship]", Value = catalogRelationshipsDataTable },
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_UpdateCatalog]
                @{"Catalog"},
                @{"CatalogRelationships"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getUpdatedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to update a catalog because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        SqlParameter resultRemovedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogId", DbValueConvert.ToDbValue(catalogId.Value)),
            resultRemovedRowsParameter = CreateResultRemovedRowsSqlParameter(out GetRowsCountDelegate getRemovedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_RemoveCatalog]
                @{"CatalogId"},
                @{resultRemovedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getRemovedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to remove a catalog because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <summary>
    /// Configures exception mapping rules for the <see cref="Catalog" /> database entity.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance to configure.</param>
    internal static void ConfigureExceptionMapper(IExceptionMapperBuilder exceptionMapperBuilder)
    {
        // dbo.Catalog - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_Catalog_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_Catalog_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_Catalog_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog's description can not be longer than 32768 byte pairs.", sqlException));

        // dbo.CatalogRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogRelationship_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogRelationship_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogRelationship_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship's description can not be longer than 32768 byte pairs.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogRelationship_CatalogIds_NotEqual", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship can not reference the same catalog as both relationship owner and target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogRelationship_Catalog_CatalogId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship must reference an existing catalog as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogRelationship_Catalog_DependentCatalogId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship must reference an existing catalog as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogRelationship_CatalogId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // Generic SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 2628,
            sqlErrorPredicate: sqlError => true,
            factoryMethod: sqlException => new EntityConstraintViolationException("String or binary data would be truncated, max length was exceeded.", sqlException));
    }
}
