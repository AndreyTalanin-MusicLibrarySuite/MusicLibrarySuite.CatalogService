using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryHierarchicalRelationship" /> database entity.
/// </summary>
public class SqlServerCatalogEntryHierarchicalRelationshipRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryHierarchicalRelationshipRepositoryExtension
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship> m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship> m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryHierarchicalRelationshipRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog entry hierarchical relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog entry hierarchical relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogEntryHierarchicalRelationshipRepositoryExtension(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor = catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(SqlServerCatalogEntryRepository.ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryHierarchicalRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryHierarchicalRelationshipTypeId: null,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryHierarchicalRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryHierarchicalRelationshipTypeId: (CatalogEntryHierarchicalRelationshipTypeId?)catalogEntryHierarchicalRelationshipTypeId,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        IEnumerable<IGrouping<Guid, CatalogEntryHierarchicalRelationship>> catalogEntryHierarchicalRelationshipsGroupings =
            catalogEntryHierarchicalRelationships.GroupBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId);
        foreach (IGrouping<Guid, CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipsGrouping in catalogEntryHierarchicalRelationshipsGroupings)
        {
            m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntryHierarchicalRelationships, byTarget);

            using DataTable catalogEntryHierarchicalRelationshipsDataTable = catalogEntryHierarchicalRelationships.ToDataTable();

            SqlParameter resultUpdatedRowsParameter;
            SqlParameter[] parameters = new SqlParameter[]
            {
                new("CatalogEntryId", DbValueConvert.ToDbValue(catalogEntryId.Value)),
                new("ParentCatalogEntryId", DbValueConvert.ToDbValue(catalogEntryId.Value)),
                new("CatalogEntryHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryHierarchicalRelationship]", Value = catalogEntryHierarchicalRelationshipsDataTable },
                new("ByTarget", DbValueConvert.ToDbValue(byTarget)),
                resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
            };

            string query = @$"
                EXEC [dbo].[sp_ReorderCatalogEntryHierarchicalRelationships]
                    @{"CatalogEntryId"},
                    @{"ParentCatalogEntryId"},
                    @{"CatalogEntryHierarchicalRelationships"},
                    @{"ByTarget"},
                    @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
                cancellationToken);

            if (catalogEntryHierarchicalRelationshipsGrouping.Any() && getUpdatedRowsCount() != catalogEntryHierarchicalRelationshipsGrouping.Count())
            {
                throw new EntityConflictException("Unable to reorder catalog entry hierarchical relationships because the provided set is different from the database state.");
            }
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    private async Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipsQueryable = Context.CatalogEntryHierarchicalRelationships;

        catalogEntryHierarchicalRelationshipsQueryable = catalogEntryHierarchicalRelationshipsQueryable
            .Include(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntry!.CatalogEntryType)
            .Include(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.ParentCatalogEntry!.CatalogEntryType)
            .Include(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.OwnerCatalogEntryType)
            .Include(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.TargetCatalogEntryType)
            .Include(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation)
            .AsNoTracking();

        if (catalogEntryHierarchicalRelationshipTypeId is not null)
        {
            Guid catalogEntryHierarchicalRelationshipTypeIdValue = ((CatalogEntryHierarchicalRelationshipTypeId)catalogEntryHierarchicalRelationshipTypeId).Value;
            catalogEntryHierarchicalRelationshipsQueryable = catalogEntryHierarchicalRelationshipsQueryable.Where(catalogEntryHierarchicalRelationship =>
                catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId == catalogEntryHierarchicalRelationshipTypeIdValue);
        }

        catalogEntryHierarchicalRelationshipsQueryable = byTarget
            ? catalogEntryHierarchicalRelationshipsQueryable
                .Where(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.ParentCatalogEntryId == catalogEntryId.Value)
                .OrderBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId)
                .ThenBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.ReferenceOrder)
            : catalogEntryHierarchicalRelationshipsQueryable
                .Where(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryId == catalogEntryId.Value)
                .OrderBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId)
                .ThenBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.Order);

        CatalogEntryHierarchicalRelationship[] catalogEntryHierarchicalRelationships = await catalogEntryHierarchicalRelationshipsQueryable.ToArrayAsync(cancellationToken);

        m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogEntryHierarchicalRelationships);

        return catalogEntryHierarchicalRelationships;
    }
}
