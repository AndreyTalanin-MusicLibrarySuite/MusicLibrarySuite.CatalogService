using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Data.Helpers;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Data.SqlServer.Extensions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

using static MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers.SqlServerRepositoryHelpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository for the <see cref="CatalogEntry" /> database entity.
/// </summary>
public class SqlServerCatalogEntryRepository : SqlServerRepositoryBase, ICatalogEntryRepository
{
    private readonly IPrimaryKeyValueProvider m_primaryKeyValueProvider;
    private readonly ISetPrimaryKeyVisitor<CatalogEntry, CatalogEntryId> m_catalogEntrySetPrimaryKeyVisitor;
    private readonly ISetNavigationPropertyForeignKeysVisitor<CatalogEntry> m_catalogEntrySetNavigationPropertyForeignKeysVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntry> m_catalogEntryCleanUpNavigationPropertiesVisitor;
    private readonly ISortNavigationPropertiesVisitor<CatalogEntry> m_catalogEntrySortNavigationPropertiesVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship> m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship> m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship> m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor;
    private readonly ISetDisplayOrderVisitor<CatalogEntryRelationship> m_catalogEntryRelationshipSetDisplayOrderVisitor;
    private readonly IExceptionMapperProxy m_exceptionMapperProxy;

    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryRepository" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    /// <param name="primaryKeyValueProvider">The primary key <see cref="Guid" /> value provider.</param>
    /// <param name="catalogEntrySetPrimaryKeyVisitor">The visitor that sets the primary key of a catalog entry so dependent entities can reference the main one via foreign keys.</param>
    /// <param name="catalogEntrySetNavigationPropertyForeignKeysVisitor">The visitor that sets foreign keys of navigation properties of a catalog entry so dependent entities are referencing the main one.</param>
    /// <param name="catalogEntryCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog entry so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntrySortNavigationPropertiesVisitor">The visitor that sorts collection-represented navigation properties of a catalog entry so their order is correct.</param>
    /// <param name="catalogEntryToCatalogRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-entry-to-catalog relationships so they can be inserted in the database as is.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog-entry-to-catalog-node relationships so they can be inserted in the database as is.</param>
    /// <param name="catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog entry hierarchical relationships so they can be inserted in the database as is.</param>
    /// <param name="catalogEntryRelationshipSetDisplayOrderVisitor">The visitor that sets the display order of a collection of catalog entry relationships so they can be inserted in the database as is.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public SqlServerCatalogEntryRepository(
        IDbContextProvider<CatalogServiceDbContext> contextProvider,
        IPrimaryKeyValueProvider primaryKeyValueProvider,
        ISetPrimaryKeyVisitor<CatalogEntry, CatalogEntryId> catalogEntrySetPrimaryKeyVisitor,
        ISetNavigationPropertyForeignKeysVisitor<CatalogEntry> catalogEntrySetNavigationPropertyForeignKeysVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogEntry> catalogEntryCleanUpNavigationPropertiesVisitor,
        ISortNavigationPropertiesVisitor<CatalogEntry> catalogEntrySortNavigationPropertiesVisitor,
        ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationshipSetDisplayOrderVisitor,
        ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor,
        ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor,
        ISetDisplayOrderVisitor<CatalogEntryRelationship> catalogEntryRelationshipSetDisplayOrderVisitor,
        IExceptionMapperProxy exceptionMapperProxy)
        : base(contextProvider)
    {
        m_primaryKeyValueProvider = primaryKeyValueProvider;
        m_catalogEntrySetPrimaryKeyVisitor = catalogEntrySetPrimaryKeyVisitor;
        m_catalogEntrySetNavigationPropertyForeignKeysVisitor = catalogEntrySetNavigationPropertyForeignKeysVisitor;
        m_catalogEntryCleanUpNavigationPropertiesVisitor = catalogEntryCleanUpNavigationPropertiesVisitor;
        m_catalogEntrySortNavigationPropertiesVisitor = catalogEntrySortNavigationPropertiesVisitor;
        m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor = catalogEntryToCatalogRelationshipSetDisplayOrderVisitor;
        m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor = catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor;
        m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor = catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor;
        m_catalogEntryRelationshipSetDisplayOrderVisitor = catalogEntryRelationshipSetDisplayOrderVisitor;
        m_exceptionMapperProxy = exceptionMapperProxy;

        m_exceptionMapperProxy.Configure(ConfigureExceptionMapper);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryId> catalogEntryIds = catalogEntryId.Yield();

        CatalogEntry? catalogEntry = (await GetCatalogEntriesAsync(catalogEntryIds, cancellationToken)).SingleOrDefault();

        return catalogEntry;
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default)
    {
        catalogEntryIds = catalogEntryIds.Distinct();
        using DataTable catalogEntryIdsDataTable = catalogEntryIds
            .Select(catalogEntryId => catalogEntryId.Value)
            .ToDataTable();

        SqlParameter catalogEntryIdsParameter = new("CatalogEntryIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogEntryIdsDataTable };

        string query = $"SELECT * FROM [dbo].[ufn_GetCatalogEntries] (@{catalogEntryIdsParameter.ParameterName})";

        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries.FromSqlRaw(query, catalogEntryIdsParameter);

        catalogEntriesQueryable = catalogEntriesQueryable
            .Include(catalogEntry => catalogEntry.CatalogEntryToCatalogRelationships)
            .ThenInclude(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.Catalog)
            .Include(catalogEntry => catalogEntry.CatalogEntryToCatalogNodeRelationships)
            .ThenInclude(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.CatalogNode)
            .Include(catalogEntry => catalogEntry.CatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.ParentCatalogEntry!.CatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.OwnerCatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.TargetCatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation)
            .Include(catalogEntry => catalogEntry.ChildCatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntry!.CatalogEntryType)
            .Include(catalogEntry => catalogEntry.ChildCatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.OwnerCatalogEntryType)
            .Include(catalogEntry => catalogEntry.ChildCatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipType!.TargetCatalogEntryType)
            .Include(catalogEntry => catalogEntry.ChildCatalogEntryHierarchicalRelationships)
            .ThenInclude(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation)
            .Include(catalogEntry => catalogEntry.CatalogEntryRelationships)
            .ThenInclude(catalogEntryRelationship => catalogEntryRelationship.DependentCatalogEntry!.CatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryRelationships)
            .ThenInclude(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipType!.OwnerCatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryRelationships)
            .ThenInclude(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipType!.TargetCatalogEntryType)
            .Include(catalogEntry => catalogEntry.CatalogEntryRelationships)
            .ThenInclude(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation)
            .Include(catalogEntry => catalogEntry.CatalogEntryType);

        catalogEntriesQueryable = catalogEntriesQueryable
            .AsNoTracking()
            .AsSplitQuery();

        CatalogEntry[] catalogEntries = await catalogEntriesQueryable.ToArrayAsync(cancellationToken);

        m_catalogEntryCleanUpNavigationPropertiesVisitor.CleanUpNavigationProperties(catalogEntries);
        m_catalogEntrySortNavigationPropertiesVisitor.SortNavigationProperties(catalogEntries);

        return catalogEntries;
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;
        catalogEntriesQueryable = catalogEntriesQueryable.Filter(filter)
            .AsNoTracking();

        CatalogEntry[] catalogEntries = await catalogEntriesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntries;
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;
        catalogEntriesQueryable = (await catalogEntriesQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        CatalogEntry[] catalogEntries = await catalogEntriesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntries;
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;
        catalogEntriesQueryable = catalogEntriesQueryable.AsNoTracking();

        CatalogEntry[] catalogEntries = await catalogEntriesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntries;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;
        catalogEntriesQueryable = catalogEntriesQueryable.Filter(filter)
            .AsNoTracking();

        int catalogEntriesCount = await catalogEntriesQueryable.CountAsync(cancellationToken);

        return catalogEntriesCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;
        catalogEntriesQueryable = (await catalogEntriesQueryable.FilterAsync(asyncFilter, cancellationToken))
            .AsNoTracking();

        int catalogEntriesCount = await catalogEntriesQueryable.CountAsync(cancellationToken);

        return catalogEntriesCount;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntry> catalogEntriesQueryable = Context.CatalogEntries;

        int catalogEntriesCount = await catalogEntriesQueryable.CountAsync(cancellationToken);

        return catalogEntriesCount;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task<CatalogEntry> AddCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryId = catalogEntry.Id == Guid.Empty
            ? await m_primaryKeyValueProvider.GetSequentialGuidValueAsync(cancellationToken)
            : Guid.Empty;

        m_catalogEntrySetPrimaryKeyVisitor.SetPrimaryKeyIfEmpty(catalogEntry, new CatalogEntryId(catalogEntryId));
        m_catalogEntrySetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalogEntry);
        m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryToCatalogRelationships, false);
        m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryToCatalogNodeRelationships, false);
        m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryHierarchicalRelationships, false);
        m_catalogEntryRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryRelationships, false);

        using DataTable catalogEntryDataTable = catalogEntry.Yield()
            .ToDataTable();

        using DataTable catalogEntryToCatalogRelationshipsDataTable = catalogEntry.CatalogEntryToCatalogRelationships.ToDataTable();
        using DataTable catalogEntryToCatalogNodeRelationshipsDataTable = catalogEntry.CatalogEntryToCatalogNodeRelationships.ToDataTable();
        using DataTable catalogEntryHierarchicalRelationshipsDataTable = catalogEntry.CatalogEntryHierarchicalRelationships.ToDataTable();
        using DataTable catalogEntryHierarchicalRelationshipAnnotationsDataTable = catalogEntry.CatalogEntryHierarchicalRelationships
            .Where(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation is not null)
            .Select(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation!)
            .ToDataTable();
        using DataTable catalogEntryRelationshipsDataTable = catalogEntry.CatalogEntryRelationships.ToDataTable();
        using DataTable catalogEntryRelationshipAnnotationsDataTable = catalogEntry.CatalogEntryRelationships
            .Where(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation is not null)
            .Select(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation!)
            .ToDataTable();

        SqlParameter resultCreatedOnParameter;
        SqlParameter resultUpdatedOnParameter;
        SqlParameter resultAddedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogEntry", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntry]", Value = catalogEntryDataTable, },
            new("CatalogEntryToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogRelationship]", Value = catalogEntryToCatalogRelationshipsDataTable },
            new("CatalogEntryToCatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogNodeRelationship]", Value = catalogEntryToCatalogNodeRelationshipsDataTable },
            new("CatalogEntryHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryHierarchicalRelationship]", Value = catalogEntryHierarchicalRelationshipsDataTable },
            new("CatalogEntryHierarchicalRelationshipAnnotations", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryHierarchicalRelationshipAnnotation]", Value = catalogEntryHierarchicalRelationshipAnnotationsDataTable },
            new("CatalogEntryRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryRelationship]", Value = catalogEntryRelationshipsDataTable },
            new("CatalogEntryRelationshipAnnotations", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryRelationshipAnnotation]", Value = catalogEntryRelationshipAnnotationsDataTable },
            resultCreatedOnParameter = CreateResultSqlParameter("CreatedOn", SqlDbType.DateTimeOffset),
            resultUpdatedOnParameter = CreateResultSqlParameter("UpdatedOn", SqlDbType.DateTimeOffset),
            resultAddedRowsParameter = CreateResultAddedRowsSqlParameter(out GetRowsCountDelegate getAddedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_AddCatalogEntry]
                @{"CatalogEntry"},
                @{"CatalogEntryToCatalogRelationships"},
                @{"CatalogEntryToCatalogNodeRelationships"},
                @{"CatalogEntryHierarchicalRelationships"},
                @{"CatalogEntryHierarchicalRelationshipAnnotations"},
                @{"CatalogEntryRelationships"},
                @{"CatalogEntryRelationshipAnnotations"},
                @{resultCreatedOnParameter.ParameterName} OUTPUT,
                @{resultUpdatedOnParameter.ParameterName} OUTPUT,
                @{resultAddedRowsParameter.ParameterName} OUTPUT;";

        using (IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken))
        {
            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
                cancellationToken);

            if (getAddedRowsCount() == 0)
            {
                throw new EntityConflictException("Unable to add a catalog entry because it already exists.");
            }

            await m_exceptionMapperProxy.InvokeAsync(
                async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
                cancellationToken);
        }

        catalogEntry.CreatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultCreatedOnParameter.Value);
        catalogEntry.UpdatedOn = DbValueConvert.ToValueType<DateTimeOffset>(resultUpdatedOnParameter.Value);

        return catalogEntry;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task UpdateCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default)
    {
        m_catalogEntrySetNavigationPropertyForeignKeysVisitor.SetNavigationPropertyForeignKeys(catalogEntry);
        m_catalogEntryToCatalogRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryToCatalogRelationships, false);
        m_catalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryToCatalogNodeRelationships, false);
        m_catalogEntryHierarchicalRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryHierarchicalRelationships, false);
        m_catalogEntryRelationshipSetDisplayOrderVisitor.SetDisplayOrder(catalogEntry.CatalogEntryRelationships, false);

        using DataTable catalogEntryDataTable = catalogEntry.Yield()
            .ToDataTable();

        using DataTable catalogEntryToCatalogRelationshipsDataTable = catalogEntry.CatalogEntryToCatalogRelationships.ToDataTable();
        using DataTable catalogEntryToCatalogNodeRelationshipsDataTable = catalogEntry.CatalogEntryToCatalogNodeRelationships.ToDataTable();
        using DataTable catalogEntryHierarchicalRelationshipsDataTable = catalogEntry.CatalogEntryHierarchicalRelationships.ToDataTable();
        using DataTable catalogEntryHierarchicalRelationshipAnnotationsDataTable = catalogEntry.CatalogEntryHierarchicalRelationships
            .Where(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation is not null)
            .Select(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation!)
            .ToDataTable();
        using DataTable catalogEntryRelationshipsDataTable = catalogEntry.CatalogEntryRelationships.ToDataTable();
        using DataTable catalogEntryRelationshipAnnotationsDataTable = catalogEntry.CatalogEntryRelationships
            .Where(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation is not null)
            .Select(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipAnnotation!)
            .ToDataTable();

        SqlParameter resultUpdatedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogEntry", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntry]", Value = catalogEntryDataTable, },
            new("CatalogEntryToCatalogRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogRelationship]", Value = catalogEntryToCatalogRelationshipsDataTable },
            new("CatalogEntryToCatalogNodeRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryToCatalogNodeRelationship]", Value = catalogEntryToCatalogNodeRelationshipsDataTable },
            new("CatalogEntryHierarchicalRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryHierarchicalRelationship]", Value = catalogEntryHierarchicalRelationshipsDataTable },
            new("CatalogEntryHierarchicalRelationshipAnnotations", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryHierarchicalRelationshipAnnotation]", Value = catalogEntryHierarchicalRelationshipAnnotationsDataTable },
            new("CatalogEntryRelationships", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryRelationship]", Value = catalogEntryRelationshipsDataTable },
            new("CatalogEntryRelationshipAnnotations", SqlDbType.Structured) { TypeName = "[dbo].[CatalogEntryRelationshipAnnotation]", Value = catalogEntryRelationshipAnnotationsDataTable },
            resultUpdatedRowsParameter = CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getUpdatedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_UpdateCatalogEntry]
                @{"CatalogEntry"},
                @{"CatalogEntryToCatalogRelationships"},
                @{"CatalogEntryToCatalogNodeRelationships"},
                @{"CatalogEntryHierarchicalRelationships"},
                @{"CatalogEntryHierarchicalRelationshipAnnotations"},
                @{"CatalogEntryRelationships"},
                @{"CatalogEntryRelationshipAnnotations"},
                @{resultUpdatedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getUpdatedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to update a catalog entry because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    public async Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        SqlParameter resultRemovedRowsParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            new("CatalogEntryId", DbValueConvert.ToDbValue(catalogEntryId.Value)),
            resultRemovedRowsParameter = CreateResultRemovedRowsSqlParameter(out GetRowsCountDelegate getRemovedRowsCount),
        };

        string query = @$"
            EXEC [dbo].[sp_RemoveCatalogEntry]
                @{"CatalogEntryId"},
                @{resultRemovedRowsParameter.ParameterName} OUTPUT;";

        using IDbContextTransaction contextTransaction = await Context.Database.BeginTransactionAsync(cancellationToken);

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await Context.Database.ExecuteSqlRawAsync(query, parameters, cancellationToken),
            cancellationToken);

        if (getRemovedRowsCount() == 0)
        {
            throw new EntityNotFoundException("Unable to remove a catalog entry because it does not exist.");
        }

        await m_exceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await contextTransaction.CommitAsync(cancellationToken),
            cancellationToken);
    }

    /// <summary>
    /// Configures exception mapping rules for the <see cref="CatalogEntry" /> database entity.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance to configure.</param>
    internal static void ConfigureExceptionMapper(IExceptionMapperBuilder exceptionMapperBuilder)
    {
        // dbo.CatalogEntry - related SQL errors.
        // No database constraints exist for the dbo.CatalogEntry object.

        // dbo.CatalogEntryToCatalogRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog relationship must reference an existing catalog entry as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog relationship must reference an existing catalog as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryToCatalogRelationship_CatalogEntryId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryToCatalogRelationship_CatalogId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // dbo.CatalogEntryToCatalogNodeRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog-node relationship must reference an existing catalog entry as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog-node relationship must reference an existing catalog node as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId_Order", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog-node relationship would break the display order on the relationship owner side. This error is most likely due to a concurrent update.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId_ReferenceOrder", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog-entry-to-catalog-node relationship would break the display order on the relationship target side. This error is most likely due to a concurrent update.", sqlException));

        // dbo.CatalogEntryHierarchicalRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryHierarchicalRelationship_CatalogEntryIds_NotEqual", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship can not reference the same catalog entry as both relationship owner and target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must reference an existing catalog entry as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must reference an existing catalog entry as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must reference an existing catalog entry hierarchical relationship type.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipOwnerUniqueView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must be unique on the relationship owner side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipTargetUniqueView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must be unique on the relationship target side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must reference a catalog entry of the correct type on the relationship owner side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must reference a catalog entry of the correct type on the relationship target side.", sqlException));

        // dbo.CatalogEntryHierarchicalRelationshipAnnotation - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship annotation's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship annotation's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship annotation's description can not be longer than 32768 byte pairs.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship annotation must reference an existing catalog entry hierarchical relationship.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipInvalidAnnotationView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must not have an annotation.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry hierarchical relationship must have an annotation.", sqlException));

        // dbo.CatalogEntryRelationship - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryRelationship_CatalogEntryIds_NotEqual", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship can not reference the same catalog entry as both relationship owner and target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must reference an existing catalog entry as relationship owner.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must reference an existing catalog entry as relationship target.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must reference an existing catalog entry relationship type.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipOwnerUniqueView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must be unique on the relationship owner side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipTargetUniqueView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must be unique on the relationship target side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must reference a catalog entry of the correct type on the relationship owner side.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must reference a catalog entry of the correct type on the relationship target side.", sqlException));

        // dbo.CatalogEntryRelationshipAnnotation - related SQL errors.
        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryRelationshipAnnotation_Name_NotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship annotation's name can not be empty.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryRelationshipAnnotation_Description_NullOrNotEmpty", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship annotation's description can not be empty, use null instead.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("CK_CatalogEntryRelationshipAnnotation_Description_NullOrMaxLength", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship annotation's description can not be longer than 32768 byte pairs.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 16,
            sqlErrorNumber: 547,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship annotation must reference an existing catalog entry relationship.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipInvalidAnnotationView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must not have an annotation.", sqlException));

        exceptionMapperBuilder.AddSqlExceptionRule(
            sqlErrorClass: 14,
            sqlErrorNumber: 2601,
            sqlErrorPredicate: sqlError => sqlError.Message.Contains("UIX_CatalogEntryRelationshipMissingAnnotationInMemoryView_MultipleColumns", StringComparison.InvariantCultureIgnoreCase),
            factoryMethod: sqlException => new EntityConstraintViolationException("A catalog entry relationship must have an annotation.", sqlException));
    }
}
