using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Extensions;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories.Base;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;

/// <summary>
/// Represents a SQL-Server-specific implementation of the repository extension for the <see cref="CatalogEntryType" /> database entity.
/// </summary>
public class SqlServerCatalogEntryTypeRepositoryExtension : SqlServerRepositoryBase, ICatalogEntryTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqlServerCatalogEntryTypeRepositoryExtension" /> type using the specified services.
    /// </summary>
    /// <param name="contextProvider">The database context provider.</param>
    public SqlServerCatalogEntryTypeRepositoryExtension(IDbContextProvider<CatalogServiceDbContext> contextProvider)
        : base(contextProvider)
    {
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds = catalogEntryTypeId.Yield();

        CatalogEntryType? catalogEntryType = (await GetCatalogEntryTypesAsync(catalogEntryTypeIds, cancellationToken)).SingleOrDefault();

        return catalogEntryType;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeCode catalogEntryTypeCode, CancellationToken cancellationToken = default)
    {
        IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes = catalogEntryTypeCode.Yield();

        CatalogEntryType? catalogEntryType = (await GetCatalogEntryTypesAsync(catalogEntryTypeCodes, cancellationToken)).SingleOrDefault();

        return catalogEntryType;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryType[] catalogEntryTypes = await GetCatalogEntryTypesAsync(
            catalogEntryTypeIds: catalogEntryTypeIds,
            catalogEntryTypeCodes: Enumerable.Empty<CatalogEntryTypeCode>(),
            cancellationToken);

        return catalogEntryTypes;
    }

    /// <inheritdoc />
    [SuppressMessage("Style", "IDE0301:Simplify collection initialization", Justification = "To increase readability for the empty IEnumerable<T> object.")]
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes, CancellationToken cancellationToken = default)
    {
        CatalogEntryType[] catalogEntryTypes = await GetCatalogEntryTypesAsync(
            catalogEntryTypeIds: Enumerable.Empty<CatalogEntryTypeId>(),
            catalogEntryTypeCodes: catalogEntryTypeCodes,
            cancellationToken);

        return catalogEntryTypes;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        IQueryable<CatalogEntryType> catalogEntryTypesQueryable = Context.CatalogEntryTypes;
        catalogEntryTypesQueryable = catalogEntryTypesQueryable.AsNoTracking();

        CatalogEntryType[] catalogEntryTypes = await catalogEntryTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryTypes;
    }

    [SuppressMessage("Style", "IDE0300:Simplify collection initialization", Justification = "To increase readability for multi-line array initialization.")]
    private async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(
        IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds,
        IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes,
        CancellationToken cancellationToken = default)
    {
        using DataTable catalogEntryTypeIdsDataTable = catalogEntryTypeIds
            .Select(catalogEntryTypeId => catalogEntryTypeId.Value)
            .ToDataTable();
        using DataTable catalogEntryTypeCodesDataTable = catalogEntryTypeCodes
            .Select(catalogEntryTypeId => catalogEntryTypeId.Value)
            .ToDataTable();

        SqlParameter catalogEntryTypeIdsParameter;
        SqlParameter catalogEntryTypeCodesParameter;
        SqlParameter[] parameters = new SqlParameter[]
        {
            catalogEntryTypeIdsParameter = new("CatalogEntryTypeIds", SqlDbType.Structured) { TypeName = "[ctt].[GuidUnorderedSet]", Value = catalogEntryTypeIdsDataTable },
            catalogEntryTypeCodesParameter = new("CatalogEntryTypeCodes", SqlDbType.Structured) { TypeName = "[ctt].[String256UnorderedSet]", Value = catalogEntryTypeCodesDataTable },
        };

        string query = $@"
            SELECT * FROM [dbo].[ufn_GetCatalogEntryTypes]
            (
                @{catalogEntryTypeIdsParameter.ParameterName},
                @{catalogEntryTypeCodesParameter.ParameterName}
            )";

        IQueryable<CatalogEntryType> catalogEntryTypesQueryable = Context.CatalogEntryTypes.FromSqlRaw(query, parameters);
        catalogEntryTypesQueryable = catalogEntryTypesQueryable.AsNoTracking();

        CatalogEntryType[] catalogEntryTypes = await catalogEntryTypesQueryable.ToArrayAsync(cancellationToken);

        return catalogEntryTypes;
    }
}
