using System;

using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding stored procedures for selecting sequential <see cref="Guid" /> values.
/// </summary>
public partial class SequentialGuidProviderMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql(@"
            CREATE PROCEDURE [utils].[sp_SelectSequentialGuids] (@Count INT)
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @SequentialGuids AS TABLE
                (
                    [Value] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWSEQUENTIALID(),
                    [Order] INT NOT NULL
                );

                WITH [CounterCte] AS
                (
                    SELECT 0 AS [Current]
                    UNION ALL
                    SELECT 1 + [counterCte].[Current]
                    FROM [CounterCte] AS [counterCte]
                    WHERE [Current] < @Count
                )
                INSERT INTO @SequentialGuids ([Order])
                SELECT [counterCte].[Current]
                FROM [CounterCte] AS [counterCte]
                WHERE [counterCte].[Current] < @Count
                OPTION (MAXRECURSION 4096);

                SELECT [sequentialGuid].[Value]
                FROM @SequentialGuids AS [sequentialGuid]
                ORDER BY [sequentialGuid].[Order];
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [utils].[sp_SelectSequentialGuids];");
    }
}
