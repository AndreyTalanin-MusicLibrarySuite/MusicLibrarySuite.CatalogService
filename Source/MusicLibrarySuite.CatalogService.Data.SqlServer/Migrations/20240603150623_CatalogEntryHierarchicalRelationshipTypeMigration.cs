using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationshipType" /> entity.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipTypeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationshipType",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                OwnerCatalogEntryTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                TargetCatalogEntryTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Code = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Annotated = table.Column<bool>(type: "bit", nullable: false),
                OwnerUnique = table.Column<bool>(type: "bit", nullable: false),
                TargetUnique = table.Column<bool>(type: "bit", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationshipType", columns: x => x.Id);
                table.UniqueConstraint(name: "AK_CatalogEntryHierarchicalRelationshipType_Code", columns: x => x.Code);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId",
                    column: x => x.OwnerCatalogEntryTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId",
                    column: x => x.TargetCatalogEntryTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Code_NotEmpty", sql: "LEN(TRIM([Code])) > 0");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryHierarchicalRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryHierarchicalRelationshipType_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryHierarchicalRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryHierarchicalRelationshipType_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryHierarchicalRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryHierarchicalRelationshipType_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationshipType_OwnerCatalogEntryTypeId",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationshipType",
            column: "OwnerCatalogEntryTypeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationshipType_TargetCatalogEntryTypeId",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationshipType",
            column: "TargetCatalogEntryTypeId");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryHierarchicalRelationshipType_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryHierarchicalRelationshipType]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryHierarchicalRelationshipType]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                INNER JOIN [inserted] AS [updatedCatalogEntryHierarchicalRelationshipType]
                    ON [updatedCatalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationshipType].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryHierarchicalRelationshipType] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [OwnerCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
                [TargetCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Code] NVARCHAR(256) NOT NULL,
                [Annotated] BIT NOT NULL,
                [OwnerUnique] BIT NOT NULL,
                [TargetUnique] BIT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipType]
            (
                @CatalogEntryHierarchicalRelationshipTypeId UNIQUEIDENTIFIER,
                @CatalogEntryHierarchicalRelationshipTypeCode NVARCHAR(256)
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalogEntryHierarchicalRelationshipType].*
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                WHERE (@CatalogEntryHierarchicalRelationshipTypeId IS NULL OR [catalogEntryHierarchicalRelationshipType].[Id] = @CatalogEntryHierarchicalRelationshipTypeId)
                    AND (@CatalogEntryHierarchicalRelationshipTypeCode IS NULL OR [catalogEntryHierarchicalRelationshipType].[Code] = @CatalogEntryHierarchicalRelationshipTypeCode)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypes]
            (
                @CatalogEntryHierarchicalRelationshipTypeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryHierarchicalRelationshipTypeCodes [ctt].[String256UnorderedSet] READONLY
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalogEntryHierarchicalRelationshipType].*
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                LEFT JOIN @CatalogEntryHierarchicalRelationshipTypeIds AS [catalogEntryHierarchicalRelationshipTypeId]
                    ON [catalogEntryHierarchicalRelationshipTypeId].[Value] = [catalogEntryHierarchicalRelationshipType].[Id]
                LEFT JOIN @CatalogEntryHierarchicalRelationshipTypeCodes AS [catalogEntryHierarchicalRelationshipTypeCode]
                    ON [catalogEntryHierarchicalRelationshipTypeCode].[Value] = [catalogEntryHierarchicalRelationshipType].[Code]
                WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryHierarchicalRelationshipTypeIds) OR [catalogEntryHierarchicalRelationshipTypeId].[Value] IS NOT NULL)
                    AND (NOT EXISTS(SELECT * FROM @CatalogEntryHierarchicalRelationshipTypeCodes) OR [catalogEntryHierarchicalRelationshipTypeCode].[Value] IS NOT NULL)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypeIdByCode] (@CatalogEntryHierarchicalRelationshipTypeCode NVARCHAR(256))
            RETURNS UNIQUEIDENTIFIER
            AS
            BEGIN
                DECLARE @CatalogEntryHierarchicalRelationshipTypeId AS UNIQUEIDENTIFIER;

                SELECT TOP (1) @CatalogEntryHierarchicalRelationshipTypeId = [catalogEntryHierarchicalRelationshipType].[Id]
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                WHERE [catalogEntryHierarchicalRelationshipType].[Code] = @CatalogEntryHierarchicalRelationshipTypeCode;

                RETURN @CatalogEntryHierarchicalRelationshipTypeId;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
            (
                @UpdateMode BIT,
                @CatalogEntryHierarchicalRelationshipTypes [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[CatalogEntryHierarchicalRelationshipType] AS [target]
                USING @CatalogEntryHierarchicalRelationshipTypes AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Code] = [source].[Code],
                    [target].[Annotated] = [source].[Annotated],
                    [target].[OwnerUnique] = [source].[OwnerUnique],
                    [target].[TargetUnique] = [source].[TargetUnique],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [Name],
                    [Code],
                    [Annotated],
                    [OwnerUnique],
                    [TargetUnique],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[Name],
                    [source].[Code],
                    [source].[Annotated],
                    [source].[OwnerUnique],
                    [source].[TargetUnique],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntryHierarchicalRelationshipType]
            (
                @CatalogEntryHierarchicalRelationshipType [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryHierarchicalRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryHierarchicalRelationshipTypeIds ([Value])
                SELECT [catalogEntryHierarchicalRelationshipType].[Id]
                FROM @CatalogEntryHierarchicalRelationshipType AS [catalogEntryHierarchicalRelationshipType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryHierarchicalRelationshipType,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntryHierarchicalRelationshipType].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntryHierarchicalRelationshipType].[UpdatedOn]
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                INNER JOIN @CatalogEntryHierarchicalRelationshipTypeIds AS [catalogEntryHierarchicalRelationshipTypeId]
                    ON [catalogEntryHierarchicalRelationshipTypeId].[Value] = [catalogEntryHierarchicalRelationshipType].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryHierarchicalRelationshipType]
            (
                @CatalogEntryHierarchicalRelationshipType [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryHierarchicalRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryHierarchicalRelationshipTypeIds ([Value])
                SELECT [catalogEntryHierarchicalRelationshipType].[Id]
                FROM @CatalogEntryHierarchicalRelationshipType AS [catalogEntryHierarchicalRelationshipType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryHierarchicalRelationshipType,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryHierarchicalRelationshipType]
            (
                @Id UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[CatalogEntryHierarchicalRelationshipType]
                FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                WHERE [catalogEntryHierarchicalRelationshipType].[Id] = @Id;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipType];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypes];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypeIdByCode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntryHierarchicalRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryHierarchicalRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryHierarchicalRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryHierarchicalRelationshipType];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryHierarchicalRelationshipType];");
    }
}
