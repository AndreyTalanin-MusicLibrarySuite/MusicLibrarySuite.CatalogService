using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                DependentCatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogEntryRelationshipTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryRelationship", columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId",
                    column: x => x.DependentCatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId",
                    column: x => x.CatalogEntryRelationshipTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryRelationshipType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryRelationship_CatalogEntryIds_NotEqual", sql: "[CatalogEntryId] <> [DependentCatalogEntryId]");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryRelationship]
            ADD CONSTRAINT [DF_CatalogEntryRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryRelationship]
            ADD CONSTRAINT [DF_CatalogEntryRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_CatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_DependentCatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryRelationship",
            column: "DependentCatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_CatalogEntryRelationshipTypeId",
            schema: "dbo",
            table: "CatalogEntryRelationship",
            column: "CatalogEntryRelationshipTypeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryRelationship_CatalogEntryId_CatalogEntryRelationshipTypeId_Order",
            schema: "dbo",
            table: "CatalogEntryRelationship",
            columns: ["CatalogEntryId", "CatalogEntryRelationshipTypeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryRelationship_DependentCatalogEntryId_CatalogEntryRelationshipTypeId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogEntryRelationship",
            columns: ["DependentCatalogEntryId", "CatalogEntryRelationshipTypeId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
                INNER JOIN [inserted] AS [updatedCatalogEntryRelationship]
                    ON [updatedCatalogEntryRelationship].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryRelationship].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
                    AND [updatedCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryRelationshipOwnerUniqueView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryRelationship].[CatalogEntryId],
                [catalogEntryRelationship].[DependentCatalogEntryId],
                [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
            INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            WHERE [catalogEntryRelationshipType].[OwnerUnique] = 1;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipOwnerUniqueView_MultipleColumns]
            ON [dbo].[CatalogEntryRelationshipOwnerUniqueView]
            (
                [CatalogEntryId],
                [CatalogEntryRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryRelationshipTargetUniqueView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryRelationship].[CatalogEntryId],
                [catalogEntryRelationship].[DependentCatalogEntryId],
                [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
            INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            WHERE [catalogEntryRelationshipType].[TargetUnique] = 1;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipTargetUniqueView_MultipleColumns]
            ON [dbo].[CatalogEntryRelationshipTargetUniqueView]
            (
                [DependentCatalogEntryId],
                [CatalogEntryRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryRelationship].[CatalogEntryId],
                [catalogEntryRelationship].[DependentCatalogEntryId],
                [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
            INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntry] AS [ownerCatalogEntry]
                ON [ownerCatalogEntry].[Id] = [catalogEntryRelationship].[CatalogEntryId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryRelationshipType].[OwnerCatalogEntryTypeId] <> [ownerCatalogEntry].[CatalogEntryTypeId];");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns]
            ON [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView]
            (
                [CatalogEntryId],
                [DependentCatalogEntryId],
                [CatalogEntryRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryRelationship].[CatalogEntryId],
                [catalogEntryRelationship].[DependentCatalogEntryId],
                [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
            INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntry] AS [targetCatalogEntry]
                ON [targetCatalogEntry].[Id] = [catalogEntryRelationship].[DependentCatalogEntryId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryRelationshipType].[TargetCatalogEntryTypeId] <> [targetCatalogEntry].[CatalogEntryTypeId];");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns]
            ON [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView]
            (
                [CatalogEntryId],
                [DependentCatalogEntryId],
                [CatalogEntryRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryRelationship] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryRelationships_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogEntryRelationship] AS
                (
                    SELECT
                        [sourceCatalogEntryRelationship].[CatalogEntryId],
                        [sourceCatalogEntryRelationship].[DependentCatalogEntryId],
                        [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                        [sourceCatalogEntryRelationship].[Order],
                        COALESCE([targetCatalogEntryRelationship].[ReferenceOrder],
                            MAX([referenceCatalogEntryRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogEntryRelationship].[Enabled]
                    FROM @CatalogEntryRelationships AS [sourceCatalogEntryRelationship]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryRelationship].[CatalogEntryId]
                    LEFT JOIN [dbo].[CatalogEntryRelationship] AS [targetCatalogEntryRelationship]
                        ON [targetCatalogEntryRelationship].[CatalogEntryId] = [sourceCatalogEntryRelationship].[CatalogEntryId]
                        AND [targetCatalogEntryRelationship].[DependentCatalogEntryId] = [sourceCatalogEntryRelationship].[DependentCatalogEntryId]
                        AND [targetCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                    LEFT JOIN [dbo].[CatalogEntryRelationship] AS [referenceCatalogEntryRelationship]
                        ON [targetCatalogEntryRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogEntryRelationship].[DependentCatalogEntryId] = [sourceCatalogEntryRelationship].[DependentCatalogEntryId]
                        AND [referenceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                    GROUP BY
                        [sourceCatalogEntryRelationship].[CatalogEntryId],
                        [sourceCatalogEntryRelationship].[DependentCatalogEntryId],
                        [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                        [sourceCatalogEntryRelationship].[Order],
                        [targetCatalogEntryRelationship].[ReferenceOrder],
                        [sourceCatalogEntryRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogEntryRelationship] AS [target]
                USING [SourceCatalogEntryRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
                    AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [DependentCatalogEntryId],
                    [CatalogEntryRelationshipTypeId],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[DependentCatalogEntryId],
                    [source].[CatalogEntryRelationshipTypeId],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;

                WITH [UpdatedCatalogEntryRelationship] AS
                (
                    SELECT
                        [catalogEntryRelationship].[CatalogEntryId],
                        [catalogEntryRelationship].[DependentCatalogEntryId],
                        [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogEntryRelationship].[DependentCatalogEntryId], [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                            ORDER BY [catalogEntryRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
                )
                UPDATE [catalogEntryRelationship]
                SET [ReferenceOrder] = [updatedCatalogEntryRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
                INNER JOIN [UpdatedCatalogEntryRelationship] AS [updatedCatalogEntryRelationship]
                    ON [updatedCatalogEntryRelationship].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryRelationship].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
                    AND [updatedCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                    ON [catalogEntryId].[Value] = [updatedCatalogEntryRelationship].[CatalogEntryId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryRelationships]
            (
                @CatalogEntryId UNIQUEIDENTIFIER,
                @DependentCatalogEntryId UNIQUEIDENTIFIER,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogEntryRelationship] AS
                (
                    SELECT [catalogEntryRelationship].*
                    FROM @CatalogEntryRelationships AS [catalogEntryRelationship]
                    WHERE (@ByTarget = 0 AND [catalogEntryRelationship].[CatalogEntryId] = @CatalogEntryId)
                        OR (@ByTarget = 1 AND [catalogEntryRelationship].[DependentCatalogEntryId] = @DependentCatalogEntryId)
                )
                MERGE INTO [dbo].[CatalogEntryRelationship] AS [target]
                USING [SourceCatalogEntryRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
                    AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryRelationships_Internal];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryRelationshipOwnerUniqueView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryRelationshipTargetUniqueView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");
    }
}
