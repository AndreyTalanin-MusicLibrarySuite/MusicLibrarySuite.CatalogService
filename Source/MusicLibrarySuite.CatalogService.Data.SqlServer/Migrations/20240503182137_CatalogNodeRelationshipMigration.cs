using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogNodeRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                DependentCatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeRelationship", columns: x => new { x.CatalogNodeId, x.DependentCatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId",
                    column: x => x.DependentCatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_CatalogNodeIds_NotEqual", sql: "[CatalogNodeId] <> [DependentCatalogNodeId]");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeRelationship]
            ADD CONSTRAINT [DF_CatalogNodeRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeRelationship]
            ADD CONSTRAINT [DF_CatalogNodeRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeRelationship_CatalogNodeId",
            schema: "dbo",
            table: "CatalogNodeRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeRelationship_DependentCatalogNodeId",
            schema: "dbo",
            table: "CatalogNodeRelationship",
            column: "DependentCatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeRelationship_CatalogNodeId_Order",
            schema: "dbo",
            table: "CatalogNodeRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeRelationship_DependentCatalogNodeId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogNodeRelationship",
            columns: ["DependentCatalogNodeId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogNodeRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogNodeRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogNodeRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogNodeRelationship] AS [catalogNodeRelationship]
                INNER JOIN [inserted] AS [updatedCatalogNodeRelationship]
                    ON [updatedCatalogNodeRelationship].[CatalogNodeId] = [catalogNodeRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeRelationship].[DependentCatalogNodeId] = [catalogNodeRelationship].[DependentCatalogNodeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogNodeRelationship] AS TABLE
            (
                [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [DependentCatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeRelationships_Internal]
            (
                @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogNodeRelationship] AS
                (
                    SELECT
                        [sourceCatalogNodeRelationship].[CatalogNodeId],
                        [sourceCatalogNodeRelationship].[DependentCatalogNodeId],
                        [sourceCatalogNodeRelationship].[Name],
                        [sourceCatalogNodeRelationship].[Description],
                        [sourceCatalogNodeRelationship].[Order],
                        COALESCE([targetCatalogNodeRelationship].[ReferenceOrder],
                            MAX([referenceCatalogNodeRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogNodeRelationship].[Enabled]
                    FROM @CatalogNodeRelationships AS [sourceCatalogNodeRelationship]
                    INNER JOIN @CatalogNodeIds AS [catalogNodeId]
                        ON [catalogNodeId].[Value] = [sourceCatalogNodeRelationship].[CatalogNodeId]
                    LEFT JOIN [dbo].[CatalogNodeRelationship] AS [targetCatalogNodeRelationship]
                        ON [targetCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogNodeRelationship].[CatalogNodeId]
                        AND [targetCatalogNodeRelationship].[DependentCatalogNodeId] = [sourceCatalogNodeRelationship].[DependentCatalogNodeId]
                    LEFT JOIN [dbo].[CatalogNodeRelationship] AS [referenceCatalogNodeRelationship]
                        ON [targetCatalogNodeRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogNodeRelationship].[DependentCatalogNodeId] = [sourceCatalogNodeRelationship].[DependentCatalogNodeId]
                    GROUP BY
                        [sourceCatalogNodeRelationship].[CatalogNodeId],
                        [sourceCatalogNodeRelationship].[DependentCatalogNodeId],
                        [sourceCatalogNodeRelationship].[Name],
                        [sourceCatalogNodeRelationship].[Description],
                        [sourceCatalogNodeRelationship].[Order],
                        [targetCatalogNodeRelationship].[ReferenceOrder],
                        [sourceCatalogNodeRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogNodeRelationship] AS [target]
                USING [SourceCatalogNodeRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[DependentCatalogNodeId] = [source].[DependentCatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description],
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogNodeId],
                    [DependentCatalogNodeId],
                    [Name],
                    [Description],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogNodeId],
                    [source].[DependentCatalogNodeId],
                    [source].[Name],
                    [source].[Description],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
                    THEN DELETE;

                WITH [UpdatedCatalogNodeRelationship] AS
                (
                    SELECT
                        [catalogNodeRelationship].[CatalogNodeId],
                        [catalogNodeRelationship].[DependentCatalogNodeId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogNodeRelationship].[DependentCatalogNodeId]
                            ORDER BY [catalogNodeRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogNodeRelationship] AS [catalogNodeRelationship]
                )
                UPDATE [catalogNodeRelationship]
                SET [ReferenceOrder] = [updatedCatalogNodeRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogNodeRelationship] AS [catalogNodeRelationship]
                INNER JOIN [UpdatedCatalogNodeRelationship] AS [updatedCatalogNodeRelationship]
                    ON [updatedCatalogNodeRelationship].[CatalogNodeId] = [catalogNodeRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeRelationship].[DependentCatalogNodeId] = [catalogNodeRelationship].[DependentCatalogNodeId]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId]
                    ON [catalogNodeId].[Value] = [updatedCatalogNodeRelationship].[CatalogNodeId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeRelationships]
            (
                @CatalogNodeId UNIQUEIDENTIFIER,
                @DependentCatalogNodeId UNIQUEIDENTIFIER,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogNodeRelationship] AS
                (
                    SELECT [catalogNodeRelationship].*
                    FROM @CatalogNodeRelationships AS [catalogNodeRelationship]
                    WHERE (@ByTarget = 0 AND [catalogNodeRelationship].[CatalogNodeId] = @CatalogNodeId)
                        OR (@ByTarget = 1 AND [catalogNodeRelationship].[DependentCatalogNodeId] = @DependentCatalogNodeId)
                )
                MERGE INTO [dbo].[CatalogNodeRelationship] AS [target]
                USING [SourceCatalogNodeRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[DependentCatalogNodeId] = [source].[DependentCatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogNodeRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogNodeRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogNodeRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");
    }
}
