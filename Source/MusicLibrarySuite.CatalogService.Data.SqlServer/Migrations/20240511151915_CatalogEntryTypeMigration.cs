using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryType" /> entity.
/// </summary>
public partial class CatalogEntryTypeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryType",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Code = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryType", columns: x => x.Id);
                table.UniqueConstraint(name: "AK_CatalogEntryType_Code", columns: x => x.Code);
                table.CheckConstraint(name: "CK_CatalogEntryType_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryType_Code_NotEmpty", sql: "LEN(TRIM([Code])) > 0");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryType]
            ADD CONSTRAINT [DF_CatalogEntryType_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryType]
            ADD CONSTRAINT [DF_CatalogEntryType_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryType]
            ADD CONSTRAINT [DF_CatalogEntryType_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryType_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryType]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryType]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                INNER JOIN [inserted] AS [updatedCatalogEntryType] ON [updatedCatalogEntryType].[Id] = [catalogEntryType].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryType] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Code] NVARCHAR(256) NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryType]
            (
                @CatalogEntryTypeId UNIQUEIDENTIFIER,
                @CatalogEntryTypeCode NVARCHAR(256)
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalogEntryType].*
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                WHERE (@CatalogEntryTypeId IS NULL OR [catalogEntryType].[Id] = @CatalogEntryTypeId)
                    AND (@CatalogEntryTypeCode IS NULL OR [catalogEntryType].[Code] = @CatalogEntryTypeCode)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryTypes]
            (
                @CatalogEntryTypeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryTypeCodes [ctt].[String256UnorderedSet] READONLY
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalogEntryType].*
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                LEFT JOIN @CatalogEntryTypeIds AS [catalogEntryTypeId] ON [catalogEntryTypeId].[Value] = [catalogEntryType].[Id]
                LEFT JOIN @CatalogEntryTypeCodes AS [catalogEntryTypeCode] ON [catalogEntryTypeCode].[Value] = [catalogEntryType].[Code]
                WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryTypeIds) OR [catalogEntryTypeId].[Value] IS NOT NULL)
                    AND (NOT EXISTS(SELECT * FROM @CatalogEntryTypeCodes) OR [catalogEntryTypeCode].[Value] IS NOT NULL)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryTypeIdByCode] (@CatalogEntryTypeCode NVARCHAR(256))
            RETURNS UNIQUEIDENTIFIER
            AS
            BEGIN
                DECLARE @CatalogEntryTypeId AS UNIQUEIDENTIFIER;

                SELECT TOP (1) @CatalogEntryTypeId = [catalogEntryType].[Id]
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                WHERE [catalogEntryType].[Code] = @CatalogEntryTypeCode;

                RETURN @CatalogEntryTypeId;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
            (
                @UpdateMode BIT,
                @CatalogEntryTypes [dbo].[CatalogEntryType] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[CatalogEntryType] AS [target]
                USING @CatalogEntryTypes AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Code] = [source].[Code],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [Name],
                    [Code],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[Name],
                    [source].[Code],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntryType]
            (
                @CatalogEntryType [dbo].[CatalogEntryType] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryTypeIds ([Value]) SELECT [catalogEntryType].[Id] FROM @CatalogEntryType AS [catalogEntryType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryType,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntryType].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntryType].[UpdatedOn]
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                INNER JOIN @CatalogEntryTypeIds AS [catalogEntryTypeId] ON [catalogEntryTypeId].[Value] = [catalogEntryType].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryType]
            (
                @CatalogEntryType [dbo].[CatalogEntryType] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryTypeIds ([Value]) SELECT [catalogEntryType].[Id] FROM @CatalogEntryType AS [catalogEntryType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryType,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryType]
            (
                @Id UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[CatalogEntryType]
                FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
                WHERE [catalogEntryType].[Id] = @Id;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryType];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryTypes];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryTypeIdByCode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntryType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryType];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryType];");
    }
}
