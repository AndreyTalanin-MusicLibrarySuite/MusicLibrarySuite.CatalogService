using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> relationship.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipAnnotationMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationshipAnnotation",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                ParentCatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogEntryHierarchicalRelationshipTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationshipAnnotation", columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns",
                    columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId },
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryHierarchicalRelationship",
                    principalColumns: ["CatalogEntryId", "ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId"],
                    onDelete: ReferentialAction.Cascade);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
            });

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryHierarchicalRelationshipAnnotation_AfterUpdate_SetCatalogEntryHierarchicalRelationshipUpdatedOn]
            ON [dbo].[CatalogEntryHierarchicalRelationshipAnnotation]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryHierarchicalRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
                INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [catalogEntryHierarchicalRelationshipAnnotation]
                    ON [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                INNER JOIN [inserted] AS [updatedCatalogEntryHierarchicalRelationshipAnnotation]
                    ON [updatedCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [catalogEntryHierarchicalRelationshipAnnotation]
                ON [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryHierarchicalRelationshipType].[Annotated] = 0;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidAnnotationView_MultipleColumns]
            ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView]
            (
                [CatalogEntryId],
                [ParentCatalogEntryId],
                [CatalogEntryHierarchicalRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY
            )
            AS
            BEGIN
                SET NOCOUNT ON;

                WITH [SourceCatalogEntryHierarchicalRelationshipAnnotation] AS
                (
                    SELECT
                        [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId],
                        [sourceCatalogEntryHierarchicalRelationshipAnnotation].[Name],
                        [sourceCatalogEntryHierarchicalRelationshipAnnotation].[Description]
                    FROM @CatalogEntryHierarchicalRelationshipAnnotations AS [sourceCatalogEntryHierarchicalRelationshipAnnotation]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId]
                )
                MERGE INTO [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [target]
                USING [SourceCatalogEntryHierarchicalRelationshipAnnotation] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
                    AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [ParentCatalogEntryId],
                    [CatalogEntryHierarchicalRelationshipTypeId],
                    [Name],
                    [Description]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[ParentCatalogEntryId],
                    [source].[CatalogEntryHierarchicalRelationshipTypeId],
                    [source].[Name],
                    [source].[Description]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
            AS
            BEGIN
                DECLARE @CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView AS TABLE
                (
                    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                    [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                    [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                    [DuplicationFactor] NCHAR(1) NOT NULL,
                    INDEX [UIX_CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView_MultipleColumns] UNIQUE CLUSTERED
                        ([CatalogEntryId], [ParentCatalogEntryId], [CatalogEntryHierarchicalRelationshipTypeId])
                );

                INSERT INTO @CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView
                (
                    [CatalogEntryId],
                    [ParentCatalogEntryId],
                    [CatalogEntryHierarchicalRelationshipTypeId],
                    [DuplicationFactor]
                )
                SELECT
                    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                    [viewDuplicationSource].[DuplicationFactor]
                FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
                LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [catalogEntryHierarchicalRelationshipAnnotation]
                    ON [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                INNER JOIN [dbo].[CatalogEntry] AS [catalogEntry]
                    ON [catalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
                WHERE [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] IS NULL
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] IS NULL
                    AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] IS NULL
                    AND [catalogEntryHierarchicalRelationshipType].[Annotated] = 1
                    AND [catalogEntry].[Id] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId]);
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryHierarchicalRelationshipAnnotation];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryHierarchicalRelationshipAnnotation];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }
}
