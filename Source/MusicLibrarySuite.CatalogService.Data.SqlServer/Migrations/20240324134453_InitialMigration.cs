using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific initial empty database migration.
/// </summary>
/// <remarks>
/// By the time this migration is installed, there will be multiple schemas already:
/// <list type="bullet">
/// <item><c>dbo</c>, the default schema</item>
/// <item><c>sys</c>, the schema containing Microsoft SQL Server metadata</item>
/// <item><c>efc</c>, the schema containing Entity Framework Core metadata</item>
/// </list>
/// These schemas should not be neither created, nor dropped manually.
/// </remarks>
public partial class InitialMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
    }
}
