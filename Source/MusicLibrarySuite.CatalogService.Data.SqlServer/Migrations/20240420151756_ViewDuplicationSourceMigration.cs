using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <c>utils.ViewDuplicationSource</c> table.
/// </summary>
public partial class ViewDuplicationSourceMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "ViewDuplicationSource",
            schema: "utils",
            columns: table => new
            {
                DuplicationFactor = table.Column<string>(type: "nchar(1)", maxLength: 1, nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_ViewDuplicationSource", columns: x => x.DuplicationFactor);
                table.CheckConstraint(name: "CK_ViewDuplicationSource_DuplicationFactor_NotEmpty", sql: "LEN(TRIM([DuplicationFactor])) > 0");
            });

        migrationBuilder.Sql("INSERT INTO [utils].[ViewDuplicationSource] ([DuplicationFactor]) VALUES (N'A'), (N'B');");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "ViewDuplicationSource", schema: "utils");
    }
}
