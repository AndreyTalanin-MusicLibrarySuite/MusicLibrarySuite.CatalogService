using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryToCatalogNodeRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryToCatalogNodeRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryToCatalogNodeRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryToCatalogNodeRelationship", columns: x => new { x.CatalogEntryId, x.CatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryToCatalogNodeRelationship]
            ADD CONSTRAINT [DF_CatalogEntryToCatalogNodeRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryToCatalogNodeRelationship]
            ADD CONSTRAINT [DF_CatalogEntryToCatalogNodeRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryToCatalogNodeRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId",
            schema: "dbo",
            table: "CatalogEntryToCatalogNodeRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId_Order",
            schema: "dbo",
            table: "CatalogEntryToCatalogNodeRelationship",
            columns: ["CatalogEntryId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogEntryToCatalogNodeRelationship",
            columns: ["CatalogNodeId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryToCatalogNodeRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryToCatalogNodeRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryToCatalogNodeRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryToCatalogNodeRelationship] AS [catalogEntryToCatalogNodeRelationship]
                INNER JOIN [inserted] AS [updatedCatalogEntryToCatalogNodeRelationship]
                    ON [updatedCatalogEntryToCatalogNodeRelationship].[CatalogEntryId] = [catalogEntryToCatalogNodeRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [catalogEntryToCatalogNodeRelationship].[CatalogNodeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryToCatalogNodeRelationship] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogEntryToCatalogNodeRelationship] AS
                (
                    SELECT
                        [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId],
                        [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId],
                        [sourceCatalogEntryToCatalogNodeRelationship].[Order],
                        COALESCE([targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder],
                            MAX([referenceCatalogEntryToCatalogNodeRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogEntryToCatalogNodeRelationship].[Enabled]
                    FROM @CatalogEntryToCatalogNodeRelationships AS [sourceCatalogEntryToCatalogNodeRelationship]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId]
                    LEFT JOIN [dbo].[CatalogEntryToCatalogNodeRelationship] AS [targetCatalogEntryToCatalogNodeRelationship]
                        ON [targetCatalogEntryToCatalogNodeRelationship].[CatalogEntryId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId]
                        AND [targetCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId]
                    LEFT JOIN [dbo].[CatalogEntryToCatalogNodeRelationship] AS [referenceCatalogEntryToCatalogNodeRelationship]
                        ON [targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId]
                    GROUP BY
                        [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId],
                        [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId],
                        [sourceCatalogEntryToCatalogNodeRelationship].[Order],
                        [targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder],
                        [sourceCatalogEntryToCatalogNodeRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogEntryToCatalogNodeRelationship] AS [target]
                USING [SourceCatalogEntryToCatalogNodeRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[CatalogNodeId] = [source].[CatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [CatalogNodeId],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[CatalogNodeId],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;

                WITH [UpdatedCatalogEntryToCatalogNodeRelationship] AS
                (
                    SELECT
                        [catalogEntryToCatalogNodeRelationship].[CatalogEntryId],
                        [catalogEntryToCatalogNodeRelationship].[CatalogNodeId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogEntryToCatalogNodeRelationship].[CatalogNodeId]
                            ORDER BY [catalogEntryToCatalogNodeRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogEntryToCatalogNodeRelationship] AS [catalogEntryToCatalogNodeRelationship]
                )
                UPDATE [catalogEntryToCatalogNodeRelationship]
                SET [ReferenceOrder] = [updatedCatalogEntryToCatalogNodeRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogEntryToCatalogNodeRelationship] AS [catalogEntryToCatalogNodeRelationship]
                INNER JOIN [UpdatedCatalogEntryToCatalogNodeRelationship] AS [updatedCatalogEntryToCatalogNodeRelationship]
                    ON [updatedCatalogEntryToCatalogNodeRelationship].[CatalogEntryId] = [catalogEntryToCatalogNodeRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [catalogEntryToCatalogNodeRelationship].[CatalogNodeId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogNodeRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogNodeRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogNodeRelationships]
            (
                @CatalogEntryId UNIQUEIDENTIFIER,
                @CatalogNodeId UNIQUEIDENTIFIER,
                @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogEntryToCatalogNodeRelationship] AS
                (
                    SELECT [catalogEntryToCatalogNodeRelationship].*
                    FROM @CatalogEntryToCatalogNodeRelationships AS [catalogEntryToCatalogNodeRelationship]
                    WHERE (@ByTarget = 0 AND [catalogEntryToCatalogNodeRelationship].[CatalogEntryId] = @CatalogEntryId)
                        OR (@ByTarget = 1 AND [catalogEntryToCatalogNodeRelationship].[CatalogNodeId] = @CatalogNodeId)
                )
                MERGE INTO [dbo].[CatalogEntryToCatalogNodeRelationship] AS [target]
                USING [SourceCatalogEntryToCatalogNodeRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[CatalogNodeId] = [source].[CatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogNodeRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryToCatalogNodeRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryToCatalogNodeRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }
}
