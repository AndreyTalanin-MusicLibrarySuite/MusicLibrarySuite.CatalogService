using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryRelationshipAnnotation" /> extension.
/// </summary>
public partial class CatalogEntryRelationshipAnnotationMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryRelationshipAnnotation",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                DependentCatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogEntryRelationshipTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryRelationshipAnnotation", columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns",
                    columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId },
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryRelationship",
                    principalColumns: ["CatalogEntryId", "DependentCatalogEntryId", "CatalogEntryRelationshipTypeId"],
                    onDelete: ReferentialAction.Cascade);
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
            });

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryRelationshipAnnotation_AfterUpdate_SetCatalogEntryRelationshipUpdatedOn]
            ON [dbo].[CatalogEntryRelationshipAnnotation]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
                INNER JOIN [dbo].[CatalogEntryRelationshipAnnotation] AS [catalogEntryRelationshipAnnotation]
                    ON [catalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
                    AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
                    AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                INNER JOIN [inserted] AS [updatedCatalogEntryRelationshipAnnotation]
                    ON [updatedCatalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationshipAnnotation].[CatalogEntryId]
                    AND [updatedCatalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId]
                    AND [updatedCatalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidAnnotationView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryRelationship].[CatalogEntryId],
                [catalogEntryRelationship].[DependentCatalogEntryId],
                [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
            INNER JOIN [dbo].[CatalogEntryRelationshipAnnotation] AS [catalogEntryRelationshipAnnotation]
                ON [catalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
                AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
                AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryRelationshipType].[Annotated] = 0;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidAnnotationView_MultipleColumns]
            ON [dbo].[CatalogEntryRelationshipInvalidAnnotationView]
            (
                [CatalogEntryId],
                [DependentCatalogEntryId],
                [CatalogEntryRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryRelationshipAnnotation] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY
            )
            AS
            BEGIN
                SET NOCOUNT ON;

                WITH [SourceCatalogEntryRelationshipAnnotation] AS
                (
                    SELECT
                        [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryId],
                        [sourceCatalogEntryRelationshipAnnotation].[DependentCatalogEntryId],
                        [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId],
                        [sourceCatalogEntryRelationshipAnnotation].[Name],
                        [sourceCatalogEntryRelationshipAnnotation].[Description]
                    FROM @CatalogEntryRelationshipAnnotations AS [sourceCatalogEntryRelationshipAnnotation]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryId]
                )
                MERGE INTO [dbo].[CatalogEntryRelationshipAnnotation] AS [target]
                USING [SourceCatalogEntryRelationshipAnnotation] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
                    AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [DependentCatalogEntryId],
                    [CatalogEntryRelationshipTypeId],
                    [Name],
                    [Description]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[DependentCatalogEntryId],
                    [source].[CatalogEntryRelationshipTypeId],
                    [source].[Name],
                    [source].[Description]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
            AS
            BEGIN
                DECLARE @CatalogEntryRelationshipMissingAnnotationInMemoryView AS TABLE
                (
                    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                    [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                    [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                    [DuplicationFactor] NCHAR(1) NOT NULL,
                    INDEX [UIX_CatalogEntryRelationshipMissingAnnotationInMemoryView_MultipleColumns] UNIQUE CLUSTERED
                        ([CatalogEntryId], [DependentCatalogEntryId], [CatalogEntryRelationshipTypeId])
                );

                INSERT INTO @CatalogEntryRelationshipMissingAnnotationInMemoryView
                (
                    [CatalogEntryId],
                    [DependentCatalogEntryId],
                    [CatalogEntryRelationshipTypeId],
                    [DuplicationFactor]
                )
                SELECT
                    [catalogEntryRelationship].[CatalogEntryId],
                    [catalogEntryRelationship].[DependentCatalogEntryId],
                    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
                    [viewDuplicationSource].[DuplicationFactor]
                FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
                LEFT JOIN [dbo].[CatalogEntryRelationshipAnnotation] AS [catalogEntryRelationshipAnnotation]
                    ON [catalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
                    AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
                    AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                INNER JOIN [dbo].[CatalogEntry] AS [catalogEntry]
                    ON [catalogEntry].[Id] = [catalogEntryRelationship].[CatalogEntryId]
                CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
                WHERE [catalogEntryRelationshipAnnotation].[CatalogEntryId] IS NULL
                    AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] IS NULL
                    AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] IS NULL
                    AND [catalogEntryRelationshipType].[Annotated] = 1
                    AND [catalogEntry].[Id] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId]);
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryRelationshipInvalidAnnotationView];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryRelationshipAnnotation];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryRelationshipAnnotation];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                COMMIT TRANSACTION;
            END;");
    }
}
