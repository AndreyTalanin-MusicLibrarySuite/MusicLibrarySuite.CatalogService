using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding commonly used table types.
/// </summary>
/// <remarks>Also creates or drops <c>ctt</c> (Common Table Types) schema for these table types.</remarks>
public partial class CommonTableTypesMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        // CTT (Common Table Types) schema
        migrationBuilder.EnsureSchema("ctt");

        // 8-bit integer value - System.Byte - TINYINT
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[ByteArray] AS TABLE
            (
                [Value] TINYINT NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[ByteUnorderedSet] AS TABLE
            (
                [Value] TINYINT NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[ByteOrderedSet] AS TABLE
            (
                [Value] TINYINT NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");

        // 16-bit signed integer value - System.Int16 - SMALLINT
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int16Array] AS TABLE
            (
                [Value] SMALLINT NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int16UnorderedSet] AS TABLE
            (
                [Value] SMALLINT NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int16OrderedSet] AS TABLE
            (
                [Value] SMALLINT NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");

        // 32-bit signed integer value - System.Int32 - INT
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int32Array] AS TABLE
            (
                [Value] INT NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int32UnorderedSet] AS TABLE
            (
                [Value] INT NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int32OrderedSet] AS TABLE
            (
                [Value] INT NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");

        // 64-bit signed integer value - System.Int64 - BIGINT
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int64Array] AS TABLE
            (
                [Value] BIGINT NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int64UnorderedSet] AS TABLE
            (
                [Value] BIGINT NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[Int64OrderedSet] AS TABLE
            (
                [Value] BIGINT NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");

        // 128-bit non-numerical integer value (GUID/UUID) - System.Guid - UNIQUEIDENTIFIER
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[GuidArray] AS TABLE
            (
                [Value] UNIQUEIDENTIFIER NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[GuidUnorderedSet] AS TABLE
            (
                [Value] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[GuidOrderedSet] AS TABLE
            (
                [Value] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");

        // UTF-16 string value - System.String - NVARCHAR(MAX)
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[StringArray] AS TABLE
            (
                [Value] NVARCHAR(MAX) NOT NULL
            );");

        // UTF-16 string value of 256 byte pairs - System.String - NVARCHAR(256)
        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[String256Array] AS TABLE
            (
                [Value] NVARCHAR(256) NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[String256UnorderedSet] AS TABLE
            (
                [Value] NVARCHAR(256) NOT NULL PRIMARY KEY CLUSTERED
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [ctt].[String256OrderedSet] AS TABLE
            (
                [Value] NVARCHAR(256) NOT NULL PRIMARY KEY CLUSTERED,
                [Order] INT NOT NULL
            );");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        // 8-bit integer value - System.Byte - TINYINT
        migrationBuilder.Sql("DROP TYPE [ctt].[ByteArray];");

        migrationBuilder.Sql("DROP TYPE [ctt].[ByteUnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[ByteOrderedSet];");

        // 16-bit signed integer value - System.Int16 - SMALLINT
        migrationBuilder.Sql("DROP TYPE [ctt].[Int16Array];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int16UnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int16OrderedSet];");

        // 32-bit signed integer value - System.Int32 - INT
        migrationBuilder.Sql("DROP TYPE [ctt].[Int32Array];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int32UnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int32OrderedSet];");

        // 64-bit signed integer value - System.Int64 - BIGINT
        migrationBuilder.Sql("DROP TYPE [ctt].[Int64Array];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int64UnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[Int64OrderedSet];");

        // 128-bit non-numerical integer value (GUID/UUID) - System.Guid - UNIQUEIDENTIFIER
        migrationBuilder.Sql("DROP TYPE [ctt].[GuidArray];");

        migrationBuilder.Sql("DROP TYPE [ctt].[GuidUnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[GuidOrderedSet];");

        // UTF-16 string value - System.String - NVARCHAR(MAX)
        migrationBuilder.Sql("DROP TYPE [ctt].[StringArray];");

        // UTF-16 string value of 256 byte pairs - System.String - NVARCHAR(256)
        migrationBuilder.Sql("DROP TYPE [ctt].[String256Array];");

        migrationBuilder.Sql("DROP TYPE [ctt].[String256UnorderedSet];");

        migrationBuilder.Sql("DROP TYPE [ctt].[String256OrderedSet];");

        // CTT (Common Table Types) schema
        migrationBuilder.DropSchema("ctt");
    }
}
