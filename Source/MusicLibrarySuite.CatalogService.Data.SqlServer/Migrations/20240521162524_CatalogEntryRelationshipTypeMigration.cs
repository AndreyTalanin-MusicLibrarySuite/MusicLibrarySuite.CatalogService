using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryRelationshipType" /> entity.
/// </summary>
public partial class CatalogEntryRelationshipTypeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryRelationshipType",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                OwnerCatalogEntryTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                TargetCatalogEntryTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Code = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Annotated = table.Column<bool>(type: "bit", nullable: false),
                OwnerUnique = table.Column<bool>(type: "bit", nullable: false),
                TargetUnique = table.Column<bool>(type: "bit", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryRelationshipType", columns: x => x.Id);
                table.UniqueConstraint(name: "AK_CatalogEntryRelationshipType_Code", columns: x => x.Code);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId",
                    column: x => x.OwnerCatalogEntryTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId",
                    column: x => x.TargetCatalogEntryTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipType_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipType_Code_NotEmpty", sql: "LEN(TRIM([Code])) > 0");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryRelationshipType_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryRelationshipType_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryRelationshipType]
            ADD CONSTRAINT [DF_CatalogEntryRelationshipType_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationshipType_OwnerCatalogEntryTypeId",
            schema: "dbo",
            table: "CatalogEntryRelationshipType",
            column: "OwnerCatalogEntryTypeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationshipType_TargetCatalogEntryTypeId",
            schema: "dbo",
            table: "CatalogEntryRelationshipType",
            column: "TargetCatalogEntryTypeId");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryRelationshipType_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryRelationshipType]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryRelationshipType]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                INNER JOIN [inserted] AS [updatedCatalogEntryRelationshipType]
                    ON [updatedCatalogEntryRelationshipType].[Id] = [catalogEntryRelationshipType].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryRelationshipType] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [OwnerCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
                [TargetCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Code] NVARCHAR(256) NOT NULL,
                [Annotated] BIT NOT NULL,
                [OwnerUnique] BIT NOT NULL,
                [TargetUnique] BIT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipType]
            (
                @CatalogEntryRelationshipTypeId UNIQUEIDENTIFIER,
                @CatalogEntryRelationshipTypeCode NVARCHAR(256)
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalogEntryRelationshipType].*
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                WHERE (@CatalogEntryRelationshipTypeId IS NULL OR [catalogEntryRelationshipType].[Id] = @CatalogEntryRelationshipTypeId)
                    AND (@CatalogEntryRelationshipTypeCode IS NULL OR [catalogEntryRelationshipType].[Code] = @CatalogEntryRelationshipTypeCode)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypes]
            (
                @CatalogEntryRelationshipTypeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryRelationshipTypeCodes [ctt].[String256UnorderedSet] READONLY
            )
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalogEntryRelationshipType].*
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                LEFT JOIN @CatalogEntryRelationshipTypeIds AS [catalogEntryRelationshipTypeId]
                    ON [catalogEntryRelationshipTypeId].[Value] = [catalogEntryRelationshipType].[Id]
                LEFT JOIN @CatalogEntryRelationshipTypeCodes AS [catalogEntryRelationshipTypeCode]
                    ON [catalogEntryRelationshipTypeCode].[Value] = [catalogEntryRelationshipType].[Code]
                WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryRelationshipTypeIds) OR [catalogEntryRelationshipTypeId].[Value] IS NOT NULL)
                    AND (NOT EXISTS(SELECT * FROM @CatalogEntryRelationshipTypeCodes) OR [catalogEntryRelationshipTypeCode].[Value] IS NOT NULL)
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypeIdByCode] (@CatalogEntryRelationshipTypeCode NVARCHAR(256))
            RETURNS UNIQUEIDENTIFIER
            AS
            BEGIN
                DECLARE @CatalogEntryRelationshipTypeId AS UNIQUEIDENTIFIER;

                SELECT TOP (1) @CatalogEntryRelationshipTypeId = [catalogEntryRelationshipType].[Id]
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                WHERE [catalogEntryRelationshipType].[Code] = @CatalogEntryRelationshipTypeCode;

                RETURN @CatalogEntryRelationshipTypeId;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal]
            (
                @UpdateMode BIT,
                @CatalogEntryRelationshipTypes [dbo].[CatalogEntryRelationshipType] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[CatalogEntryRelationshipType] AS [target]
                USING @CatalogEntryRelationshipTypes AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Code] = [source].[Code],
                    [target].[Annotated] = [source].[Annotated],
                    [target].[OwnerUnique] = [source].[OwnerUnique],
                    [target].[TargetUnique] = [source].[TargetUnique],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [Name],
                    [Code],
                    [Annotated],
                    [OwnerUnique],
                    [TargetUnique],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[Name],
                    [source].[Code],
                    [source].[Annotated],
                    [source].[OwnerUnique],
                    [source].[TargetUnique],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntryRelationshipType]
            (
                @CatalogEntryRelationshipType [dbo].[CatalogEntryRelationshipType] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryRelationshipTypeIds ([Value])
                SELECT [catalogEntryRelationshipType].[Id]
                FROM @CatalogEntryRelationshipType AS [catalogEntryRelationshipType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryRelationshipType,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntryRelationshipType].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntryRelationshipType].[UpdatedOn]
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                INNER JOIN @CatalogEntryRelationshipTypeIds AS [catalogEntryRelationshipTypeId]
                    ON [catalogEntryRelationshipTypeId].[Value] = [catalogEntryRelationshipType].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryRelationshipType]
            (
                @CatalogEntryRelationshipType [dbo].[CatalogEntryRelationshipType] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryRelationshipTypeIds ([Value])
                SELECT [catalogEntryRelationshipType].[Id]
                FROM @CatalogEntryRelationshipType AS [catalogEntryRelationshipType];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal]
                    @UpdateMode,
                    @CatalogEntryRelationshipType,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryRelationshipType]
            (
                @Id UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[CatalogEntryRelationshipType]
                FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
                WHERE [catalogEntryRelationshipType].[Id] = @Id;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipType];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypes];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypeIdByCode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntryRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryRelationshipType];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryRelationshipType];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryRelationshipType];");
    }
}
