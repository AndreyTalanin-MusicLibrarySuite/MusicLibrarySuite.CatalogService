using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogNode" /> entity.
/// </summary>
public partial class CatalogNodeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNode",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true),
                SystemProtected = table.Column<bool>(type: "bit", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNode", columns: x => x.Id);
                table.CheckConstraint(name: "CK_CatalogNode_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogNode_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_CatalogNode_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNode]
            ADD CONSTRAINT [DF_CatalogNode_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNode]
            ADD CONSTRAINT [DF_CatalogNode_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNode]
            ADD CONSTRAINT [DF_CatalogNode_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogNode_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogNode]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogNode]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN [inserted] AS [updatedCatalogNode] ON [updatedCatalogNode].[Id] = [catalogNode].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogNode] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL,
                [SystemProtected] BIT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogNode] (@CatalogNodeId UNIQUEIDENTIFIER)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalogNode].*
                FROM [dbo].[CatalogNode] AS [catalogNode]
                WHERE [catalogNode].[Id] = @CatalogNodeId
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogNodes] (@CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalogNode].*
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
            (
                @UpdateMode BIT,
                @CatalogNodes [dbo].[CatalogNode] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[CatalogNode] AS [target]
                USING @CatalogNodes AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description],
                    [target].[SystemProtected] = [source].[SystemProtected],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [Name],
                    [Description],
                    [SystemProtected],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[Name],
                    [source].[Description],
                    [source].[SystemProtected],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalogNode]
            (
                @CatalogNodeId UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[CatalogNode]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                WHERE [catalogNode].[Id] = @CatalogNodeId;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogNode];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogNodes];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogNodes_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogNode];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogNode];");
    }
}
