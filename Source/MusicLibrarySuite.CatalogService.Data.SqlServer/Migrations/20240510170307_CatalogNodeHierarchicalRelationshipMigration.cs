using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogNodeHierarchicalRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeHierarchicalRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeHierarchicalRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                ParentCatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeHierarchicalRelationship", columns: x => new { x.CatalogNodeId, x.ParentCatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId",
                    column: x => x.ParentCatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogNodeHierarchicalRelationship_Order_EqualsZero", sql: "[Order] = 0");
                table.CheckConstraint(name: "CK_CatalogNodeHierarchicalRelationship_CatalogNodeIds_NotEqual", sql: "[CatalogNodeId] <> [ParentCatalogNodeId]");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeHierarchicalRelationship]
            ADD CONSTRAINT [DF_CatalogNodeHierarchicalRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeHierarchicalRelationship]
            ADD CONSTRAINT [DF_CatalogNodeHierarchicalRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeHierarchicalRelationship_CatalogNodeId",
            schema: "dbo",
            table: "CatalogNodeHierarchicalRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId",
            schema: "dbo",
            table: "CatalogNodeHierarchicalRelationship",
            column: "ParentCatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeHierarchicalRelationship_CatalogNodeId_Order",
            schema: "dbo",
            table: "CatalogNodeHierarchicalRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogNodeHierarchicalRelationship",
            columns: ["ParentCatalogNodeId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogNodeHierarchicalRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogNodeHierarchicalRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogNodeHierarchicalRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogNodeHierarchicalRelationship] AS [catalogNodeHierarchicalRelationship]
                INNER JOIN [inserted] AS [updatedCatalogNodeHierarchicalRelationship]
                    ON [updatedCatalogNodeHierarchicalRelationship].[CatalogNodeId] = [catalogNodeHierarchicalRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogNodeHierarchicalRelationship] AS TABLE
            (
                [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [ParentCatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
            (
                @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogNodeHierarchicalRelationship] AS
                (
                    SELECT
                        [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId],
                        [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
                        [sourceCatalogNodeHierarchicalRelationship].[Order],
                        COALESCE([targetCatalogNodeHierarchicalRelationship].[ReferenceOrder],
                            MAX([referenceCatalogNodeHierarchicalRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder]
                    FROM @CatalogNodeHierarchicalRelationships AS [sourceCatalogNodeHierarchicalRelationship]
                    INNER JOIN @CatalogNodeIds AS [catalogNodeId]
                        ON [catalogNodeId].[Value] = [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId]
                    LEFT JOIN [dbo].[CatalogNodeHierarchicalRelationship] AS [targetCatalogNodeHierarchicalRelationship]
                        ON [targetCatalogNodeHierarchicalRelationship].[CatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId]
                        AND [targetCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
                    LEFT JOIN [dbo].[CatalogNodeHierarchicalRelationship] AS [referenceCatalogNodeHierarchicalRelationship]
                        ON [targetCatalogNodeHierarchicalRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
                    GROUP BY
                        [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId],
                        [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
                        [sourceCatalogNodeHierarchicalRelationship].[Order],
                        [targetCatalogNodeHierarchicalRelationship].[ReferenceOrder]
                )
                MERGE INTO [dbo].[CatalogNodeHierarchicalRelationship] AS [target]
                USING [SourceCatalogNodeHierarchicalRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[ParentCatalogNodeId] = [source].[ParentCatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogNodeId],
                    [ParentCatalogNodeId],
                    [Order],
                    [ReferenceOrder]
                )
                VALUES
                (
                    [source].[CatalogNodeId],
                    [source].[ParentCatalogNodeId],
                    [source].[Order],
                    [source].[ReferenceOrder]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
                    THEN DELETE;

                WITH [UpdatedCatalogNodeHierarchicalRelationship] AS
                (
                    SELECT
                        [catalogNodeHierarchicalRelationship].[CatalogNodeId],
                        [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
                            ORDER BY [catalogNodeHierarchicalRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogNodeHierarchicalRelationship] AS [catalogNodeHierarchicalRelationship]
                )
                UPDATE [catalogNodeHierarchicalRelationship]
                SET [ReferenceOrder] = [updatedCatalogNodeHierarchicalRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogNodeHierarchicalRelationship] AS [catalogNodeHierarchicalRelationship]
                INNER JOIN [UpdatedCatalogNodeHierarchicalRelationship] AS [updatedCatalogNodeHierarchicalRelationship]
                    ON [updatedCatalogNodeHierarchicalRelationship].[CatalogNodeId] = [CatalogNodeHierarchicalRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [CatalogNodeHierarchicalRelationship].[ParentCatalogNodeId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeHierarchicalRelationships]
            (
                @CatalogNodeId UNIQUEIDENTIFIER,
                @ParentCatalogNodeId UNIQUEIDENTIFIER,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogNodeHierarchicalRelationship] AS
                (
                    SELECT [catalogNodeHierarchicalRelationship].*
                    FROM @CatalogNodeHierarchicalRelationships AS [catalogNodeHierarchicalRelationship]
                    WHERE (@ByTarget = 0 AND [catalogNodeHierarchicalRelationship].[CatalogNodeId] = @CatalogNodeId)
                        OR (@ByTarget = 1 AND [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = @ParentCatalogNodeId)
                )
                MERGE INTO [dbo].[CatalogNodeHierarchicalRelationship] AS [target]
                USING [SourceCatalogNodeHierarchicalRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[ParentCatalogNodeId] = [source].[ParentCatalogNodeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeHierarchicalRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogNodeHierarchicalRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogNodeHierarchicalRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;
            END;");
    }
}
