using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogNodeToCatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeToCatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeToCatalogRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeToCatalogRelationship", columns: x => new { x.CatalogNodeId, x.CatalogId });
                table.ForeignKey(
                    name: "FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalSchema: "dbo",
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeToCatalogRelationship]
            ADD CONSTRAINT [DF_CatalogNodeToCatalogRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogNodeToCatalogRelationship]
            ADD CONSTRAINT [DF_CatalogNodeToCatalogRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeToCatalogRelationship_CatalogNodeId",
            schema: "dbo",
            table: "CatalogNodeToCatalogRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeToCatalogRelationship_CatalogId",
            schema: "dbo",
            table: "CatalogNodeToCatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeToCatalogRelationship_CatalogNodeId_Order",
            schema: "dbo",
            table: "CatalogNodeToCatalogRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeToCatalogRelationship_CatalogId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogNodeToCatalogRelationship",
            columns: ["CatalogId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogNodeToCatalogRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogNodeToCatalogRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogNodeToCatalogRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogNodeToCatalogRelationship] AS [catalogNodeToCatalogRelationship]
                INNER JOIN [inserted] AS [updatedCatalogNodeToCatalogRelationship]
                    ON [updatedCatalogNodeToCatalogRelationship].[CatalogNodeId] = [catalogNodeToCatalogRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeToCatalogRelationship].[CatalogId] = [catalogNodeToCatalogRelationship].[CatalogId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogNodeToCatalogRelationship] AS TABLE
            (
                [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
            (
                @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogNodeToCatalogRelationship] AS
                (
                    SELECT
                        [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId],
                        [sourceCatalogNodeToCatalogRelationship].[CatalogId],
                        [sourceCatalogNodeToCatalogRelationship].[Order],
                        COALESCE([targetCatalogNodeToCatalogRelationship].[ReferenceOrder],
                            MAX([referenceCatalogNodeToCatalogRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogNodeToCatalogRelationship].[Enabled]
                    FROM @CatalogNodeToCatalogRelationships AS [sourceCatalogNodeToCatalogRelationship]
                    INNER JOIN @CatalogNodeIds AS [catalogNodeId]
                        ON [catalogNodeId].[Value] = [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId]
                    LEFT JOIN [dbo].[CatalogNodeToCatalogRelationship] AS [targetCatalogNodeToCatalogRelationship]
                        ON [targetCatalogNodeToCatalogRelationship].[CatalogNodeId] = [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId]
                        AND [targetCatalogNodeToCatalogRelationship].[CatalogId] = [sourceCatalogNodeToCatalogRelationship].[CatalogId]
                    LEFT JOIN [dbo].[CatalogNodeToCatalogRelationship] AS [referenceCatalogNodeToCatalogRelationship]
                        ON [targetCatalogNodeToCatalogRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogNodeToCatalogRelationship].[CatalogId] = [sourceCatalogNodeToCatalogRelationship].[CatalogId]
                    GROUP BY
                        [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId],
                        [sourceCatalogNodeToCatalogRelationship].[CatalogId],
                        [sourceCatalogNodeToCatalogRelationship].[Order],
                        [targetCatalogNodeToCatalogRelationship].[ReferenceOrder],
                        [sourceCatalogNodeToCatalogRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogNodeToCatalogRelationship] AS [target]
                USING [SourceCatalogNodeToCatalogRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[CatalogId] = [source].[CatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogNodeId],
                    [CatalogId],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogNodeId],
                    [source].[CatalogId],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
                    THEN DELETE;

                WITH [UpdatedCatalogNodeToCatalogRelationship] AS
                (
                    SELECT
                        [catalogNodeToCatalogRelationship].[CatalogNodeId],
                        [catalogNodeToCatalogRelationship].[CatalogId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogNodeToCatalogRelationship].[CatalogId]
                            ORDER BY [catalogNodeToCatalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogNodeToCatalogRelationship] AS [catalogNodeToCatalogRelationship]
                )
                UPDATE [catalogNodeToCatalogRelationship]
                SET [ReferenceOrder] = [updatedCatalogNodeToCatalogRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogNodeToCatalogRelationship] AS [catalogNodeToCatalogRelationship]
                INNER JOIN [UpdatedCatalogNodeToCatalogRelationship] AS [updatedCatalogNodeToCatalogRelationship]
                    ON [updatedCatalogNodeToCatalogRelationship].[CatalogNodeId] = [catalogNodeToCatalogRelationship].[CatalogNodeId]
                    AND [updatedCatalogNodeToCatalogRelationship].[CatalogId] = [catalogNodeToCatalogRelationship].[CatalogId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeToCatalogRelationships]
            (
                @CatalogNodeId UNIQUEIDENTIFIER,
                @CatalogId UNIQUEIDENTIFIER,
                @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogNodeToCatalogRelationship] AS
                (
                    SELECT [catalogNodeToCatalogRelationship].*
                    FROM @CatalogNodeToCatalogRelationships AS [catalogNodeToCatalogRelationship]
                    WHERE (@ByTarget = 0 AND [catalogNodeToCatalogRelationship].[CatalogNodeId] = @CatalogNodeId)
                        OR (@ByTarget = 1 AND [catalogNodeToCatalogRelationship].[CatalogId] = @CatalogId)
                )
                MERGE INTO [dbo].[CatalogNodeToCatalogRelationship] AS [target]
                USING [SourceCatalogNodeToCatalogRelationship] AS [source]
                ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
                    AND [target].[CatalogId] = [source].[CatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeToCatalogRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogNodeToCatalogRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogNodeToCatalogRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogNode].[CreatedOn],
                    @ResultUpdatedOn = [catalogNode].[UpdatedOn]
                FROM [dbo].[CatalogNode] AS [catalogNode]
                INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
            (
                @CatalogNode [dbo].[CatalogNode] READONLY,
                @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
                @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
                    @UpdateMode,
                    @CatalogNode,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
                    @CatalogNodeIds,
                    @CatalogNodeRelationships;

                COMMIT TRANSACTION;
            END;");
    }
}
