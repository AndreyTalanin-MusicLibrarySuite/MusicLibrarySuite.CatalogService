using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                DependentCatalogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogRelationship", columns: x => new { x.CatalogId, x.DependentCatalogId });
                table.ForeignKey(
                    name: "FK_CatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalSchema: "dbo",
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogRelationship_Catalog_DependentCatalogId",
                    column: x => x.DependentCatalogId,
                    principalSchema: "dbo",
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogRelationship_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_CatalogRelationship_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_CatalogRelationship_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
                table.CheckConstraint(name: "CK_CatalogRelationship_CatalogIds_NotEqual", sql: "[CatalogId] <> [DependentCatalogId]");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogRelationship]
            ADD CONSTRAINT [DF_CatalogRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogRelationship]
            ADD CONSTRAINT [DF_CatalogRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogRelationship_CatalogId",
            schema: "dbo",
            table: "CatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogRelationship_DependentCatalogId",
            schema: "dbo",
            table: "CatalogRelationship",
            column: "DependentCatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogRelationship_CatalogId_Order",
            schema: "dbo",
            table: "CatalogRelationship",
            columns: ["CatalogId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogRelationship",
            columns: ["DependentCatalogId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogRelationship] AS [catalogRelationship]
                INNER JOIN [inserted] AS [updatedCatalogRelationship]
                    ON [updatedCatalogRelationship].[CatalogId] = [catalogRelationship].[CatalogId]
                    AND [updatedCatalogRelationship].[DependentCatalogId] = [catalogRelationship].[DependentCatalogId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogRelationship] AS TABLE
            (
                [CatalogId] UNIQUEIDENTIFIER NOT NULL,
                [DependentCatalogId] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogRelationships_Internal]
            (
                @CatalogIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogRelationships [dbo].[CatalogRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogRelationship] AS
                (
                    SELECT
                        [sourceCatalogRelationship].[CatalogId],
                        [sourceCatalogRelationship].[DependentCatalogId],
                        [sourceCatalogRelationship].[Name],
                        [sourceCatalogRelationship].[Description],
                        [sourceCatalogRelationship].[Order],
                        COALESCE([targetCatalogRelationship].[ReferenceOrder],
                            MAX([referenceCatalogRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogRelationship].[Enabled]
                    FROM @CatalogRelationships AS [sourceCatalogRelationship]
                    INNER JOIN @CatalogIds AS [catalogId]
                        ON [catalogId].[Value] = [sourceCatalogRelationship].[CatalogId]
                    LEFT JOIN [dbo].[CatalogRelationship] AS [targetCatalogRelationship]
                        ON [targetCatalogRelationship].[CatalogId] = [sourceCatalogRelationship].[CatalogId]
                        AND [targetCatalogRelationship].[DependentCatalogId] = [sourceCatalogRelationship].[DependentCatalogId]
                    LEFT JOIN [dbo].[CatalogRelationship] AS [referenceCatalogRelationship]
                        ON [targetCatalogRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogRelationship].[DependentCatalogId] = [sourceCatalogRelationship].[DependentCatalogId]
                    GROUP BY
                        [sourceCatalogRelationship].[CatalogId],
                        [sourceCatalogRelationship].[DependentCatalogId],
                        [sourceCatalogRelationship].[Name],
                        [sourceCatalogRelationship].[Description],
                        [sourceCatalogRelationship].[Order],
                        [targetCatalogRelationship].[ReferenceOrder],
                        [sourceCatalogRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogRelationship] AS [target]
                USING [SourceCatalogRelationship] AS [source]
                ON [target].[CatalogId] = [source].[CatalogId]
                    AND [target].[DependentCatalogId] = [source].[DependentCatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description],
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogId],
                    [DependentCatalogId],
                    [Name],
                    [Description],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogId],
                    [source].[DependentCatalogId],
                    [source].[Name],
                    [source].[Description],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogId] IN (SELECT [catalogId].[Value] FROM @CatalogIds AS [catalogId])
                    THEN DELETE;

                WITH [UpdatedCatalogRelationship] AS
                (
                    SELECT
                        [catalogRelationship].[CatalogId],
                        [catalogRelationship].[DependentCatalogId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogRelationship].[DependentCatalogId]
                            ORDER BY [catalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogRelationship] AS [catalogRelationship]
                )
                UPDATE [catalogRelationship]
                SET [ReferenceOrder] = [updatedCatalogRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogRelationship] AS [catalogRelationship]
                INNER JOIN [UpdatedCatalogRelationship] AS [updatedCatalogRelationship]
                    ON [updatedCatalogRelationship].[CatalogId] = [catalogRelationship].[CatalogId]
                    AND [updatedCatalogRelationship].[DependentCatalogId] = [catalogRelationship].[DependentCatalogId]
                INNER JOIN @CatalogIds AS [catalogId]
                    ON [catalogId].[Value] = [updatedCatalogRelationship].[CatalogId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalog];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogRelationships_Internal]
                    @CatalogIds,
                    @CatalogRelationships;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalog].[CreatedOn],
                    @ResultUpdatedOn = [catalog].[UpdatedOn]
                FROM [dbo].[Catalog] AS [catalog]
                INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogRelationships_Internal]
                    @CatalogIds,
                    @CatalogRelationships;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogRelationships]
            (
                @CatalogId UNIQUEIDENTIFIER,
                @DependentCatalogId UNIQUEIDENTIFIER,
                @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogRelationship] AS
                (
                    SELECT [catalogRelationship].*
                    FROM @CatalogRelationships AS [catalogRelationship]
                    WHERE (@ByTarget = 0 AND [catalogRelationship].[CatalogId] = @CatalogId)
                        OR (@ByTarget = 1 AND [catalogRelationship].[DependentCatalogId] = @DependentCatalogId)
                )
                MERGE INTO [dbo].[CatalogRelationship] AS [target]
                USING [SourceCatalogRelationship] AS [source]
                ON [target].[CatalogId] = [source].[CatalogId]
                    AND [target].[DependentCatalogId] = [source].[DependentCatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalog].[CreatedOn],
                    @ResultUpdatedOn = [catalog].[UpdatedOn]
                FROM [dbo].[Catalog] AS [catalog]
                INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");
    }
}
