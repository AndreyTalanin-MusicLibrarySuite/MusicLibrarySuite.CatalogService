using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntry" /> entity.
/// </summary>
public partial class CatalogEntryMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntry",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogEntryTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                SystemProtected = table.Column<bool>(type: "bit", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntry", columns: x => x.Id);
                table.ForeignKey(
                    name: "FK_CatalogEntry_CatalogEntryType_CatalogEntryTypeId",
                    column: x => x.CatalogEntryTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntry]
            ADD CONSTRAINT [DF_CatalogEntry_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntry]
            ADD CONSTRAINT [DF_CatalogEntry_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntry]
            ADD CONSTRAINT [DF_CatalogEntry_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntry_CatalogEntryTypeId",
            schema: "dbo",
            table: "CatalogEntry",
            column: "CatalogEntryTypeId");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntry_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntry]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntry]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN [inserted] AS [updatedCatalogEntry] ON [updatedCatalogEntry].[Id] = [catalogEntry].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntry] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [CatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
                [SystemProtected] BIT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntry] (@CatalogEntryId UNIQUEIDENTIFIER)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalogEntry].*
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                WHERE [catalogEntry].[Id] = @CatalogEntryId
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogEntries] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalogEntry].*
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
            (
                @UpdateMode BIT,
                @CatalogEntries [dbo].[CatalogEntry] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[CatalogEntry] AS [target]
                USING @CatalogEntries AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[SystemProtected] = [source].[SystemProtected],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [CatalogEntryTypeId],
                    [SystemProtected],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[CatalogEntryTypeId],
                    [source].[SystemProtected],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntry]
            (
                @CatalogEntryId UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[CatalogEntry]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                WHERE [catalogEntry].[Id] = @CatalogEntryId;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntry];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogEntries];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntries_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntry];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntry];");
    }
}
