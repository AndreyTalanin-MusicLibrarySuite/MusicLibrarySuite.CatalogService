using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                ParentCatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogEntryHierarchicalRelationshipTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationship", columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId",
                    column: x => x.ParentCatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId",
                    column: x => x.CatalogEntryHierarchicalRelationshipTypeId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntryHierarchicalRelationshipType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationship_CatalogEntryIds_NotEqual", sql: "[CatalogEntryId] <> [ParentCatalogEntryId]");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryHierarchicalRelationship]
            ADD CONSTRAINT [DF_CatalogEntryHierarchicalRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryHierarchicalRelationship]
            ADD CONSTRAINT [DF_CatalogEntryHierarchicalRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationship",
            column: "ParentCatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipTypeId",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationship",
            column: "CatalogEntryHierarchicalRelationshipTypeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryHierarchicalRelationship_CatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_Order",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationship",
            columns: ["CatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogEntryHierarchicalRelationship",
            columns: ["ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryHierarchicalRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryHierarchicalRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryHierarchicalRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
                INNER JOIN [inserted] AS [updatedCatalogEntryHierarchicalRelationship]
                    ON [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId];
            END;");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            WHERE [catalogEntryHierarchicalRelationshipType].[OwnerUnique] = 1;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipOwnerUniqueView_MultipleColumns]
            ON [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView]
            (
                [CatalogEntryId],
                [CatalogEntryHierarchicalRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            WHERE [catalogEntryHierarchicalRelationshipType].[TargetUnique] = 1;");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipTargetUniqueView_MultipleColumns]
            ON [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView]
            (
                [ParentCatalogEntryId],
                [CatalogEntryHierarchicalRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntry] AS [ownerCatalogEntry]
                ON [ownerCatalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryHierarchicalRelationshipType].[OwnerCatalogEntryTypeId] <> [ownerCatalogEntry].[CatalogEntryTypeId];");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns]
            ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView]
            (
                [CatalogEntryId],
                [ParentCatalogEntryId],
                [CatalogEntryHierarchicalRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView]
            WITH SCHEMABINDING
            AS
            SELECT
                [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                [viewDuplicationSource].[DuplicationFactor]
            FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
            INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
                ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
            INNER JOIN [dbo].[CatalogEntry] AS [targetCatalogEntry]
                ON [targetCatalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
            CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
            WHERE [catalogEntryHierarchicalRelationshipType].[TargetCatalogEntryTypeId] <> [targetCatalogEntry].[CatalogEntryTypeId];");

        migrationBuilder.Sql(@"
            CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns]
            ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView]
            (
                [CatalogEntryId],
                [ParentCatalogEntryId],
                [CatalogEntryHierarchicalRelationshipTypeId]
            );");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryHierarchicalRelationship] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogEntryHierarchicalRelationship] AS
                (
                    SELECT
                        [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                        [sourceCatalogEntryHierarchicalRelationship].[Order],
                        COALESCE([targetCatalogEntryHierarchicalRelationship].[ReferenceOrder],
                            MAX([referenceCatalogEntryHierarchicalRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogEntryHierarchicalRelationship].[Enabled]
                    FROM @CatalogEntryHierarchicalRelationships AS [sourceCatalogEntryHierarchicalRelationship]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId]
                    LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationship] AS [targetCatalogEntryHierarchicalRelationship]
                        ON [targetCatalogEntryHierarchicalRelationship].[CatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId]
                        AND [targetCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                        AND [targetCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                    LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationship] AS [referenceCatalogEntryHierarchicalRelationship]
                        ON [targetCatalogEntryHierarchicalRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                        AND [referenceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                    GROUP BY
                        [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                        [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                        [sourceCatalogEntryHierarchicalRelationship].[Order],
                        [targetCatalogEntryHierarchicalRelationship].[ReferenceOrder],
                        [sourceCatalogEntryHierarchicalRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogEntryHierarchicalRelationship] AS [target]
                USING [SourceCatalogEntryHierarchicalRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
                    AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [ParentCatalogEntryId],
                    [CatalogEntryHierarchicalRelationshipTypeId],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[ParentCatalogEntryId],
                    [source].[CatalogEntryHierarchicalRelationshipTypeId],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;

                WITH [UpdatedCatalogEntryHierarchicalRelationship] AS
                (
                    SELECT
                        [catalogEntryHierarchicalRelationship].[CatalogEntryId],
                        [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
                        [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId], [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                            ORDER BY [catalogEntryHierarchicalRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
                )
                UPDATE [catalogEntryHierarchicalRelationship]
                SET [ReferenceOrder] = [updatedCatalogEntryHierarchicalRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
                INNER JOIN [UpdatedCatalogEntryHierarchicalRelationship] AS [updatedCatalogEntryHierarchicalRelationship]
                    ON [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
                    AND [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                    ON [catalogEntryId].[Value] = [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryHierarchicalRelationships]
            (
                @CatalogEntryId UNIQUEIDENTIFIER,
                @ParentCatalogEntryId UNIQUEIDENTIFIER,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogEntryHierarchicalRelationship] AS
                (
                    SELECT [catalogEntryHierarchicalRelationship].*
                    FROM @CatalogEntryHierarchicalRelationships AS [catalogEntryHierarchicalRelationship]
                    WHERE (@ByTarget = 0 AND [catalogEntryHierarchicalRelationship].[CatalogEntryId] = @CatalogEntryId)
                        OR (@ByTarget = 1 AND [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = @ParentCatalogEntryId)
                )
                MERGE INTO [dbo].[CatalogEntryHierarchicalRelationship] AS [target]
                USING [SourceCatalogEntryHierarchicalRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
                    AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryHierarchicalRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView];");

        migrationBuilder.Sql("DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryHierarchicalRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryHierarchicalRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }
}
