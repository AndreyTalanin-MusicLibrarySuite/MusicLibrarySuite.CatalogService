using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="CatalogEntryToCatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryToCatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryToCatalogRelationship",
            schema: "dbo",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                CatalogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Order = table.Column<int>(type: "int", nullable: false),
                ReferenceOrder = table.Column<int>(type: "int", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryToCatalogRelationship", columns: x => new { x.CatalogEntryId, x.CatalogId });
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalSchema: "dbo",
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalSchema: "dbo",
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryToCatalogRelationship]
            ADD CONSTRAINT [DF_CatalogEntryToCatalogRelationship_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[CatalogEntryToCatalogRelationship]
            ADD CONSTRAINT [DF_CatalogEntryToCatalogRelationship_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogRelationship_CatalogEntryId",
            schema: "dbo",
            table: "CatalogEntryToCatalogRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogRelationship_CatalogId",
            schema: "dbo",
            table: "CatalogEntryToCatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogRelationship_CatalogEntryId_Order",
            schema: "dbo",
            table: "CatalogEntryToCatalogRelationship",
            columns: ["CatalogEntryId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogRelationship_CatalogId_ReferenceOrder",
            schema: "dbo",
            table: "CatalogEntryToCatalogRelationship",
            columns: ["CatalogId", "ReferenceOrder"],
            unique: true);

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_CatalogEntryToCatalogRelationship_AfterUpdate_SetUpdatedOn]
            ON [dbo].[CatalogEntryToCatalogRelationship]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[CatalogEntryToCatalogRelationship]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[CatalogEntryToCatalogRelationship] AS [catalogEntryToCatalogRelationship]
                INNER JOIN [inserted] AS [updatedCatalogEntryToCatalogRelationship]
                    ON [updatedCatalogEntryToCatalogRelationship].[CatalogEntryId] = [catalogEntryToCatalogRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryToCatalogRelationship].[CatalogId] = [catalogEntryToCatalogRelationship].[CatalogId];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[CatalogEntryToCatalogRelationship] AS TABLE
            (
                [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
                [CatalogId] UNIQUEIDENTIFIER NOT NULL,
                [Order] INT NOT NULL,
                [ReferenceOrder] INT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
            (
                @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY
            )
            AS
            BEGIN
                WITH [SourceCatalogEntryToCatalogRelationship] AS
                (
                    SELECT
                        [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId],
                        [sourceCatalogEntryToCatalogRelationship].[CatalogId],
                        [sourceCatalogEntryToCatalogRelationship].[Order],
                        COALESCE([targetCatalogEntryToCatalogRelationship].[ReferenceOrder],
                            MAX([referenceCatalogEntryToCatalogRelationship].[ReferenceOrder]) + 1,
                            0) AS [ReferenceOrder],
                        [sourceCatalogEntryToCatalogRelationship].[Enabled]
                    FROM @CatalogEntryToCatalogRelationships AS [sourceCatalogEntryToCatalogRelationship]
                    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
                        ON [catalogEntryId].[Value] = [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId]
                    LEFT JOIN [dbo].[CatalogEntryToCatalogRelationship] AS [targetCatalogEntryToCatalogRelationship]
                        ON [targetCatalogEntryToCatalogRelationship].[CatalogEntryId] = [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId]
                        AND [targetCatalogEntryToCatalogRelationship].[CatalogId] = [sourceCatalogEntryToCatalogRelationship].[CatalogId]
                    LEFT JOIN [dbo].[CatalogEntryToCatalogRelationship] AS [referenceCatalogEntryToCatalogRelationship]
                        ON [targetCatalogEntryToCatalogRelationship].[ReferenceOrder] IS NULL
                        AND [referenceCatalogEntryToCatalogRelationship].[CatalogId] = [sourceCatalogEntryToCatalogRelationship].[CatalogId]
                    GROUP BY
                        [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId],
                        [sourceCatalogEntryToCatalogRelationship].[CatalogId],
                        [sourceCatalogEntryToCatalogRelationship].[Order],
                        [targetCatalogEntryToCatalogRelationship].[ReferenceOrder],
                        [sourceCatalogEntryToCatalogRelationship].[Enabled]
                )
                MERGE INTO [dbo].[CatalogEntryToCatalogRelationship] AS [target]
                USING [SourceCatalogEntryToCatalogRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[CatalogId] = [source].[CatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = [source].[Order],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED THEN INSERT
                (
                    [CatalogEntryId],
                    [CatalogId],
                    [Order],
                    [ReferenceOrder],
                    [Enabled]
                )
                VALUES
                (
                    [source].[CatalogEntryId],
                    [source].[CatalogId],
                    [source].[Order],
                    [source].[ReferenceOrder],
                    [source].[Enabled]
                )
                WHEN NOT MATCHED BY SOURCE
                    AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
                    THEN DELETE;

                WITH [UpdatedCatalogEntryToCatalogRelationship] AS
                (
                    SELECT
                        [catalogEntryToCatalogRelationship].[CatalogEntryId],
                        [catalogEntryToCatalogRelationship].[CatalogId],
                        ROW_NUMBER() OVER (PARTITION BY [catalogEntryToCatalogRelationship].[CatalogId]
                            ORDER BY [catalogEntryToCatalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
                    FROM [dbo].[CatalogEntryToCatalogRelationship] AS [catalogEntryToCatalogRelationship]
                )
                UPDATE [catalogEntryToCatalogRelationship]
                SET [ReferenceOrder] = [updatedCatalogEntryToCatalogRelationship].[UpdatedReferenceOrder]
                FROM [dbo].[CatalogEntryToCatalogRelationship] AS [catalogEntryToCatalogRelationship]
                INNER JOIN [UpdatedCatalogEntryToCatalogRelationship] AS [updatedCatalogEntryToCatalogRelationship]
                    ON [updatedCatalogEntryToCatalogRelationship].[CatalogEntryId] = [catalogEntryToCatalogRelationship].[CatalogEntryId]
                    AND [updatedCatalogEntryToCatalogRelationship].[CatalogId] = [catalogEntryToCatalogRelationship].[CatalogId];
            END;");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryToCatalogRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogRelationships]
            (
                @CatalogEntryId UNIQUEIDENTIFIER,
                @CatalogId UNIQUEIDENTIFIER,
                @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
                @ByTarget BIT,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                WITH [SourceCatalogEntryToCatalogRelationship] AS
                (
                    SELECT [catalogEntryToCatalogRelationship].*
                    FROM @CatalogEntryToCatalogRelationships AS [catalogEntryToCatalogRelationship]
                    WHERE (@ByTarget = 0 AND [catalogEntryToCatalogRelationship].[CatalogEntryId] = @CatalogEntryId)
                        OR (@ByTarget = 1 AND [catalogEntryToCatalogRelationship].[CatalogId] = @CatalogId)
                )
                MERGE INTO [dbo].[CatalogEntryToCatalogRelationship] AS [target]
                USING [SourceCatalogEntryToCatalogRelationship] AS [source]
                ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
                    AND [target].[CatalogId] = [source].[CatalogId]
                WHEN MATCHED THEN UPDATE
                SET
                    [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
                    [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

                SET @ResultUpdatedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalogEntry];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogRelationships];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[CatalogEntryToCatalogRelationship];");

        migrationBuilder.Sql("DROP TYPE [dbo].[CatalogEntryToCatalogRelationship];");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultAddedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalogEntry].[CreatedOn],
                    @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
                FROM [dbo].[CatalogEntry] AS [catalogEntry]
                INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntry]
            (
                @CatalogEntry [dbo].[CatalogEntry] READONLY,
                @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
                @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
                @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
                @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
                    @UpdateMode,
                    @CatalogEntry,
                    @ResultUpdatedRows OUTPUT;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryHierarchicalRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

                EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationships;

                EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
                    @CatalogEntryIds,
                    @CatalogEntryRelationshipAnnotations;

                EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

                COMMIT TRANSACTION;
            END;");
    }
}
