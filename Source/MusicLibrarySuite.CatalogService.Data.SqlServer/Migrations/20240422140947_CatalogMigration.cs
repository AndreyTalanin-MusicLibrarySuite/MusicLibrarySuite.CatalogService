using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Migrations;

/// <summary>
/// Represents the SQL-Server-specific database migration adding the <see cref="Catalog" /> entity.
/// </summary>
public partial class CatalogMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "Catalog",
            schema: "dbo",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "nvarchar(max)", maxLength: 32768, nullable: true),
                SystemProtected = table.Column<bool>(type: "bit", nullable: false),
                Enabled = table.Column<bool>(type: "bit", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_Catalog", columns: x => x.Id);
                table.CheckConstraint(name: "CK_Catalog_Name_NotEmpty", sql: "LEN(TRIM([Name])) > 0");
                table.CheckConstraint(name: "CK_Catalog_Description_NullOrNotEmpty", sql: "[Description] IS NULL OR LEN(TRIM([Description])) > 0");
                table.CheckConstraint(name: "CK_Catalog_Description_NullOrMaxLength", sql: "[Description] IS NULL OR LEN(TRIM([Description])) <= 32768");
            });

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[Catalog]
            ADD CONSTRAINT [DF_Catalog_Id] DEFAULT NEWSEQUENTIALID() FOR [Id];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[Catalog]
            ADD CONSTRAINT [DF_Catalog_CreatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [CreatedOn];");

        migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[Catalog]
            ADD CONSTRAINT [DF_Catalog_UpdatedOn] DEFAULT SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00') FOR [UpdatedOn];");

        migrationBuilder.Sql(@"
            CREATE TRIGGER [dbo].[TR_Catalog_AfterUpdate_SetUpdatedOn]
            ON [dbo].[Catalog]
            AFTER UPDATE
            AS
            BEGIN
                SET NOCOUNT ON;

                UPDATE [dbo].[Catalog]
                SET [UpdatedOn] = SWITCHOFFSET(SYSDATETIMEOFFSET(), '+00:00')
                FROM [dbo].[Catalog] AS [catalog]
                INNER JOIN [inserted] AS [updatedCatalog] ON [updatedCatalog].[Id] = [catalog].[Id];
            END;");

        migrationBuilder.Sql(@"
            CREATE TYPE [dbo].[Catalog] AS TABLE
            (
                [Id] UNIQUEIDENTIFIER NOT NULL,
                [Name] NVARCHAR(256) NOT NULL,
                [Description] NVARCHAR(MAX) NULL,
                [SystemProtected] BIT NOT NULL,
                [Enabled] BIT NOT NULL,
                [CreatedOn] DATETIMEOFFSET NOT NULL,
                [UpdatedOn] DATETIMEOFFSET NOT NULL
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalog] (@CatalogId UNIQUEIDENTIFIER)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT TOP (1) [catalog].*
                FROM [dbo].[Catalog] AS [catalog]
                WHERE [catalog].[Id] = @CatalogId
            );");

        migrationBuilder.Sql(@"
            CREATE FUNCTION [dbo].[ufn_GetCatalogs] (@CatalogIds [ctt].[GuidUnorderedSet] READONLY)
            RETURNS TABLE
            AS
            RETURN
            (
                SELECT [catalog].*
                FROM [dbo].[Catalog] AS [catalog]
                INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
            );");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogs_Internal]
            (
                @UpdateMode BIT,
                @Catalogs [dbo].[Catalog] READONLY,
                @ResultAffectedRows INT OUTPUT
            )
            AS
            BEGIN
                MERGE INTO [dbo].[Catalog] AS [target]
                USING @Catalogs AS [source]
                ON [target].[Id] = [source].[Id]
                WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
                SET
                    [target].[Name] = [source].[Name],
                    [target].[Description] = [source].[Description],
                    [target].[SystemProtected] = [source].[SystemProtected],
                    [target].[Enabled] = [source].[Enabled]
                WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
                (
                    [Id],
                    [Name],
                    [Description],
                    [SystemProtected],
                    [Enabled]
                )
                VALUES
                (
                    [source].[Id],
                    [source].[Name],
                    [source].[Description],
                    [source].[SystemProtected],
                    [source].[Enabled]
                );

                SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_AddCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @ResultCreatedOn DATETIMEOFFSET OUTPUT,
                @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
                @ResultAddedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 0;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultAddedRows OUTPUT;

                COMMIT TRANSACTION;

                SELECT TOP (1)
                    @ResultCreatedOn = [catalog].[CreatedOn],
                    @ResultUpdatedOn = [catalog].[UpdatedOn]
                FROM [dbo].[Catalog] AS [catalog]
                INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
                    AND @ResultAddedRows > 0;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_UpdateCatalog]
            (
                @Catalog [dbo].[Catalog] READONLY,
                @ResultUpdatedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
                INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

                BEGIN TRANSACTION;

                DECLARE @UpdateMode AS BIT = 1;
                EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
                    @UpdateMode,
                    @Catalog,
                    @ResultUpdatedRows OUTPUT;

                COMMIT TRANSACTION;
            END;");

        migrationBuilder.Sql(@"
            CREATE PROCEDURE [dbo].[sp_RemoveCatalog]
            (
                @CatalogId UNIQUEIDENTIFIER,
                @ResultRemovedRows INT OUTPUT
            )
            AS
            BEGIN
                SET NOCOUNT ON;
                SET XACT_ABORT ON;

                BEGIN TRANSACTION;

                DELETE [dbo].[Catalog]
                FROM [dbo].[Catalog] AS [catalog]
                WHERE [catalog].[Id] = @CatalogId;

                SET @ResultRemovedRows = @@ROWCOUNT;

                COMMIT TRANSACTION;
            END;");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalog];");

        migrationBuilder.Sql("DROP FUNCTION [dbo].[ufn_GetCatalogs];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_UpdateCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_RemoveCatalog];");

        migrationBuilder.Sql("DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogs_Internal];");

        migrationBuilder.Sql("DROP TABLE [dbo].[Catalog];");

        migrationBuilder.Sql("DROP TYPE [dbo].[Catalog];");
    }
}
