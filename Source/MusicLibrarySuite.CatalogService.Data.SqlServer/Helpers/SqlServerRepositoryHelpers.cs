using System.Data;

using Microsoft.Data.SqlClient;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Helpers;

/// <summary>
/// Contains a set of constants and helper methods for SQL-Server-specific repository implementations.
/// </summary>
internal static class SqlServerRepositoryHelpers
{
    /// <summary>
    /// Represents the <c>Result</c> string.
    /// </summary>
    public static readonly string ResultParameterNamePrefix = "Result";

    /// <summary>
    /// Represents the <c>ResultAddedRows</c> string.
    /// </summary>
    public static readonly string ResultAddedRowsParameterName = "ResultAddedRows";

    /// <summary>
    /// Represents the <c>ResultUpdatedRows</c> string.
    /// </summary>
    public static readonly string ResultUpdatedRowsParameterName = "ResultUpdatedRows";

    /// <summary>
    /// Represents the <c>ResultRemovedRows</c> string.
    /// </summary>
    public static readonly string ResultRemovedRowsParameterName = "ResultRemovedRows";

    /// <summary>
    /// Represents the <c>ResultAffectedRows</c> string.
    /// </summary>
    public static readonly string ResultAffectedRowsParameterName = "ResultAffectedRows";

    /// <summary>
    /// Creates an output <see cref="SqlParameter" /> instance with the <c>Result</c>-prefixed name.
    /// </summary>
    /// <param name="parameterName">The parameter name that will be prefixed with <c>Result</c>.</param>
    /// <param name="dbType">The <see cref="SqlDbType" /> type of the parameter.</param>
    /// <returns>The created <see cref="SqlParameter" /> instance.</returns>
    public static SqlParameter CreateResultSqlParameter(string parameterName, SqlDbType dbType)
    {
        return new SqlParameter($"{ResultParameterNamePrefix}{parameterName}", dbType) { Direction = ParameterDirection.Output };
    }

    /// <summary>
    /// Creates an output <see cref="SqlParameter" /> with the <c>ResultAddedRows</c> name.
    /// </summary>
    /// <param name="getRowsCount">The function to get the number of added rows later.</param>
    /// <returns>The created <see cref="SqlParameter" /> instance.</returns>
    public static SqlParameter CreateResultAddedRowsSqlParameter(out GetRowsCountDelegate getRowsCount)
    {
        SqlParameter addedRowsParameter = new(ResultAddedRowsParameterName, SqlDbType.Int) { Direction = ParameterDirection.Output };
        getRowsCount = () => DbValueConvert.ToValueType<int>(addedRowsParameter.Value);
        return addedRowsParameter;
    }

    /// <summary>
    /// Creates an output <see cref="SqlParameter" /> with the <c>ResultUpdatedRows</c> name.
    /// </summary>
    /// <param name="getRowsCount">The function to get the number of updated rows later.</param>
    /// <returns>The created <see cref="SqlParameter" /> instance.</returns>
    public static SqlParameter CreateResultUpdatedRowsSqlParameter(out GetRowsCountDelegate getRowsCount)
    {
        SqlParameter updatedRowsParameter = new(ResultUpdatedRowsParameterName, SqlDbType.Int) { Direction = ParameterDirection.Output };
        getRowsCount = () => DbValueConvert.ToValueType<int>(updatedRowsParameter.Value);
        return updatedRowsParameter;
    }

    /// <summary>
    /// Creates an output <see cref="SqlParameter" /> with the <c>ResultRemovedRows</c> name.
    /// </summary>
    /// <param name="getRowsCount">The function to get the number of removed rows later.</param>
    /// <returns>The created <see cref="SqlParameter" /> instance.</returns>
    public static SqlParameter CreateResultRemovedRowsSqlParameter(out GetRowsCountDelegate getRowsCount)
    {
        SqlParameter removedRowsParameter = new(ResultRemovedRowsParameterName, SqlDbType.Int) { Direction = ParameterDirection.Output };
        getRowsCount = () => DbValueConvert.ToValueType<int>(removedRowsParameter.Value);
        return removedRowsParameter;
    }

    /// <summary>
    /// Creates an output <see cref="SqlParameter" /> with the <c>ResultAffectedRows</c> name.
    /// </summary>
    /// <param name="getRowsCount">The function to get the number of affected rows later.</param>
    /// <returns>The created <see cref="SqlParameter" /> instance.</returns>
    public static SqlParameter CreateResultAffectedRowsSqlParameter(out GetRowsCountDelegate getRowsCount)
    {
        SqlParameter affectedRowsParameter = new(ResultAffectedRowsParameterName, SqlDbType.Int) { Direction = ParameterDirection.Output };
        getRowsCount = () => DbValueConvert.ToValueType<int>(affectedRowsParameter.Value);
        return affectedRowsParameter;
    }
}

/// <summary>
/// Represents a method that gets the number of affected rows (added, updated or removed).
/// </summary>
/// <returns>The number of affected rows.</returns>
internal delegate int GetRowsCountDelegate();
