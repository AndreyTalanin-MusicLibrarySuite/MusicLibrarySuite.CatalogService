DROP TYPE [ctt].[Int32UnorderedSet];
GO

CREATE TYPE [ctt].[Int32UnorderedSet] AS TABLE
(
    [Value] INT NOT NULL PRIMARY KEY CLUSTERED
);
