DROP TYPE [dbo].[CatalogEntryRelationshipType];
GO

CREATE TYPE [dbo].[CatalogEntryRelationshipType] AS TABLE
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [OwnerCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
    [TargetCatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Code] NVARCHAR(256) NOT NULL,
    [Annotated] BIT NOT NULL,
    [OwnerUnique] BIT NOT NULL,
    [TargetUnique] BIT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
