DROP TYPE [ctt].[ByteUnorderedSet];
GO

CREATE TYPE [ctt].[ByteUnorderedSet] AS TABLE
(
    [Value] TINYINT NOT NULL PRIMARY KEY CLUSTERED
);
