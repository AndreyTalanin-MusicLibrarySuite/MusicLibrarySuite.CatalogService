DROP TYPE [dbo].[CatalogEntry];
GO

CREATE TYPE [dbo].[CatalogEntry] AS TABLE
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [CatalogEntryTypeId] UNIQUEIDENTIFIER NOT NULL,
    [SystemProtected] BIT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
