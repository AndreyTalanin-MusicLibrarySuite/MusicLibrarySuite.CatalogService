DROP TYPE [dbo].[CatalogRelationship];
GO

CREATE TYPE [dbo].[CatalogRelationship] AS TABLE
(
    [CatalogId] UNIQUEIDENTIFIER NOT NULL,
    [DependentCatalogId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Description] NVARCHAR(MAX) NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
