DROP TYPE [dbo].[CatalogEntryHierarchicalRelationshipAnnotation];
GO

CREATE TYPE [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Description] NVARCHAR(MAX) NULL
);
