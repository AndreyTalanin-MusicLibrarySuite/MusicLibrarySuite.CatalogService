DROP TYPE [dbo].[CatalogEntryType];
GO

CREATE TYPE [dbo].[CatalogEntryType] AS TABLE
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Code] NVARCHAR(256) NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
