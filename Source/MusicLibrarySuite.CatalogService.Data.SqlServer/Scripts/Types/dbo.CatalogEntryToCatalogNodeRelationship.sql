DROP TYPE [dbo].[CatalogEntryToCatalogNodeRelationship];
GO

CREATE TYPE [dbo].[CatalogEntryToCatalogNodeRelationship] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
