DROP TYPE [dbo].[Catalog];
GO

CREATE TYPE [dbo].[Catalog] AS TABLE
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Description] NVARCHAR(MAX) NULL,
    [SystemProtected] BIT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
