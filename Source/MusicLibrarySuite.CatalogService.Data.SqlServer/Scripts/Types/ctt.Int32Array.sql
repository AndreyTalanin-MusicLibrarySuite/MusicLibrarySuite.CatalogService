DROP TYPE [ctt].[Int32Array];
GO

CREATE TYPE [ctt].[Int32Array] AS TABLE
(
    [Value] INT NOT NULL
);
