DROP TYPE [dbo].[CatalogNodeHierarchicalRelationship];
GO

CREATE TYPE [dbo].[CatalogNodeHierarchicalRelationship] AS TABLE
(
    [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [ParentCatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
