DROP TYPE [ctt].[StringArray];
GO

CREATE TYPE [ctt].[StringArray] AS TABLE
(
    [Value] NVARCHAR(MAX) NOT NULL
);
