DROP TYPE [dbo].[CatalogEntryHierarchicalRelationship];
GO

CREATE TYPE [dbo].[CatalogEntryHierarchicalRelationship] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
