DROP TYPE [ctt].[Int16UnorderedSet];
GO

CREATE TYPE [ctt].[Int16UnorderedSet] AS TABLE
(
    [Value] SMALLINT NOT NULL PRIMARY KEY CLUSTERED
);
