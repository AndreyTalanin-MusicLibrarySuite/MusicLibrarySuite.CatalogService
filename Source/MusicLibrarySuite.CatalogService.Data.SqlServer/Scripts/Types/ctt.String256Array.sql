DROP TYPE [ctt].[String256Array];
GO

CREATE TYPE [ctt].[String256Array] AS TABLE
(
    [Value] NVARCHAR(256) NOT NULL
);
