DROP TYPE [ctt].[ByteOrderedSet];
GO

CREATE TYPE [ctt].[ByteOrderedSet] AS TABLE
(
    [Value] TINYINT NOT NULL PRIMARY KEY CLUSTERED,
    [Order] INT NOT NULL
);
