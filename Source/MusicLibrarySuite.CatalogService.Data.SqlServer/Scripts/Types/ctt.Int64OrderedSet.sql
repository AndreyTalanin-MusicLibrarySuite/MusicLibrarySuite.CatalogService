DROP TYPE [ctt].[Int64OrderedSet];
GO

CREATE TYPE [ctt].[Int64OrderedSet] AS TABLE
(
    [Value] BIGINT NOT NULL PRIMARY KEY CLUSTERED,
    [Order] INT NOT NULL
);
