DROP TYPE [dbo].[CatalogEntryRelationship];
GO

CREATE TYPE [dbo].[CatalogEntryRelationship] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
