DROP TYPE [ctt].[String256OrderedSet];
GO

CREATE TYPE [ctt].[String256OrderedSet] AS TABLE
(
    [Value] NVARCHAR(256) NOT NULL PRIMARY KEY CLUSTERED,
    [Order] INT NOT NULL
);
