DROP TYPE [ctt].[Int32OrderedSet];
GO

CREATE TYPE [ctt].[Int32OrderedSet] AS TABLE
(
    [Value] INT NOT NULL PRIMARY KEY CLUSTERED,
    [Order] INT NOT NULL
);
