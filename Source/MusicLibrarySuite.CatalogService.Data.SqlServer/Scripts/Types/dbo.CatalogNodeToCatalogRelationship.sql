DROP TYPE [dbo].[CatalogNodeToCatalogRelationship];
GO

CREATE TYPE [dbo].[CatalogNodeToCatalogRelationship] AS TABLE
(
    [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
