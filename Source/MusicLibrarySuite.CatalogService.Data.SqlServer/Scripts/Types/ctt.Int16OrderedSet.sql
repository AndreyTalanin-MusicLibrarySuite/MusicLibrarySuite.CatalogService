DROP TYPE [ctt].[Int16OrderedSet];
GO

CREATE TYPE [ctt].[Int16OrderedSet] AS TABLE
(
    [Value] SMALLINT NOT NULL PRIMARY KEY CLUSTERED,
    [Order] INT NOT NULL
);
