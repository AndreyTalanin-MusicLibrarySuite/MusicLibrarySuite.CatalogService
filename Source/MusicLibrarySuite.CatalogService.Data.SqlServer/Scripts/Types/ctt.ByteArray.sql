DROP TYPE [ctt].[ByteArray];
GO

CREATE TYPE [ctt].[ByteArray] AS TABLE
(
    [Value] TINYINT NOT NULL
);
