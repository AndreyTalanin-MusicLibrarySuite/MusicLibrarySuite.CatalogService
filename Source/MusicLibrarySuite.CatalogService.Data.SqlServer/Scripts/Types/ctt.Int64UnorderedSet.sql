DROP TYPE [ctt].[Int64UnorderedSet];
GO

CREATE TYPE [ctt].[Int64UnorderedSet] AS TABLE
(
    [Value] BIGINT NOT NULL PRIMARY KEY CLUSTERED
);
