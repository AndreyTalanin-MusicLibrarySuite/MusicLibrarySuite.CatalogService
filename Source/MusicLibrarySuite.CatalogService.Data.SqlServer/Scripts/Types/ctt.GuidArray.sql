DROP TYPE [ctt].[GuidArray];
GO

CREATE TYPE [ctt].[GuidArray] AS TABLE
(
    [Value] UNIQUEIDENTIFIER NOT NULL
);
