DROP TYPE [dbo].[CatalogNodeRelationship];
GO

CREATE TYPE [dbo].[CatalogNodeRelationship] AS TABLE
(
    [CatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [DependentCatalogNodeId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Description] NVARCHAR(MAX) NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
