DROP TYPE [dbo].[CatalogEntryToCatalogRelationship];
GO

CREATE TYPE [dbo].[CatalogEntryToCatalogRelationship] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogId] UNIQUEIDENTIFIER NOT NULL,
    [Order] INT NOT NULL,
    [ReferenceOrder] INT NOT NULL,
    [Enabled] BIT NOT NULL,
    [CreatedOn] DATETIMEOFFSET NOT NULL,
    [UpdatedOn] DATETIMEOFFSET NOT NULL
);
