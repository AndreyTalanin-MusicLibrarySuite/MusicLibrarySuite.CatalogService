DROP TYPE [dbo].[CatalogEntryRelationshipAnnotation];
GO

CREATE TYPE [dbo].[CatalogEntryRelationshipAnnotation] AS TABLE
(
    [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
    [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(256) NOT NULL,
    [Description] NVARCHAR(MAX) NULL
);
