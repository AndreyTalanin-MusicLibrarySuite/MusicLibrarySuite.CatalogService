DROP TYPE [ctt].[Int16Array];
GO

CREATE TYPE [ctt].[Int16Array] AS TABLE
(
    [Value] SMALLINT NOT NULL
);
