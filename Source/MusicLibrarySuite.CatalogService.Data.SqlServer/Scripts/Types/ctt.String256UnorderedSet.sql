DROP TYPE [ctt].[String256UnorderedSet];
GO

CREATE TYPE [ctt].[String256UnorderedSet] AS TABLE
(
    [Value] NVARCHAR(256) NOT NULL PRIMARY KEY CLUSTERED
);
