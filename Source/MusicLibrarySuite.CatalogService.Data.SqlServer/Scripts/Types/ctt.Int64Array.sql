DROP TYPE [ctt].[Int64Array];
GO

CREATE TYPE [ctt].[Int64Array] AS TABLE
(
    [Value] BIGINT NOT NULL
);
