DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypeIdByCode];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypeIdByCode] (@CatalogEntryRelationshipTypeCode NVARCHAR(256))
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
    DECLARE @CatalogEntryRelationshipTypeId AS UNIQUEIDENTIFIER;

    SELECT TOP (1) @CatalogEntryRelationshipTypeId = [catalogEntryRelationshipType].[Id]
    FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    WHERE [catalogEntryRelationshipType].[Code] = @CatalogEntryRelationshipTypeCode;

    RETURN @CatalogEntryRelationshipTypeId;
END;
