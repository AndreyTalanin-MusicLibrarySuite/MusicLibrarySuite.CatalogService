DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipType];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipType]
(
    @CatalogEntryHierarchicalRelationshipTypeId UNIQUEIDENTIFIER,
    @CatalogEntryHierarchicalRelationshipTypeCode NVARCHAR(256)
)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalogEntryHierarchicalRelationshipType].*
    FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    WHERE (@CatalogEntryHierarchicalRelationshipTypeId IS NULL OR [catalogEntryHierarchicalRelationshipType].[Id] = @CatalogEntryHierarchicalRelationshipTypeId)
        AND (@CatalogEntryHierarchicalRelationshipTypeCode IS NULL OR [catalogEntryHierarchicalRelationshipType].[Code] = @CatalogEntryHierarchicalRelationshipTypeCode)
);
