DROP FUNCTION [dbo].[ufn_GetCatalog];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalog] (@CatalogId UNIQUEIDENTIFIER)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalog].*
    FROM [dbo].[Catalog] AS [catalog]
    WHERE [catalog].[Id] = @CatalogId
);
