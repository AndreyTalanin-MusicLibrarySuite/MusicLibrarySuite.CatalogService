DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipType];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipType]
(
    @CatalogEntryRelationshipTypeId UNIQUEIDENTIFIER,
    @CatalogEntryRelationshipTypeCode NVARCHAR(256)
)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalogEntryRelationshipType].*
    FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    WHERE (@CatalogEntryRelationshipTypeId IS NULL OR [catalogEntryRelationshipType].[Id] = @CatalogEntryRelationshipTypeId)
        AND (@CatalogEntryRelationshipTypeCode IS NULL OR [catalogEntryRelationshipType].[Code] = @CatalogEntryRelationshipTypeCode)
);
