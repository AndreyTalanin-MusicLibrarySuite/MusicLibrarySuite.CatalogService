DROP FUNCTION [dbo].[ufn_GetCatalogNodes];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogNodes] (@CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalogNode].*
    FROM [dbo].[CatalogNode] AS [catalogNode]
    INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
);
