DROP FUNCTION [dbo].[ufn_GetCatalogNode];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogNode] (@CatalogNodeId UNIQUEIDENTIFIER)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalogNode].*
    FROM [dbo].[CatalogNode] AS [catalogNode]
    WHERE [catalogNode].[Id] = @CatalogNodeId
);
