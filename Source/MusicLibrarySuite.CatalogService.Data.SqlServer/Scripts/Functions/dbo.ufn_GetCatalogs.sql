DROP FUNCTION [dbo].[ufn_GetCatalogs];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogs] (@CatalogIds [ctt].[GuidUnorderedSet] READONLY)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalog].*
    FROM [dbo].[Catalog] AS [catalog]
    INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
);
