DROP FUNCTION [dbo].[ufn_GetCatalogEntry];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntry] (@CatalogEntryId UNIQUEIDENTIFIER)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalogEntry].*
    FROM [dbo].[CatalogEntry] AS [catalogEntry]
    WHERE [catalogEntry].[Id] = @CatalogEntryId
);
