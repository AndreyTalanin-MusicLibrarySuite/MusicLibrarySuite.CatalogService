DROP FUNCTION [dbo].[ufn_GetCatalogEntryType];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryType]
(
    @CatalogEntryTypeId UNIQUEIDENTIFIER,
    @CatalogEntryTypeCode NVARCHAR(256)
)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP (1) [catalogEntryType].*
    FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
    WHERE (@CatalogEntryTypeId IS NULL OR [catalogEntryType].[Id] = @CatalogEntryTypeId)
        AND (@CatalogEntryTypeCode IS NULL OR [catalogEntryType].[Code] = @CatalogEntryTypeCode)
);
