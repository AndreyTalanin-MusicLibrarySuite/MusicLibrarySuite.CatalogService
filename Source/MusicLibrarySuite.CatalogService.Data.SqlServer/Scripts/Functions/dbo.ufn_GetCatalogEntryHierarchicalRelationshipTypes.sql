DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypes];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypes]
(
    @CatalogEntryHierarchicalRelationshipTypeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryHierarchicalRelationshipTypeCodes [ctt].[String256UnorderedSet] READONLY
)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalogEntryHierarchicalRelationshipType].*
    FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    LEFT JOIN @CatalogEntryHierarchicalRelationshipTypeIds AS [catalogEntryHierarchicalRelationshipTypeId]
        ON [catalogEntryHierarchicalRelationshipTypeId].[Value] = [catalogEntryHierarchicalRelationshipType].[Id]
    LEFT JOIN @CatalogEntryHierarchicalRelationshipTypeCodes AS [catalogEntryHierarchicalRelationshipTypeCode]
        ON [catalogEntryHierarchicalRelationshipTypeCode].[Value] = [catalogEntryHierarchicalRelationshipType].[Code]
    WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryHierarchicalRelationshipTypeIds) OR [catalogEntryHierarchicalRelationshipTypeId].[Value] IS NOT NULL)
        AND (NOT EXISTS(SELECT * FROM @CatalogEntryHierarchicalRelationshipTypeCodes) OR [catalogEntryHierarchicalRelationshipTypeCode].[Value] IS NOT NULL)
);
