DROP FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypes];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryRelationshipTypes]
(
    @CatalogEntryRelationshipTypeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryRelationshipTypeCodes [ctt].[String256UnorderedSet] READONLY
)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalogEntryRelationshipType].*
    FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    LEFT JOIN @CatalogEntryRelationshipTypeIds AS [catalogEntryRelationshipTypeId]
        ON [catalogEntryRelationshipTypeId].[Value] = [catalogEntryRelationshipType].[Id]
    LEFT JOIN @CatalogEntryRelationshipTypeCodes AS [catalogEntryRelationshipTypeCode]
        ON [catalogEntryRelationshipTypeCode].[Value] = [catalogEntryRelationshipType].[Code]
    WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryRelationshipTypeIds) OR [catalogEntryRelationshipTypeId].[Value] IS NOT NULL)
        AND (NOT EXISTS(SELECT * FROM @CatalogEntryRelationshipTypeCodes) OR [catalogEntryRelationshipTypeCode].[Value] IS NOT NULL)
);
