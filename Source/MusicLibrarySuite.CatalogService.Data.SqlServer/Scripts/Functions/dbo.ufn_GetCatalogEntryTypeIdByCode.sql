DROP FUNCTION [dbo].[ufn_GetCatalogEntryTypeIdByCode];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryTypeIdByCode] (@CatalogEntryTypeCode NVARCHAR(256))
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
    DECLARE @CatalogEntryTypeId AS UNIQUEIDENTIFIER;

    SELECT TOP (1) @CatalogEntryTypeId = [catalogEntryType].[Id]
    FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
    WHERE [catalogEntryType].[Code] = @CatalogEntryTypeCode;

    RETURN @CatalogEntryTypeId;
END;
