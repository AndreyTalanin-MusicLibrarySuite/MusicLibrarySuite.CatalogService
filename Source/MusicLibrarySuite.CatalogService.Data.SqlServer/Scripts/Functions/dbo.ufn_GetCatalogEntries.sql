DROP FUNCTION [dbo].[ufn_GetCatalogEntries];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntries] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalogEntry].*
    FROM [dbo].[CatalogEntry] AS [catalogEntry]
    INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
);
