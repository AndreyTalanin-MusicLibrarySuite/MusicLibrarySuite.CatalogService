DROP FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypeIdByCode];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryHierarchicalRelationshipTypeIdByCode] (@CatalogEntryHierarchicalRelationshipTypeCode NVARCHAR(256))
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
    DECLARE @CatalogEntryHierarchicalRelationshipTypeId AS UNIQUEIDENTIFIER;

    SELECT TOP (1) @CatalogEntryHierarchicalRelationshipTypeId = [catalogEntryHierarchicalRelationshipType].[Id]
    FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    WHERE [catalogEntryHierarchicalRelationshipType].[Code] = @CatalogEntryHierarchicalRelationshipTypeCode;

    RETURN @CatalogEntryHierarchicalRelationshipTypeId;
END;
