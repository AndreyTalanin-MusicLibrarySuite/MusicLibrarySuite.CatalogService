DROP FUNCTION [dbo].[ufn_GetCatalogEntryTypes];
GO

CREATE FUNCTION [dbo].[ufn_GetCatalogEntryTypes]
(
    @CatalogEntryTypeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryTypeCodes [ctt].[String256UnorderedSet] READONLY
)
RETURNS TABLE
AS
RETURN
(
    SELECT [catalogEntryType].*
    FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
    LEFT JOIN @CatalogEntryTypeIds AS [catalogEntryTypeId] ON [catalogEntryTypeId].[Value] = [catalogEntryType].[Id]
    LEFT JOIN @CatalogEntryTypeCodes AS [catalogEntryTypeCode] ON [catalogEntryTypeCode].[Value] = [catalogEntryType].[Code]
    WHERE (NOT EXISTS(SELECT * FROM @CatalogEntryTypeIds) OR [catalogEntryTypeId].[Value] IS NOT NULL)
        AND (NOT EXISTS(SELECT * FROM @CatalogEntryTypeCodes) OR [catalogEntryTypeCode].[Value] IS NOT NULL)
);
