DROP PROCEDURE [dbo].[sp_UpdateCatalogNode];
GO

CREATE PROCEDURE [dbo].[sp_UpdateCatalogNode]
(
    @CatalogNode [dbo].[CatalogNode] READONLY,
    @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
    @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
    @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 1;
    EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
        @UpdateMode,
        @CatalogNode,
        @ResultUpdatedRows OUTPUT;

    EXEC [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeToCatalogRelationships;

    EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeHierarchicalRelationships;

    EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeRelationships;

    COMMIT TRANSACTION;
END;
