DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryRelationshipType]
(
    @CatalogEntryRelationshipType [dbo].[CatalogEntryRelationshipType] READONLY,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryRelationshipTypeIds ([Value])
    SELECT [catalogEntryRelationshipType].[Id]
    FROM @CatalogEntryRelationshipType AS [catalogEntryRelationshipType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 1;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal]
        @UpdateMode,
        @CatalogEntryRelationshipType,
        @ResultUpdatedRows OUTPUT;

    COMMIT TRANSACTION;
END;
