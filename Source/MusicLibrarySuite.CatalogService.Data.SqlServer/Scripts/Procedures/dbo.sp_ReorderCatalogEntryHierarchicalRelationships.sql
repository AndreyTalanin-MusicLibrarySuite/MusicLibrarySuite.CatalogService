DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryHierarchicalRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryHierarchicalRelationships]
(
    @CatalogEntryId UNIQUEIDENTIFIER,
    @ParentCatalogEntryId UNIQUEIDENTIFIER,
    @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogEntryHierarchicalRelationship] AS
    (
        SELECT [catalogEntryHierarchicalRelationship].*
        FROM @CatalogEntryHierarchicalRelationships AS [catalogEntryHierarchicalRelationship]
        WHERE (@ByTarget = 0 AND [catalogEntryHierarchicalRelationship].[CatalogEntryId] = @CatalogEntryId)
            OR (@ByTarget = 1 AND [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = @ParentCatalogEntryId)
    )
    MERGE INTO [dbo].[CatalogEntryHierarchicalRelationship] AS [target]
    USING [SourceCatalogEntryHierarchicalRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
        AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
