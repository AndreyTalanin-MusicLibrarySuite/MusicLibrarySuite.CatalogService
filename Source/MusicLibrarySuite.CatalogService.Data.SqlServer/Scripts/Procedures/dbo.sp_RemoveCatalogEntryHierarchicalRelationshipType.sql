DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryHierarchicalRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryHierarchicalRelationshipType]
(
    @Id UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[CatalogEntryHierarchicalRelationshipType]
    FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    WHERE [catalogEntryHierarchicalRelationshipType].[Id] = @Id;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
