DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeRelationships]
(
    @CatalogNodeId UNIQUEIDENTIFIER,
    @DependentCatalogNodeId UNIQUEIDENTIFIER,
    @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogNodeRelationship] AS
    (
        SELECT [catalogNodeRelationship].*
        FROM @CatalogNodeRelationships AS [catalogNodeRelationship]
        WHERE (@ByTarget = 0 AND [catalogNodeRelationship].[CatalogNodeId] = @CatalogNodeId)
            OR (@ByTarget = 1 AND [catalogNodeRelationship].[DependentCatalogNodeId] = @DependentCatalogNodeId)
    )
    MERGE INTO [dbo].[CatalogNodeRelationship] AS [target]
    USING [SourceCatalogNodeRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[DependentCatalogNodeId] = [source].[DependentCatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
