DROP PROCEDURE [dbo].[sp_RemoveCatalog];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalog]
(
    @CatalogId UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[Catalog]
    FROM [dbo].[Catalog] AS [catalog]
    WHERE [catalog].[Id] = @CatalogId;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
