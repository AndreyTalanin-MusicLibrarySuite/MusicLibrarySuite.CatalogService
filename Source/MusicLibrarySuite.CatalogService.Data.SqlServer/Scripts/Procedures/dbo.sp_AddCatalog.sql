DROP PROCEDURE [dbo].[sp_AddCatalog];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalog]
(
    @Catalog [dbo].[Catalog] READONLY,
    @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
        @UpdateMode,
        @Catalog,
        @ResultAddedRows OUTPUT;

    EXEC [dbo].[sp_MergeCatalogRelationships_Internal]
        @CatalogIds,
        @CatalogRelationships;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalog].[CreatedOn],
        @ResultUpdatedOn = [catalog].[UpdatedOn]
    FROM [dbo].[Catalog] AS [catalog]
    INNER JOIN @CatalogIds AS [catalogId] ON [catalogId].[Value] = [catalog].[Id]
        AND @ResultAddedRows > 0;
END;
