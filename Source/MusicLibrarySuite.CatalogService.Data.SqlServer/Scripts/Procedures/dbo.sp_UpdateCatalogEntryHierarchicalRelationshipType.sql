DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryHierarchicalRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryHierarchicalRelationshipType]
(
    @CatalogEntryHierarchicalRelationshipType [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryHierarchicalRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryHierarchicalRelationshipTypeIds ([Value])
    SELECT [catalogEntryHierarchicalRelationshipType].[Id]
    FROM @CatalogEntryHierarchicalRelationshipType AS [catalogEntryHierarchicalRelationshipType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 1;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
        @UpdateMode,
        @CatalogEntryHierarchicalRelationshipType,
        @ResultUpdatedRows OUTPUT;

    COMMIT TRANSACTION;
END;
