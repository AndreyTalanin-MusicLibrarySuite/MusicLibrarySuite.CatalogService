DROP PROCEDURE [dbo].[sp_UpdateCatalogEntryType];
GO

CREATE PROCEDURE [dbo].[sp_UpdateCatalogEntryType]
(
    @CatalogEntryType [dbo].[CatalogEntryType] READONLY,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryTypeIds ([Value]) SELECT [catalogEntryType].[Id] FROM @CatalogEntryType AS [catalogEntryType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 1;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
        @UpdateMode,
        @CatalogEntryType,
        @ResultUpdatedRows OUTPUT;

    COMMIT TRANSACTION;
END;
