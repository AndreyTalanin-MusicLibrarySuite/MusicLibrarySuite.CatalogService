DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryRelationships]
(
    @CatalogEntryId UNIQUEIDENTIFIER,
    @DependentCatalogEntryId UNIQUEIDENTIFIER,
    @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogEntryRelationship] AS
    (
        SELECT [catalogEntryRelationship].*
        FROM @CatalogEntryRelationships AS [catalogEntryRelationship]
        WHERE (@ByTarget = 0 AND [catalogEntryRelationship].[CatalogEntryId] = @CatalogEntryId)
            OR (@ByTarget = 1 AND [catalogEntryRelationship].[DependentCatalogEntryId] = @DependentCatalogEntryId)
    )
    MERGE INTO [dbo].[CatalogEntryRelationship] AS [target]
    USING [SourceCatalogEntryRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
        AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
