DROP PROCEDURE [dbo].[sp_RemoveCatalogEntry];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntry]
(
    @CatalogEntryId UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[CatalogEntry]
    FROM [dbo].[CatalogEntry] AS [catalogEntry]
    WHERE [catalogEntry].[Id] = @CatalogEntryId;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
