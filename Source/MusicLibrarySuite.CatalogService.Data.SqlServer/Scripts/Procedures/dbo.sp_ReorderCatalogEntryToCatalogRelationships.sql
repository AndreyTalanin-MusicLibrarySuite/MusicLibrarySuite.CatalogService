DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogRelationships]
(
    @CatalogEntryId UNIQUEIDENTIFIER,
    @CatalogId UNIQUEIDENTIFIER,
    @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogEntryToCatalogRelationship] AS
    (
        SELECT [catalogEntryToCatalogRelationship].*
        FROM @CatalogEntryToCatalogRelationships AS [catalogEntryToCatalogRelationship]
        WHERE (@ByTarget = 0 AND [catalogEntryToCatalogRelationship].[CatalogEntryId] = @CatalogEntryId)
            OR (@ByTarget = 1 AND [catalogEntryToCatalogRelationship].[CatalogId] = @CatalogId)
    )
    MERGE INTO [dbo].[CatalogEntryToCatalogRelationship] AS [target]
    USING [SourceCatalogEntryToCatalogRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[CatalogId] = [source].[CatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
