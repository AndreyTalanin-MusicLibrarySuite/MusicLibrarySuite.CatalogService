DROP PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogNodeRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogEntryToCatalogNodeRelationships]
(
    @CatalogEntryId UNIQUEIDENTIFIER,
    @CatalogNodeId UNIQUEIDENTIFIER,
    @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogEntryToCatalogNodeRelationship] AS
    (
        SELECT [catalogEntryToCatalogNodeRelationship].*
        FROM @CatalogEntryToCatalogNodeRelationships AS [catalogEntryToCatalogNodeRelationship]
        WHERE (@ByTarget = 0 AND [catalogEntryToCatalogNodeRelationship].[CatalogEntryId] = @CatalogEntryId)
            OR (@ByTarget = 1 AND [catalogEntryToCatalogNodeRelationship].[CatalogNodeId] = @CatalogNodeId)
    )
    MERGE INTO [dbo].[CatalogEntryToCatalogNodeRelationship] AS [target]
    USING [SourceCatalogEntryToCatalogNodeRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[CatalogNodeId] = [source].[CatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
