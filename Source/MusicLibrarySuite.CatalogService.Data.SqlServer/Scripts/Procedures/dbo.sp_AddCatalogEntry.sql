DROP PROCEDURE [dbo].[sp_AddCatalogEntry];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalogEntry]
(
    @CatalogEntry [dbo].[CatalogEntry] READONLY,
    @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY,
    @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY,
    @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY,
    @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY,
    @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY,
    @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryIds ([Value]) SELECT [catalogEntry].[Id] FROM @CatalogEntry AS [catalogEntry];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
        @UpdateMode,
        @CatalogEntry,
        @ResultAddedRows OUTPUT;

    EXEC [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
        @CatalogEntryIds,
        @CatalogEntryToCatalogRelationships;

    EXEC [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal]
        @CatalogEntryIds,
        @CatalogEntryToCatalogNodeRelationships;

    EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
        @CatalogEntryIds,
        @CatalogEntryHierarchicalRelationships;

    EXEC [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
        @CatalogEntryIds,
        @CatalogEntryHierarchicalRelationshipAnnotations;

    EXEC [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] @CatalogEntryIds;

    EXEC [dbo].[sp_MergeCatalogEntryRelationships_Internal]
        @CatalogEntryIds,
        @CatalogEntryRelationships;

    EXEC [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
        @CatalogEntryIds,
        @CatalogEntryRelationshipAnnotations;

    EXEC [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] @CatalogEntryIds;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalogEntry].[CreatedOn],
        @ResultUpdatedOn = [catalogEntry].[UpdatedOn]
    FROM [dbo].[CatalogEntry] AS [catalogEntry]
    INNER JOIN @CatalogEntryIds AS [catalogEntryId] ON [catalogEntryId].[Value] = [catalogEntry].[Id]
        AND @ResultAddedRows > 0;
END;
