DROP PROCEDURE [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal];
GO

CREATE PROCEDURE [dbo].[sp_CheckCatalogEntryHierarchicalRelationshipAnnotations_Internal] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
AS
BEGIN
    DECLARE @CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView AS TABLE
    (
        [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
        [ParentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
        [CatalogEntryHierarchicalRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
        [DuplicationFactor] NCHAR(1) NOT NULL,
        INDEX [UIX_CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView_MultipleColumns] UNIQUE CLUSTERED
            ([CatalogEntryId], [ParentCatalogEntryId], [CatalogEntryHierarchicalRelationshipTypeId])
    );

    INSERT INTO @CatalogEntryHierarchicalRelationshipMissingAnnotationInMemoryView
    (
        [CatalogEntryId],
        [ParentCatalogEntryId],
        [CatalogEntryHierarchicalRelationshipTypeId],
        [DuplicationFactor]
    )
    SELECT
        [catalogEntryHierarchicalRelationship].[CatalogEntryId],
        [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
        [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
        [viewDuplicationSource].[DuplicationFactor]
    FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
    LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [catalogEntryHierarchicalRelationshipAnnotation]
        ON [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
        AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
        AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
    INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
        ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
    INNER JOIN [dbo].[CatalogEntry] AS [catalogEntry]
        ON [catalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
    CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
    WHERE [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] IS NULL
        AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] IS NULL
        AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] IS NULL
        AND [catalogEntryHierarchicalRelationshipType].[Annotated] = 1
        AND [catalogEntry].[Id] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId]);
END;
