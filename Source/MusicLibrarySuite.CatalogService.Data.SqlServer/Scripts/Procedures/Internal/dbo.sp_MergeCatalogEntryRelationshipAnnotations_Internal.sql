DROP PROCEDURE [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryRelationshipAnnotations_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryRelationshipAnnotations [dbo].[CatalogEntryRelationshipAnnotation] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    WITH [SourceCatalogEntryRelationshipAnnotation] AS
    (
        SELECT
            [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryId],
            [sourceCatalogEntryRelationshipAnnotation].[DependentCatalogEntryId],
            [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId],
            [sourceCatalogEntryRelationshipAnnotation].[Name],
            [sourceCatalogEntryRelationshipAnnotation].[Description]
        FROM @CatalogEntryRelationshipAnnotations AS [sourceCatalogEntryRelationshipAnnotation]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryRelationshipAnnotation].[CatalogEntryId]
    )
    MERGE INTO [dbo].[CatalogEntryRelationshipAnnotation] AS [target]
    USING [SourceCatalogEntryRelationshipAnnotation] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
        AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Description] = [source].[Description]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [DependentCatalogEntryId],
        [CatalogEntryRelationshipTypeId],
        [Name],
        [Description]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[DependentCatalogEntryId],
        [source].[CatalogEntryRelationshipTypeId],
        [source].[Name],
        [source].[Description]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;
END;
