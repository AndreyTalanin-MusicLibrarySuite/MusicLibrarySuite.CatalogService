DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal];
GO

CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
(
    @UpdateMode BIT,
    @CatalogEntryHierarchicalRelationshipTypes [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
    @ResultAffectedRows INT OUTPUT
)
AS
BEGIN
    MERGE INTO [dbo].[CatalogEntryHierarchicalRelationshipType] AS [target]
    USING @CatalogEntryHierarchicalRelationshipTypes AS [source]
    ON [target].[Id] = [source].[Id]
    WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Code] = [source].[Code],
        [target].[Annotated] = [source].[Annotated],
        [target].[OwnerUnique] = [source].[OwnerUnique],
        [target].[TargetUnique] = [source].[TargetUnique],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
    (
        [Id],
        [Name],
        [Code],
        [Annotated],
        [OwnerUnique],
        [TargetUnique],
        [Enabled]
    )
    VALUES
    (
        [source].[Id],
        [source].[Name],
        [source].[Code],
        [source].[Annotated],
        [source].[OwnerUnique],
        [source].[TargetUnique],
        [source].[Enabled]
    );

    SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
END;
