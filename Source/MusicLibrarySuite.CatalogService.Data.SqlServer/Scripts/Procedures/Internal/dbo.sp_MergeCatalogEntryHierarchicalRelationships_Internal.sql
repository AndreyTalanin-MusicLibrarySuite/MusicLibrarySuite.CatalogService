DROP PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationships_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryHierarchicalRelationships [dbo].[CatalogEntryHierarchicalRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogEntryHierarchicalRelationship] AS
    (
        SELECT
            [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
            [sourceCatalogEntryHierarchicalRelationship].[Order],
            COALESCE([targetCatalogEntryHierarchicalRelationship].[ReferenceOrder],
                MAX([referenceCatalogEntryHierarchicalRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogEntryHierarchicalRelationship].[Enabled]
        FROM @CatalogEntryHierarchicalRelationships AS [sourceCatalogEntryHierarchicalRelationship]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId]
        LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationship] AS [targetCatalogEntryHierarchicalRelationship]
            ON [targetCatalogEntryHierarchicalRelationship].[CatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId]
            AND [targetCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
            AND [targetCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
        LEFT JOIN [dbo].[CatalogEntryHierarchicalRelationship] AS [referenceCatalogEntryHierarchicalRelationship]
            ON [targetCatalogEntryHierarchicalRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
            AND [referenceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
        GROUP BY
            [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
            [sourceCatalogEntryHierarchicalRelationship].[Order],
            [targetCatalogEntryHierarchicalRelationship].[ReferenceOrder],
            [sourceCatalogEntryHierarchicalRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogEntryHierarchicalRelationship] AS [target]
    USING [SourceCatalogEntryHierarchicalRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
        AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [ParentCatalogEntryId],
        [CatalogEntryHierarchicalRelationshipTypeId],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[ParentCatalogEntryId],
        [source].[CatalogEntryHierarchicalRelationshipTypeId],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;

    WITH [UpdatedCatalogEntryHierarchicalRelationship] AS
    (
        SELECT
            [catalogEntryHierarchicalRelationship].[CatalogEntryId],
            [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
            [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
            ROW_NUMBER() OVER (PARTITION BY [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId], [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
                ORDER BY [catalogEntryHierarchicalRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
    )
    UPDATE [catalogEntryHierarchicalRelationship]
    SET [ReferenceOrder] = [updatedCatalogEntryHierarchicalRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
    INNER JOIN [UpdatedCatalogEntryHierarchicalRelationship] AS [updatedCatalogEntryHierarchicalRelationship]
        ON [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
        AND [updatedCatalogEntryHierarchicalRelationship].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
        AND [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
        ON [catalogEntryId].[Value] = [updatedCatalogEntryHierarchicalRelationship].[CatalogEntryId];
END;
