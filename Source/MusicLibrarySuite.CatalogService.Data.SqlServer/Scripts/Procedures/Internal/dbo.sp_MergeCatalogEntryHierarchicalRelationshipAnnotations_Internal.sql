DROP PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryHierarchicalRelationshipAnnotations_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryHierarchicalRelationshipAnnotations [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] READONLY
)
AS
BEGIN
    SET NOCOUNT ON;

    WITH [SourceCatalogEntryHierarchicalRelationshipAnnotation] AS
    (
        SELECT
            [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId],
            [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId],
            [sourceCatalogEntryHierarchicalRelationshipAnnotation].[Name],
            [sourceCatalogEntryHierarchicalRelationshipAnnotation].[Description]
        FROM @CatalogEntryHierarchicalRelationshipAnnotations AS [sourceCatalogEntryHierarchicalRelationshipAnnotation]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId]
    )
    MERGE INTO [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [target]
    USING [SourceCatalogEntryHierarchicalRelationshipAnnotation] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[ParentCatalogEntryId] = [source].[ParentCatalogEntryId]
        AND [target].[CatalogEntryHierarchicalRelationshipTypeId] = [source].[CatalogEntryHierarchicalRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Description] = [source].[Description]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [ParentCatalogEntryId],
        [CatalogEntryHierarchicalRelationshipTypeId],
        [Name],
        [Description]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[ParentCatalogEntryId],
        [source].[CatalogEntryHierarchicalRelationshipTypeId],
        [source].[Name],
        [source].[Description]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;
END;
