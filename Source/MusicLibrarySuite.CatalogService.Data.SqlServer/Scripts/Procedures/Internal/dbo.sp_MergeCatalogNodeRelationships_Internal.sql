DROP PROCEDURE [dbo].[sp_MergeCatalogNodeRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeRelationships_Internal]
(
    @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogNodeRelationship] AS
    (
        SELECT
            [sourceCatalogNodeRelationship].[CatalogNodeId],
            [sourceCatalogNodeRelationship].[DependentCatalogNodeId],
            [sourceCatalogNodeRelationship].[Name],
            [sourceCatalogNodeRelationship].[Description],
            [sourceCatalogNodeRelationship].[Order],
            COALESCE([targetCatalogNodeRelationship].[ReferenceOrder],
                MAX([referenceCatalogNodeRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogNodeRelationship].[Enabled]
        FROM @CatalogNodeRelationships AS [sourceCatalogNodeRelationship]
        INNER JOIN @CatalogNodeIds AS [catalogNodeId]
            ON [catalogNodeId].[Value] = [sourceCatalogNodeRelationship].[CatalogNodeId]
        LEFT JOIN [dbo].[CatalogNodeRelationship] AS [targetCatalogNodeRelationship]
            ON [targetCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogNodeRelationship].[CatalogNodeId]
            AND [targetCatalogNodeRelationship].[DependentCatalogNodeId] = [sourceCatalogNodeRelationship].[DependentCatalogNodeId]
        LEFT JOIN [dbo].[CatalogNodeRelationship] AS [referenceCatalogNodeRelationship]
            ON [targetCatalogNodeRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogNodeRelationship].[DependentCatalogNodeId] = [sourceCatalogNodeRelationship].[DependentCatalogNodeId]
        GROUP BY
            [sourceCatalogNodeRelationship].[CatalogNodeId],
            [sourceCatalogNodeRelationship].[DependentCatalogNodeId],
            [sourceCatalogNodeRelationship].[Name],
            [sourceCatalogNodeRelationship].[Description],
            [sourceCatalogNodeRelationship].[Order],
            [targetCatalogNodeRelationship].[ReferenceOrder],
            [sourceCatalogNodeRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogNodeRelationship] AS [target]
    USING [SourceCatalogNodeRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[DependentCatalogNodeId] = [source].[DependentCatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Description] = [source].[Description],
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogNodeId],
        [DependentCatalogNodeId],
        [Name],
        [Description],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogNodeId],
        [source].[DependentCatalogNodeId],
        [source].[Name],
        [source].[Description],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
        THEN DELETE;

    WITH [UpdatedCatalogNodeRelationship] AS
    (
        SELECT
            [catalogNodeRelationship].[CatalogNodeId],
            [catalogNodeRelationship].[DependentCatalogNodeId],
            ROW_NUMBER() OVER (PARTITION BY [catalogNodeRelationship].[DependentCatalogNodeId]
                ORDER BY [catalogNodeRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogNodeRelationship] AS [catalogNodeRelationship]
    )
    UPDATE [catalogNodeRelationship]
    SET [ReferenceOrder] = [updatedCatalogNodeRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogNodeRelationship] AS [catalogNodeRelationship]
    INNER JOIN [UpdatedCatalogNodeRelationship] AS [updatedCatalogNodeRelationship]
        ON [updatedCatalogNodeRelationship].[CatalogNodeId] = [catalogNodeRelationship].[CatalogNodeId]
        AND [updatedCatalogNodeRelationship].[DependentCatalogNodeId] = [catalogNodeRelationship].[DependentCatalogNodeId]
    INNER JOIN @CatalogNodeIds AS [catalogNodeId]
        ON [catalogNodeId].[Value] = [updatedCatalogNodeRelationship].[CatalogNodeId];
END;
