DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogNodes_Internal];
GO

CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
(
    @UpdateMode BIT,
    @CatalogNodes [dbo].[CatalogNode] READONLY,
    @ResultAffectedRows INT OUTPUT
)
AS
BEGIN
    MERGE INTO [dbo].[CatalogNode] AS [target]
    USING @CatalogNodes AS [source]
    ON [target].[Id] = [source].[Id]
    WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Description] = [source].[Description],
        [target].[SystemProtected] = [source].[SystemProtected],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
    (
        [Id],
        [Name],
        [Description],
        [SystemProtected],
        [Enabled]
    )
    VALUES
    (
        [source].[Id],
        [source].[Name],
        [source].[Description],
        [source].[SystemProtected],
        [source].[Enabled]
    );

    SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
END;
