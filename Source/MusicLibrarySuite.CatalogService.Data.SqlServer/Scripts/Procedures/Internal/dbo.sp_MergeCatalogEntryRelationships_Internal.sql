DROP PROCEDURE [dbo].[sp_MergeCatalogEntryRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryRelationships_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryRelationships [dbo].[CatalogEntryRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogEntryRelationship] AS
    (
        SELECT
            [sourceCatalogEntryRelationship].[CatalogEntryId],
            [sourceCatalogEntryRelationship].[DependentCatalogEntryId],
            [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId],
            [sourceCatalogEntryRelationship].[Order],
            COALESCE([targetCatalogEntryRelationship].[ReferenceOrder],
                MAX([referenceCatalogEntryRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogEntryRelationship].[Enabled]
        FROM @CatalogEntryRelationships AS [sourceCatalogEntryRelationship]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryRelationship].[CatalogEntryId]
        LEFT JOIN [dbo].[CatalogEntryRelationship] AS [targetCatalogEntryRelationship]
            ON [targetCatalogEntryRelationship].[CatalogEntryId] = [sourceCatalogEntryRelationship].[CatalogEntryId]
            AND [targetCatalogEntryRelationship].[DependentCatalogEntryId] = [sourceCatalogEntryRelationship].[DependentCatalogEntryId]
            AND [targetCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId]
        LEFT JOIN [dbo].[CatalogEntryRelationship] AS [referenceCatalogEntryRelationship]
            ON [targetCatalogEntryRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogEntryRelationship].[DependentCatalogEntryId] = [sourceCatalogEntryRelationship].[DependentCatalogEntryId]
            AND [referenceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId]
        GROUP BY
            [sourceCatalogEntryRelationship].[CatalogEntryId],
            [sourceCatalogEntryRelationship].[DependentCatalogEntryId],
            [sourceCatalogEntryRelationship].[CatalogEntryRelationshipTypeId],
            [sourceCatalogEntryRelationship].[Order],
            [targetCatalogEntryRelationship].[ReferenceOrder],
            [sourceCatalogEntryRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogEntryRelationship] AS [target]
    USING [SourceCatalogEntryRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[DependentCatalogEntryId] = [source].[DependentCatalogEntryId]
        AND [target].[CatalogEntryRelationshipTypeId] = [source].[CatalogEntryRelationshipTypeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [DependentCatalogEntryId],
        [CatalogEntryRelationshipTypeId],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[DependentCatalogEntryId],
        [source].[CatalogEntryRelationshipTypeId],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;

    WITH [UpdatedCatalogEntryRelationship] AS
    (
        SELECT
            [catalogEntryRelationship].[CatalogEntryId],
            [catalogEntryRelationship].[DependentCatalogEntryId],
            [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
            ROW_NUMBER() OVER (PARTITION BY [catalogEntryRelationship].[DependentCatalogEntryId], [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
                ORDER BY [catalogEntryRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
    )
    UPDATE [catalogEntryRelationship]
    SET [ReferenceOrder] = [updatedCatalogEntryRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
    INNER JOIN [UpdatedCatalogEntryRelationship] AS [updatedCatalogEntryRelationship]
        ON [updatedCatalogEntryRelationship].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
        AND [updatedCatalogEntryRelationship].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
        AND [updatedCatalogEntryRelationship].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
    INNER JOIN @CatalogEntryIds AS [catalogEntryId]
        ON [catalogEntryId].[Value] = [updatedCatalogEntryRelationship].[CatalogEntryId];
END;
