DROP PROCEDURE [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
(
    @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogNodeHierarchicalRelationship] AS
    (
        SELECT
            [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId],
            [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
            [sourceCatalogNodeHierarchicalRelationship].[Order],
            COALESCE([targetCatalogNodeHierarchicalRelationship].[ReferenceOrder],
                MAX([referenceCatalogNodeHierarchicalRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder]
        FROM @CatalogNodeHierarchicalRelationships AS [sourceCatalogNodeHierarchicalRelationship]
        INNER JOIN @CatalogNodeIds AS [catalogNodeId]
            ON [catalogNodeId].[Value] = [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId]
        LEFT JOIN [dbo].[CatalogNodeHierarchicalRelationship] AS [targetCatalogNodeHierarchicalRelationship]
            ON [targetCatalogNodeHierarchicalRelationship].[CatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId]
            AND [targetCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
        LEFT JOIN [dbo].[CatalogNodeHierarchicalRelationship] AS [referenceCatalogNodeHierarchicalRelationship]
            ON [targetCatalogNodeHierarchicalRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
        GROUP BY
            [sourceCatalogNodeHierarchicalRelationship].[CatalogNodeId],
            [sourceCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
            [sourceCatalogNodeHierarchicalRelationship].[Order],
            [targetCatalogNodeHierarchicalRelationship].[ReferenceOrder]
    )
    MERGE INTO [dbo].[CatalogNodeHierarchicalRelationship] AS [target]
    USING [SourceCatalogNodeHierarchicalRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[ParentCatalogNodeId] = [source].[ParentCatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogNodeId],
        [ParentCatalogNodeId],
        [Order],
        [ReferenceOrder]
    )
    VALUES
    (
        [source].[CatalogNodeId],
        [source].[ParentCatalogNodeId],
        [source].[Order],
        [source].[ReferenceOrder]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
        THEN DELETE;

    WITH [UpdatedCatalogNodeHierarchicalRelationship] AS
    (
        SELECT
            [catalogNodeHierarchicalRelationship].[CatalogNodeId],
            [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId],
            ROW_NUMBER() OVER (PARTITION BY [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId]
                ORDER BY [catalogNodeHierarchicalRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogNodeHierarchicalRelationship] AS [catalogNodeHierarchicalRelationship]
    )
    UPDATE [catalogNodeHierarchicalRelationship]
    SET [ReferenceOrder] = [updatedCatalogNodeHierarchicalRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogNodeHierarchicalRelationship] AS [catalogNodeHierarchicalRelationship]
    INNER JOIN [UpdatedCatalogNodeHierarchicalRelationship] AS [updatedCatalogNodeHierarchicalRelationship]
        ON [updatedCatalogNodeHierarchicalRelationship].[CatalogNodeId] = [CatalogNodeHierarchicalRelationship].[CatalogNodeId]
        AND [updatedCatalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = [CatalogNodeHierarchicalRelationship].[ParentCatalogNodeId];
END;
