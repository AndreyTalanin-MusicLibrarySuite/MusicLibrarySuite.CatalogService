DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntries_Internal];
GO

CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntries_Internal]
(
    @UpdateMode BIT,
    @CatalogEntries [dbo].[CatalogEntry] READONLY,
    @ResultAffectedRows INT OUTPUT
)
AS
BEGIN
    MERGE INTO [dbo].[CatalogEntry] AS [target]
    USING @CatalogEntries AS [source]
    ON [target].[Id] = [source].[Id]
    WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
    SET
        [target].[SystemProtected] = [source].[SystemProtected],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
    (
        [Id],
        [CatalogEntryTypeId],
        [SystemProtected],
        [Enabled]
    )
    VALUES
    (
        [source].[Id],
        [source].[CatalogEntryTypeId],
        [source].[SystemProtected],
        [source].[Enabled]
    );

    SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
END;
