DROP PROCEDURE [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal];
GO

CREATE PROCEDURE [dbo].[sp_CheckCatalogEntryRelationshipAnnotations_Internal] (@CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY)
AS
BEGIN
    DECLARE @CatalogEntryRelationshipMissingAnnotationInMemoryView AS TABLE
    (
        [CatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
        [DependentCatalogEntryId] UNIQUEIDENTIFIER NOT NULL,
        [CatalogEntryRelationshipTypeId] UNIQUEIDENTIFIER NOT NULL,
        [DuplicationFactor] NCHAR(1) NOT NULL,
        INDEX [UIX_CatalogEntryRelationshipMissingAnnotationInMemoryView_MultipleColumns] UNIQUE CLUSTERED
            ([CatalogEntryId], [DependentCatalogEntryId], [CatalogEntryRelationshipTypeId])
    );

    INSERT INTO @CatalogEntryRelationshipMissingAnnotationInMemoryView
    (
        [CatalogEntryId],
        [DependentCatalogEntryId],
        [CatalogEntryRelationshipTypeId],
        [DuplicationFactor]
    )
    SELECT
        [catalogEntryRelationship].[CatalogEntryId],
        [catalogEntryRelationship].[DependentCatalogEntryId],
        [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
        [viewDuplicationSource].[DuplicationFactor]
    FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
    LEFT JOIN [dbo].[CatalogEntryRelationshipAnnotation] AS [catalogEntryRelationshipAnnotation]
        ON [catalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
        AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
        AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
    INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
        ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
    INNER JOIN [dbo].[CatalogEntry] AS [catalogEntry]
        ON [catalogEntry].[Id] = [catalogEntryRelationship].[CatalogEntryId]
    CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
    WHERE [catalogEntryRelationshipAnnotation].[CatalogEntryId] IS NULL
        AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] IS NULL
        AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] IS NULL
        AND [catalogEntryRelationshipType].[Annotated] = 1
        AND [catalogEntry].[Id] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId]);
END;
