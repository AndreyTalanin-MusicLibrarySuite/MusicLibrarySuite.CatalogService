DROP PROCEDURE [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
(
    @CatalogNodeIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogNodeToCatalogRelationship] AS
    (
        SELECT
            [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId],
            [sourceCatalogNodeToCatalogRelationship].[CatalogId],
            [sourceCatalogNodeToCatalogRelationship].[Order],
            COALESCE([targetCatalogNodeToCatalogRelationship].[ReferenceOrder],
                MAX([referenceCatalogNodeToCatalogRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogNodeToCatalogRelationship].[Enabled]
        FROM @CatalogNodeToCatalogRelationships AS [sourceCatalogNodeToCatalogRelationship]
        INNER JOIN @CatalogNodeIds AS [catalogNodeId]
            ON [catalogNodeId].[Value] = [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId]
        LEFT JOIN [dbo].[CatalogNodeToCatalogRelationship] AS [targetCatalogNodeToCatalogRelationship]
            ON [targetCatalogNodeToCatalogRelationship].[CatalogNodeId] = [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId]
            AND [targetCatalogNodeToCatalogRelationship].[CatalogId] = [sourceCatalogNodeToCatalogRelationship].[CatalogId]
        LEFT JOIN [dbo].[CatalogNodeToCatalogRelationship] AS [referenceCatalogNodeToCatalogRelationship]
            ON [targetCatalogNodeToCatalogRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogNodeToCatalogRelationship].[CatalogId] = [sourceCatalogNodeToCatalogRelationship].[CatalogId]
        GROUP BY
            [sourceCatalogNodeToCatalogRelationship].[CatalogNodeId],
            [sourceCatalogNodeToCatalogRelationship].[CatalogId],
            [sourceCatalogNodeToCatalogRelationship].[Order],
            [targetCatalogNodeToCatalogRelationship].[ReferenceOrder],
            [sourceCatalogNodeToCatalogRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogNodeToCatalogRelationship] AS [target]
    USING [SourceCatalogNodeToCatalogRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[CatalogId] = [source].[CatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogNodeId],
        [CatalogId],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogNodeId],
        [source].[CatalogId],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogNodeId] IN (SELECT [catalogNodeId].[Value] FROM @CatalogNodeIds AS [catalogNodeId])
        THEN DELETE;

    WITH [UpdatedCatalogNodeToCatalogRelationship] AS
    (
        SELECT
            [catalogNodeToCatalogRelationship].[CatalogNodeId],
            [catalogNodeToCatalogRelationship].[CatalogId],
            ROW_NUMBER() OVER (PARTITION BY [catalogNodeToCatalogRelationship].[CatalogId]
                ORDER BY [catalogNodeToCatalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogNodeToCatalogRelationship] AS [catalogNodeToCatalogRelationship]
    )
    UPDATE [catalogNodeToCatalogRelationship]
    SET [ReferenceOrder] = [updatedCatalogNodeToCatalogRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogNodeToCatalogRelationship] AS [catalogNodeToCatalogRelationship]
    INNER JOIN [UpdatedCatalogNodeToCatalogRelationship] AS [updatedCatalogNodeToCatalogRelationship]
        ON [updatedCatalogNodeToCatalogRelationship].[CatalogNodeId] = [catalogNodeToCatalogRelationship].[CatalogNodeId]
        AND [updatedCatalogNodeToCatalogRelationship].[CatalogId] = [catalogNodeToCatalogRelationship].[CatalogId];
END;
