DROP PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogNodeRelationships_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryToCatalogNodeRelationships [dbo].[CatalogEntryToCatalogNodeRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogEntryToCatalogNodeRelationship] AS
    (
        SELECT
            [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId],
            [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId],
            [sourceCatalogEntryToCatalogNodeRelationship].[Order],
            COALESCE([targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder],
                MAX([referenceCatalogEntryToCatalogNodeRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogEntryToCatalogNodeRelationship].[Enabled]
        FROM @CatalogEntryToCatalogNodeRelationships AS [sourceCatalogEntryToCatalogNodeRelationship]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId]
        LEFT JOIN [dbo].[CatalogEntryToCatalogNodeRelationship] AS [targetCatalogEntryToCatalogNodeRelationship]
            ON [targetCatalogEntryToCatalogNodeRelationship].[CatalogEntryId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId]
            AND [targetCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId]
        LEFT JOIN [dbo].[CatalogEntryToCatalogNodeRelationship] AS [referenceCatalogEntryToCatalogNodeRelationship]
            ON [targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId]
        GROUP BY
            [sourceCatalogEntryToCatalogNodeRelationship].[CatalogEntryId],
            [sourceCatalogEntryToCatalogNodeRelationship].[CatalogNodeId],
            [sourceCatalogEntryToCatalogNodeRelationship].[Order],
            [targetCatalogEntryToCatalogNodeRelationship].[ReferenceOrder],
            [sourceCatalogEntryToCatalogNodeRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogEntryToCatalogNodeRelationship] AS [target]
    USING [SourceCatalogEntryToCatalogNodeRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[CatalogNodeId] = [source].[CatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [CatalogNodeId],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[CatalogNodeId],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;

    WITH [UpdatedCatalogEntryToCatalogNodeRelationship] AS
    (
        SELECT
            [catalogEntryToCatalogNodeRelationship].[CatalogEntryId],
            [catalogEntryToCatalogNodeRelationship].[CatalogNodeId],
            ROW_NUMBER() OVER (PARTITION BY [catalogEntryToCatalogNodeRelationship].[CatalogNodeId]
                ORDER BY [catalogEntryToCatalogNodeRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogEntryToCatalogNodeRelationship] AS [catalogEntryToCatalogNodeRelationship]
    )
    UPDATE [catalogEntryToCatalogNodeRelationship]
    SET [ReferenceOrder] = [updatedCatalogEntryToCatalogNodeRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogEntryToCatalogNodeRelationship] AS [catalogEntryToCatalogNodeRelationship]
    INNER JOIN [UpdatedCatalogEntryToCatalogNodeRelationship] AS [updatedCatalogEntryToCatalogNodeRelationship]
        ON [updatedCatalogEntryToCatalogNodeRelationship].[CatalogEntryId] = [catalogEntryToCatalogNodeRelationship].[CatalogEntryId]
        AND [updatedCatalogEntryToCatalogNodeRelationship].[CatalogNodeId] = [catalogEntryToCatalogNodeRelationship].[CatalogNodeId];
END;
