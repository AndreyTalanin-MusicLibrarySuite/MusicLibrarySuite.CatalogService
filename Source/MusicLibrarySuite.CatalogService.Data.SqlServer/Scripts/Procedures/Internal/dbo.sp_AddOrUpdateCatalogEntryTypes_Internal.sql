DROP PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal];
GO

CREATE PROCEDURE [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
(
    @UpdateMode BIT,
    @CatalogEntryTypes [dbo].[CatalogEntryType] READONLY,
    @ResultAffectedRows INT OUTPUT
)
AS
BEGIN
    MERGE INTO [dbo].[CatalogEntryType] AS [target]
    USING @CatalogEntryTypes AS [source]
    ON [target].[Id] = [source].[Id]
    WHEN MATCHED AND @UpdateMode = 1 THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Code] = [source].[Code],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED AND @UpdateMode = 0 THEN INSERT
    (
        [Id],
        [Name],
        [Code],
        [Enabled]
    )
    VALUES
    (
        [source].[Id],
        [source].[Name],
        [source].[Code],
        [source].[Enabled]
    );

    SET @ResultAffectedRows = COALESCE(@ResultAffectedRows, 0) + @@ROWCOUNT;
END;
