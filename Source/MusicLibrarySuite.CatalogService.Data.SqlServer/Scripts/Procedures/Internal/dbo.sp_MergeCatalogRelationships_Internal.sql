DROP PROCEDURE [dbo].[sp_MergeCatalogRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogRelationships_Internal]
(
    @CatalogIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogRelationships [dbo].[CatalogRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogRelationship] AS
    (
        SELECT
            [sourceCatalogRelationship].[CatalogId],
            [sourceCatalogRelationship].[DependentCatalogId],
            [sourceCatalogRelationship].[Name],
            [sourceCatalogRelationship].[Description],
            [sourceCatalogRelationship].[Order],
            COALESCE([targetCatalogRelationship].[ReferenceOrder],
                MAX([referenceCatalogRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogRelationship].[Enabled]
        FROM @CatalogRelationships AS [sourceCatalogRelationship]
        INNER JOIN @CatalogIds AS [catalogId]
            ON [catalogId].[Value] = [sourceCatalogRelationship].[CatalogId]
        LEFT JOIN [dbo].[CatalogRelationship] AS [targetCatalogRelationship]
            ON [targetCatalogRelationship].[CatalogId] = [sourceCatalogRelationship].[CatalogId]
            AND [targetCatalogRelationship].[DependentCatalogId] = [sourceCatalogRelationship].[DependentCatalogId]
        LEFT JOIN [dbo].[CatalogRelationship] AS [referenceCatalogRelationship]
            ON [targetCatalogRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogRelationship].[DependentCatalogId] = [sourceCatalogRelationship].[DependentCatalogId]
        GROUP BY
            [sourceCatalogRelationship].[CatalogId],
            [sourceCatalogRelationship].[DependentCatalogId],
            [sourceCatalogRelationship].[Name],
            [sourceCatalogRelationship].[Description],
            [sourceCatalogRelationship].[Order],
            [targetCatalogRelationship].[ReferenceOrder],
            [sourceCatalogRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogRelationship] AS [target]
    USING [SourceCatalogRelationship] AS [source]
    ON [target].[CatalogId] = [source].[CatalogId]
        AND [target].[DependentCatalogId] = [source].[DependentCatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Name] = [source].[Name],
        [target].[Description] = [source].[Description],
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogId],
        [DependentCatalogId],
        [Name],
        [Description],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogId],
        [source].[DependentCatalogId],
        [source].[Name],
        [source].[Description],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogId] IN (SELECT [catalogId].[Value] FROM @CatalogIds AS [catalogId])
        THEN DELETE;

    WITH [UpdatedCatalogRelationship] AS
    (
        SELECT
            [catalogRelationship].[CatalogId],
            [catalogRelationship].[DependentCatalogId],
            ROW_NUMBER() OVER (PARTITION BY [catalogRelationship].[DependentCatalogId]
                ORDER BY [catalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogRelationship] AS [catalogRelationship]
    )
    UPDATE [catalogRelationship]
    SET [ReferenceOrder] = [updatedCatalogRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogRelationship] AS [catalogRelationship]
    INNER JOIN [UpdatedCatalogRelationship] AS [updatedCatalogRelationship]
        ON [updatedCatalogRelationship].[CatalogId] = [catalogRelationship].[CatalogId]
        AND [updatedCatalogRelationship].[DependentCatalogId] = [catalogRelationship].[DependentCatalogId]
    INNER JOIN @CatalogIds AS [catalogId]
        ON [catalogId].[Value] = [updatedCatalogRelationship].[CatalogId];
END;
