DROP PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal];
GO

CREATE PROCEDURE [dbo].[sp_MergeCatalogEntryToCatalogRelationships_Internal]
(
    @CatalogEntryIds [ctt].[GuidUnorderedSet] READONLY,
    @CatalogEntryToCatalogRelationships [dbo].[CatalogEntryToCatalogRelationship] READONLY
)
AS
BEGIN
    WITH [SourceCatalogEntryToCatalogRelationship] AS
    (
        SELECT
            [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId],
            [sourceCatalogEntryToCatalogRelationship].[CatalogId],
            [sourceCatalogEntryToCatalogRelationship].[Order],
            COALESCE([targetCatalogEntryToCatalogRelationship].[ReferenceOrder],
                MAX([referenceCatalogEntryToCatalogRelationship].[ReferenceOrder]) + 1,
                0) AS [ReferenceOrder],
            [sourceCatalogEntryToCatalogRelationship].[Enabled]
        FROM @CatalogEntryToCatalogRelationships AS [sourceCatalogEntryToCatalogRelationship]
        INNER JOIN @CatalogEntryIds AS [catalogEntryId]
            ON [catalogEntryId].[Value] = [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId]
        LEFT JOIN [dbo].[CatalogEntryToCatalogRelationship] AS [targetCatalogEntryToCatalogRelationship]
            ON [targetCatalogEntryToCatalogRelationship].[CatalogEntryId] = [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId]
            AND [targetCatalogEntryToCatalogRelationship].[CatalogId] = [sourceCatalogEntryToCatalogRelationship].[CatalogId]
        LEFT JOIN [dbo].[CatalogEntryToCatalogRelationship] AS [referenceCatalogEntryToCatalogRelationship]
            ON [targetCatalogEntryToCatalogRelationship].[ReferenceOrder] IS NULL
            AND [referenceCatalogEntryToCatalogRelationship].[CatalogId] = [sourceCatalogEntryToCatalogRelationship].[CatalogId]
        GROUP BY
            [sourceCatalogEntryToCatalogRelationship].[CatalogEntryId],
            [sourceCatalogEntryToCatalogRelationship].[CatalogId],
            [sourceCatalogEntryToCatalogRelationship].[Order],
            [targetCatalogEntryToCatalogRelationship].[ReferenceOrder],
            [sourceCatalogEntryToCatalogRelationship].[Enabled]
    )
    MERGE INTO [dbo].[CatalogEntryToCatalogRelationship] AS [target]
    USING [SourceCatalogEntryToCatalogRelationship] AS [source]
    ON [target].[CatalogEntryId] = [source].[CatalogEntryId]
        AND [target].[CatalogId] = [source].[CatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = [source].[Order],
        [target].[Enabled] = [source].[Enabled]
    WHEN NOT MATCHED THEN INSERT
    (
        [CatalogEntryId],
        [CatalogId],
        [Order],
        [ReferenceOrder],
        [Enabled]
    )
    VALUES
    (
        [source].[CatalogEntryId],
        [source].[CatalogId],
        [source].[Order],
        [source].[ReferenceOrder],
        [source].[Enabled]
    )
    WHEN NOT MATCHED BY SOURCE
        AND [target].[CatalogEntryId] IN (SELECT [catalogEntryId].[Value] FROM @CatalogEntryIds AS [catalogEntryId])
        THEN DELETE;

    WITH [UpdatedCatalogEntryToCatalogRelationship] AS
    (
        SELECT
            [catalogEntryToCatalogRelationship].[CatalogEntryId],
            [catalogEntryToCatalogRelationship].[CatalogId],
            ROW_NUMBER() OVER (PARTITION BY [catalogEntryToCatalogRelationship].[CatalogId]
                ORDER BY [catalogEntryToCatalogRelationship].[ReferenceOrder]) - 1 AS [UpdatedReferenceOrder]
        FROM [dbo].[CatalogEntryToCatalogRelationship] AS [catalogEntryToCatalogRelationship]
    )
    UPDATE [catalogEntryToCatalogRelationship]
    SET [ReferenceOrder] = [updatedCatalogEntryToCatalogRelationship].[UpdatedReferenceOrder]
    FROM [dbo].[CatalogEntryToCatalogRelationship] AS [catalogEntryToCatalogRelationship]
    INNER JOIN [UpdatedCatalogEntryToCatalogRelationship] AS [updatedCatalogEntryToCatalogRelationship]
        ON [updatedCatalogEntryToCatalogRelationship].[CatalogEntryId] = [catalogEntryToCatalogRelationship].[CatalogEntryId]
        AND [updatedCatalogEntryToCatalogRelationship].[CatalogId] = [catalogEntryToCatalogRelationship].[CatalogId];
END;
