DROP PROCEDURE [dbo].[sp_AddCatalogEntryRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalogEntryRelationshipType]
(
    @CatalogEntryRelationshipType [dbo].[CatalogEntryRelationshipType] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryRelationshipTypeIds ([Value])
    SELECT [catalogEntryRelationshipType].[Id]
    FROM @CatalogEntryRelationshipType AS [catalogEntryRelationshipType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryRelationshipTypes_Internal]
        @UpdateMode,
        @CatalogEntryRelationshipType,
        @ResultAddedRows OUTPUT;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalogEntryRelationshipType].[CreatedOn],
        @ResultUpdatedOn = [catalogEntryRelationshipType].[UpdatedOn]
    FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    INNER JOIN @CatalogEntryRelationshipTypeIds AS [catalogEntryRelationshipTypeId]
        ON [catalogEntryRelationshipTypeId].[Value] = [catalogEntryRelationshipType].[Id]
        AND @ResultAddedRows > 0;
END;
