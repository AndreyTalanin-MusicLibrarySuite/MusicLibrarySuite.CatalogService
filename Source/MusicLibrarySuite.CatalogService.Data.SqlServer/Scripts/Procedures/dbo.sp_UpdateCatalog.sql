DROP PROCEDURE [dbo].[sp_UpdateCatalog];
GO

CREATE PROCEDURE [dbo].[sp_UpdateCatalog]
(
    @Catalog [dbo].[Catalog] READONLY,
    @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogIds ([Value]) SELECT [catalog].[Id] FROM @Catalog AS [catalog];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 1;
    EXEC [dbo].[sp_AddOrUpdateCatalogs_Internal]
        @UpdateMode,
        @Catalog,
        @ResultUpdatedRows OUTPUT;

    EXEC [dbo].[sp_MergeCatalogRelationships_Internal]
        @CatalogIds,
        @CatalogRelationships;

    COMMIT TRANSACTION;
END;
