DROP PROCEDURE [dbo].[sp_ReorderCatalogRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogRelationships]
(
    @CatalogId UNIQUEIDENTIFIER,
    @DependentCatalogId UNIQUEIDENTIFIER,
    @CatalogRelationships [dbo].[CatalogRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogRelationship] AS
    (
        SELECT [catalogRelationship].*
        FROM @CatalogRelationships AS [catalogRelationship]
        WHERE (@ByTarget = 0 AND [catalogRelationship].[CatalogId] = @CatalogId)
            OR (@ByTarget = 1 AND [catalogRelationship].[DependentCatalogId] = @DependentCatalogId)
    )
    MERGE INTO [dbo].[CatalogRelationship] AS [target]
    USING [SourceCatalogRelationship] AS [source]
    ON [target].[CatalogId] = [source].[CatalogId]
        AND [target].[DependentCatalogId] = [source].[DependentCatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
