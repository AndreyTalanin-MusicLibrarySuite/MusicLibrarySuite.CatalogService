DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryType];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryType]
(
    @Id UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[CatalogEntryType]
    FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
    WHERE [catalogEntryType].[Id] = @Id;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
