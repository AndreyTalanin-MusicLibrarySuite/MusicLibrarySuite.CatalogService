DROP PROCEDURE [dbo].[sp_AddCatalogEntryType];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalogEntryType]
(
    @CatalogEntryType [dbo].[CatalogEntryType] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryTypeIds ([Value]) SELECT [catalogEntryType].[Id] FROM @CatalogEntryType AS [catalogEntryType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryTypes_Internal]
        @UpdateMode,
        @CatalogEntryType,
        @ResultAddedRows OUTPUT;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalogEntryType].[CreatedOn],
        @ResultUpdatedOn = [catalogEntryType].[UpdatedOn]
    FROM [dbo].[CatalogEntryType] AS [catalogEntryType]
    INNER JOIN @CatalogEntryTypeIds AS [catalogEntryTypeId] ON [catalogEntryTypeId].[Value] = [catalogEntryType].[Id]
        AND @ResultAddedRows > 0;
END;
