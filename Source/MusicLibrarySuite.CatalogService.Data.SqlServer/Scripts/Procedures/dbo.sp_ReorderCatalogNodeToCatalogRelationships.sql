DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeToCatalogRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeToCatalogRelationships]
(
    @CatalogNodeId UNIQUEIDENTIFIER,
    @CatalogId UNIQUEIDENTIFIER,
    @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogNodeToCatalogRelationship] AS
    (
        SELECT [catalogNodeToCatalogRelationship].*
        FROM @CatalogNodeToCatalogRelationships AS [catalogNodeToCatalogRelationship]
        WHERE (@ByTarget = 0 AND [catalogNodeToCatalogRelationship].[CatalogNodeId] = @CatalogNodeId)
            OR (@ByTarget = 1 AND [catalogNodeToCatalogRelationship].[CatalogId] = @CatalogId)
    )
    MERGE INTO [dbo].[CatalogNodeToCatalogRelationship] AS [target]
    USING [SourceCatalogNodeToCatalogRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[CatalogId] = [source].[CatalogId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
