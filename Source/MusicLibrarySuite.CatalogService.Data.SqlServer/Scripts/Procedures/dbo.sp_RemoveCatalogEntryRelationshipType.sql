DROP PROCEDURE [dbo].[sp_RemoveCatalogEntryRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalogEntryRelationshipType]
(
    @Id UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[CatalogEntryRelationshipType]
    FROM [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    WHERE [catalogEntryRelationshipType].[Id] = @Id;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
