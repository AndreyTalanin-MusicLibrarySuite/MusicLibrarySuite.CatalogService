DROP PROCEDURE [dbo].[sp_RemoveCatalogNode];
GO

CREATE PROCEDURE [dbo].[sp_RemoveCatalogNode]
(
    @CatalogNodeId UNIQUEIDENTIFIER,
    @ResultRemovedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DELETE [dbo].[CatalogNode]
    FROM [dbo].[CatalogNode] AS [catalogNode]
    WHERE [catalogNode].[Id] = @CatalogNodeId;

    SET @ResultRemovedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
