DROP PROCEDURE [dbo].[sp_AddCatalogNode];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalogNode]
(
    @CatalogNode [dbo].[CatalogNode] READONLY,
    @CatalogNodeToCatalogRelationships [dbo].[CatalogNodeToCatalogRelationship] READONLY,
    @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
    @CatalogNodeRelationships [dbo].[CatalogNodeRelationship] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogNodeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogNodeIds ([Value]) SELECT [catalogNode].[Id] FROM @CatalogNode AS [catalogNode];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogNodes_Internal]
        @UpdateMode,
        @CatalogNode,
        @ResultAddedRows OUTPUT;

    EXEC [dbo].[sp_MergeCatalogNodeToCatalogRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeToCatalogRelationships;

    EXEC [dbo].[sp_MergeCatalogNodeHierarchicalRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeHierarchicalRelationships;

    EXEC [dbo].[sp_MergeCatalogNodeRelationships_Internal]
        @CatalogNodeIds,
        @CatalogNodeRelationships;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalogNode].[CreatedOn],
        @ResultUpdatedOn = [catalogNode].[UpdatedOn]
    FROM [dbo].[CatalogNode] AS [catalogNode]
    INNER JOIN @CatalogNodeIds AS [catalogNodeId] ON [catalogNodeId].[Value] = [catalogNode].[Id]
        AND @ResultAddedRows > 0;
END;
