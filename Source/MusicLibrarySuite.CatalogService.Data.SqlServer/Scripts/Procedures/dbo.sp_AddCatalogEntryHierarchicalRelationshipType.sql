DROP PROCEDURE [dbo].[sp_AddCatalogEntryHierarchicalRelationshipType];
GO

CREATE PROCEDURE [dbo].[sp_AddCatalogEntryHierarchicalRelationshipType]
(
    @CatalogEntryHierarchicalRelationshipType [dbo].[CatalogEntryHierarchicalRelationshipType] READONLY,
    @ResultCreatedOn DATETIMEOFFSET OUTPUT,
    @ResultUpdatedOn DATETIMEOFFSET OUTPUT,
    @ResultAddedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CatalogEntryHierarchicalRelationshipTypeIds AS [ctt].[GuidUnorderedSet];
    INSERT INTO @CatalogEntryHierarchicalRelationshipTypeIds ([Value])
    SELECT [catalogEntryHierarchicalRelationshipType].[Id]
    FROM @CatalogEntryHierarchicalRelationshipType AS [catalogEntryHierarchicalRelationshipType];

    BEGIN TRANSACTION;

    DECLARE @UpdateMode AS BIT = 0;
    EXEC [dbo].[sp_AddOrUpdateCatalogEntryHierarchicalRelationshipTypes_Internal]
        @UpdateMode,
        @CatalogEntryHierarchicalRelationshipType,
        @ResultAddedRows OUTPUT;

    COMMIT TRANSACTION;

    SELECT TOP (1)
        @ResultCreatedOn = [catalogEntryHierarchicalRelationshipType].[CreatedOn],
        @ResultUpdatedOn = [catalogEntryHierarchicalRelationshipType].[UpdatedOn]
    FROM [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    INNER JOIN @CatalogEntryHierarchicalRelationshipTypeIds AS [catalogEntryHierarchicalRelationshipTypeId]
        ON [catalogEntryHierarchicalRelationshipTypeId].[Value] = [catalogEntryHierarchicalRelationshipType].[Id]
        AND @ResultAddedRows > 0;
END;
