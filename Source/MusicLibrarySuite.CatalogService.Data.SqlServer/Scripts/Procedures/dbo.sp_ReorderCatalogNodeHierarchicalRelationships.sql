DROP PROCEDURE [dbo].[sp_ReorderCatalogNodeHierarchicalRelationships];
GO

CREATE PROCEDURE [dbo].[sp_ReorderCatalogNodeHierarchicalRelationships]
(
    @CatalogNodeId UNIQUEIDENTIFIER,
    @ParentCatalogNodeId UNIQUEIDENTIFIER,
    @CatalogNodeHierarchicalRelationships [dbo].[CatalogNodeHierarchicalRelationship] READONLY,
    @ByTarget BIT,
    @ResultUpdatedRows INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    WITH [SourceCatalogNodeHierarchicalRelationship] AS
    (
        SELECT [catalogNodeHierarchicalRelationship].*
        FROM @CatalogNodeHierarchicalRelationships AS [catalogNodeHierarchicalRelationship]
        WHERE (@ByTarget = 0 AND [catalogNodeHierarchicalRelationship].[CatalogNodeId] = @CatalogNodeId)
            OR (@ByTarget = 1 AND [catalogNodeHierarchicalRelationship].[ParentCatalogNodeId] = @ParentCatalogNodeId)
    )
    MERGE INTO [dbo].[CatalogNodeHierarchicalRelationship] AS [target]
    USING [SourceCatalogNodeHierarchicalRelationship] AS [source]
    ON [target].[CatalogNodeId] = [source].[CatalogNodeId]
        AND [target].[ParentCatalogNodeId] = [source].[ParentCatalogNodeId]
    WHEN MATCHED THEN UPDATE
    SET
        [target].[Order] = CASE WHEN @ByTarget = 0 THEN [source].[Order] ELSE [target].[Order] END,
        [target].[ReferenceOrder] = CASE WHEN @ByTarget = 1 THEN [source].[ReferenceOrder] ELSE [target].[ReferenceOrder] END;

    SET @ResultUpdatedRows = @@ROWCOUNT;

    COMMIT TRANSACTION;
END;
