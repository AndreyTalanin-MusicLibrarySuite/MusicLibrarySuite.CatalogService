DROP VIEW [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView];
GO

CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryRelationship].[CatalogEntryId],
    [catalogEntryRelationship].[DependentCatalogEntryId],
    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntry] AS [ownerCatalogEntry]
    ON [ownerCatalogEntry].[Id] = [catalogEntryRelationship].[CatalogEntryId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryRelationshipType].[OwnerCatalogEntryTypeId] <> [ownerCatalogEntry].[CatalogEntryTypeId];
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns]
ON [dbo].[CatalogEntryRelationshipInvalidOwnerCatalogEntryTypeView]
(
    [CatalogEntryId],
    [DependentCatalogEntryId],
    [CatalogEntryRelationshipTypeId]
);
