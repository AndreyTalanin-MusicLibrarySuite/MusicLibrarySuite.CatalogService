DROP VIEW [dbo].[CatalogEntryRelationshipInvalidAnnotationView];
GO

CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidAnnotationView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryRelationship].[CatalogEntryId],
    [catalogEntryRelationship].[DependentCatalogEntryId],
    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
INNER JOIN [dbo].[CatalogEntryRelationshipAnnotation] AS [catalogEntryRelationshipAnnotation]
    ON [catalogEntryRelationshipAnnotation].[CatalogEntryId] = [catalogEntryRelationship].[CatalogEntryId]
    AND [catalogEntryRelationshipAnnotation].[DependentCatalogEntryId] = [catalogEntryRelationship].[DependentCatalogEntryId]
    AND [catalogEntryRelationshipAnnotation].[CatalogEntryRelationshipTypeId] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryRelationshipType].[Annotated] = 0;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidAnnotationView_MultipleColumns]
ON [dbo].[CatalogEntryRelationshipInvalidAnnotationView]
(
    [CatalogEntryId],
    [DependentCatalogEntryId],
    [CatalogEntryRelationshipTypeId]
);
