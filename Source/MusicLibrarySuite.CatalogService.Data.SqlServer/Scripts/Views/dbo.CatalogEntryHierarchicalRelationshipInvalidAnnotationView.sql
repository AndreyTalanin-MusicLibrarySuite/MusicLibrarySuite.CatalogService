DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView];
GO

CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipAnnotation] AS [catalogEntryHierarchicalRelationshipAnnotation]
    ON [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryId] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
    AND [catalogEntryHierarchicalRelationshipAnnotation].[ParentCatalogEntryId] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
    AND [catalogEntryHierarchicalRelationshipAnnotation].[CatalogEntryHierarchicalRelationshipTypeId] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryHierarchicalRelationshipType].[Annotated] = 0;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidAnnotationView_MultipleColumns]
ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidAnnotationView]
(
    [CatalogEntryId],
    [ParentCatalogEntryId],
    [CatalogEntryHierarchicalRelationshipTypeId]
);
