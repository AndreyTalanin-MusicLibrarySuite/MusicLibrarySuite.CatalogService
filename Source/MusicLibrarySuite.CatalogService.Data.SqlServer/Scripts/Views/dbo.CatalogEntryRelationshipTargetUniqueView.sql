DROP VIEW [dbo].[CatalogEntryRelationshipTargetUniqueView];
GO

CREATE VIEW [dbo].[CatalogEntryRelationshipTargetUniqueView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryRelationship].[CatalogEntryId],
    [catalogEntryRelationship].[DependentCatalogEntryId],
    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
WHERE [catalogEntryRelationshipType].[TargetUnique] = 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipTargetUniqueView_MultipleColumns]
ON [dbo].[CatalogEntryRelationshipTargetUniqueView]
(
    [DependentCatalogEntryId],
    [CatalogEntryRelationshipTypeId]
);
