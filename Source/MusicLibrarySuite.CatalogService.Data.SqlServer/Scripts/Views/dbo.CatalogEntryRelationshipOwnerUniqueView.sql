DROP VIEW [dbo].[CatalogEntryRelationshipOwnerUniqueView];
GO

CREATE VIEW [dbo].[CatalogEntryRelationshipOwnerUniqueView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryRelationship].[CatalogEntryId],
    [catalogEntryRelationship].[DependentCatalogEntryId],
    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
WHERE [catalogEntryRelationshipType].[OwnerUnique] = 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipOwnerUniqueView_MultipleColumns]
ON [dbo].[CatalogEntryRelationshipOwnerUniqueView]
(
    [CatalogEntryId],
    [CatalogEntryRelationshipTypeId]
);
