DROP VIEW [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView];
GO

CREATE VIEW [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryRelationship].[CatalogEntryId],
    [catalogEntryRelationship].[DependentCatalogEntryId],
    [catalogEntryRelationship].[CatalogEntryRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryRelationship] AS [catalogEntryRelationship]
INNER JOIN [dbo].[CatalogEntryRelationshipType] AS [catalogEntryRelationshipType]
    ON [catalogEntryRelationshipType].[Id] = [catalogEntryRelationship].[CatalogEntryRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntry] AS [targetCatalogEntry]
    ON [targetCatalogEntry].[Id] = [catalogEntryRelationship].[DependentCatalogEntryId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryRelationshipType].[TargetCatalogEntryTypeId] <> [targetCatalogEntry].[CatalogEntryTypeId];
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns]
ON [dbo].[CatalogEntryRelationshipInvalidTargetCatalogEntryTypeView]
(
    [CatalogEntryId],
    [DependentCatalogEntryId],
    [CatalogEntryRelationshipTypeId]
);
