DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView];
GO

CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntry] AS [ownerCatalogEntry]
    ON [ownerCatalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryHierarchicalRelationshipType].[OwnerCatalogEntryTypeId] <> [ownerCatalogEntry].[CatalogEntryTypeId];
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView_MultipleColumns]
ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidOwnerCatalogEntryTypeView]
(
    [CatalogEntryId],
    [ParentCatalogEntryId],
    [CatalogEntryHierarchicalRelationshipTypeId]
);
