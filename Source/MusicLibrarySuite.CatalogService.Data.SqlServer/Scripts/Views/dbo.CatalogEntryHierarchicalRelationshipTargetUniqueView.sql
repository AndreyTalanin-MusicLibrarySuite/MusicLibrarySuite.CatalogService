DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView];
GO

CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
WHERE [catalogEntryHierarchicalRelationshipType].[TargetUnique] = 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipTargetUniqueView_MultipleColumns]
ON [dbo].[CatalogEntryHierarchicalRelationshipTargetUniqueView]
(
    [ParentCatalogEntryId],
    [CatalogEntryHierarchicalRelationshipTypeId]
);
