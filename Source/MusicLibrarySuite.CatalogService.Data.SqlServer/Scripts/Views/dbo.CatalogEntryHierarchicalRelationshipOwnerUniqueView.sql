DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView];
GO

CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
WHERE [catalogEntryHierarchicalRelationshipType].[OwnerUnique] = 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipOwnerUniqueView_MultipleColumns]
ON [dbo].[CatalogEntryHierarchicalRelationshipOwnerUniqueView]
(
    [CatalogEntryId],
    [CatalogEntryHierarchicalRelationshipTypeId]
);
