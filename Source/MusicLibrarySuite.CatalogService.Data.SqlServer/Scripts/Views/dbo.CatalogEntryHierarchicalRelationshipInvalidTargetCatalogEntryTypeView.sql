DROP VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView];
GO

CREATE VIEW [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView]
WITH SCHEMABINDING
AS
SELECT
    [catalogEntryHierarchicalRelationship].[CatalogEntryId],
    [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId],
    [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId],
    [viewDuplicationSource].[DuplicationFactor]
FROM [dbo].[CatalogEntryHierarchicalRelationship] AS [catalogEntryHierarchicalRelationship]
INNER JOIN [dbo].[CatalogEntryHierarchicalRelationshipType] AS [catalogEntryHierarchicalRelationshipType]
    ON [catalogEntryHierarchicalRelationshipType].[Id] = [catalogEntryHierarchicalRelationship].[CatalogEntryHierarchicalRelationshipTypeId]
INNER JOIN [dbo].[CatalogEntry] AS [targetCatalogEntry]
    ON [targetCatalogEntry].[Id] = [catalogEntryHierarchicalRelationship].[ParentCatalogEntryId]
CROSS JOIN [utils].[ViewDuplicationSource] AS [viewDuplicationSource]
WHERE [catalogEntryHierarchicalRelationshipType].[TargetCatalogEntryTypeId] <> [targetCatalogEntry].[CatalogEntryTypeId];
GO

CREATE UNIQUE CLUSTERED INDEX [UIX_CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView_MultipleColumns]
ON [dbo].[CatalogEntryHierarchicalRelationshipInvalidTargetCatalogEntryTypeView]
(
    [CatalogEntryId],
    [ParentCatalogEntryId],
    [CatalogEntryHierarchicalRelationshipTypeId]
);
