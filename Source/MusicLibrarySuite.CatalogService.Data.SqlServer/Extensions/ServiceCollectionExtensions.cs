using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Repositories;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Contexts;
using MusicLibrarySuite.CatalogService.Data.SqlServer.Repositories;
using MusicLibrarySuite.Infrastructure.Data.Extensions;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.SqlServer.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers the <see cref="CatalogServiceDbContext" /> database context,
    /// the <see cref="IDbContextFactory{TContext}" /> database context factory,
    /// the <see cref="IDbContextScope{TContext}" /> database context scope,
    /// the <see cref="IDbContextScopeFactory{TContext}" /> database context scope factory,
    /// and the <see cref="IDbContextProvider{TContext}" /> database context provider
    /// as services in the specified <see cref="IServiceCollection" /> collection.
    /// <para>
    /// The <see cref="SqlServerCatalogServiceDbContext" /> implementation type is used for the <see cref="CatalogServiceDbContext" /> service type.
    /// </para>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <param name="configuration">The <see cref="IConfiguration" /> application configuration.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">When there is no <c>CatalogServiceConnectionString</c> connection string.</exception>
    public static IServiceCollection AddSqlServerDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContextFactory<CatalogServiceDbContext, SqlServerCatalogServiceDbContext>(contextOptions =>
        {
            string connectionStringName = "CatalogServiceConnectionString";

            string? connectionString = configuration.GetConnectionString(connectionStringName);
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new InvalidOperationException($"Unable to get the connection string using the \"{connectionStringName}\" configuration key.");
            }

            contextOptions.UseSqlServer(connectionString, sqlServerOptions =>
            {
                sqlServerOptions
                    .MigrationsAssembly(typeof(SqlServerCatalogServiceDbContext).Assembly.FullName)
                    .MigrationsHistoryTable("MigrationsHistory", "efc")
                    .CommandTimeout(120);
            });
        });

        return services;
    }

    /// <summary>
    /// Registers the CMS (Content Management System) repositories in the specified <see cref="IServiceCollection" /> collection.
    /// <para>
    /// The <see cref="SqlServerCatalogServiceDbContext" /> implementation type is expected for the <see cref="CatalogServiceDbContext" /> service type.
    /// </para>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddSqlServerCmsRepositories(this IServiceCollection services)
    {
        services.AddTransient<ICatalogRepository, SqlServerCatalogRepository>();
        services.AddTransient<ICatalogRelationshipRepositoryExtension, SqlServerCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogRepositoryFacade, CatalogRepositoryFacade>();

        services.AddTransient<ICatalogNodeRepository, SqlServerCatalogNodeRepository>();
        services.AddTransient<ICatalogNodeToCatalogRelationshipRepositoryExtension, SqlServerCatalogNodeToCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeHierarchicalRelationshipRepositoryExtension, SqlServerCatalogNodeHierarchicalRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeRelationshipRepositoryExtension, SqlServerCatalogNodeRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeRepositoryFacade, CatalogNodeRepositoryFacade>();

        services.AddTransient<ICatalogEntryRepository, SqlServerCatalogEntryRepository>();
        services.AddTransient<ICatalogEntryToCatalogRelationshipRepositoryExtension, SqlServerCatalogEntryToCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryToCatalogNodeRelationshipRepositoryExtension, SqlServerCatalogEntryToCatalogNodeRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipRepositoryExtension, SqlServerCatalogEntryHierarchicalRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension, SqlServerCatalogEntryHierarchicalRelationshipTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryRelationshipRepositoryExtension, SqlServerCatalogEntryRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryRelationshipTypeRepositoryExtension, SqlServerCatalogEntryRelationshipTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryTypeRepositoryExtension, SqlServerCatalogEntryTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryRepositoryFacade, CatalogEntryRepositoryFacade>();

        return services;
    }
}
