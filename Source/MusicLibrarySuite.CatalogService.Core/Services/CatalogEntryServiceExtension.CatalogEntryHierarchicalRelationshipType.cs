using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> service model.
/// </summary>
public class CatalogEntryHierarchicalRelationshipTypeServiceExtension : ICatalogEntryHierarchicalRelationshipTypeServiceExtension
{
    private readonly ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryHierarchicalRelationshipTypeServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryHierarchicalRelationshipType" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryHierarchicalRelationshipTypeServiceExtension(
        ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension catalogEntryHierarchicalRelationshipTypeRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension = catalogEntryHierarchicalRelationshipTypeRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipType? catalogEntryHierarchicalRelationshipType =
            await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeId, cancellationToken);
        CatalogEntryHierarchicalRelationshipTypeModel? catalogEntryHierarchicalRelationshipTypeModel =
            m_mapper.Map<CatalogEntryHierarchicalRelationshipTypeModel?>(catalogEntryHierarchicalRelationshipType);
        return catalogEntryHierarchicalRelationshipTypeModel;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipType =
            await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIds, cancellationToken);
        CatalogEntryHierarchicalRelationshipTypeModel[] catalogEntryHierarchicalRelationshipTypeModels =
            m_mapper.Map<CatalogEntryHierarchicalRelationshipTypeModel[]>(catalogEntryHierarchicalRelationshipType);
        return catalogEntryHierarchicalRelationshipTypeModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationshipType[] catalogEntryHierarchicalRelationshipType =
            await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(cancellationToken);
        CatalogEntryHierarchicalRelationshipTypeModel[] catalogEntryHierarchicalRelationshipTypeModels =
            m_mapper.Map<CatalogEntryHierarchicalRelationshipTypeModel[]>(catalogEntryHierarchicalRelationshipType);
        return catalogEntryHierarchicalRelationshipTypeModels;
    }
}
