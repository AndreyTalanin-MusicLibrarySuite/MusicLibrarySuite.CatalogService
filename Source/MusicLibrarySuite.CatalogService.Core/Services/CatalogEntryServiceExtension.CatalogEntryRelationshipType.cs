using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryRelationshipTypeModel" /> service model.
/// </summary>
public class CatalogEntryRelationshipTypeServiceExtension : ICatalogEntryRelationshipTypeServiceExtension
{
    private readonly ICatalogEntryRelationshipTypeRepositoryExtension m_catalogEntryRelationshipTypeRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRelationshipTypeServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryRelationshipType" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryRelationshipTypeServiceExtension(
        ICatalogEntryRelationshipTypeRepositoryExtension catalogEntryRelationshipTypeRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryRelationshipTypeRepositoryExtension = catalogEntryRelationshipTypeRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipType? catalogEntryRelationshipType =
            await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypeAsync(catalogEntryRelationshipTypeId, cancellationToken);
        CatalogEntryRelationshipTypeModel? catalogEntryRelationshipTypeModel =
            m_mapper.Map<CatalogEntryRelationshipTypeModel?>(catalogEntryRelationshipType);
        return catalogEntryRelationshipTypeModel;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipType[] catalogEntryRelationshipType =
            await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeIds, cancellationToken);
        CatalogEntryRelationshipTypeModel[] catalogEntryRelationshipTypeModels =
            m_mapper.Map<CatalogEntryRelationshipTypeModel[]>(catalogEntryRelationshipType);
        return catalogEntryRelationshipTypeModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipTypeModel[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationshipType[] catalogEntryRelationshipType =
            await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypesAsync(cancellationToken);
        CatalogEntryRelationshipTypeModel[] catalogEntryRelationshipTypeModels =
            m_mapper.Map<CatalogEntryRelationshipTypeModel[]>(catalogEntryRelationshipType);
        return catalogEntryRelationshipTypeModels;
    }
}
