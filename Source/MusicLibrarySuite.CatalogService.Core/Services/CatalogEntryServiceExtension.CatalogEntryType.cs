using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryTypeModel" /> service model.
/// </summary>
public class CatalogEntryTypeServiceExtension : ICatalogEntryTypeServiceExtension
{
    private readonly ICatalogEntryTypeRepositoryExtension m_catalogEntryTypeRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryTypeServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryType" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryTypeServiceExtension(
        ICatalogEntryTypeRepositoryExtension catalogEntryTypeRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryTypeRepositoryExtension = catalogEntryTypeRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryType? catalogEntryType =
            await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypeAsync(catalogEntryTypeId, cancellationToken);
        CatalogEntryTypeModel? catalogEntryTypeModel =
            m_mapper.Map<CatalogEntryTypeModel?>(catalogEntryType);
        return catalogEntryTypeModel;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        CatalogEntryType[] catalogEntryTypes =
            await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypesAsync(catalogEntryTypeIds, cancellationToken);
        CatalogEntryTypeModel[] catalogEntryTypeModels =
            m_mapper.Map<CatalogEntryTypeModel[]>(catalogEntryTypes);
        return catalogEntryTypeModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntryType[] catalogEntryTypes =
            await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypesAsync(cancellationToken);
        CatalogEntryTypeModel[] catalogEntryTypeModels =
            m_mapper.Map<CatalogEntryTypeModel[]>(catalogEntryTypes);
        return catalogEntryTypeModels;
    }
}
