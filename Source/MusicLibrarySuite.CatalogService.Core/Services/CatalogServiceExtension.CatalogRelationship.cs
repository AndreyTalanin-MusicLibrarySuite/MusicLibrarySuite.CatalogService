using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogRelationshipModel" /> service model.
/// </summary>
public class CatalogRelationshipServiceExtension : ICatalogRelationshipServiceExtension
{
    private readonly ICatalogRelationshipRepositoryExtension m_catalogRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogRelationshipServiceExtension(
        ICatalogRelationshipRepositoryExtension catalogRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogRelationshipRepositoryExtension = catalogRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogRelationshipModel[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogRelationship[] catalogRelationships =
            await m_catalogRelationshipRepositoryExtension.GetCatalogRelationshipsAsync(catalogId, byTarget, cancellationToken);
        CatalogRelationshipModel[] catalogRelationshipModels =
            m_mapper.Map<CatalogRelationshipModel[]>(catalogRelationships);
        return catalogRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationshipModel> catalogRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogRelationship[] catalogRelationships =
            m_mapper.Map<CatalogRelationship[]>(catalogRelationshipModels);
        await m_catalogRelationshipRepositoryExtension.ReorderCatalogRelationshipsAsync(catalogId, catalogRelationships, byTarget, cancellationToken);
    }
}
