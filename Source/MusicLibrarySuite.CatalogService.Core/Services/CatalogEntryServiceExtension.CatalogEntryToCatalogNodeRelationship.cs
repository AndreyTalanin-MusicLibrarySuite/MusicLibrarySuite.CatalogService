using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryToCatalogNodeRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryToCatalogNodeRelationshipServiceExtension : ICatalogEntryToCatalogNodeRelationshipServiceExtension
{
    private readonly ICatalogEntryToCatalogNodeRelationshipRepositoryExtension m_catalogEntryToCatalogNodeRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryToCatalogNodeRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryToCatalogNodeRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryToCatalogNodeRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryToCatalogNodeRelationshipServiceExtension(
        ICatalogEntryToCatalogNodeRelationshipRepositoryExtension catalogEntryToCatalogNodeRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryToCatalogNodeRelationshipRepositoryExtension = catalogEntryToCatalogNodeRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationshipModel[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogNodeRelationship[] catalogEntryToCatalogNodeRelationships =
            await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, cancellationToken);
        CatalogEntryToCatalogNodeRelationshipModel[] catalogEntryToCatalogNodeRelationshipModels =
            m_mapper.Map<CatalogEntryToCatalogNodeRelationshipModel[]>(catalogEntryToCatalogNodeRelationships);
        return catalogEntryToCatalogNodeRelationshipModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationshipModel[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogNodeRelationship[] catalogEntryToCatalogNodeRelationships =
            await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, cancellationToken);
        CatalogEntryToCatalogNodeRelationshipModel[] catalogEntryToCatalogNodeRelationshipModels =
            m_mapper.Map<CatalogEntryToCatalogNodeRelationshipModel[]>(catalogEntryToCatalogNodeRelationships);
        return catalogEntryToCatalogNodeRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationshipModel> catalogEntryToCatalogNodeRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogNodeRelationship[] catalogEntryToCatalogNodeRelationships =
            m_mapper.Map<CatalogEntryToCatalogNodeRelationship[]>(catalogEntryToCatalogNodeRelationshipModels);
        await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, catalogEntryToCatalogNodeRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationshipModel> catalogEntryToCatalogNodeRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogNodeRelationship[] catalogEntryToCatalogNodeRelationships =
            m_mapper.Map<CatalogEntryToCatalogNodeRelationship[]>(catalogEntryToCatalogNodeRelationshipModels);
        await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, catalogEntryToCatalogNodeRelationships, cancellationToken);
    }
}
