using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to service extensions for the <see cref="CatalogEntryModel" /> service model.
/// <para>
/// List of available service extensions:
/// <list type="bullet">
/// <item><see cref="CatalogEntryToCatalogRelationshipServiceExtension" /></item>
/// <item><see cref="CatalogEntryToCatalogNodeRelationshipServiceExtension" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipServiceExtension" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipTypeServiceExtension" /></item>
/// <item><see cref="CatalogEntryRelationshipServiceExtension" /></item>
/// <item><see cref="CatalogEntryRelationshipTypeServiceExtension" /></item>
/// <item><see cref="CatalogEntryTypeServiceExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __CatalogEntryServiceExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __CatalogEntryServiceExtensions
{
}
