using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the catalog node service.
/// </summary>
public class CatalogNodeService : ICatalogNodeService
{
    private readonly ICatalogNodeRepositoryFacade m_catalogNodeRepositoryFacade;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeService" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeRepositoryFacade">The catalog node repository facade.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogNodeService(ICatalogNodeRepositoryFacade catalogNodeRepositoryFacade, IMapper mapper)
    {
        m_catalogNodeRepositoryFacade = catalogNodeRepositoryFacade;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        CatalogNode? catalogNode = await m_catalogNodeRepositoryFacade.GetCatalogNodeAsync(catalogNodeId, cancellationToken);
        CatalogNodeModel? catalogNodeModel = m_mapper.Map<CatalogNodeModel?>(catalogNode);
        return catalogNodeModel;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        CatalogNode[] catalogNodes = await m_catalogNodeRepositoryFacade.GetCatalogNodesAsync(catalogNodeIds, cancellationToken);
        CatalogNodeModel[] catalogNodeModels = m_mapper.Map<CatalogNodeModel[]>(catalogNodes);
        return catalogNodeModels;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        CatalogNode[] catalogNodes = await m_catalogNodeRepositoryFacade.GetCatalogNodesAsync(cancellationToken);
        CatalogNodeModel[] catalogNodeModels = m_mapper.Map<CatalogNodeModel[]>(catalogNodes);
        return catalogNodeModels;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        int catalogNodesCount = await m_catalogNodeRepositoryFacade.CountCatalogNodesAsync(cancellationToken);
        return catalogNodesCount;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeModel> AddCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        CatalogNode catalogNode = m_mapper.Map<CatalogNode>(catalogNodeModel);
        CatalogNode addedCatalogNode = await m_catalogNodeRepositoryFacade.AddCatalogNodeAsync(catalogNode, cancellationToken);
        CatalogNodeModel addedCatalogNodeModel = m_mapper.Map<CatalogNodeModel>(addedCatalogNode);
        return addedCatalogNodeModel;
    }

    /// <inheritdoc />
    public async Task UpdateCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        CatalogNode catalogNode = m_mapper.Map<CatalogNode>(catalogNodeModel);
        await m_catalogNodeRepositoryFacade.UpdateCatalogNodeAsync(catalogNode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeRepositoryFacade.RemoveCatalogNodeAsync(catalogNodeId, cancellationToken);
    }
}
