using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the catalog service.
/// </summary>
public class CatalogService : ICatalogService
{
    private readonly ICatalogRepositoryFacade m_catalogRepositoryFacade;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogService" /> type using the specified services.
    /// </summary>
    /// <param name="catalogRepositoryFacade">The catalog repository facade.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogService(ICatalogRepositoryFacade catalogRepositoryFacade, IMapper mapper)
    {
        m_catalogRepositoryFacade = catalogRepositoryFacade;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogModel?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        Catalog? catalog = await m_catalogRepositoryFacade.GetCatalogAsync(catalogId, cancellationToken);
        CatalogModel? catalogModel = m_mapper.Map<CatalogModel?>(catalog);
        return catalogModel;
    }

    /// <inheritdoc />
    public async Task<CatalogModel[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default)
    {
        Catalog[] catalogs = await m_catalogRepositoryFacade.GetCatalogsAsync(catalogIds, cancellationToken);
        CatalogModel[] catalogModels = m_mapper.Map<CatalogModel[]>(catalogs);
        return catalogModels;
    }

    /// <inheritdoc />
    public async Task<CatalogModel[]> GetCatalogsAsync(CancellationToken cancellationToken = default)
    {
        Catalog[] catalogs = await m_catalogRepositoryFacade.GetCatalogsAsync(cancellationToken);
        CatalogModel[] catalogModels = m_mapper.Map<CatalogModel[]>(catalogs);
        return catalogModels;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default)
    {
        int catalogsCount = await m_catalogRepositoryFacade.CountCatalogsAsync(cancellationToken);
        return catalogsCount;
    }

    /// <inheritdoc />
    public async Task<CatalogModel> AddCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default)
    {
        Catalog catalog = m_mapper.Map<Catalog>(catalogModel);
        Catalog addedCatalog = await m_catalogRepositoryFacade.AddCatalogAsync(catalog, cancellationToken);
        CatalogModel addedCatalogModel = m_mapper.Map<CatalogModel>(addedCatalog);
        return addedCatalogModel;
    }

    /// <inheritdoc />
    public async Task UpdateCatalogAsync(CatalogModel catalogModel, CancellationToken cancellationToken = default)
    {
        Catalog catalog = m_mapper.Map<Catalog>(catalogModel);
        await m_catalogRepositoryFacade.UpdateCatalogAsync(catalog, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        await m_catalogRepositoryFacade.RemoveCatalogAsync(catalogId, cancellationToken);
    }
}
