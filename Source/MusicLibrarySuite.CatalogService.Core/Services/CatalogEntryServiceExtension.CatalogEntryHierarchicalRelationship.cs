using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryHierarchicalRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryHierarchicalRelationshipServiceExtension : ICatalogEntryHierarchicalRelationshipServiceExtension
{
    private readonly ICatalogEntryHierarchicalRelationshipRepositoryExtension m_catalogEntryHierarchicalRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryHierarchicalRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryHierarchicalRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryHierarchicalRelationshipServiceExtension(ICatalogEntryHierarchicalRelationshipRepositoryExtension catalogEntryHierarchicalRelationshipRepositoryExtension, IMapper mapper)
    {
        m_catalogEntryHierarchicalRelationshipRepositoryExtension = catalogEntryHierarchicalRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryHierarchicalRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryHierarchicalRelationshipTypeId: null,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationship[] catalogEntryHierarchicalRelationships = catalogEntryHierarchicalRelationshipTypeId is not null
            ? await m_catalogEntryHierarchicalRelationshipRepositoryExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, (CatalogEntryHierarchicalRelationshipTypeId)catalogEntryHierarchicalRelationshipTypeId, byTarget, cancellationToken)
            : await m_catalogEntryHierarchicalRelationshipRepositoryExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
        CatalogEntryHierarchicalRelationshipModel[] catalogEntryHierarchicalRelationshipModels =
            m_mapper.Map<CatalogEntryHierarchicalRelationshipModel[]>(catalogEntryHierarchicalRelationships);
        return catalogEntryHierarchicalRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogEntryHierarchicalRelationship[] catalogEntryHierarchicalRelationships =
            m_mapper.Map<CatalogEntryHierarchicalRelationship[]>(catalogEntryHierarchicalRelationshipModels);
        await m_catalogEntryHierarchicalRelationshipRepositoryExtension.ReorderCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, catalogEntryHierarchicalRelationships, byTarget, cancellationToken);
    }
}
