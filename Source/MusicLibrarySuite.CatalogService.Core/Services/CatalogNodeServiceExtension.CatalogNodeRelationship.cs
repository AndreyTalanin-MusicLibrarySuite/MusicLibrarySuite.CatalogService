using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogNodeRelationshipModel" /> service model.
/// </summary>
public class CatalogNodeRelationshipServiceExtension : ICatalogNodeRelationshipServiceExtension
{
    private readonly ICatalogNodeRelationshipRepositoryExtension m_catalogNodeRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogNodeRelationshipServiceExtension(
        ICatalogNodeRelationshipRepositoryExtension catalogNodeRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogNodeRelationshipRepositoryExtension = catalogNodeRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeRelationshipModel[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogNodeRelationship[] catalogNodeRelationships =
            await m_catalogNodeRelationshipRepositoryExtension.GetCatalogNodeRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
        CatalogNodeRelationshipModel[] catalogNodeRelationshipModels =
            m_mapper.Map<CatalogNodeRelationshipModel[]>(catalogNodeRelationships);
        return catalogNodeRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationshipModel> catalogNodeRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogNodeRelationship[] catalogNodeRelationships =
            m_mapper.Map<CatalogNodeRelationship[]>(catalogNodeRelationshipModels);
        await m_catalogNodeRelationshipRepositoryExtension.ReorderCatalogNodeRelationshipsAsync(catalogNodeId, catalogNodeRelationships, byTarget, cancellationToken);
    }
}
