using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryRelationshipServiceExtension : ICatalogEntryRelationshipServiceExtension
{
    private readonly ICatalogEntryRelationshipRepositoryExtension m_catalogEntryRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryRelationshipServiceExtension(
        ICatalogEntryRelationshipRepositoryExtension catalogEntryRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryRelationshipRepositoryExtension = catalogEntryRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await GetCatalogEntryRelationshipsAsync(
            catalogEntryId: catalogEntryId,
            catalogEntryRelationshipTypeId: null,
            byTarget,
            cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationship[] catalogEntryRelationships = catalogEntryRelationshipTypeId is not null
            ? await m_catalogEntryRelationshipRepositoryExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, (CatalogEntryRelationshipTypeId)catalogEntryRelationshipTypeId, byTarget, cancellationToken)
            : await m_catalogEntryRelationshipRepositoryExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
        CatalogEntryRelationshipModel[] catalogEntryRelationshipModels =
            m_mapper.Map<CatalogEntryRelationshipModel[]>(catalogEntryRelationships);
        return catalogEntryRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationshipModel> catalogEntryRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogEntryRelationship[] catalogEntryRelationships =
            m_mapper.Map<CatalogEntryRelationship[]>(catalogEntryRelationshipModels);
        await m_catalogEntryRelationshipRepositoryExtension.ReorderCatalogEntryRelationshipsAsync(catalogEntryId, catalogEntryRelationships, byTarget, cancellationToken);
    }
}
