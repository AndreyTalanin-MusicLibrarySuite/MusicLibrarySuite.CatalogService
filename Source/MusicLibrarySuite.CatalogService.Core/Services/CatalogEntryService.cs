using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the catalog entry service.
/// </summary>
public class CatalogEntryService : ICatalogEntryService
{
    private readonly ICatalogEntryRepositoryFacade m_catalogEntryRepositoryFacade;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryService" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryRepositoryFacade">The catalog entry repository facade.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryService(ICatalogEntryRepositoryFacade catalogEntryRepositoryFacade, IMapper mapper)
    {
        m_catalogEntryRepositoryFacade = catalogEntryRepositoryFacade;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        CatalogEntry? catalogEntry = await m_catalogEntryRepositoryFacade.GetCatalogEntryAsync(catalogEntryId, cancellationToken);
        CatalogEntryModel? catalogEntryModel = m_mapper.Map<CatalogEntryModel?>(catalogEntry);
        return catalogEntryModel;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default)
    {
        CatalogEntry[] catalogEntries = await m_catalogEntryRepositoryFacade.GetCatalogEntriesAsync(catalogEntryIds, cancellationToken);
        CatalogEntryModel[] catalogEntryModels = m_mapper.Map<CatalogEntryModel[]>(catalogEntries);
        return catalogEntryModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        CatalogEntry[] catalogEntries = await m_catalogEntryRepositoryFacade.GetCatalogEntriesAsync(cancellationToken);
        CatalogEntryModel[] catalogEntryModels = m_mapper.Map<CatalogEntryModel[]>(catalogEntries);
        return catalogEntryModels;
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        int catalogEntriesCount = await m_catalogEntryRepositoryFacade.CountCatalogEntriesAsync(cancellationToken);
        return catalogEntriesCount;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryModel> AddCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        CatalogEntry catalogEntry = m_mapper.Map<CatalogEntry>(catalogEntryModel);
        CatalogEntry addedCatalogEntry = await m_catalogEntryRepositoryFacade.AddCatalogEntryAsync(catalogEntry, cancellationToken);
        CatalogEntryModel addedCatalogEntryModel = m_mapper.Map<CatalogEntryModel>(addedCatalogEntry);
        return addedCatalogEntryModel;
    }

    /// <inheritdoc />
    public async Task UpdateCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        CatalogEntry catalogEntry = m_mapper.Map<CatalogEntry>(catalogEntryModel);
        await m_catalogEntryRepositoryFacade.UpdateCatalogEntryAsync(catalogEntry, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryRepositoryFacade.RemoveCatalogEntryAsync(catalogEntryId, cancellationToken);
    }
}
