using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogNodeToCatalogRelationshipModel" /> service model.
/// </summary>
public class CatalogNodeToCatalogRelationshipServiceExtension : ICatalogNodeToCatalogRelationshipServiceExtension
{
    private readonly ICatalogNodeToCatalogRelationshipRepositoryExtension m_catalogNodeToCatalogRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeToCatalogRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeToCatalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeToCatalogRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogNodeToCatalogRelationshipServiceExtension(
        ICatalogNodeToCatalogRelationshipRepositoryExtension catalogNodeToCatalogRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogNodeToCatalogRelationshipRepositoryExtension = catalogNodeToCatalogRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationshipModel[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        CatalogNodeToCatalogRelationship[] catalogNodeToCatalogRelationships =
            await m_catalogNodeToCatalogRelationshipRepositoryExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, cancellationToken);
        CatalogNodeToCatalogRelationshipModel[] catalogNodeToCatalogRelationshipModels =
            m_mapper.Map<CatalogNodeToCatalogRelationshipModel[]>(catalogNodeToCatalogRelationships);
        return catalogNodeToCatalogRelationshipModels;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationshipModel[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        CatalogNodeToCatalogRelationship[] catalogNodeToCatalogRelationships =
            await m_catalogNodeToCatalogRelationshipRepositoryExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogId, cancellationToken);
        CatalogNodeToCatalogRelationshipModel[] catalogNodeToCatalogRelationshipModels =
            m_mapper.Map<CatalogNodeToCatalogRelationshipModel[]>(catalogNodeToCatalogRelationships);
        return catalogNodeToCatalogRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeToCatalogRelationshipModel> catalogNodeToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogNodeToCatalogRelationship[] catalogNodeToCatalogRelationships =
            m_mapper.Map<CatalogNodeToCatalogRelationship[]>(catalogNodeToCatalogRelationshipModels);
        await m_catalogNodeToCatalogRelationshipRepositoryExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, catalogNodeToCatalogRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogNodeToCatalogRelationshipModel> catalogNodeToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogNodeToCatalogRelationship[] catalogNodeToCatalogRelationships =
            m_mapper.Map<CatalogNodeToCatalogRelationship[]>(catalogNodeToCatalogRelationshipModels);
        await m_catalogNodeToCatalogRelationshipRepositoryExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogId, catalogNodeToCatalogRelationships, cancellationToken);
    }
}
