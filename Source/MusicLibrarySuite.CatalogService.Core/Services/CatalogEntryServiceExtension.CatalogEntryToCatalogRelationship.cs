using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogEntryToCatalogRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryToCatalogRelationshipServiceExtension : ICatalogEntryToCatalogRelationshipServiceExtension
{
    private readonly ICatalogEntryToCatalogRelationshipRepositoryExtension m_catalogEntryToCatalogRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryToCatalogRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryToCatalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryToCatalogRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogEntryToCatalogRelationshipServiceExtension(
        ICatalogEntryToCatalogRelationshipRepositoryExtension catalogEntryToCatalogRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogEntryToCatalogRelationshipRepositoryExtension = catalogEntryToCatalogRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogRelationship[] catalogEntryToCatalogRelationships =
            await m_catalogEntryToCatalogRelationshipRepositoryExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, cancellationToken);
        CatalogEntryToCatalogRelationshipModel[] catalogEntryToCatalogRelationshipModels =
            m_mapper.Map<CatalogEntryToCatalogRelationshipModel[]>(catalogEntryToCatalogRelationships);
        return catalogEntryToCatalogRelationshipModels;
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogRelationship[] catalogEntryToCatalogRelationships =
            await m_catalogEntryToCatalogRelationshipRepositoryExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogId, cancellationToken);
        CatalogEntryToCatalogRelationshipModel[] catalogEntryToCatalogRelationshipModels =
            m_mapper.Map<CatalogEntryToCatalogRelationshipModel[]>(catalogEntryToCatalogRelationships);
        return catalogEntryToCatalogRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogRelationship[] catalogEntryToCatalogRelationships =
            m_mapper.Map<CatalogEntryToCatalogRelationship[]>(catalogEntryToCatalogRelationshipModels);
        await m_catalogEntryToCatalogRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, catalogEntryToCatalogRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        CatalogEntryToCatalogRelationship[] catalogEntryToCatalogRelationships =
            m_mapper.Map<CatalogEntryToCatalogRelationship[]>(catalogEntryToCatalogRelationshipModels);
        await m_catalogEntryToCatalogRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogId, catalogEntryToCatalogRelationships, cancellationToken);
    }
}
