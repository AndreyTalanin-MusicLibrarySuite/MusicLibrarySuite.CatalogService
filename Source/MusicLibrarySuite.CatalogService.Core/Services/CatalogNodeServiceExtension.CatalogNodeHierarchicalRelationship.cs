using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Core.Services;

/// <summary>
/// Represents the default implementation of the service extension for the <see cref="CatalogNodeHierarchicalRelationshipModel" /> service model.
/// </summary>
public class CatalogNodeHierarchicalRelationshipServiceExtension : ICatalogNodeHierarchicalRelationshipServiceExtension
{
    private readonly ICatalogNodeHierarchicalRelationshipRepositoryExtension m_catalogNodeHierarchicalRelationshipRepositoryExtension;
    private readonly IMapper m_mapper;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeHierarchicalRelationshipServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeHierarchicalRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeHierarchicalRelationship" /> database entity.</param>
    /// <param name="mapper">The AutoMapper mapper.</param>
    public CatalogNodeHierarchicalRelationshipServiceExtension(
        ICatalogNodeHierarchicalRelationshipRepositoryExtension catalogNodeHierarchicalRelationshipRepositoryExtension,
        IMapper mapper)
    {
        m_catalogNodeHierarchicalRelationshipRepositoryExtension = catalogNodeHierarchicalRelationshipRepositoryExtension;
        m_mapper = mapper;
    }

    /// <inheritdoc />
    public async Task<CatalogNodeHierarchicalRelationshipModel[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogNodeHierarchicalRelationship[] catalogNodeHierarchicalRelationships =
            await m_catalogNodeHierarchicalRelationshipRepositoryExtension.GetCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
        CatalogNodeHierarchicalRelationshipModel[] catalogNodeHierarchicalRelationshipModels =
            m_mapper.Map<CatalogNodeHierarchicalRelationshipModel[]>(catalogNodeHierarchicalRelationships);
        return catalogNodeHierarchicalRelationshipModels;
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationshipModel> catalogNodeHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        CatalogNodeHierarchicalRelationship[] catalogNodeHierarchicalRelationships =
            m_mapper.Map<CatalogNodeHierarchicalRelationship[]>(catalogNodeHierarchicalRelationshipModels);
        await m_catalogNodeHierarchicalRelationshipRepositoryExtension.ReorderCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, catalogNodeHierarchicalRelationships, byTarget, cancellationToken);
    }
}
