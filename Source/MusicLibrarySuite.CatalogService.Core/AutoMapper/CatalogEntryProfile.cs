using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Core.AutoMapper;

/// <summary>
/// Represents a database-layer AutoMapper profile for the <see cref="CatalogEntry" /> entity.
/// </summary>
public class CatalogEntryProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryProfile" /> type.
    /// </summary>
    public CatalogEntryProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

        CreateMap<CatalogEntry, CatalogEntryModel>();
        CreateMap<CatalogEntryToCatalogRelationship, CatalogEntryToCatalogRelationshipModel>().ReverseMap();
        CreateMap<CatalogEntryToCatalogNodeRelationship, CatalogEntryToCatalogNodeRelationshipModel>().ReverseMap();
        CreateMap<CatalogEntryHierarchicalRelationship, CatalogEntryHierarchicalRelationshipModel>().ReverseMap();
        CreateMap<CatalogEntryHierarchicalRelationshipAnnotation, CatalogEntryHierarchicalRelationshipAnnotationModel>().ReverseMap();
        CreateMap<CatalogEntryHierarchicalRelationshipType, CatalogEntryHierarchicalRelationshipTypeModel>().ReverseMap();
        CreateMap<CatalogEntryRelationship, CatalogEntryRelationshipModel>().ReverseMap();
        CreateMap<CatalogEntryRelationshipAnnotation, CatalogEntryRelationshipAnnotationModel>().ReverseMap();
        CreateMap<CatalogEntryRelationshipType, CatalogEntryRelationshipTypeModel>().ReverseMap();
        CreateMap<CatalogEntryType, CatalogEntryTypeModel>().ReverseMap();

        CreateMap<CatalogEntryModel, CatalogEntry>()
            .AfterMap(SetCatalogEntryHierarchicalRelationshipAnnotationProperties)
            .AfterMap(SetCatalogEntryRelationshipAnnotationProperties);
    }

    private void SetCatalogEntryHierarchicalRelationshipAnnotationProperties(CatalogEntryModel catalogEntryModel, CatalogEntry catalogEntry)
    {
        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntry.CatalogEntryHierarchicalRelationships)
        {
            CatalogEntryHierarchicalRelationshipAnnotation? catalogEntryHierarchicalRelationshipAnnotation = catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipAnnotation;
            if (catalogEntryHierarchicalRelationshipAnnotation is not null)
            {
                catalogEntryHierarchicalRelationshipAnnotation.CatalogEntryId = catalogEntryHierarchicalRelationship.CatalogEntryId;
                catalogEntryHierarchicalRelationshipAnnotation.ParentCatalogEntryId = catalogEntryHierarchicalRelationship.ParentCatalogEntryId;
                catalogEntryHierarchicalRelationshipAnnotation.CatalogEntryHierarchicalRelationshipTypeId = catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId;
            }
        }
    }

    private void SetCatalogEntryRelationshipAnnotationProperties(CatalogEntryModel catalogEntryModel, CatalogEntry catalogEntry)
    {
        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntry.CatalogEntryRelationships)
        {
            CatalogEntryRelationshipAnnotation? catalogEntryRelationshipAnnotation = catalogEntryRelationship.CatalogEntryRelationshipAnnotation;
            if (catalogEntryRelationshipAnnotation is not null)
            {
                catalogEntryRelationshipAnnotation.CatalogEntryId = catalogEntryRelationship.CatalogEntryId;
                catalogEntryRelationshipAnnotation.DependentCatalogEntryId = catalogEntryRelationship.DependentCatalogEntryId;
                catalogEntryRelationshipAnnotation.CatalogEntryRelationshipTypeId = catalogEntryRelationship.CatalogEntryRelationshipTypeId;
            }
        }
    }
}
