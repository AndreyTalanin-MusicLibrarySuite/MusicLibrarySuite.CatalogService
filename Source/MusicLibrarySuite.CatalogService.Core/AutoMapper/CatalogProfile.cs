using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Core.AutoMapper;

/// <summary>
/// Represents a database-layer AutoMapper profile for the <see cref="Catalog" /> entity.
/// </summary>
public class CatalogProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogProfile" /> type.
    /// </summary>
    public CatalogProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

        CreateMap<Catalog, CatalogModel>().ReverseMap();
        CreateMap<CatalogRelationship, CatalogRelationshipModel>().ReverseMap();
    }
}
