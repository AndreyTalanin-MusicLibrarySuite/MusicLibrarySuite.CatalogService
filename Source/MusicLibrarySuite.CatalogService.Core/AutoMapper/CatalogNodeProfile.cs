using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Core.AutoMapper;

/// <summary>
/// Represents a database-layer AutoMapper profile for the <see cref="CatalogNode" /> entity.
/// </summary>
public class CatalogNodeProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeProfile" /> type.
    /// </summary>
    public CatalogNodeProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

        CreateMap<CatalogNode, CatalogNodeModel>().ReverseMap();
        CreateMap<CatalogNodeToCatalogRelationship, CatalogNodeToCatalogRelationshipModel>().ReverseMap();
        CreateMap<CatalogNodeHierarchicalRelationship, CatalogNodeHierarchicalRelationshipModel>().ReverseMap();
        CreateMap<CatalogNodeRelationship, CatalogNodeRelationshipModel>().ReverseMap();
    }
}
