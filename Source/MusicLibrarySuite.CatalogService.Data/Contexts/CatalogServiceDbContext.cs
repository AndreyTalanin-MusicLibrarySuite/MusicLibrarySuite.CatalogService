using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Contexts;

/// <summary>
/// Represents a base class for the catalog service database context.
/// </summary>
public class CatalogServiceDbContext : DbContext
{
    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="Catalog" /> type.
    /// </summary>
    public DbSet<Catalog> Catalogs { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogRelationship" /> type.
    /// </summary>
    public DbSet<CatalogRelationship> CatalogRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogNode" /> type.
    /// </summary>
    public DbSet<CatalogNode> CatalogNodes { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogNodeToCatalogRelationship" /> type.
    /// </summary>
    public DbSet<CatalogNodeToCatalogRelationship> CatalogNodeToCatalogRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogNodeHierarchicalRelationship" /> type.
    /// </summary>
    public DbSet<CatalogNodeHierarchicalRelationship> CatalogNodeHierarchicalRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogNodeRelationship" /> type.
    /// </summary>
    public DbSet<CatalogNodeRelationship> CatalogNodeRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntry" /> type.
    /// </summary>
    public DbSet<CatalogEntry> CatalogEntries { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryToCatalogRelationship" /> type.
    /// </summary>
    public DbSet<CatalogEntryToCatalogRelationship> CatalogEntryToCatalogRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryToCatalogNodeRelationship" /> type.
    /// </summary>
    public DbSet<CatalogEntryToCatalogNodeRelationship> CatalogEntryToCatalogNodeRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryHierarchicalRelationship" /> type.
    /// </summary>
    public DbSet<CatalogEntryHierarchicalRelationship> CatalogEntryHierarchicalRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> type.
    /// </summary>
    public DbSet<CatalogEntryHierarchicalRelationshipAnnotation> CatalogEntryHierarchicalRelationshipAnnotations { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryHierarchicalRelationshipType" /> type.
    /// </summary>
    public DbSet<CatalogEntryHierarchicalRelationshipType> CatalogEntryHierarchicalRelationshipTypes { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryRelationship" /> type.
    /// </summary>
    public DbSet<CatalogEntryRelationship> CatalogEntryRelationships { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryRelationshipAnnotation" /> type.
    /// </summary>
    public DbSet<CatalogEntryRelationshipAnnotation> CatalogEntryRelationshipAnnotations { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryRelationshipType" /> type.
    /// </summary>
    public DbSet<CatalogEntryRelationshipType> CatalogEntryRelationshipTypes { get; }

    /// <summary>
    /// Gets a <see cref="DbSet{TEntity}" /> object for entities of the <see cref="CatalogEntryType" /> type.
    /// </summary>
    public DbSet<CatalogEntryType> CatalogEntryTypes { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogServiceDbContext" /> type.
    /// </summary>
    protected CatalogServiceDbContext()
        : base()
    {
        Catalogs = Set<Catalog>();
        CatalogRelationships = Set<CatalogRelationship>();
        CatalogNodes = Set<CatalogNode>();
        CatalogNodeToCatalogRelationships = Set<CatalogNodeToCatalogRelationship>();
        CatalogNodeHierarchicalRelationships = Set<CatalogNodeHierarchicalRelationship>();
        CatalogNodeRelationships = Set<CatalogNodeRelationship>();
        CatalogEntries = Set<CatalogEntry>();
        CatalogEntryToCatalogRelationships = Set<CatalogEntryToCatalogRelationship>();
        CatalogEntryToCatalogNodeRelationships = Set<CatalogEntryToCatalogNodeRelationship>();
        CatalogEntryHierarchicalRelationships = Set<CatalogEntryHierarchicalRelationship>();
        CatalogEntryHierarchicalRelationshipAnnotations = Set<CatalogEntryHierarchicalRelationshipAnnotation>();
        CatalogEntryHierarchicalRelationshipTypes = Set<CatalogEntryHierarchicalRelationshipType>();
        CatalogEntryRelationships = Set<CatalogEntryRelationship>();
        CatalogEntryRelationshipAnnotations = Set<CatalogEntryRelationshipAnnotation>();
        CatalogEntryRelationshipTypes = Set<CatalogEntryRelationshipType>();
        CatalogEntryTypes = Set<CatalogEntryType>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogServiceDbContext" /> type using the specified database context options.
    /// </summary>
    /// <param name="contextOptions">The database context options.</param>
    protected CatalogServiceDbContext(DbContextOptions contextOptions)
        : base(contextOptions)
    {
        Catalogs = Set<Catalog>();
        CatalogRelationships = Set<CatalogRelationship>();
        CatalogNodes = Set<CatalogNode>();
        CatalogNodeToCatalogRelationships = Set<CatalogNodeToCatalogRelationship>();
        CatalogNodeHierarchicalRelationships = Set<CatalogNodeHierarchicalRelationship>();
        CatalogNodeRelationships = Set<CatalogNodeRelationship>();
        CatalogEntries = Set<CatalogEntry>();
        CatalogEntryToCatalogRelationships = Set<CatalogEntryToCatalogRelationship>();
        CatalogEntryToCatalogNodeRelationships = Set<CatalogEntryToCatalogNodeRelationship>();
        CatalogEntryHierarchicalRelationships = Set<CatalogEntryHierarchicalRelationship>();
        CatalogEntryHierarchicalRelationshipAnnotations = Set<CatalogEntryHierarchicalRelationshipAnnotation>();
        CatalogEntryHierarchicalRelationshipTypes = Set<CatalogEntryHierarchicalRelationshipType>();
        CatalogEntryRelationships = Set<CatalogEntryRelationship>();
        CatalogEntryRelationshipAnnotations = Set<CatalogEntryRelationshipAnnotation>();
        CatalogEntryRelationshipTypes = Set<CatalogEntryRelationshipType>();
        CatalogEntryTypes = Set<CatalogEntryType>();
    }
}
