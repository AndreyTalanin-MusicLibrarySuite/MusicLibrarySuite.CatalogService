using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.CatalogService.Data.Identifiers;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to the types in the namespace.
/// </summary>
/// <remarks>
/// <para>
/// List of available CMS (Content Management System) database entities' unique identifiers:
/// <list type="bullet">
/// <item><see cref="CatalogEntryHierarchicalRelationshipTypeCode" /></item>
/// <item><see cref="CatalogEntryRelationshipTypeCode" /></item>
/// <item><see cref="CatalogEntryTypeCode" /></item>
/// </list>
/// </para>
/// </remarks>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __AboutNamespace class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __AboutNamespace
{
}
