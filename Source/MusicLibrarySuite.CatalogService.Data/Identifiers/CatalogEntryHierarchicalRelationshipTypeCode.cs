namespace MusicLibrarySuite.CatalogService.Data.Identifiers;

/// <summary>
/// Represents a catalog entry hierarchical relationship type's code.
/// </summary>
public record CatalogEntryHierarchicalRelationshipTypeCode
{
    /// <summary>
    /// Gets the underlying <see cref="string" /> value.
    /// </summary>
    public string Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryHierarchicalRelationshipTypeCode" /> type using the specified <see cref="string" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="string" /> value.</param>
    public CatalogEntryHierarchicalRelationshipTypeCode(string value)
    {
        Value = value;
    }
}
