namespace MusicLibrarySuite.CatalogService.Data.Identifiers;

/// <summary>
/// Represents a catalog entry type's code.
/// </summary>
public record CatalogEntryTypeCode
{
    /// <summary>
    /// Gets the underlying <see cref="string" /> value.
    /// </summary>
    public string Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryTypeCode" /> type using the specified <see cref="string" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="string" /> value.</param>
    public CatalogEntryTypeCode(string value)
    {
        Value = value;
    }
}
