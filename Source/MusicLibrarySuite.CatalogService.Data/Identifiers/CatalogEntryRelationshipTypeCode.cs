namespace MusicLibrarySuite.CatalogService.Data.Identifiers;

/// <summary>
/// Represents a catalog entry relationship type's code.
/// </summary>
public record CatalogEntryRelationshipTypeCode
{
    /// <summary>
    /// Gets the underlying <see cref="string" /> value.
    /// </summary>
    public string Value { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRelationshipTypeCode" /> type using the specified <see cref="string" /> value.
    /// </summary>
    /// <param name="value">The underlying <see cref="string" /> value.</param>
    public CatalogEntryRelationshipTypeCode(string value)
    {
        Value = value;
    }
}
