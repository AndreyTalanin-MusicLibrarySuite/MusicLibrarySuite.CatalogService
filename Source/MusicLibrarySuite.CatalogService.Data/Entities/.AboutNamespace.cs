using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to the types in the namespace.
/// </summary>
/// <remarks>
/// <para>
/// List of available CMS (Content Management System) database entities:
/// <list type="bullet">
/// <item><see cref="Catalog" /></item>
/// <item><see cref="CatalogRelationship" /></item>
/// <item><see cref="CatalogNode" /></item>
/// <item><see cref="CatalogNodeToCatalogRelationship" /></item>
/// <item><see cref="CatalogNodeHierarchicalRelationship" /></item>
/// <item><see cref="CatalogNodeRelationship" /></item>
/// <item><see cref="CatalogEntry" /></item>
/// <item><see cref="CatalogEntryToCatalogRelationship" /></item>
/// <item><see cref="CatalogEntryToCatalogNodeRelationship" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationship" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipAnnotation" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipType" /></item>
/// <item><see cref="CatalogEntryRelationship" /></item>
/// <item><see cref="CatalogEntryRelationshipAnnotation" /></item>
/// <item><see cref="CatalogEntryRelationshipType" /></item>
/// <item><see cref="CatalogEntryType" /></item>
/// </list>
/// </para>
/// </remarks>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __AboutNamespace class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __AboutNamespace
{
}
