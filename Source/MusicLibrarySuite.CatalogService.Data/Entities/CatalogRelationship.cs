using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog relationship.
/// </summary>
public class CatalogRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog's unique identifier.
    /// </summary>
    public Guid CatalogId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog's unique identifier.
    /// </summary>
    public Guid DependentCatalogId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the relationship's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public Catalog? Catalog { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public Catalog? DependentCatalog { get; set; }

    /// <summary>
    /// Creates a new catalog relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogRelationship" /> class.
/// </summary>
internal static class CatalogRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogRelationship" /> collection.
    /// </summary>
    /// <param name="catalogRelationships">The <see cref="CatalogRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogRelationship> catalogRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogRelationship.CatalogId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogRelationship.DependentCatalogId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogRelationship.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogRelationship.Description), typeof(string));
        dataTable.Columns.Add(nameof(CatalogRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogRelationship catalogRelationship in catalogRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogRelationship.CatalogId),
                DbValueConvert.ToDbValue(catalogRelationship.DependentCatalogId),
                DbValueConvert.ToDbValue(catalogRelationship.Name),
                DbValueConvert.ToDbValue(catalogRelationship.Description),
                DbValueConvert.ToDbValue(catalogRelationship.Order),
                DbValueConvert.ToDbValue(catalogRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
