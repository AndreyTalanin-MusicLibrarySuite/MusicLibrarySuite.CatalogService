using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry relationship type.
/// </summary>
public class CatalogEntryRelationshipType : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog entry relationship type's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship owner.
    /// </summary>
    public Guid OwnerCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship target.
    /// </summary>
    public Guid TargetCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship type's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry relationship type's code.
    /// </summary>
    [StringLength(256)]
    public string Code { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be annotated.
    /// </summary>
    public bool Annotated { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be unique for the catalog entry acting as a relationship owner.
    /// </summary>
    public bool OwnerUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship should be unique for the catalog entry acting as a relationship target.
    /// </summary>
    public bool TargetUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry relationship type is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry relationship type was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry relationship type was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship owner.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryType? OwnerCatalogEntryType { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship target.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryType? TargetCatalogEntryType { get; set; }

    /// <summary>
    /// Creates a new catalog entry relationship type that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry relationship type that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryRelationshipType" /> class.
/// </summary>
internal static class CatalogEntryRelationshipTypeExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryRelationshipType" /> collection.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypes">The <see cref="CatalogEntryRelationshipType" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryRelationshipType" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryRelationshipType> catalogEntryRelationshipTypes)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.OwnerCatalogEntryTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.TargetCatalogEntryTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.Code), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.Annotated), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.OwnerUnique), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.TargetUnique), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipType.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryRelationshipType catalogEntryRelationshipType in catalogEntryRelationshipTypes)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.Id),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.OwnerCatalogEntryTypeId),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.TargetCatalogEntryTypeId),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.Name),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.Code),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.Annotated),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.OwnerUnique),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.TargetUnique),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.Enabled),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryRelationshipType.UpdatedOn));
        }

        return dataTable;
    }
}
