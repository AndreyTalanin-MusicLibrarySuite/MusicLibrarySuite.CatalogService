using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog node hierarchical relationship.
/// </summary>
public class CatalogNodeHierarchicalRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the child (principal) catalog node's unique identifier.
    /// </summary>
    public Guid CatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog node's unique identifier.
    /// </summary>
    public Guid ParentCatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the  catalog node acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog node acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the child (principal) catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? CatalogNode { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? ParentCatalogNode { get; set; }

    /// <summary>
    /// Creates a new catalog node hierarchical relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog node hierarchical relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogNodeHierarchicalRelationship" /> class.
/// </summary>
internal static class CatalogNodeHierarchicalRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogNodeHierarchicalRelationship" /> collection.
    /// </summary>
    /// <param name="catalogNodeHierarchicalRelationships">The <see cref="CatalogNodeHierarchicalRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogNodeHierarchicalRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.CatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.ParentCatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogNodeHierarchicalRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNodeHierarchicalRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.CatalogNodeId),
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.ParentCatalogNodeId),
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.Order),
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogNodeHierarchicalRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
