using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry hierarchical relationship annotation.
/// </summary>
public class CatalogEntryHierarchicalRelationshipAnnotation : ICloneable
{
    /// <summary>
    /// Gets or sets the child (principal) catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog entry's unique identifier.
    /// </summary>
    public Guid ParentCatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    public Guid CatalogEntryHierarchicalRelationshipTypeId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Creates a new catalog entry hierarchical relationship annotation that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry hierarchical relationship annotation that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> class.
/// </summary>
internal static class CatalogEntryHierarchicalRelationshipAnnotationExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> collection.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipAnnotations">The <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryHierarchicalRelationshipAnnotation> catalogEntryHierarchicalRelationshipAnnotations)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipAnnotation.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipAnnotation.ParentCatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipAnnotation.CatalogEntryHierarchicalRelationshipTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipAnnotation.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipAnnotation.Description), typeof(string));

        foreach (CatalogEntryHierarchicalRelationshipAnnotation catalogEntryHierarchicalRelationshipAnnotation in catalogEntryHierarchicalRelationshipAnnotations)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipAnnotation.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipAnnotation.ParentCatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipAnnotation.CatalogEntryHierarchicalRelationshipTypeId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipAnnotation.Name),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipAnnotation.Description));
        }

        return dataTable;
    }
}
