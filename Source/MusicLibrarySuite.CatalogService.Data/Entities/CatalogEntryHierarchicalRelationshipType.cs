using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry hierarchical relationship type.
/// </summary>
public class CatalogEntryHierarchicalRelationshipType : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship owner.
    /// </summary>
    public Guid OwnerCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier of the catalog entry type acting as a relationship target.
    /// </summary>
    public Guid TargetCatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's code.
    /// </summary>
    [StringLength(256)]
    public string Code { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry hierarchical relationship should be annotated.
    /// </summary>
    public bool Annotated { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry hierarchical relationship should be unique for the catalog entry acting as a relationship owner.
    /// </summary>
    public bool OwnerUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry hierarchical relationship should be unique for the catalog entry acting as a relationship target.
    /// </summary>
    public bool TargetUnique { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry hierarchical relationship type is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry hierarchical relationship type was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry hierarchical relationship type was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship owner.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryType? OwnerCatalogEntryType { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type acting as a relationship target.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryType? TargetCatalogEntryType { get; set; }

    /// <summary>
    /// Creates a new catalog entry hierarchical relationship type that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry hierarchical relationship type that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryHierarchicalRelationshipType" /> class.
/// </summary>
internal static class CatalogEntryHierarchicalRelationshipTypeExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryHierarchicalRelationshipType" /> collection.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationshipTypes">The <see cref="CatalogEntryHierarchicalRelationshipType" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryHierarchicalRelationshipType" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryHierarchicalRelationshipType> catalogEntryHierarchicalRelationshipTypes)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.OwnerCatalogEntryTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.TargetCatalogEntryTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.Code), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.Annotated), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.OwnerUnique), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.TargetUnique), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationshipType.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryHierarchicalRelationshipType catalogEntryHierarchicalRelationshipType in catalogEntryHierarchicalRelationshipTypes)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.Id),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.OwnerCatalogEntryTypeId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.TargetCatalogEntryTypeId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.Name),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.Code),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.Annotated),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.OwnerUnique),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.TargetUnique),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.Enabled),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationshipType.UpdatedOn));
        }

        return dataTable;
    }
}
