using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog-entry-to-catalog relationship.
/// </summary>
public class CatalogEntryToCatalogRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog's unique identifier.
    /// </summary>
    public Guid CatalogId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public Catalog? Catalog { get; set; }

    /// <summary>
    /// Creates a new catalog-entry-to-catalog relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog-entry-to-catalog relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryToCatalogRelationship" /> class.
/// </summary>
internal static class CatalogEntryToCatalogRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryToCatalogRelationship" /> collection.
    /// </summary>
    /// <param name="catalogEntryToCatalogRelationships">The <see cref="CatalogEntryToCatalogRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryToCatalogRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.CatalogId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship in catalogEntryToCatalogRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.CatalogId),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.Order),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryToCatalogRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
