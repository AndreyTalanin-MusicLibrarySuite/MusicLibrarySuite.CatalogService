using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry relationship.
/// </summary>
public class CatalogEntryRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog entry's unique identifier.
    /// </summary>
    public Guid DependentCatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship type's unique identifier.
    /// </summary>
    public Guid CatalogEntryRelationshipTypeId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the relationship's annotation.
    /// </summary>
    public CatalogEntryRelationshipAnnotation? CatalogEntryRelationshipAnnotation { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? DependentCatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship type.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryRelationshipType? CatalogEntryRelationshipType { get; set; }

    /// <summary>
    /// Creates a new catalog entry relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryRelationship" /> class.
/// </summary>
internal static class CatalogEntryRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryRelationship" /> collection.
    /// </summary>
    /// <param name="catalogEntryRelationships">The <see cref="CatalogEntryRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryRelationship> catalogEntryRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryRelationship.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.DependentCatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.CatalogEntryRelationshipTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntryRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryRelationship.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryRelationship.DependentCatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryRelationship.CatalogEntryRelationshipTypeId),
                DbValueConvert.ToDbValue(catalogEntryRelationship.Order),
                DbValueConvert.ToDbValue(catalogEntryRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogEntryRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogEntryRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
