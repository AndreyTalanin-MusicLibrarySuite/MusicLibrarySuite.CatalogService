using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog-entry-to-catalog-node relationship.
/// </summary>
public class CatalogEntryToCatalogNodeRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog node's unique identifier.
    /// </summary>
    public Guid CatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog node acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? CatalogNode { get; set; }

    /// <summary>
    /// Creates a new catalog-entry-to-catalog-node relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog-entry-to-catalog-node relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryToCatalogNodeRelationship" /> class.
/// </summary>
internal static class CatalogEntryToCatalogNodeRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryToCatalogNodeRelationship" /> collection.
    /// </summary>
    /// <param name="catalogEntryToCatalogNodeRelationships">The <see cref="CatalogEntryToCatalogNodeRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryToCatalogNodeRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.CatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryToCatalogNodeRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship in catalogEntryToCatalogNodeRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.CatalogNodeId),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.Order),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryToCatalogNodeRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
