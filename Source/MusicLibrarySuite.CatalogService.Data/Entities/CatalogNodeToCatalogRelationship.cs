using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog-node-to-catalog relationship.
/// </summary>
public class CatalogNodeToCatalogRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog node's unique identifier.
    /// </summary>
    public Guid CatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog's unique identifier.
    /// </summary>
    public Guid CatalogId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog node acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? CatalogNode { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public Catalog? Catalog { get; set; }

    /// <summary>
    /// Creates a new catalog-node-to-catalog relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog-node-to-catalog relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogNodeToCatalogRelationship" /> class.
/// </summary>
internal static class CatalogNodeToCatalogRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogNodeToCatalogRelationship" /> collection.
    /// </summary>
    /// <param name="catalogNodeToCatalogRelationships">The <see cref="CatalogNodeToCatalogRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogNodeToCatalogRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.CatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.Catalog), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogNodeToCatalogRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship in catalogNodeToCatalogRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.CatalogNodeId),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.CatalogId),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.Order),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogNodeToCatalogRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
