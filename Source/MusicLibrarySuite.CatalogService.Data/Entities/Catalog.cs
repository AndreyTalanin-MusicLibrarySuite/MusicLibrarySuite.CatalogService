using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog.
/// </summary>
public class Catalog : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog is system-protected.
    /// </summary>
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog relationships where the current catalog is the principal entity.
    /// </summary>
    public ICollection<CatalogRelationship> CatalogRelationships { get; set; } = new List<CatalogRelationship>();

    /// <summary>
    /// Creates a new catalog that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="Catalog" /> class.
/// </summary>
internal static class CatalogExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="Catalog" /> collection.
    /// </summary>
    /// <param name="catalogs">The <see cref="Catalog" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="Catalog" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<Catalog> catalogs)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(Catalog.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(Catalog.Name), typeof(string));
        dataTable.Columns.Add(nameof(Catalog.Description), typeof(string));
        dataTable.Columns.Add(nameof(Catalog.SystemProtected), typeof(bool));
        dataTable.Columns.Add(nameof(Catalog.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(Catalog.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(Catalog.UpdatedOn), typeof(DateTimeOffset));

        foreach (Catalog catalog in catalogs)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalog.Id),
                DbValueConvert.ToDbValue(catalog.Name),
                DbValueConvert.ToDbValue(catalog.Description),
                DbValueConvert.ToDbValue(catalog.SystemProtected),
                DbValueConvert.ToDbValue(catalog.Enabled),
                DbValueConvert.ToDbValue(catalog.CreatedOn),
                DbValueConvert.ToDbValue(catalog.UpdatedOn));
        }

        return dataTable;
    }
}
