using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog node relationship.
/// </summary>
public class CatalogNodeRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog node's unique identifier.
    /// </summary>
    public Guid CatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog node's unique identifier.
    /// </summary>
    public Guid DependentCatalogNodeId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the relationship's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog node acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog node acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the principal catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? CatalogNode { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog node.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogNode? DependentCatalogNode { get; set; }

    /// <summary>
    /// Creates a new catalog node relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog node relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogNodeRelationship" /> class.
/// </summary>
internal static class CatalogNodeRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogNodeRelationship" /> collection.
    /// </summary>
    /// <param name="catalogNodeRelationships">The <see cref="CatalogNodeRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogNodeRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogNodeRelationship> catalogNodeRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogNodeRelationship.CatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.DependentCatalogNodeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.Description), typeof(string));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogNodeRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogNodeRelationship catalogNodeRelationship in catalogNodeRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogNodeRelationship.CatalogNodeId),
                DbValueConvert.ToDbValue(catalogNodeRelationship.DependentCatalogNodeId),
                DbValueConvert.ToDbValue(catalogNodeRelationship.Name),
                DbValueConvert.ToDbValue(catalogNodeRelationship.Description),
                DbValueConvert.ToDbValue(catalogNodeRelationship.Order),
                DbValueConvert.ToDbValue(catalogNodeRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogNodeRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogNodeRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogNodeRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
