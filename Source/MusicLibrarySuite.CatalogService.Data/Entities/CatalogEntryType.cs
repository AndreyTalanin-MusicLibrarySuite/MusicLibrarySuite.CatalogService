using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry type.
/// </summary>
public class CatalogEntryType : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog entry type's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry type's code.
    /// </summary>
    [StringLength(256)]
    public string Code { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry type is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry type was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry type was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Creates a new catalog entry type that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry type that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryType" /> class.
/// </summary>
internal static class CatalogEntryTypeExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryType" /> collection.
    /// </summary>
    /// <param name="catalogEntryTypes">The <see cref="CatalogEntryType" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryType" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryType> catalogEntryTypes)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryType.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryType.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryType.Code), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryType.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryType.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryType.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryType catalogEntryType in catalogEntryTypes)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryType.Id),
                DbValueConvert.ToDbValue(catalogEntryType.Name),
                DbValueConvert.ToDbValue(catalogEntryType.Code),
                DbValueConvert.ToDbValue(catalogEntryType.Enabled),
                DbValueConvert.ToDbValue(catalogEntryType.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryType.UpdatedOn));
        }

        return dataTable;
    }
}
