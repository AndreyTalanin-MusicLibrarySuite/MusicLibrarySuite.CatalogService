using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry relationship annotation.
/// </summary>
public class CatalogEntryRelationshipAnnotation : ICloneable
{
    /// <summary>
    /// Gets or sets the principal catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the dependent catalog entry's unique identifier.
    /// </summary>
    public Guid DependentCatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship type's unique identifier.
    /// </summary>
    public Guid CatalogEntryRelationshipTypeId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry relationship's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog entry relationship's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Creates a new catalog entry relationship annotation that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry relationship annotation that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryRelationshipAnnotation" /> class.
/// </summary>
internal static class CatalogEntryRelationshipAnnotationExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryRelationshipAnnotation" /> collection.
    /// </summary>
    /// <param name="catalogEntryRelationshipAnnotations">The <see cref="CatalogEntryRelationshipAnnotation" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryRelationshipAnnotation" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryRelationshipAnnotation> catalogEntryRelationshipAnnotations)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryRelationshipAnnotation.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipAnnotation.DependentCatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipAnnotation.CatalogEntryRelationshipTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipAnnotation.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogEntryRelationshipAnnotation.Description), typeof(string));

        foreach (CatalogEntryRelationshipAnnotation catalogEntryRelationshipAnnotation in catalogEntryRelationshipAnnotations)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryRelationshipAnnotation.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryRelationshipAnnotation.DependentCatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryRelationshipAnnotation.CatalogEntryRelationshipTypeId),
                DbValueConvert.ToDbValue(catalogEntryRelationshipAnnotation.Name),
                DbValueConvert.ToDbValue(catalogEntryRelationshipAnnotation.Description));
        }

        return dataTable;
    }
}
