using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry hierarchical relationship.
/// </summary>
public class CatalogEntryHierarchicalRelationship : ICloneable
{
    /// <summary>
    /// Gets or sets the child (principal) catalog entry's unique identifier.
    /// </summary>
    public Guid CatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog entry's unique identifier.
    /// </summary>
    public Guid ParentCatalogEntryId { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    public Guid CatalogEntryHierarchicalRelationshipTypeId { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship owner.
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Gets or sets the relationship's display order for the catalog entry acting as a relationship target.
    /// </summary>
    public int ReferenceOrder { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the relationship is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the relationship was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets the relationship's annotation.
    /// </summary>
    public CatalogEntryHierarchicalRelationshipAnnotation? CatalogEntryHierarchicalRelationshipAnnotation { get; set; }

    /// <summary>
    /// Gets or sets the child (principal) catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? CatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the parent (dependent) catalog entry.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntry? ParentCatalogEntry { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry hierarchical relationship type.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryHierarchicalRelationshipType? CatalogEntryHierarchicalRelationshipType { get; set; }

    /// <summary>
    /// Creates a new catalog entry hierarchical relationship that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry hierarchical relationship that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntryHierarchicalRelationship" /> class.
/// </summary>
internal static class CatalogEntryHierarchicalRelationshipExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntryHierarchicalRelationship" /> collection.
    /// </summary>
    /// <param name="catalogEntryHierarchicalRelationships">The <see cref="CatalogEntryHierarchicalRelationship" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntryHierarchicalRelationship" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.CatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.ParentCatalogEntryId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.Order), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.ReferenceOrder), typeof(int));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntryHierarchicalRelationship.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntryHierarchicalRelationships)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.CatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.ParentCatalogEntryId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.Order),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.ReferenceOrder),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.Enabled),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntryHierarchicalRelationship.UpdatedOn));
        }

        return dataTable;
    }
}
