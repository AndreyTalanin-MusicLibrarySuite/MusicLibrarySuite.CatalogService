using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog entry.
/// </summary>
public class CatalogEntry : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog entry's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog entry type's unique identifier.
    /// </summary>
    public Guid CatalogEntryTypeId { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry is system-protected.
    /// </summary>
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog entry is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog entry was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog-entry-to-catalog relationships where the current catalog entry is the principal entity.
    /// </summary>
    public ICollection<CatalogEntryToCatalogRelationship> CatalogEntryToCatalogRelationships { get; set; } = new List<CatalogEntryToCatalogRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog-entry-to-catalog-node relationships where the current catalog entry is the principal entity.
    /// </summary>
    public ICollection<CatalogEntryToCatalogNodeRelationship> CatalogEntryToCatalogNodeRelationships { get; set; } = new List<CatalogEntryToCatalogNodeRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog entry hierarchical relationships where the current catalog entry is the child (principal) entity.
    /// </summary>
    public ICollection<CatalogEntryHierarchicalRelationship> CatalogEntryHierarchicalRelationships { get; set; } = new List<CatalogEntryHierarchicalRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog entry hierarchical relationships where the current catalog entry is the parent (dependent) entity.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public ICollection<CatalogEntryHierarchicalRelationship> ChildCatalogEntryHierarchicalRelationships { get; set; } = new List<CatalogEntryHierarchicalRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog entry relationships where the current catalog entry is the principal entity.
    /// </summary>
    public ICollection<CatalogEntryRelationship> CatalogEntryRelationships { get; set; } = new List<CatalogEntryRelationship>();

    /// <summary>
    /// Gets or sets the catalog entry type.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public CatalogEntryType? CatalogEntryType { get; set; }

    /// <summary>
    /// Creates a new catalog entry that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog entry that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogEntry" /> class.
/// </summary>
internal static class CatalogEntryExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogEntry" /> collection.
    /// </summary>
    /// <param name="catalogEntries">The <see cref="CatalogEntry" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogEntry" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogEntry> catalogEntries)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogEntry.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntry.CatalogEntryTypeId), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogEntry.SystemProtected), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntry.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogEntry.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogEntry.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogEntry catalogEntry in catalogEntries)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogEntry.Id),
                DbValueConvert.ToDbValue(catalogEntry.CatalogEntryTypeId),
                DbValueConvert.ToDbValue(catalogEntry.SystemProtected),
                DbValueConvert.ToDbValue(catalogEntry.Enabled),
                DbValueConvert.ToDbValue(catalogEntry.CreatedOn),
                DbValueConvert.ToDbValue(catalogEntry.UpdatedOn));
        }

        return dataTable;
    }
}
