using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;

using MusicLibrarySuite.Infrastructure.Data.Helpers;

namespace MusicLibrarySuite.CatalogService.Data.Entities;

/// <summary>
/// Represents a database entity for a catalog node.
/// </summary>
public class CatalogNode : ICloneable
{
    /// <summary>
    /// Gets or sets the catalog node's unique identifier.
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the catalog node's name.
    /// </summary>
    [StringLength(256)]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the catalog node's description.
    /// </summary>
    [StringLength(32768)]
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog node is system-protected.
    /// </summary>
    public bool SystemProtected { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the catalog node is enabled.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog node was initially created.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset CreatedOn { get; set; }

    /// <summary>
    /// Gets or sets a value representing the moment of time when the catalog node was updated the last time.
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTimeOffset UpdatedOn { get; set; }

    /// <summary>
    /// Gets or sets a collection of catalog-node-to-catalog relationships where the current catalog node is the principal entity.
    /// </summary>
    public ICollection<CatalogNodeToCatalogRelationship> CatalogNodeToCatalogRelationships { get; set; } = new List<CatalogNodeToCatalogRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog node hierarchical relationships where the current catalog node is the child (principal) entity.
    /// </summary>
    /// <remarks>
    /// This property is currently representing a one-to-one relationship. Each catalog node can have at most one parent.
    /// Use the <see cref="Enumerable.SingleOrDefault{TSource}(IEnumerable{TSource})" /> method to work with this property.
    /// </remarks>
    public ICollection<CatalogNodeHierarchicalRelationship> CatalogNodeHierarchicalRelationships { get; set; } = new List<CatalogNodeHierarchicalRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog node hierarchical relationships where the current catalog node is the parent (dependent) entity.
    /// </summary>
    /// <remarks>This property is only used to provide data from the database, not to save it.</remarks>
    public ICollection<CatalogNodeHierarchicalRelationship> ChildCatalogNodeHierarchicalRelationships { get; set; } = new List<CatalogNodeHierarchicalRelationship>();

    /// <summary>
    /// Gets or sets a collection of catalog node relationships where the current catalog node is the principal entity.
    /// </summary>
    public ICollection<CatalogNodeRelationship> CatalogNodeRelationships { get; set; } = new List<CatalogNodeRelationship>();

    /// <summary>
    /// Creates a new catalog node that is a shallow copy of the current instance.
    /// </summary>
    /// <returns>A new catalog node that is a shallow copy of the current instance.</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}

/// <summary>
/// Provides a set of extension methods for the <see cref="CatalogNode" /> class.
/// </summary>
internal static class CatalogNodeExtensions
{
    /// <summary>
    /// Creates a <see cref="DataTable" /> from a <see cref="CatalogNode" /> collection.
    /// </summary>
    /// <param name="catalogNodes">The <see cref="CatalogNode" /> collection.</param>
    /// <returns>
    /// A new <see cref="DataTable" /> instance with the columns corresponding to the <see cref="CatalogNode" /> properties.
    /// The rows of the data table contain values from the provided collection.
    /// </returns>
    public static DataTable ToDataTable(this IEnumerable<CatalogNode> catalogNodes)
    {
        DataTable dataTable = new();

        dataTable.Columns.Add(nameof(CatalogNode.Id), typeof(Guid));
        dataTable.Columns.Add(nameof(CatalogNode.Name), typeof(string));
        dataTable.Columns.Add(nameof(CatalogNode.Description), typeof(string));
        dataTable.Columns.Add(nameof(CatalogNode.SystemProtected), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogNode.Enabled), typeof(bool));
        dataTable.Columns.Add(nameof(CatalogNode.CreatedOn), typeof(DateTimeOffset));
        dataTable.Columns.Add(nameof(CatalogNode.UpdatedOn), typeof(DateTimeOffset));

        foreach (CatalogNode catalogNode in catalogNodes)
        {
            dataTable.Rows.Add(
                DbValueConvert.ToDbValue(catalogNode.Id),
                DbValueConvert.ToDbValue(catalogNode.Name),
                DbValueConvert.ToDbValue(catalogNode.Description),
                DbValueConvert.ToDbValue(catalogNode.SystemProtected),
                DbValueConvert.ToDbValue(catalogNode.Enabled),
                DbValueConvert.ToDbValue(catalogNode.CreatedOn),
                DbValueConvert.ToDbValue(catalogNode.UpdatedOn));
        }

        return dataTable;
    }
}
