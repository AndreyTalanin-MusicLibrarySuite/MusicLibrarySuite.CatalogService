using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog node hierarchical relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogNodeHierarchicalRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogNodeHierarchicalRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNodeHierarchicalRelationships)
        {
            if (byTarget)
                catalogNodeHierarchicalRelationship.ReferenceOrder = order++;
            else
                catalogNodeHierarchicalRelationship.Order = order++;
        }
    }
}
