using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sorts collection-represented navigation properties of a catalog so their order is correct.
/// </summary>
public class CatalogSortNavigationPropertiesVisitor : ISortNavigationPropertiesVisitor<Catalog>
{
    /// <inheritdoc />
    public void SortNavigationProperties(Catalog catalog)
    {
        catalog.CatalogRelationships = catalog.CatalogRelationships
            .OrderBy(catalogRelationship => catalogRelationship.Order)
            .ToList();
    }

    /// <inheritdoc />
    public void SortNavigationProperties(IEnumerable<Catalog> catalogs)
    {
        foreach (Catalog catalog in catalogs)
        {
            SortNavigationProperties(catalog);
        }
    }
}
