using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog entry relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogEntryRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogEntryRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogEntryRelationship> catalogEntryRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntryRelationships)
        {
            if (byTarget)
                catalogEntryRelationship.ReferenceOrder = order++;
            else
                catalogEntryRelationship.Order = order++;
        }
    }
}
