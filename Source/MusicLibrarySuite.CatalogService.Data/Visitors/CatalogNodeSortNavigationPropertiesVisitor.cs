using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sorts collection-represented navigation properties of a catalog node so their order is correct.
/// </summary>
public class CatalogNodeSortNavigationPropertiesVisitor : ISortNavigationPropertiesVisitor<CatalogNode>
{
    /// <inheritdoc />
    public void SortNavigationProperties(CatalogNode catalogNode)
    {
        catalogNode.CatalogNodeToCatalogRelationships = catalogNode.CatalogNodeToCatalogRelationships
            .OrderBy(catalogNodeToCatalogRelationship => catalogNodeToCatalogRelationship.Order)
            .ToList();

        catalogNode.CatalogNodeHierarchicalRelationships = catalogNode.CatalogNodeHierarchicalRelationships
            .OrderBy(catalogNodeHierarchicalRelationship => catalogNodeHierarchicalRelationship.Order)
            .ToList();
        catalogNode.ChildCatalogNodeHierarchicalRelationships = catalogNode.ChildCatalogNodeHierarchicalRelationships
            .OrderBy(catalogNodeHierarchicalRelationship => catalogNodeHierarchicalRelationship.ReferenceOrder)
            .ToList();

        catalogNode.CatalogNodeRelationships = catalogNode.CatalogNodeRelationships
            .OrderBy(catalogNodeRelationship => catalogNodeRelationship.Order)
            .ToList();
    }

    /// <inheritdoc />
    public void SortNavigationProperties(IEnumerable<CatalogNode> catalogNodes)
    {
        foreach (CatalogNode catalogNode in catalogNodes)
        {
            SortNavigationProperties(catalogNode);
        }
    }
}
