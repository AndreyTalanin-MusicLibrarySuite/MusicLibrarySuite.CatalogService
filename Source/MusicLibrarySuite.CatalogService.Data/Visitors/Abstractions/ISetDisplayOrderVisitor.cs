using System.Collections.Generic;

namespace MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

/// <summary>
/// Describes a visitor that sets the display order of a collection of entities so they can be inserted in the database as is.
/// </summary>
/// <typeparam name="TEntity">The entity type.</typeparam>
public interface ISetDisplayOrderVisitor<TEntity>
{
    /// <summary>
    /// Sets the display order of an entity so it can be inserted in the database as is.
    /// </summary>
    /// <param name="entities">The collection of entities to set the display order for.</param>
    /// <param name="byTarget">If set to <see langword="true" />, the method sets the secondary display order used by the target entity instead of the primary one used by the owner entity.</param>
    public void SetDisplayOrder(IEnumerable<TEntity> entities, bool byTarget);
}
