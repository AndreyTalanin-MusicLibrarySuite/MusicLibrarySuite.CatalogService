using System.Collections.Generic;

namespace MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

/// <summary>
/// Describes a visitor that cleans up navigation properties of an entity so there are no reference cycles in the object graph.
/// </summary>
/// <typeparam name="TEntity">The entity type.</typeparam>
public interface ICleanUpNavigationPropertiesVisitor<TEntity>
    where TEntity : class
{
    /// <summary>
    /// Cleans up navigation properties of a single entity so there are no reference cycles in the object graph.
    /// </summary>
    /// <param name="entity">The entity to clean up navigation properties for.</param>
    public void CleanUpNavigationProperties(TEntity entity);

    /// <summary>
    /// Cleans up navigation properties of a collection of entities so there are no reference cycles in the object graph.
    /// </summary>
    /// <param name="entities">The collection of entities to clean up navigation properties for.</param>
    public void CleanUpNavigationProperties(IEnumerable<TEntity> entities);
}
