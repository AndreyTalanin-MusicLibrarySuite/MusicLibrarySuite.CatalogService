using System;
using System.Collections.Generic;

namespace MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

/// <summary>
/// Describes a visitor that sets the primary key of an entity so dependent entities can reference the main one via foreign keys.
/// </summary>
/// <typeparam name="TEntity">The entity type.</typeparam>
/// <typeparam name="TEntityId">The entity unique identifier type.</typeparam>
public interface ISetPrimaryKeyVisitor<TEntity, TEntityId>
    where TEntity : class
    where TEntityId : struct
{
    /// <summary>
    /// Sets the primary key of an entity so dependent entities can reference the main one via foreign keys.
    /// </summary>
    /// <param name="entity">The entity to set the primary key for.</param>
    /// <param name="entityId">The value to set to the primary key.</param>
    public void SetPrimaryKey(TEntity entity, TEntityId entityId);

    /// <summary>
    /// Sets the primary key of an entity if it is empty so dependent entities can reference the main one via foreign keys.
    /// </summary>
    /// <param name="entity">The entity to set the primary key for.</param>
    /// <param name="entityId">The value to set to the primary key.</param>
    public void SetPrimaryKeyIfEmpty(TEntity entity, TEntityId entityId);

    /// <summary>
    /// Sets the primary keys of a collection of entities so dependent entities can reference the main one via foreign keys.
    /// </summary>
    /// <param name="entities">The collection of entities to set the primary keys for.</param>
    /// <param name="entityIds">The collection of values to set to the primary keys.</param>
    /// <exception cref="ArgumentException">Thrown if the collections have different length.</exception>
    public void SetPrimaryKeys(ICollection<TEntity> entities, ICollection<TEntityId> entityIds);

    /// <summary>
    /// Sets the primary keys of a collection of entities if it is empty so dependent entities can reference the main one via foreign keys.
    /// </summary>
    /// <param name="entities">The collection of entities to set the primary keys for.</param>
    /// <param name="entityIds">The collection of values to set to the primary keys.</param>
    /// <exception cref="ArgumentException">Thrown if the collections have different length.</exception>
    public void SetPrimaryKeysIfEmpty(ICollection<TEntity> entities, ICollection<TEntityId> entityIds);
}
