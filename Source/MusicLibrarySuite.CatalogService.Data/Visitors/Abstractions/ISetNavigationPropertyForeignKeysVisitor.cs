using System.Collections.Generic;

namespace MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

/// <summary>
/// Describes a visitor that sets foreign keys of navigation properties of an entity so dependent entities are referencing the main one.
/// </summary>
/// <typeparam name="TEntity">The entity type.</typeparam>
public interface ISetNavigationPropertyForeignKeysVisitor<TEntity>
    where TEntity : class
{
    /// <summary>
    /// Sets foreign keys of navigation properties of an entity so dependent entities are referencing the main one.
    /// </summary>
    /// <param name="entity">The entity to set foreign keys of navigation properties for.</param>
    public void SetNavigationPropertyForeignKeys(TEntity entity);

    /// <summary>
    /// Sets foreign keys of navigation properties of a collection of entities so dependent entities are referencing the main one.
    /// </summary>
    /// <param name="entities">The collection of entities to set foreign keys of navigation properties for.</param>
    public void SetNavigationPropertyForeignKeys(IEnumerable<TEntity> entities);
}
