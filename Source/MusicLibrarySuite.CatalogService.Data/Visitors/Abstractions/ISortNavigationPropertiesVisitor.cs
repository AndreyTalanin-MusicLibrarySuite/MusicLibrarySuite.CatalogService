using System.Collections.Generic;

namespace MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

/// <summary>
/// Describes a visitor that sorts collection-represented navigation properties of an entity so their order is correct.
/// </summary>
/// <typeparam name="TEntity">The entity type.</typeparam>
public interface ISortNavigationPropertiesVisitor<TEntity>
    where TEntity : class
{
    /// <summary>
    /// Sorts collection-represented navigation properties of a single entity so their order is correct.
    /// </summary>
    /// <param name="entity">The entity to sort collection-represented navigation properties for.</param>
    public void SortNavigationProperties(TEntity entity);

    /// <summary>
    /// Sorts collection-represented navigation properties of a collection of entities so their order is correct.
    /// </summary>
    /// <param name="entities">The collection of entities to sort collection-represented navigation properties for.</param>
    public void SortNavigationProperties(IEnumerable<TEntity> entities);
}
