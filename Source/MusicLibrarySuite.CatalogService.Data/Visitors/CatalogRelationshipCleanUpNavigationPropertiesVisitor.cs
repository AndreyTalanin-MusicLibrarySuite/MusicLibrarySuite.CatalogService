using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogRelationship catalogRelationship)
    {
        catalogRelationship.Catalog?.CatalogRelationships.Clear();
        catalogRelationship.DependentCatalog?.CatalogRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogRelationship> catalogRelationships)
    {
        foreach (CatalogRelationship catalogRelationship in catalogRelationships)
        {
            CleanUpNavigationProperties(catalogRelationship);
        }
    }
}
