using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog-node-to-catalog relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogNodeToCatalogRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogNodeToCatalogRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship in catalogNodeToCatalogRelationships)
        {
            if (byTarget)
                catalogNodeToCatalogRelationship.ReferenceOrder = order++;
            else
                catalogNodeToCatalogRelationship.Order = order++;
        }
    }
}
