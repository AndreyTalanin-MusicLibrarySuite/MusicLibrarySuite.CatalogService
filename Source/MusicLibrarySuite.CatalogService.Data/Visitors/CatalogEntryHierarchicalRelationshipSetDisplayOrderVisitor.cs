using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog entry hierarchical relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogEntryHierarchicalRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogEntryHierarchicalRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntryHierarchicalRelationships)
        {
            if (byTarget)
                catalogEntryHierarchicalRelationship.ReferenceOrder = order++;
            else
                catalogEntryHierarchicalRelationship.Order = order++;
        }
    }
}
