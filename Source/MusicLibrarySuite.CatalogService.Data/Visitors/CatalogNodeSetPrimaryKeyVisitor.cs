using System;
using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the primary key of a catalog node so dependent entities can reference the main one via foreign keys.
/// </summary>
public class CatalogNodeSetPrimaryKeyVisitor : ISetPrimaryKeyVisitor<CatalogNode, CatalogNodeId>
{
    /// <inheritdoc />
    public void SetPrimaryKey(CatalogNode catalogNode, CatalogNodeId catalogNodeId)
    {
        catalogNode.Id = catalogNodeId.Value;
    }

    /// <inheritdoc />
    public void SetPrimaryKeyIfEmpty(CatalogNode catalogNode, CatalogNodeId catalogNodeId)
    {
        if (catalogNode.Id != Guid.Empty)
            return;

        SetPrimaryKey(catalogNode, catalogNodeId);
    }

    /// <inheritdoc />
    public void SetPrimaryKeys(ICollection<CatalogNode> catalogNodes, ICollection<CatalogNodeId> catalogNodeIds)
    {
        if (catalogNodes.Count != catalogNodes.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogNodeIds));

        foreach ((CatalogNode catalogNode, CatalogNodeId catalogNodeId) in catalogNodes.Zip(catalogNodeIds))
        {
            SetPrimaryKey(catalogNode, catalogNodeId);
        }
    }

    /// <inheritdoc />
    public void SetPrimaryKeysIfEmpty(ICollection<CatalogNode> catalogNodes, ICollection<CatalogNodeId> catalogNodeIds)
    {
        if (catalogNodes.Count != catalogNodes.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogNodeIds));

        foreach ((CatalogNode catalogNode, CatalogNodeId catalogNodeId) in catalogNodes.Zip(catalogNodeIds))
        {
            SetPrimaryKeyIfEmpty(catalogNode, catalogNodeId);
        }
    }
}
