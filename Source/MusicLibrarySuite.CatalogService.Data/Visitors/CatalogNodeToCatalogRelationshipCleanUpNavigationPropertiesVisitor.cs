using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog-node-to-catalog relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship)
    {
        catalogNodeToCatalogRelationship.CatalogNode?.CatalogNodeToCatalogRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships)
    {
        foreach (CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship in catalogNodeToCatalogRelationships)
        {
            CleanUpNavigationProperties(catalogNodeToCatalogRelationship);
        }
    }
}
