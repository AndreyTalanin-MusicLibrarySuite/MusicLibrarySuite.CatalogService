using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog-entry-to-catalog-node relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogEntryToCatalogNodeRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship in catalogEntryToCatalogNodeRelationships)
        {
            if (byTarget)
                catalogEntryToCatalogNodeRelationship.ReferenceOrder = order++;
            else
                catalogEntryToCatalogNodeRelationship.Order = order++;
        }
    }
}
