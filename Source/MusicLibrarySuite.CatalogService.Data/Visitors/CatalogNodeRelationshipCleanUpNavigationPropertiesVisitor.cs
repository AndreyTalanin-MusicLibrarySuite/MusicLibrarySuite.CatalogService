using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog node relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogNodeRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogNodeRelationship catalogNodeRelationship)
    {
        catalogNodeRelationship.CatalogNode?.CatalogNodeRelationships.Clear();
        catalogNodeRelationship.DependentCatalogNode?.CatalogNodeRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogNodeRelationship> catalogNodeRelationships)
    {
        foreach (CatalogNodeRelationship catalogNodeRelationship in catalogNodeRelationships)
        {
            CleanUpNavigationProperties(catalogNodeRelationship);
        }
    }
}
