using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog-entry-to-catalog-node relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship)
    {
        catalogEntryToCatalogNodeRelationship.CatalogEntry?.CatalogEntryToCatalogNodeRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships)
    {
        foreach (CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship in catalogEntryToCatalogNodeRelationships)
        {
            CleanUpNavigationProperties(catalogEntryToCatalogNodeRelationship);
        }
    }
}
