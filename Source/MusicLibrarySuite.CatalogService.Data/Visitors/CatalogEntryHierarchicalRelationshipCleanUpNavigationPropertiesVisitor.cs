using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog entry hierarchical relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship)
    {
        catalogEntryHierarchicalRelationship.CatalogEntry?.CatalogEntryHierarchicalRelationships.Clear();
        catalogEntryHierarchicalRelationship.CatalogEntry?.ChildCatalogEntryHierarchicalRelationships.Clear();
        catalogEntryHierarchicalRelationship.ParentCatalogEntry?.CatalogEntryHierarchicalRelationships.Clear();
        catalogEntryHierarchicalRelationship.ParentCatalogEntry?.ChildCatalogEntryHierarchicalRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships)
    {
        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntryHierarchicalRelationships)
        {
            CleanUpNavigationProperties(catalogEntryHierarchicalRelationship);
        }
    }
}
