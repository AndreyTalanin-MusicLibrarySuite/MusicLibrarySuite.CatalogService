using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog so there are no reference cycles in the object graph.
/// </summary>
public class CatalogCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<Catalog>
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogRelationship> m_catalogRelationshipCleanUpNavigationPropertiesVisitor;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogCleanUpNavigationPropertiesVisitor" /> type using the specified services.
    /// </summary>
    /// <param name="catalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog relationship so there are no reference cycles in the object graph.</param>
    public CatalogCleanUpNavigationPropertiesVisitor(ICleanUpNavigationPropertiesVisitor<CatalogRelationship> catalogRelationshipCleanUpNavigationPropertiesVisitor)
    {
        m_catalogRelationshipCleanUpNavigationPropertiesVisitor = catalogRelationshipCleanUpNavigationPropertiesVisitor;
    }

    /// <inhericdoc />
    public void CleanUpNavigationProperties(Catalog catalog)
    {
        foreach (CatalogRelationship catalogRelationship in catalog.CatalogRelationships)
        {
            catalogRelationship.Catalog = null;
            catalogRelationship.DependentCatalog?.CatalogRelationships.Clear();
        }
        m_catalogRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalog.CatalogRelationships);
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<Catalog> catalogs)
    {
        foreach (Catalog catalog in catalogs)
        {
            CleanUpNavigationProperties(catalog);
        }
    }
}
