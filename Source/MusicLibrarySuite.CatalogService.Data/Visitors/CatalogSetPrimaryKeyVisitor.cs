using System;
using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the primary key of a catalog so dependent entities can reference the main one via foreign keys.
/// </summary>
public class CatalogSetPrimaryKeyVisitor : ISetPrimaryKeyVisitor<Catalog, CatalogId>
{
    /// <inheritdoc />
    public void SetPrimaryKey(Catalog catalog, CatalogId catalogId)
    {
        catalog.Id = catalogId.Value;
    }

    /// <inheritdoc />
    public void SetPrimaryKeyIfEmpty(Catalog catalog, CatalogId catalogId)
    {
        if (catalog.Id != Guid.Empty)
            return;

        SetPrimaryKey(catalog, catalogId);
    }

    /// <inheritdoc />
    public void SetPrimaryKeys(ICollection<Catalog> catalogs, ICollection<CatalogId> catalogIds)
    {
        if (catalogs.Count != catalogs.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogIds));

        foreach ((Catalog catalog, CatalogId catalogId) in catalogs.Zip(catalogIds))
        {
            SetPrimaryKey(catalog, catalogId);
        }
    }

    /// <inheritdoc />
    public void SetPrimaryKeysIfEmpty(ICollection<Catalog> catalogs, ICollection<CatalogId> catalogIds)
    {
        if (catalogs.Count != catalogs.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogIds));

        foreach ((Catalog catalog, CatalogId catalogId) in catalogs.Zip(catalogIds))
        {
            SetPrimaryKeyIfEmpty(catalog, catalogId);
        }
    }
}
