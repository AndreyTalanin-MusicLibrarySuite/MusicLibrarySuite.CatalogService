using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog-entry-to-catalog relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship)
    {
        catalogEntryToCatalogRelationship.CatalogEntry?.CatalogEntryToCatalogRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships)
    {
        foreach (CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship in catalogEntryToCatalogRelationships)
        {
            CleanUpNavigationProperties(catalogEntryToCatalogRelationship);
        }
    }
}
