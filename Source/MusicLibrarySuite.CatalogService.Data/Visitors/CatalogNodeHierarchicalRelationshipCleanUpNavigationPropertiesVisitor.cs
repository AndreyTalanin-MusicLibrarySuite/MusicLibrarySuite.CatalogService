using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog node hierarchical relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship)
    {
        catalogNodeHierarchicalRelationship.CatalogNode?.CatalogNodeHierarchicalRelationships.Clear();
        catalogNodeHierarchicalRelationship.CatalogNode?.ChildCatalogNodeHierarchicalRelationships.Clear();
        catalogNodeHierarchicalRelationship.ParentCatalogNode?.CatalogNodeHierarchicalRelationships.Clear();
        catalogNodeHierarchicalRelationship.ParentCatalogNode?.ChildCatalogNodeHierarchicalRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships)
    {
        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNodeHierarchicalRelationships)
        {
            CleanUpNavigationProperties(catalogNodeHierarchicalRelationship);
        }
    }
}
