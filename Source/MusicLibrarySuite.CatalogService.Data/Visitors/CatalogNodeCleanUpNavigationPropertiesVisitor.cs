using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog node so there are no reference cycles in the object graph.
/// </summary>
public class CatalogNodeCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogNode>
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship> m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship> m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship> m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeCleanUpNavigationPropertiesVisitor" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-node-to-catalog relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog node hierarchical relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogNodeRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog node relationship so there are no reference cycles in the object graph.</param>
    public CatalogNodeCleanUpNavigationPropertiesVisitor(
        ICleanUpNavigationPropertiesVisitor<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogNodeRelationship> catalogNodeRelationshipCleanUpNavigationPropertiesVisitor)
    {
        m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor = catalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogNode catalogNode)
    {
        foreach (CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship in catalogNode.CatalogNodeToCatalogRelationships)
        {
            catalogNodeToCatalogRelationship.CatalogNode = null;
        }
        m_catalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogNode.CatalogNodeToCatalogRelationships);

        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNode.CatalogNodeHierarchicalRelationships)
        {
            catalogNodeHierarchicalRelationship.CatalogNode = null;
        }
        m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogNode.CatalogNodeHierarchicalRelationships);
        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNode.ChildCatalogNodeHierarchicalRelationships)
        {
            catalogNodeHierarchicalRelationship.ParentCatalogNode = null;
        }
        m_catalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogNode.ChildCatalogNodeHierarchicalRelationships);

        foreach (CatalogNodeRelationship catalogNodeRelationship in catalogNode.CatalogNodeRelationships)
        {
            catalogNodeRelationship.CatalogNode = null;
        }
        m_catalogNodeRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogNode.CatalogNodeRelationships);
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogNode> catalogNodes)
    {
        foreach (CatalogNode catalogNode in catalogNodes)
        {
            CleanUpNavigationProperties(catalogNode);
        }
    }
}
