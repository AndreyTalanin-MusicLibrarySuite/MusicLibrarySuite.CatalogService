using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets foreign keys of navigation properties of a catalog entry so dependent entities are referencing the main one.
/// </summary>
public class CatalogEntrySetNavigationPropertyForeignKeysVisitor : ISetNavigationPropertyForeignKeysVisitor<CatalogEntry>
{
    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(CatalogEntry catalogEntry)
    {
        foreach (CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship in catalogEntry.CatalogEntryToCatalogRelationships)
        {
            catalogEntryToCatalogRelationship.CatalogEntryId = catalogEntry.Id;
        }

        foreach (CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship in catalogEntry.CatalogEntryToCatalogNodeRelationships)
        {
            catalogEntryToCatalogNodeRelationship.CatalogEntryId = catalogEntry.Id;
        }

        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntry.CatalogEntryHierarchicalRelationships)
        {
            catalogEntryHierarchicalRelationship.CatalogEntryId = catalogEntry.Id;
        }

        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntry.CatalogEntryRelationships)
        {
            catalogEntryRelationship.CatalogEntryId = catalogEntry.Id;
        }
    }

    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(IEnumerable<CatalogEntry> catalogEntries)
    {
        foreach (CatalogEntry catalogEntry in catalogEntries)
        {
            SetNavigationPropertyForeignKeys(catalogEntry);
        }
    }
}
