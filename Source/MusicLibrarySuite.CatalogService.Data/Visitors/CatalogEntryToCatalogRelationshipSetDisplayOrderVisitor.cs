using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog-entry-to-catalog relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogEntryToCatalogRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogEntryToCatalogRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship in catalogEntryToCatalogRelationships)
        {
            if (byTarget)
                catalogEntryToCatalogRelationship.ReferenceOrder = order++;
            else
                catalogEntryToCatalogRelationship.Order = order++;
        }
    }
}
