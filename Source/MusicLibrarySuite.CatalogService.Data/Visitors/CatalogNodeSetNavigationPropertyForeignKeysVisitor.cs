using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets foreign keys of navigation properties of a catalog node so dependent entities are referencing the main one.
/// </summary>
public class CatalogNodeSetNavigationPropertyForeignKeysVisitor : ISetNavigationPropertyForeignKeysVisitor<CatalogNode>
{
    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(CatalogNode catalogNode)
    {
        foreach (CatalogNodeToCatalogRelationship catalogNodeToCatalogRelationship in catalogNode.CatalogNodeToCatalogRelationships)
        {
            catalogNodeToCatalogRelationship.CatalogNodeId = catalogNode.Id;
        }

        foreach (CatalogNodeHierarchicalRelationship catalogNodeHierarchicalRelationship in catalogNode.CatalogNodeHierarchicalRelationships)
        {
            catalogNodeHierarchicalRelationship.CatalogNodeId = catalogNode.Id;
        }

        foreach (CatalogNodeRelationship catalogNodeRelationship in catalogNode.CatalogNodeRelationships)
        {
            catalogNodeRelationship.CatalogNodeId = catalogNode.Id;
        }
    }

    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(IEnumerable<CatalogNode> catalogNodes)
    {
        foreach (CatalogNode catalogNode in catalogNodes)
        {
            SetNavigationPropertyForeignKeys(catalogNode);
        }
    }
}
