using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets foreign keys of navigation properties of a catalog so dependent entities are referencing the main one.
/// </summary>
public class CatalogSetNavigationPropertyForeignKeysVisitor : ISetNavigationPropertyForeignKeysVisitor<Catalog>
{
    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(Catalog catalog)
    {
        foreach (CatalogRelationship catalogRelationship in catalog.CatalogRelationships)
        {
            catalogRelationship.CatalogId = catalog.Id;
        }
    }

    /// <inheritdoc />
    public void SetNavigationPropertyForeignKeys(IEnumerable<Catalog> catalogs)
    {
        foreach (Catalog catalog in catalogs)
        {
            SetNavigationPropertyForeignKeys(catalog);
        }
    }
}
