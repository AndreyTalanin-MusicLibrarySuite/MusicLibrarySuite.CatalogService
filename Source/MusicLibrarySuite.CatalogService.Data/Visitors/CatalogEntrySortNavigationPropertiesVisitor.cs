using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sorts collection-represented navigation properties of a catalog entry so their order is correct.
/// </summary>
public class CatalogEntrySortNavigationPropertiesVisitor : ISortNavigationPropertiesVisitor<CatalogEntry>
{
    /// <inheritdoc />
    public void SortNavigationProperties(CatalogEntry catalogEntry)
    {
        catalogEntry.CatalogEntryToCatalogRelationships = catalogEntry.CatalogEntryToCatalogRelationships
            .OrderBy(catalogEntryToCatalogRelationship => catalogEntryToCatalogRelationship.Order)
            .ToList();

        catalogEntry.CatalogEntryToCatalogNodeRelationships = catalogEntry.CatalogEntryToCatalogNodeRelationships
            .OrderBy(catalogEntryToCatalogNodeRelationship => catalogEntryToCatalogNodeRelationship.Order)
            .ToList();

        catalogEntry.CatalogEntryHierarchicalRelationships = catalogEntry.CatalogEntryHierarchicalRelationships
            .OrderBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId)
            .ThenBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.Order)
            .ToList();
        catalogEntry.ChildCatalogEntryHierarchicalRelationships = catalogEntry.ChildCatalogEntryHierarchicalRelationships
            .OrderBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.CatalogEntryHierarchicalRelationshipTypeId)
            .ThenBy(catalogEntryHierarchicalRelationship => catalogEntryHierarchicalRelationship.ReferenceOrder)
            .ToList();

        catalogEntry.CatalogEntryRelationships = catalogEntry.CatalogEntryRelationships
            .OrderBy(catalogEntryRelationship => catalogEntryRelationship.CatalogEntryRelationshipTypeId)
            .ThenBy(catalogEntryRelationship => catalogEntryRelationship.Order)
            .ToList();
    }

    /// <inheritdoc />
    public void SortNavigationProperties(IEnumerable<CatalogEntry> catalogEntries)
    {
        foreach (CatalogEntry catalogEntry in catalogEntries)
        {
            SortNavigationProperties(catalogEntry);
        }
    }
}
