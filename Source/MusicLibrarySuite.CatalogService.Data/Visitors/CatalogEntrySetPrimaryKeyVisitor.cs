using System;
using System.Collections.Generic;
using System.Linq;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the primary key of a catalog entry so dependent entities can reference the main one via foreign keys.
/// </summary>
public class CatalogEntrySetPrimaryKeyVisitor : ISetPrimaryKeyVisitor<CatalogEntry, CatalogEntryId>
{
    /// <inheritdoc />
    public void SetPrimaryKey(CatalogEntry catalogEntry, CatalogEntryId catalogEntryId)
    {
        catalogEntry.Id = catalogEntryId.Value;
    }

    /// <inheritdoc />
    public void SetPrimaryKeyIfEmpty(CatalogEntry catalogEntry, CatalogEntryId catalogEntryId)
    {
        if (catalogEntry.Id != Guid.Empty)
            return;

        SetPrimaryKey(catalogEntry, catalogEntryId);
    }

    /// <inheritdoc />
    public void SetPrimaryKeys(ICollection<CatalogEntry> catalogEntries, ICollection<CatalogEntryId> catalogEntryIds)
    {
        if (catalogEntries.Count != catalogEntryIds.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogEntryIds));

        foreach ((CatalogEntry catalogEntry, CatalogEntryId catalogEntryId) in catalogEntries.Zip(catalogEntryIds))
        {
            SetPrimaryKey(catalogEntry, catalogEntryId);
        }
    }

    /// <inheritdoc />
    public void SetPrimaryKeysIfEmpty(ICollection<CatalogEntry> catalogEntries, ICollection<CatalogEntryId> catalogEntryIds)
    {
        if (catalogEntries.Count != catalogEntryIds.Count)
            throw new ArgumentException("The collection has different length.", nameof(catalogEntryIds));

        foreach ((CatalogEntry catalogEntry, CatalogEntryId catalogEntryId) in catalogEntries.Zip(catalogEntryIds))
        {
            SetPrimaryKeyIfEmpty(catalogEntry, catalogEntryId);
        }
    }
}
