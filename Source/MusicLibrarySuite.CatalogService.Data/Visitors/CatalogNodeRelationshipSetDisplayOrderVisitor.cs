using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that sets the display order of a collection of catalog node relationships so they can be inserted in the database as is.
/// </summary>
public class CatalogNodeRelationshipSetDisplayOrderVisitor : ISetDisplayOrderVisitor<CatalogNodeRelationship>
{
    /// <inheritdoc />
    public void SetDisplayOrder(IEnumerable<CatalogNodeRelationship> catalogNodeRelationships, bool byTarget)
    {
        int order = 0;
        foreach (CatalogNodeRelationship catalogNodeRelationship in catalogNodeRelationships)
        {
            if (byTarget)
                catalogNodeRelationship.ReferenceOrder = order++;
            else
                catalogNodeRelationship.Order = order++;
        }
    }
}
