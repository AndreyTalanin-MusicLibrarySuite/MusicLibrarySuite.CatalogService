using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to the types in the namespace.
/// </summary>
/// <remarks>
/// <para>
/// List of available visitors for the <see cref="Catalog" /> database entity:
/// <list type="bullet">
/// <item><see cref="CatalogSetPrimaryKeyVisitor" /></item>
/// <item><see cref="CatalogSetNavigationPropertyForeignKeysVisitor" /></item>
/// <item><see cref="CatalogCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogSortNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogRelationshipSetDisplayOrderVisitor" /></item>
/// </list>
/// </para>
/// <para>
/// List of available visitors for the <see cref="CatalogNode" /> database entity:
/// <list type="bullet">
/// <item><see cref="CatalogNodeSetPrimaryKeyVisitor" /></item>
/// <item><see cref="CatalogNodeSetNavigationPropertyForeignKeysVisitor" /></item>
/// <item><see cref="CatalogNodeCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogNodeSortNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogNodeToCatalogRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogNodeToCatalogRelationshipSetDisplayOrderVisitor" /></item>
/// <item><see cref="CatalogNodeHierarchicalRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogNodeHierarchicalRelationshipSetDisplayOrderVisitor" /></item>
/// <item><see cref="CatalogNodeRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogNodeRelationshipSetDisplayOrderVisitor" /></item>
/// </list>
/// </para>
/// <para>
/// List of available visitors for the <see cref="CatalogEntry" /> database entity:
/// <list type="bullet">
/// <item><see cref="CatalogEntrySetPrimaryKeyVisitor" /></item>
/// <item><see cref="CatalogEntrySetNavigationPropertyForeignKeysVisitor" /></item>
/// <item><see cref="CatalogEntryCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntrySortNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntryToCatalogRelationshipSetDisplayOrderVisitor" /></item>
/// <item><see cref="CatalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntryToCatalogNodeRelationshipSetDisplayOrderVisitor" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntryHierarchicalRelationshipSetDisplayOrderVisitor" /></item>
/// <item><see cref="CatalogEntryRelationshipCleanUpNavigationPropertiesVisitor" /></item>
/// <item><see cref="CatalogEntryRelationshipSetDisplayOrderVisitor" /></item>
/// </list>
/// </para>
/// </remarks>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __AboutNamespace class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __AboutNamespace
{
}
