using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog entry relationship so there are no reference cycles in the object graph.
/// </summary>
public class CatalogEntryRelationshipCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship>
{
    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogEntryRelationship catalogEntryRelationship)
    {
        catalogEntryRelationship.CatalogEntry?.CatalogEntryRelationships.Clear();
        catalogEntryRelationship.DependentCatalogEntry?.CatalogEntryRelationships.Clear();
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogEntryRelationship> catalogEntryRelationships)
    {
        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntryRelationships)
        {
            CleanUpNavigationProperties(catalogEntryRelationship);
        }
    }
}
