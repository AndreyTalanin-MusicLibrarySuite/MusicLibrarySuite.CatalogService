using System.Collections.Generic;

using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Visitors.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Visitors;

/// <summary>
/// Represents a visitor that cleans up navigation properties of a catalog entry so there are no reference cycles in the object graph.
/// </summary>
public class CatalogEntryCleanUpNavigationPropertiesVisitor : ICleanUpNavigationPropertiesVisitor<CatalogEntry>
{
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship> m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship> m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship> m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
    private readonly ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship> m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryCleanUpNavigationPropertiesVisitor" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-entry-to-catalog relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog-entry-to-catalog-node relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog entry hierarchical relationship so there are no reference cycles in the object graph.</param>
    /// <param name="catalogEntryRelationshipCleanUpNavigationPropertiesVisitor">The visitor that cleans up navigation properties of a catalog entry relationship so there are no reference cycles in the object graph.</param>
    public CatalogEntryCleanUpNavigationPropertiesVisitor(
        ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor,
        ICleanUpNavigationPropertiesVisitor<CatalogEntryRelationship> catalogEntryRelationshipCleanUpNavigationPropertiesVisitor)
    {
        m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor;
        m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor = catalogEntryRelationshipCleanUpNavigationPropertiesVisitor;
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(CatalogEntry catalogEntry)
    {
        foreach (CatalogEntryToCatalogRelationship catalogEntryToCatalogRelationship in catalogEntry.CatalogEntryToCatalogRelationships)
        {
            catalogEntryToCatalogRelationship.CatalogEntry = null;
        }
        m_catalogEntryToCatalogRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogEntry.CatalogEntryToCatalogRelationships);

        foreach (CatalogEntryToCatalogNodeRelationship catalogEntryToCatalogNodeRelationship in catalogEntry.CatalogEntryToCatalogNodeRelationships)
        {
            catalogEntryToCatalogNodeRelationship.CatalogEntry = null;
        }
        m_catalogEntryToCatalogNodeRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogEntry.CatalogEntryToCatalogNodeRelationships);

        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntry.CatalogEntryHierarchicalRelationships)
        {
            catalogEntryHierarchicalRelationship.CatalogEntry = null;
        }
        m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogEntry.CatalogEntryHierarchicalRelationships);
        foreach (CatalogEntryHierarchicalRelationship catalogEntryHierarchicalRelationship in catalogEntry.ChildCatalogEntryHierarchicalRelationships)
        {
            catalogEntryHierarchicalRelationship.ParentCatalogEntry = null;
        }
        m_catalogEntryHierarchicalRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogEntry.ChildCatalogEntryHierarchicalRelationships);

        foreach (CatalogEntryRelationship catalogEntryRelationship in catalogEntry.CatalogEntryRelationships)
        {
            catalogEntryRelationship.CatalogEntry = null;
        }
        m_catalogEntryRelationshipCleanUpNavigationPropertiesVisitor
            .CleanUpNavigationProperties(catalogEntry.CatalogEntryRelationships);
    }

    /// <inheritdoc />
    public void CleanUpNavigationProperties(IEnumerable<CatalogEntry> catalogEntries)
    {
        foreach (CatalogEntry catalogEntry in catalogEntries)
        {
            CleanUpNavigationProperties(catalogEntry);
        }
    }
}
