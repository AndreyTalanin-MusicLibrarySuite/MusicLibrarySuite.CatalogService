using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;

namespace MusicLibrarySuite.CatalogService.Data.Repositories;

/// <summary>
/// Represents the repository facade for the <see cref="Catalog" /> database entity.
/// </summary>
public class CatalogRepositoryFacade : ICatalogRepositoryFacade
{
    private readonly ICatalogRepository m_catalogRepository;
    private readonly ICatalogRelationshipRepositoryExtension m_catalogRelationshipRepositoryExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogRepositoryFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogRepository">The repository for the <see cref="Catalog" /> database entity.</param>
    /// <param name="catalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogRelationship" /> database entity.</param>
    public CatalogRepositoryFacade(
        ICatalogRepository catalogRepository,
        ICatalogRelationshipRepositoryExtension catalogRelationshipRepositoryExtension)
    {
        m_catalogRepository = catalogRepository;
        m_catalogRelationshipRepositoryExtension = catalogRelationshipRepositoryExtension;
    }

    #region ICatalogRepository Repository Core Methods

    /// <inheritdoc />
    public async Task<Catalog?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.GetCatalogAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.GetCatalogsAsync(catalogIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.GetCatalogsAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.GetCatalogsAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<Catalog[]> GetCatalogsAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.GetCatalogsAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.CountCatalogsAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.CountCatalogsAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.CountCatalogsAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<Catalog> AddCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        return await m_catalogRepository.AddCatalogAsync(catalog, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        await m_catalogRepository.UpdateCatalogAsync(catalog, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        await m_catalogRepository.RemoveCatalogAsync(catalogId, cancellationToken);
    }

    #endregion

    #region ICatalogRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogRelationship[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogRelationshipRepositoryExtension.GetCatalogRelationshipsAsync(catalogId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationship> catalogRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogRelationshipRepositoryExtension.ReorderCatalogRelationshipsAsync(catalogId, catalogRelationships, byTarget, cancellationToken);
    }

    #endregion
}
