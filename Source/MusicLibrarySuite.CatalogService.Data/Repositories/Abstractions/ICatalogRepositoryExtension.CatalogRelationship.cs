using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogRelationship" /> database entity.
/// </summary>
public interface ICatalogRelationshipRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets catalog relationships by a catalog's unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogRelationship.DependentCatalog" /> instead of <see cref="CatalogRelationship.Catalog" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog relationships.
    /// </returns>
    public Task<CatalogRelationship[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog relationships for an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to reorder catalog relationships for.</param>
    /// <param name="catalogRelationships">The collection of catalog relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog relationships where <paramref name="catalogId" /> references <see cref="CatalogRelationship.DependentCatalog" /> instead of <see cref="CatalogRelationship.Catalog" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationship> catalogRelationships, bool byTarget, CancellationToken cancellationToken = default);
}
