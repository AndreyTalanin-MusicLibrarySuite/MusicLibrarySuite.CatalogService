using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to repository extensions for the <see cref="CatalogEntry" /> database entity.
/// <para>
/// List of available repository extensions:
/// <list type="bullet">
/// <item><see cref="ICatalogEntryToCatalogRelationshipRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryToCatalogNodeRelationshipRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryHierarchicalRelationshipRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryRelationshipRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryRelationshipTypeRepositoryExtension" /></item>
/// <item><see cref="ICatalogEntryTypeRepositoryExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __CatalogEntryRepositoryExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __CatalogEntryRepositoryExtensions
{
}
