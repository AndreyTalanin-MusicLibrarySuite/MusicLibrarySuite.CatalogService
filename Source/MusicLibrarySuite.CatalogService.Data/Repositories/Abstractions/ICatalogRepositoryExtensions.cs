using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to repository extensions for the <see cref="Catalog" /> database entity.
/// <para>
/// List of available repository extensions:
/// <list type="bullet">
/// <item><see cref="ICatalogRelationshipRepositoryExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __CatalogRepositoryExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __CatalogRepositoryExtensions
{
}
