using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository facade for the <see cref="Catalog" /> database entity.
/// </summary>
public interface ICatalogRepositoryFacade :
    ICatalogRepository,
    ICatalogRelationshipRepositoryExtension
{
}
