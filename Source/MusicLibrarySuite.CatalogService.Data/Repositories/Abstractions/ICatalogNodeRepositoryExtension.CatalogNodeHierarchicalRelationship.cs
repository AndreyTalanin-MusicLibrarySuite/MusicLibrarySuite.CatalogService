using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogNodeHierarchicalRelationship" /> database entity.
/// </summary>
public interface ICatalogNodeHierarchicalRelationshipRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets catalog node hierarchical relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationship.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationship.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog node hierarchical relationships.
    /// </returns>
    public Task<CatalogNodeHierarchicalRelationship[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog node hierarchical relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog node hierarchical relationships for.</param>
    /// <param name="catalogNodeHierarchicalRelationships">The collection of catalog node hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog node hierarchical relationships where <paramref name="catalogNodeId" /> references <see cref="CatalogNodeHierarchicalRelationship.ParentCatalogNode" /> instead of <see cref="CatalogNodeHierarchicalRelationship.CatalogNode" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog node hierarchical relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default);
}
