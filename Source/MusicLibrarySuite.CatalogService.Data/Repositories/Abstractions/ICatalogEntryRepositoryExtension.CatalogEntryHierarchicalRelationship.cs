using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogEntryHierarchicalRelationship" /> database entity.
/// </summary>
public interface ICatalogEntryHierarchicalRelationshipRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationship.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationship.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry hierarchical relationships.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry hierarchical relationships by a catalog entry's unique identifier and a catalog entry hierarchical relationship type's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="catalogEntryHierarchicalRelationshipTypeId">The catalog entry hierarchical relationship type's unique identifier.</param>
    /// <param name="byTarget">If set to <see langword="true" />, gets catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationship.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationship.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entry hierarchical relationships.
    /// </returns>
    public Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog entry hierarchical relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog entry hierarchical relationships for.</param>
    /// <param name="catalogEntryHierarchicalRelationships">The collection of catalog entry hierarchical relationships.</param>
    /// <param name="byTarget">If set to <see langword="true" />, reorders catalog entry hierarchical relationships where <paramref name="catalogEntryId" /> references <see cref="CatalogEntryHierarchicalRelationship.ParentCatalogEntry" /> instead of <see cref="CatalogEntryHierarchicalRelationship.CatalogEntry" />.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog entry hierarchical relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default);
}
