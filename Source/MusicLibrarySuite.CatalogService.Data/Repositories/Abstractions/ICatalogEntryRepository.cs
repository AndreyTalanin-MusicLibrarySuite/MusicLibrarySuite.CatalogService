using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository for the <see cref="CatalogEntry" /> database entity.
/// </summary>
public interface ICatalogEntryRepository
{
    /// <summary>
    /// Asynchronously gets a catalog entry by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntry?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entries by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entries.
    /// </returns>
    public Task<CatalogEntry[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entries filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entries.
    /// </returns>
    public Task<CatalogEntry[]> GetCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entries filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog entries.
    /// </returns>
    public Task<CatalogEntry[]> GetCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entries.
    /// </returns>
    public Task<CatalogEntry[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalog entries filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalog entries.
    /// </returns>
    public Task<int> CountCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalog entries filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalog entries.
    /// </returns>
    public Task<int> CountCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalog entries.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalog entries.
    /// </returns>
    public Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog entry.
    /// </summary>
    /// <param name="catalogEntry">The catalog entry to add to the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog entry with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogEntry.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog entry with the given <see cref="CatalogEntry.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<CatalogEntry> AddCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntry">The catalog entry to update in the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog entry with the given <see cref="CatalogEntry.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to remove from the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog entry with the given <see cref="CatalogEntry.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);
}
