using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository facade for the <see cref="CatalogEntry" /> database entity.
/// </summary>
public interface ICatalogEntryRepositoryFacade :
    ICatalogEntryRepository,
    ICatalogEntryToCatalogRelationshipRepositoryExtension,
    ICatalogEntryToCatalogNodeRelationshipRepositoryExtension,
    ICatalogEntryHierarchicalRelationshipRepositoryExtension,
    ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension,
    ICatalogEntryRelationshipRepositoryExtension,
    ICatalogEntryRelationshipTypeRepositoryExtension,
    ICatalogEntryTypeRepositoryExtension
{
}
