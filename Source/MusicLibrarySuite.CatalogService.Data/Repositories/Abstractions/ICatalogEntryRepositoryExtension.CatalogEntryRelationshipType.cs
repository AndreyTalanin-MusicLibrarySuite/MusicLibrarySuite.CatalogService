using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogEntryRelationshipType" /> database entity.
/// </summary>
public interface ICatalogEntryRelationshipTypeRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets a catalog entry relationship type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeId">The catalog entry relationship type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry relationship type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets a catalog entry relationship type by its code.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeCode">The catalog entry relationship type's code.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry relationship type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeCode catalogEntryRelationshipTypeCode, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry relationship types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry relationship types.
    /// </returns>
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry relationship types by a collection of codes.
    /// </summary>
    /// <param name="catalogEntryRelationshipTypeCodes">The collection of codes to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry relationship types.
    /// </returns>
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entry relationship types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entry relationship types.
    /// </returns>
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default);
}
