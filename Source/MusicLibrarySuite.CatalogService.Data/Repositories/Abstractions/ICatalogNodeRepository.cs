using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository for the <see cref="CatalogNode" /> database entity.
/// </summary>
public interface ICatalogNodeRepository
{
    /// <summary>
    /// Asynchronously gets a catalog node by its unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog node found or <see langword="null" />.
    /// </returns>
    public Task<CatalogNode?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog nodes by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogNodeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog nodes.
    /// </returns>
    public Task<CatalogNode[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog nodes filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog nodes.
    /// </returns>
    public Task<CatalogNode[]> GetCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog nodes filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog nodes.
    /// </returns>
    public Task<CatalogNode[]> GetCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog nodes.
    /// </returns>
    public Task<CatalogNode[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalog nodes filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalog nodes.
    /// </returns>
    public Task<int> CountCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalog nodes filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalog nodes.
    /// </returns>
    public Task<int> CountCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalog nodes.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalog nodes.
    /// </returns>
    public Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog node.
    /// </summary>
    /// <param name="catalogNode">The catalog node to add to the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog node with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="CatalogNode.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog node with the given <see cref="CatalogNode.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<CatalogNode> AddCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog node.
    /// </summary>
    /// <param name="catalogNode">The catalog node to update in the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog node with the given <see cref="CatalogNode.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to remove from the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog node with the given <see cref="CatalogNode.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default);
}
