using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogEntryToCatalogNodeRelationship" /> database entity.
/// </summary>
public interface ICatalogEntryToCatalogNodeRelationshipRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog-node relationships by a catalog entry's unique identifier.
    /// </summary>
    /// <param name="catalogEntryId">The catalog entry's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog-entry-to-catalog-node relationships.
    /// </returns>
    public Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog-entry-to-catalog-node relationships by a catalog node's unique identifier.
    /// </summary>
    /// <param name="catalogNodeId">The catalog node's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalog-entry-to-catalog-node relationships.
    /// </returns>
    public Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog-node relationships for an existing catalog entry.
    /// </summary>
    /// <param name="catalogEntryId">The unique identifier of the catalog entry to reorder catalog-entry-to-catalog-node relationships for.</param>
    /// <param name="catalogEntryToCatalogNodeRelationships">The collection of catalog-entry-to-catalog-node relationships.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog-entry-to-catalog-node relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously reorders catalog-entry-to-catalog-node relationships for an existing catalog node.
    /// </summary>
    /// <param name="catalogNodeId">The unique identifier of the catalog node to reorder catalog-entry-to-catalog-node relationships for.</param>
    /// <param name="catalogEntryToCatalogNodeRelationships">The collection of catalog-entry-to-catalog-node relationships.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityConflictException">Thrown if the number of reordered catalog-entry-to-catalog-node relationships does not match the original collection's size.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default);
}
