using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository extension for the <see cref="CatalogEntryType" /> database entity.
/// </summary>
public interface ICatalogEntryTypeRepositoryExtension
{
    /// <summary>
    /// Asynchronously gets a catalog entry type by its unique identifier.
    /// </summary>
    /// <param name="catalogEntryTypeId">The catalog entry type's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets a catalog entry type by its code.
    /// </summary>
    /// <param name="catalogEntryTypeCode">The catalog entry type's code.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog entry type found or <see langword="null" />.
    /// </returns>
    public Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeCode catalogEntryTypeCode, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry types by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogEntryTypeIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry types.
    /// </returns>
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalog entry types by a collection of codes.
    /// </summary>
    /// <param name="catalogEntryTypeCodes">The collection of codes to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalog entry types.
    /// </returns>
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalog entry types.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalog entry types.
    /// </returns>
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default);
}
