using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository facade for the <see cref="CatalogNode" /> database entity.
/// </summary>
public interface ICatalogNodeRepositoryFacade :
    ICatalogNodeRepository,
    ICatalogNodeToCatalogRelationshipRepositoryExtension,
    ICatalogNodeHierarchicalRelationshipRepositoryExtension,
    ICatalogNodeRelationshipRepositoryExtension
{
}
