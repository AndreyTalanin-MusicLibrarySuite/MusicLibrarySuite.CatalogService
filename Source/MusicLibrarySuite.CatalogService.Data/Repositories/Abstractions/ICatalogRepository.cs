using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.Infrastructure.Data;
using MusicLibrarySuite.Infrastructure.Exceptions;

namespace MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

/// <summary>
/// Describes a repository for the <see cref="Catalog" /> database entity.
/// </summary>
public interface ICatalogRepository
{
    /// <summary>
    /// Asynchronously gets a catalog by its unique identifier.
    /// </summary>
    /// <param name="catalogId">The catalog's unique identifier.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the catalog found or <see langword="null" />.
    /// </returns>
    public Task<Catalog?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalogs by a collection of unique identifiers.
    /// </summary>
    /// <param name="catalogIds">The collection of unique identifiers to search for.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all found catalogs.
    /// </returns>
    public Task<Catalog[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalogs filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalogs.
    /// </returns>
    public Task<Catalog[]> GetCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets catalogs filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all filtered catalogs.
    /// </returns>
    public Task<Catalog[]> GetCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously gets all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array containing all catalogs.
    /// </returns>
    public Task<Catalog[]> GetCatalogsAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalogs filtered by a <see cref="DbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalogs.
    /// </returns>
    public Task<int> CountCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts catalogs filtered by an <see cref="AsyncDbSetFilter{TEntity}" /> object.
    /// </summary>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> object to use.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a number of filtered catalogs.
    /// </returns>
    public Task<int> CountCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously counts all catalogs.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be a total number of catalogs.
    /// </returns>
    public Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously adds a new catalog.
    /// </summary>
    /// <param name="catalog">The catalog to add to the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the added catalog with <see cref="DatabaseGeneratedOption.Identity" />
    /// and <see cref="DatabaseGeneratedOption.Computed" /> properties like <see cref="Catalog.Id" /> set.
    /// </returns>
    /// <exception cref="EntityConflictException">Thrown if a catalog with the given <see cref="Catalog.Id" /> already exists.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task<Catalog> AddCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously updates an existing catalog.
    /// </summary>
    /// <param name="catalog">The catalog to update in the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog with the given <see cref="Catalog.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task UpdateCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default);

    /// <summary>
    /// Asynchronously removes an existing catalog.
    /// </summary>
    /// <param name="catalogId">The unique identifier of the catalog to remove from the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The task object representing the asynchronous operation.</returns>
    /// <exception cref="EntityNotFoundException">Thrown if a catalog with the given <see cref="Catalog.Id" /> does not exist.</exception>
    /// <exception cref="EntityConstraintViolationException">Thrown if a persistence-layer constraint is violated.</exception>
    public Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default);
}
