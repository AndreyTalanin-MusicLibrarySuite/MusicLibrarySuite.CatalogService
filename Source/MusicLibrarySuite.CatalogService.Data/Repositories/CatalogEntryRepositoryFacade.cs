using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;

namespace MusicLibrarySuite.CatalogService.Data.Repositories;

/// <summary>
/// Represents the repository facade for the <see cref="CatalogEntry" /> database entity.
/// </summary>
public class CatalogEntryRepositoryFacade : ICatalogEntryRepositoryFacade
{
    private readonly ICatalogEntryRepository m_catalogEntryRepository;
    private readonly ICatalogEntryToCatalogRelationshipRepositoryExtension m_catalogEntryToCatalogRelationshipRepositoryExtension;
    private readonly ICatalogEntryToCatalogNodeRelationshipRepositoryExtension m_catalogEntryToCatalogNodeRelationshipRepositoryExtension;
    private readonly ICatalogEntryHierarchicalRelationshipRepositoryExtension m_catalogEntryHierarchicalRelationshipRepositoryExtension;
    private readonly ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension;
    private readonly ICatalogEntryRelationshipRepositoryExtension m_catalogEntryRelationshipRepositoryExtension;
    private readonly ICatalogEntryRelationshipTypeRepositoryExtension m_catalogEntryRelationshipTypeRepositoryExtension;
    private readonly ICatalogEntryTypeRepositoryExtension m_catalogEntryTypeRepositoryExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRepositoryFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogEntryRepository">The repository for the <see cref="CatalogEntry" /> database entity.</param>
    /// <param name="catalogEntryToCatalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryToCatalogRelationship" /> database entity.</param>
    /// <param name="catalogEntryToCatalogNodeRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryToCatalogNodeRelationship" /> database entity.</param>
    /// <param name="catalogEntryHierarchicalRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryHierarchicalRelationship" /> database entity.</param>
    /// <param name="catalogEntryHierarchicalRelationshipTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryHierarchicalRelationshipType" /> database entity.</param>
    /// <param name="catalogEntryRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogEntryRelationship" /> database entity.</param>
    /// <param name="catalogEntryRelationshipTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryRelationshipType" /> database entity.</param>
    /// <param name="catalogEntryTypeRepositoryExtension">The repository extension for the <see cref="CatalogEntryType" /> database entity.</param>
    public CatalogEntryRepositoryFacade(
        ICatalogEntryRepository catalogEntryRepository,
        ICatalogEntryToCatalogRelationshipRepositoryExtension catalogEntryToCatalogRelationshipRepositoryExtension,
        ICatalogEntryToCatalogNodeRelationshipRepositoryExtension catalogEntryToCatalogNodeRelationshipRepositoryExtension,
        ICatalogEntryHierarchicalRelationshipRepositoryExtension catalogEntryHierarchicalRelationshipRepositoryExtension,
        ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension catalogEntryHierarchicalRelationshipTypeRepositoryExtension,
        ICatalogEntryRelationshipRepositoryExtension catalogEntryRelationshipRepositoryExtension,
        ICatalogEntryRelationshipTypeRepositoryExtension catalogEntryRelationshipTypeRepositoryExtension,
        ICatalogEntryTypeRepositoryExtension catalogEntryTypeRepositoryExtension)
    {
        m_catalogEntryRepository = catalogEntryRepository;
        m_catalogEntryToCatalogRelationshipRepositoryExtension = catalogEntryToCatalogRelationshipRepositoryExtension;
        m_catalogEntryToCatalogNodeRelationshipRepositoryExtension = catalogEntryToCatalogNodeRelationshipRepositoryExtension;
        m_catalogEntryHierarchicalRelationshipRepositoryExtension = catalogEntryHierarchicalRelationshipRepositoryExtension;
        m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension = catalogEntryHierarchicalRelationshipTypeRepositoryExtension;
        m_catalogEntryRelationshipRepositoryExtension = catalogEntryRelationshipRepositoryExtension;
        m_catalogEntryRelationshipTypeRepositoryExtension = catalogEntryRelationshipTypeRepositoryExtension;
        m_catalogEntryTypeRepositoryExtension = catalogEntryTypeRepositoryExtension;
    }

    #region ICatalogEntryRepository Repository Core Methods

    /// <inheritdoc />
    public async Task<CatalogEntry?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.GetCatalogEntryAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.GetCatalogEntriesAsync(catalogEntryIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.GetCatalogEntriesAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.GetCatalogEntriesAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.GetCatalogEntriesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(DbSetFilter<CatalogEntry> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.CountCatalogEntriesAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(AsyncDbSetFilter<CatalogEntry> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.CountCatalogEntriesAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.CountCatalogEntriesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntry> AddCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRepository.AddCatalogEntryAsync(catalogEntry, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogEntryAsync(CatalogEntry catalogEntry, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryRepository.UpdateCatalogEntryAsync(catalogEntry, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryRepository.RemoveCatalogEntryAsync(catalogEntryId, cancellationToken);
    }

    #endregion

    #region ICatalogEntryToCatalogRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationship[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogRelationshipRepositoryExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogRelationship[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogRelationshipRepositoryExtension.GetCatalogEntryToCatalogRelationshipsAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogEntryId, catalogEntryToCatalogRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationship> catalogEntryToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogRelationshipsAsync(catalogId, catalogEntryToCatalogRelationships, cancellationToken);
    }

    #endregion

    #region ICatalogEntryToCatalogNodeRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.GetCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogEntryId, catalogEntryToCatalogNodeRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryToCatalogNodeRelationshipRepositoryExtension.ReorderCatalogEntryToCatalogNodeRelationshipsAsync(catalogNodeId, catalogEntryToCatalogNodeRelationships, cancellationToken);
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipRepositoryExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationship[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipRepositoryExtension.GetCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, catalogEntryHierarchicalRelationshipTypeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationship> catalogEntryHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryHierarchicalRelationshipRepositoryExtension.ReorderCatalogEntryHierarchicalRelationshipsAsync(catalogEntryId, catalogEntryHierarchicalRelationships, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeCode catalogEntryHierarchicalRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeCode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeCode> catalogEntryHierarchicalRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeCodes, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryHierarchicalRelationshipTypeRepositoryExtension.GetCatalogEntryHierarchicalRelationshipTypesAsync(cancellationToken);
    }

    #endregion

    #region ICatalogEntryRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipRepositoryExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipRepositoryExtension.GetCatalogEntryRelationshipsAsync(catalogEntryId, catalogEntryRelationshipTypeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationship> catalogEntryRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogEntryRelationshipRepositoryExtension.ReorderCatalogEntryRelationshipsAsync(catalogEntryId, catalogEntryRelationships, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogEntryRelationshipTypeRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypeAsync(catalogEntryRelationshipTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeCode catalogEntryRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypeAsync(catalogEntryRelationshipTypeCode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypesAsync(catalogEntryRelationshipTypeCodes, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryRelationshipTypeRepositoryExtension.GetCatalogEntryRelationshipTypesAsync(cancellationToken);
    }

    #endregion

    #region ICatalogEntryTypeRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypeAsync(catalogEntryTypeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeCode catalogEntryTypeCode, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypeAsync(catalogEntryTypeCode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypesAsync(catalogEntryTypeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes, CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypesAsync(catalogEntryTypeCodes, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogEntryTypeRepositoryExtension.GetCatalogEntryTypesAsync(cancellationToken);
    }

    #endregion
}
