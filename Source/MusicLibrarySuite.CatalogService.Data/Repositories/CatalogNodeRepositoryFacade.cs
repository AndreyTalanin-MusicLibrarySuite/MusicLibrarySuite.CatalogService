using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;

namespace MusicLibrarySuite.CatalogService.Data.Repositories;

/// <summary>
/// Represents the repository facade for the <see cref="CatalogNode" /> database entity.
/// </summary>
public class CatalogNodeRepositoryFacade : ICatalogNodeRepositoryFacade
{
    private readonly ICatalogNodeRepository m_catalogNodeRepository;
    private readonly ICatalogNodeToCatalogRelationshipRepositoryExtension m_catalogNodeToCatalogRelationshipRepositoryExtension;
    private readonly ICatalogNodeHierarchicalRelationshipRepositoryExtension m_catalogNodeHierarchicalRelationshipRepositoryExtension;
    private readonly ICatalogNodeRelationshipRepositoryExtension m_catalogNodeRelationshipRepositoryExtension;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeRepositoryFacade" /> type using the specified services.
    /// </summary>
    /// <param name="catalogNodeRepository">The repository for the <see cref="CatalogNode" /> database entity.</param>
    /// <param name="catalogNodeToCatalogRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeToCatalogRelationship" /> database entity.</param>
    /// <param name="catalogNodeHierarchicalRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeHierarchicalRelationship" /> database entity.</param>
    /// <param name="catalogNodeRelationshipRepositoryExtension">The repository extension for the <see cref="CatalogNodeRelationship" /> database entity.</param>
    public CatalogNodeRepositoryFacade(
        ICatalogNodeRepository catalogNodeRepository,
        ICatalogNodeToCatalogRelationshipRepositoryExtension catalogNodeToCatalogRelationshipRepositoryExtension,
        ICatalogNodeHierarchicalRelationshipRepositoryExtension catalogNodeHierarchicalRelationshipRepositoryExtension,
        ICatalogNodeRelationshipRepositoryExtension catalogNodeRelationshipRepositoryExtension)
    {
        m_catalogNodeRepository = catalogNodeRepository;
        m_catalogNodeToCatalogRelationshipRepositoryExtension = catalogNodeToCatalogRelationshipRepositoryExtension;
        m_catalogNodeHierarchicalRelationshipRepositoryExtension = catalogNodeHierarchicalRelationshipRepositoryExtension;
        m_catalogNodeRelationshipRepositoryExtension = catalogNodeRelationshipRepositoryExtension;
    }

    #region ICatalogNodeRepository Repository Core Methods

    /// <inheritdoc />
    public async Task<CatalogNode?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.GetCatalogNodeAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.GetCatalogNodesAsync(catalogNodeIds, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.GetCatalogNodesAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.GetCatalogNodesAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNode[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.GetCatalogNodesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.CountCatalogNodesAsync(filter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.CountCatalogNodesAsync(asyncFilter, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.CountCatalogNodesAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNode> AddCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRepository.AddCatalogNodeAsync(catalogNode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task UpdateCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeRepository.UpdateCatalogNodeAsync(catalogNode, cancellationToken);
    }

    /// <inheritdoc />
    public async Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeRepository.RemoveCatalogNodeAsync(catalogNodeId, cancellationToken);
    }

    #endregion

    #region ICatalogNodeToCatalogRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeToCatalogRelationshipRepositoryExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeToCatalogRelationshipRepositoryExtension.GetCatalogNodeToCatalogRelationshipsAsync(catalogId, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeToCatalogRelationshipRepositoryExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogNodeId, catalogNodeToCatalogRelationships, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeToCatalogRelationshipRepositoryExtension.ReorderCatalogNodeToCatalogRelationshipsAsync(catalogId, catalogNodeToCatalogRelationships, cancellationToken);
    }

    #endregion

    #region ICatalogNodeHierarchicalRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeHierarchicalRelationship[]> GetCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeHierarchicalRelationshipRepositoryExtension.GetCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeHierarchicalRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeHierarchicalRelationship> catalogNodeHierarchicalRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeHierarchicalRelationshipRepositoryExtension.ReorderCatalogNodeHierarchicalRelationshipsAsync(catalogNodeId, catalogNodeHierarchicalRelationships, byTarget, cancellationToken);
    }

    #endregion

    #region ICatalogNodeRelationshipRepositoryExtension Repository Extension Methods

    /// <inheritdoc />
    public async Task<CatalogNodeRelationship[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        return await m_catalogNodeRelationshipRepositoryExtension.GetCatalogNodeRelationshipsAsync(catalogNodeId, byTarget, cancellationToken);
    }

    /// <inheritdoc />
    public async Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationship> catalogNodeRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        await m_catalogNodeRelationshipRepositoryExtension.ReorderCatalogNodeRelationshipsAsync(catalogNodeId, catalogNodeRelationships, byTarget, cancellationToken);
    }

    #endregion
}
