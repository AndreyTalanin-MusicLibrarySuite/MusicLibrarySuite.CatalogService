using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MusicLibrarySuite.CatalogService.Data.Sqlite")]
[assembly: InternalsVisibleTo("MusicLibrarySuite.CatalogService.Data.SqlServer")]
