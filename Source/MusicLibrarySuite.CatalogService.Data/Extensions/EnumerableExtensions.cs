using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Disable the IDE0301 (Simplify collection initialization) notification for better readability.
#pragma warning disable IDE0301

namespace MusicLibrarySuite.CatalogService.Data.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IEnumerable" /> interface.
/// </summary>
/// <remarks>Move to the <c>MusicLibrarySuite.Infrastructure.Linq</c> NuGet package.</remarks>
public static class EnumerableExtensions
{
    /// <summary>
    /// Creates an <see cref="IEnumerable{T}" /> sequence containing a single item.
    /// </summary>
    /// <typeparam name="T">The type of objects to enumerate.</typeparam>
    /// <param name="item">The item to create an <see cref="IEnumerable{T}" /> sequence from.</param>
    /// <returns>An <see cref="IEnumerable{T}" /> sequence containing a single item.</returns>
    public static IEnumerable<T> Yield<T>(this T item)
    {
        yield return item;
    }

    /// <summary>
    /// Creates an <see cref="IEnumerable{T}" /> sequence containing a single item if it is not a <see langword="null" /> reference,
    /// otherwise creates an empty sequence.
    /// </summary>
    /// <typeparam name="T">The type of objects to enumerate.</typeparam>
    /// <param name="item">The item to create an <see cref="IEnumerable{T}" /> sequence from or a <see langword="null" /> reference.</param>
    /// <returns>An <see cref="IEnumerable{T}" /> sequence containing a single item or an empty sequence.</returns>
    public static IEnumerable<T> YieldOrEmpty<T>(this T? item)
    {
        return item?.Yield() ?? Enumerable.Empty<T>();
    }

    /// <summary>
    /// Enumerates a sequence without any aggregation.
    /// </summary>
    /// <param name="collection">The sequence to enumerate.</param>
    /// <remarks>
    /// Use this method to explicitly indicate that sequence enumeration has desired side effects.
    /// Prefer it over the <see cref="Enumerable.Count{TSource}(IEnumerable{TSource})" />
    /// or <see cref="Enumerable.ToArray{TSource}(IEnumerable{TSource})" /> methods,
    /// as it does not create a new collection, reducing memory costs, and makes the side effects of enumeration more obvious.
    /// </remarks>
    public static void Enumerate(this IEnumerable collection)
    {
        foreach (object item in collection)
        {
        }
    }

    /// <summary>
    /// Enumerates a sequence supporting asynchronous iteration without any aggregation.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the collection.</typeparam>
    /// <param name="collection">The sequence to enumerate.</param>
    /// <returns>
    /// Use this method to explicitly indicate that sequence enumeration has desired side effects.
    /// Prefer it over the <see cref="Enumerable.Count{TSource}(IEnumerable{TSource})" />
    /// or <see cref="Enumerable.ToArray{TSource}(IEnumerable{TSource})" /> methods,
    /// as it does not create a new collection, reducing memory costs, and makes the side effects of enumeration more obvious.
    /// </returns>
    public static async Task EnumerateAsync<T>(this IAsyncEnumerable<T> collection)
    {
        await foreach (T item in collection)
        {
        }
    }
}
