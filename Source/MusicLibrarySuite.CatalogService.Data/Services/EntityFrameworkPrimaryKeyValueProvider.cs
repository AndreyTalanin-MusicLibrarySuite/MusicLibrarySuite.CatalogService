using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore.ValueGeneration;

using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Services;

/// <summary>
/// Represents the Entity-Framework-utilizing implementation of a primary key <see cref="Guid" /> value provider.
/// </summary>
public class EntityFrameworkPrimaryKeyValueProvider : IPrimaryKeyValueProvider
{
    private readonly GuidValueGenerator m_guidValueGenerator = new();
    private readonly SequentialGuidValueGenerator m_sequentialGuidValueGenerator = new();

    /// <inheritdoc />
    public Guid GetRandomGuidValue()
    {
        // The GuidValueGenerator type does not make use of the EntityEntry parameter.
        Guid randomGuidValue = m_guidValueGenerator.Next(null!);
        return randomGuidValue;
    }

    /// <inheritdoc />
    public Guid[] GetRandomGuidValues(int count)
    {
        Guid[] resultArray = new Guid[count];
        for (int i = 0; i < count; i++)
        {
            Guid randomGuidValue = GetRandomGuidValue();
            resultArray[i] = randomGuidValue;
        }
        return resultArray;
    }

    /// <inheritdoc />
    public Guid GetSequentialGuidValue()
    {
        // The SequentialGuidValueGenerator type does not make use of the EntityEntry parameter.
        Guid sequentialGuidValue = m_sequentialGuidValueGenerator.Next(null!);
        return sequentialGuidValue;
    }

    /// <inheritdoc />
    public Task<Guid> GetSequentialGuidValueAsync(CancellationToken cancellationToken = default)
    {
        return Task.FromResult(GetSequentialGuidValue());
    }

    /// <inheritdoc />
    public Guid[] GetSequentialGuidValues(int count)
    {
        Guid[] resultArray = new Guid[count];
        for (int i = 0; i < count; i++)
        {
            Guid sequentialGuidValue = GetSequentialGuidValue();
            resultArray[i] = sequentialGuidValue;
        }
        return resultArray;
    }

    /// <inheritdoc />
    public Task<Guid[]> GetSequentialGuidValuesAsync(int count, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(GetSequentialGuidValues(count));
    }
}
