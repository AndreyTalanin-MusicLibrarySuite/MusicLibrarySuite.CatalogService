using System;
using System.Threading;
using System.Threading.Tasks;

namespace MusicLibrarySuite.CatalogService.Data.Services.Abstractions;

/// <summary>
/// Describes a primary key <see cref="Guid" /> value provider.
/// </summary>
/// <remarks>Implementations of this interface can be platform-specific. Depending on your platform and database provider, there may be none available.</remarks>
public interface IPrimaryKeyValueProvider
{
    /// <summary>
    /// Creates a random <see cref="Guid" /> value.
    /// </summary>
    /// <returns>The random <see cref="Guid" /> value generated.</returns>
    public Guid GetRandomGuidValue();

    /// <summary>
    /// Creates multiple random <see cref="Guid" /> values.
    /// </summary>
    /// <param name="count">The number of <see cref="Guid" /> values to generate.</param>
    /// <returns>An array of random <see cref="Guid" /> values generated.</returns>
    public Guid[] GetRandomGuidValues(int count);

    /// <summary>
    /// Creates a sequential <see cref="Guid" /> value that is greater than any previously generated on a specified computer.
    /// </summary>
    /// <returns>The sequential <see cref="Guid" /> value generated.</returns>
    public Guid GetSequentialGuidValue();

    /// <summary>
    /// Asynchronously creates a sequential <see cref="Guid" /> value that is greater than any previously generated on a specified computer.
    /// </summary>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the sequential <see cref="Guid" /> value generated.
    /// </returns>
    public Task<Guid> GetSequentialGuidValueAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Creates multiple sequential <see cref="Guid" /> values, each being greater than any previously generated on a specified computer.
    /// </summary>
    /// <param name="count">The number of <see cref="Guid" /> values to generate.</param>
    /// <returns>An array of sequential <see cref="Guid" /> values generated.</returns>
    public Guid[] GetSequentialGuidValues(int count);

    /// <summary>
    /// Asynchronously creates multiple sequential <see cref="Guid" /> values, each being greater than any previously generated on a specified computer.
    /// </summary>
    /// <param name="count">The number of <see cref="Guid" /> values to generate.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be an array of sequential <see cref="Guid" /> values generated.
    /// </returns>
    public Task<Guid[]> GetSequentialGuidValuesAsync(int count, CancellationToken cancellationToken = default);
}
