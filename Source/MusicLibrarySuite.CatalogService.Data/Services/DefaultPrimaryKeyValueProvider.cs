using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Data.Services.Abstractions;

// Disable the IDE0300 (Simplify collection initialization) notification for array length check.
#pragma warning disable IDE0300

namespace MusicLibrarySuite.CatalogService.Data.Services;

/// <summary>
/// Represents the default implementation of a primary key <see cref="Guid" /> value provider.
/// The <see cref="DefaultPrimaryKeyValueProvider" /> type is not thread-safe.
/// </summary>
/// <remarks>This implementation only provides sequential <see cref="Guid" /> values within one instance. Independent calls will produce non-sequential values.</remarks>
public class DefaultPrimaryKeyValueProvider : IPrimaryKeyValueProvider
{
    private const int c_guidLengthBytes = 16;

    private static readonly int[] s_guidValueByteOrder = new int[c_guidLengthBytes] { 15, 14, 13, 12, 11, 10, 9, 8, 6, 7, 4, 5, 0, 1, 2, 3 };

    private readonly byte[] m_guidValueBytes = new byte[c_guidLengthBytes];

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultPrimaryKeyValueProvider" /> type.
    /// </summary>
    public DefaultPrimaryKeyValueProvider()
    {
        Guid.NewGuid().TryWriteBytes(m_guidValueBytes);
    }

    /// <inheritdoc />
    public Guid GetRandomGuidValue()
    {
        Guid randomGuidValue = Guid.NewGuid();
        return randomGuidValue;
    }

    /// <inheritdoc />
    public Guid[] GetRandomGuidValues(int count)
    {
        Guid[] resultArray = new Guid[count];
        for (int i = 0; i < count; i++)
        {
            Guid randomGuidValue = GetRandomGuidValue();
            resultArray[i] = randomGuidValue;
        }
        return resultArray;
    }

    /// <inheritdoc />
    public virtual Guid GetSequentialGuidValue()
    {
        Guid sequentialGuidValue = GetNextSequentialGuidValue();
        return sequentialGuidValue;
    }

    /// <inheritdoc />
    public virtual Task<Guid> GetSequentialGuidValueAsync(CancellationToken cancellationToken = default)
    {
        return Task.FromResult(GetSequentialGuidValue());
    }

    /// <inheritdoc />
    public virtual Guid[] GetSequentialGuidValues(int count)
    {
        Guid[] resultArray = new Guid[count];
        for (int i = 0; i < count; i++)
        {
            Guid sequentialGuidValue = GetNextSequentialGuidValue();
            resultArray[i] = sequentialGuidValue;
        }
        return resultArray;
    }

    /// <inheritdoc />
    public virtual Task<Guid[]> GetSequentialGuidValuesAsync(int count, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(GetSequentialGuidValues(count));
    }

    private Guid GetNextSequentialGuidValue()
    {
        // Increment the bytes until they stop overflowing.
        // https://stackoverflow.com/questions/30404965/increment-guid-in-c-sharp
        bool emptyGuidValue = !s_guidValueByteOrder.Any(index => !IncrementByte(index));
        Guid nextSequentialGuidValue = emptyGuidValue
            ? throw new OverflowException()
            : new Guid(m_guidValueBytes);

        return nextSequentialGuidValue;
    }

    private bool IncrementByte(int index)
    {
        bool overflow = ++m_guidValueBytes[index] == 0;
        return overflow;
    }
}
