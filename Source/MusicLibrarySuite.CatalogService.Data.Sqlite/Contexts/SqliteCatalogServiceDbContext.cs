using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Contexts;

/// <summary>
/// Represents a SQLite-specific implementation of the catalog service database context.
/// </summary>
public class SqliteCatalogServiceDbContext : CatalogServiceDbContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogServiceDbContext" /> type.
    /// </summary>
    public SqliteCatalogServiceDbContext()
        : base()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogServiceDbContext" /> type using the specified database context options.
    /// </summary>
    /// <param name="contextOptions">The database context options.</param>
    public SqliteCatalogServiceDbContext(DbContextOptions contextOptions)
        : base(contextOptions)
    {
    }

    /// <inheritdoc />
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Catalog>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.ToTable("Catalog", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_Catalog_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_Catalog_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_Catalog_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_Catalog_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
        });

        modelBuilder.Entity<CatalogRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogId, entity.DependentCatalogId });

            entityBuilder.HasOne(entity => entity.Catalog)
                .WithMany(entity => entity.CatalogRelationships)
                .HasForeignKey(entity => entity.CatalogId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.DependentCatalog)
                .WithMany()
                .HasForeignKey(entity => entity.DependentCatalogId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogId)
                .HasDatabaseName("IX_CatalogRelationship_CatalogId");
            entityBuilder.HasIndex(entity => entity.DependentCatalogId)
                .HasDatabaseName("IX_CatalogRelationship_DependentCatalogId");
            entityBuilder.HasIndex(entity => new { entity.CatalogId, entity.Order })
                .HasDatabaseName("UIX_CatalogRelationship_CatalogId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.DependentCatalogId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogRelationship", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogRelationship_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogRelationship_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogRelationship_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogRelationship_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
                tableBuilder.HasCheckConstraint("CK_CatalogRelationship_CatalogIds_NotEqual", "\"CatalogId\" <> \"DependentCatalogId\"");
            });
        });

        modelBuilder.Entity<CatalogNode>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.ToTable("CatalogNode", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogNode_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogNode_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogNode_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogNode_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
        });

        modelBuilder.Entity<CatalogNodeToCatalogRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogNodeId, entity.CatalogId });

            entityBuilder.HasOne(entity => entity.CatalogNode)
                .WithMany(entity => entity.CatalogNodeToCatalogRelationships)
                .HasForeignKey(entity => entity.CatalogNodeId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.Catalog)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogNodeId)
                .HasDatabaseName("IX_CatalogNodeToCatalogRelationship_CatalogNodeId");
            entityBuilder.HasIndex(entity => entity.CatalogId)
                .HasDatabaseName("IX_CatalogNodeToCatalogRelationship_CatalogId");
            entityBuilder.HasIndex(entity => new { entity.CatalogNodeId, entity.Order })
                .HasDatabaseName("UIX_CatalogNodeToCatalogRelationship_CatalogNodeId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.CatalogId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogNodeToCatalogRelationship_CatalogId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogNodeToCatalogRelationship", tableBuilder =>
            {
            });
        });

        modelBuilder.Entity<CatalogNodeHierarchicalRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogNodeId, entity.ParentCatalogNodeId });

            entityBuilder.HasOne(entity => entity.CatalogNode)
                .WithMany(entity => entity.CatalogNodeHierarchicalRelationships)
                .HasForeignKey(entity => entity.CatalogNodeId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.ParentCatalogNode)
                .WithMany(entity => entity.ChildCatalogNodeHierarchicalRelationships)
                .HasForeignKey(entity => entity.ParentCatalogNodeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogNodeId)
                .HasDatabaseName("IX_CatalogNodeHierarchicalRelationship_CatalogNodeId");
            entityBuilder.HasIndex(entity => entity.ParentCatalogNodeId)
                .HasDatabaseName("IX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId");
            entityBuilder.HasIndex(entity => new { entity.CatalogNodeId, entity.Order })
                .HasDatabaseName("UIX_CatalogNodeHierarchicalRelationship_CatalogNodeId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.ParentCatalogNodeId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogNodeHierarchicalRelationship", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogNodeHierarchicalRelationship_Order_EqualsZero", "\"Order\" = 0");
                tableBuilder.HasCheckConstraint("CK_CatalogNodeHierarchicalRelationship_CatalogNodeIds_NotEqual", "\"CatalogNodeId\" <> \"ParentCatalogNodeId\"");
            });
        });

        modelBuilder.Entity<CatalogNodeRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogNodeId, entity.DependentCatalogNodeId });

            entityBuilder.HasOne(entity => entity.CatalogNode)
                .WithMany(entity => entity.CatalogNodeRelationships)
                .HasForeignKey(entity => entity.CatalogNodeId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.DependentCatalogNode)
                .WithMany()
                .HasForeignKey(entity => entity.DependentCatalogNodeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogNodeId)
                .HasDatabaseName("IX_CatalogNodeRelationship_CatalogNodeId");
            entityBuilder.HasIndex(entity => entity.DependentCatalogNodeId)
                .HasDatabaseName("IX_CatalogNodeRelationship_DependentCatalogNodeId");
            entityBuilder.HasIndex(entity => new { entity.CatalogNodeId, entity.Order })
                .HasDatabaseName("UIX_CatalogNodeRelationship_CatalogNodeId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.DependentCatalogNodeId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogNodeRelationship_DependentCatalogNodeId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogNodeRelationship", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogNodeRelationship_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogNodeRelationship_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogNodeRelationship_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogNodeRelationship_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
                tableBuilder.HasCheckConstraint("CK_CatalogNodeRelationship_CatalogNodeIds_NotEqual", "\"CatalogNodeId\" <> \"DependentCatalogNodeId\"");
            });
        });

        modelBuilder.Entity<CatalogEntry>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.HasOne(entity => entity.CatalogEntryType)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogEntryTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogEntryTypeId)
                .HasDatabaseName("IX_CatalogEntry_CatalogEntryTypeId");

            entityBuilder.ToTable("CatalogEntry", tableBuilder =>
            {
            });
        });

        modelBuilder.Entity<CatalogEntryToCatalogRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.CatalogId });

            entityBuilder.HasOne(entity => entity.CatalogEntry)
                .WithMany(entity => entity.CatalogEntryToCatalogRelationships)
                .HasForeignKey(entity => entity.CatalogEntryId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.Catalog)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryToCatalogRelationship_CatalogEntryId");
            entityBuilder.HasIndex(entity => entity.CatalogId)
                .HasDatabaseName("IX_CatalogEntryToCatalogRelationship_CatalogId");
            entityBuilder.HasIndex(entity => new { entity.CatalogEntryId, entity.Order })
                .HasDatabaseName("UIX_CatalogEntryToCatalogRelationship_CatalogEntryId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.CatalogId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogEntryToCatalogRelationship_CatalogId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogEntryToCatalogRelationship", tableBuilder =>
            {
            });
        });

        modelBuilder.Entity<CatalogEntryToCatalogNodeRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.CatalogNodeId });

            entityBuilder.HasOne(entity => entity.CatalogEntry)
                .WithMany(entity => entity.CatalogEntryToCatalogNodeRelationships)
                .HasForeignKey(entity => entity.CatalogEntryId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.CatalogNode)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogNodeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId");
            entityBuilder.HasIndex(entity => entity.CatalogNodeId)
                .HasDatabaseName("IX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId");
            entityBuilder.HasIndex(entity => new { entity.CatalogEntryId, entity.Order })
                .HasDatabaseName("UIX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.CatalogNodeId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogEntryToCatalogNodeRelationship", tableBuilder =>
            {
            });
        });

        modelBuilder.Entity<CatalogEntryHierarchicalRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.ParentCatalogEntryId, entity.CatalogEntryHierarchicalRelationshipTypeId });

            entityBuilder.HasOne(entity => entity.CatalogEntry)
                .WithMany(entity => entity.CatalogEntryHierarchicalRelationships)
                .HasForeignKey(entity => entity.CatalogEntryId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.ParentCatalogEntry)
                .WithMany(entity => entity.ChildCatalogEntryHierarchicalRelationships)
                .HasForeignKey(entity => entity.ParentCatalogEntryId)
                .OnDelete(DeleteBehavior.Restrict);
            entityBuilder.HasOne(entity => entity.CatalogEntryHierarchicalRelationshipType)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogEntryHierarchicalRelationshipTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryHierarchicalRelationship_CatalogEntryId");
            entityBuilder.HasIndex(entity => entity.ParentCatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId");
            entityBuilder.HasIndex(entity => entity.CatalogEntryHierarchicalRelationshipTypeId)
                .HasDatabaseName("IX_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipTypeId");
            entityBuilder.HasIndex(entity => new { entity.CatalogEntryId, entity.CatalogEntryHierarchicalRelationshipTypeId, entity.Order })
                .HasDatabaseName("UIX_CatalogEntryHierarchicalRelationship_CatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.ParentCatalogEntryId, entity.CatalogEntryHierarchicalRelationshipTypeId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogEntryHierarchicalRelationship", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationship_CatalogEntryIds_NotEqual", "\"CatalogEntryId\" <> \"ParentCatalogEntryId\"");
            });
        });

        modelBuilder.Entity<CatalogEntryHierarchicalRelationshipAnnotation>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.ParentCatalogEntryId, entity.CatalogEntryHierarchicalRelationshipTypeId });

            entityBuilder.HasOne<CatalogEntryHierarchicalRelationship>()
                .WithOne(entity => entity.CatalogEntryHierarchicalRelationshipAnnotation)
                .HasForeignKey<CatalogEntryHierarchicalRelationshipAnnotation>(entity => new { entity.CatalogEntryId, entity.ParentCatalogEntryId, entity.CatalogEntryHierarchicalRelationshipTypeId })
                .HasConstraintName("FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns")
                .OnDelete(DeleteBehavior.Cascade);

            entityBuilder.ToTable("CatalogEntryHierarchicalRelationshipAnnotation", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
        });

        modelBuilder.Entity<CatalogEntryHierarchicalRelationshipType>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.HasAlternateKey(entity => entity.Code);

            entityBuilder.HasOne(entity => entity.OwnerCatalogEntryType)
                .WithMany()
                .HasForeignKey(entity => entity.OwnerCatalogEntryTypeId)
                .OnDelete(DeleteBehavior.Restrict);
            entityBuilder.HasOne(entity => entity.TargetCatalogEntryType)
                .WithMany()
                .HasForeignKey(entity => entity.TargetCatalogEntryTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.OwnerCatalogEntryTypeId)
                .HasDatabaseName("IX_CatalogEntryHierarchicalRelationshipType_OwnerCatalogEntryTypeId");
            entityBuilder.HasIndex(entity => entity.TargetCatalogEntryTypeId)
                .HasDatabaseName("IX_CatalogEntryHierarchicalRelationshipType_TargetCatalogEntryTypeId");

            entityBuilder.ToTable("CatalogEntryHierarchicalRelationshipType", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipType_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipType_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipType_Code_NotEmpty", "LENGTH(TRIM(\"Code\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryHierarchicalRelationshipType_Code_MaxLength", "LENGTH(TRIM(\"Code\")) <= 256");
            });
        });

        modelBuilder.Entity<CatalogEntryRelationship>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.DependentCatalogEntryId, entity.CatalogEntryRelationshipTypeId });

            entityBuilder.HasOne(entity => entity.CatalogEntry)
                .WithMany(entity => entity.CatalogEntryRelationships)
                .HasForeignKey(entity => entity.CatalogEntryId)
                .OnDelete(DeleteBehavior.Cascade);
            entityBuilder.HasOne(entity => entity.DependentCatalogEntry)
                .WithMany()
                .HasForeignKey(entity => entity.DependentCatalogEntryId)
                .OnDelete(DeleteBehavior.Restrict);
            entityBuilder.HasOne(entity => entity.CatalogEntryRelationshipType)
                .WithMany()
                .HasForeignKey(entity => entity.CatalogEntryRelationshipTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.HasIndex(entity => entity.CatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryRelationship_CatalogEntryId");
            entityBuilder.HasIndex(entity => entity.DependentCatalogEntryId)
                .HasDatabaseName("IX_CatalogEntryRelationship_DependentCatalogEntryId");
            entityBuilder.HasIndex(entity => entity.CatalogEntryRelationshipTypeId)
                .HasDatabaseName("IX_CatalogEntryRelationship_CatalogEntryRelationshipTypeId");
            entityBuilder.HasIndex(entity => new { entity.CatalogEntryId, entity.CatalogEntryRelationshipTypeId, entity.Order })
                .HasDatabaseName("UIX_CatalogEntryRelationship_CatalogEntryId_CatalogEntryRelationshipTypeId_Order")
                .IsUnique();
            entityBuilder.HasIndex(entity => new { entity.DependentCatalogEntryId, entity.CatalogEntryRelationshipTypeId, entity.ReferenceOrder })
                .HasDatabaseName("UIX_CatalogEntryRelationship_DependentCatalogEntryId_CatalogEntryRelationshipTypeId_ReferenceOrder")
                .IsUnique();

            entityBuilder.ToTable("CatalogEntryRelationship", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationship_CatalogEntryIds_NotEqual", "\"CatalogEntryId\" <> \"DependentCatalogEntryId\"");
            });
        });

        modelBuilder.Entity<CatalogEntryRelationshipAnnotation>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => new { entity.CatalogEntryId, entity.DependentCatalogEntryId, entity.CatalogEntryRelationshipTypeId });

            entityBuilder.HasOne<CatalogEntryRelationship>()
                .WithOne(entity => entity.CatalogEntryRelationshipAnnotation)
                .HasForeignKey<CatalogEntryRelationshipAnnotation>(entity => new { entity.CatalogEntryId, entity.DependentCatalogEntryId, entity.CatalogEntryRelationshipTypeId })
                .HasConstraintName("FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns")
                .OnDelete(DeleteBehavior.Cascade);

            entityBuilder.ToTable("CatalogEntryRelationshipAnnotation", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipAnnotation_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipAnnotation_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipAnnotation_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipAnnotation_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
        });

        modelBuilder.Entity<CatalogEntryRelationshipType>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.HasAlternateKey(entity => entity.Code);

            entityBuilder.HasOne(entity => entity.OwnerCatalogEntryType)
                .WithMany()
                .HasForeignKey(entity => entity.OwnerCatalogEntryTypeId)
                .OnDelete(DeleteBehavior.Restrict);
            entityBuilder.HasOne(entity => entity.TargetCatalogEntryType)
                .WithMany()
                .HasForeignKey(entity => entity.TargetCatalogEntryTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            entityBuilder.ToTable("CatalogEntryRelationshipType", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipType_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipType_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipType_Code_NotEmpty", "LENGTH(TRIM(\"Code\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryRelationshipType_Code_MaxLength", "LENGTH(TRIM(\"Code\")) <= 256");
            });
        });

        modelBuilder.Entity<CatalogEntryType>(entityBuilder =>
        {
            entityBuilder.HasKey(entity => entity.Id);

            entityBuilder.HasAlternateKey(entity => entity.Code);

            entityBuilder.ToTable("CatalogEntryType", tableBuilder =>
            {
                tableBuilder.HasCheckConstraint("CK_CatalogEntryType_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryType_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryType_Code_NotEmpty", "LENGTH(TRIM(\"Code\")) > 0");
                tableBuilder.HasCheckConstraint("CK_CatalogEntryType_Code_MaxLength", "LENGTH(TRIM(\"Code\")) <= 256");
            });
        });

        base.OnModelCreating(modelBuilder);
    }
}
