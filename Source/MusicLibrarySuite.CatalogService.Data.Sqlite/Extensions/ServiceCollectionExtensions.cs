using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.CatalogService.Data.Contexts;
using MusicLibrarySuite.CatalogService.Data.Repositories;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.CatalogService.Data.Sqlite.Contexts;
using MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;
using MusicLibrarySuite.Infrastructure.Data.Extensions;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers the <see cref="CatalogServiceDbContext" /> database context,
    /// the <see cref="IDbContextFactory{TContext}" /> database context factory,
    /// the <see cref="IDbContextScope{TContext}" /> database context scope,
    /// the <see cref="IDbContextScopeFactory{TContext}" /> database context scope factory,
    /// and the <see cref="IDbContextProvider{TContext}" /> database context provider
    /// as services in the specified <see cref="IServiceCollection" /> collection.
    /// <para>
    /// The <see cref="SqliteCatalogServiceDbContext" /> implementation type is used for the <see cref="CatalogServiceDbContext" /> service type.
    /// </para>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <param name="configuration">The <see cref="IConfiguration" /> application configuration.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">When there is no <c>CatalogServiceConnectionString</c> connection string.</exception>
    public static IServiceCollection AddSqliteDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContextFactory<CatalogServiceDbContext, SqliteCatalogServiceDbContext>(contextOptions =>
        {
            string connectionStringName = "CatalogServiceConnectionString";

            string? connectionString = configuration.GetConnectionString(connectionStringName);
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new InvalidOperationException($"Unable to get the connection string using the \"{connectionStringName}\" configuration key.");
            }

            contextOptions.UseSqlite(connectionString, sqliteOptions =>
            {
                sqliteOptions
                    .MigrationsAssembly(typeof(SqliteCatalogServiceDbContext).Assembly.FullName)
                    .MigrationsHistoryTable("MigrationsHistory")
                    .CommandTimeout(120);
            });
        });

        return services;
    }

    /// <summary>
    /// Registers the CMS (Content Management System) repositories in the specified <see cref="IServiceCollection" /> collection.
    /// <para>
    /// The <see cref="SqliteCatalogServiceDbContext" /> implementation type is expected for the <see cref="CatalogServiceDbContext" /> service type.
    /// </para>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddSqliteCmsRepositories(this IServiceCollection services)
    {
        services.AddTransient<ICatalogRepository, SqliteCatalogRepository>();
        services.AddTransient<ICatalogRelationshipRepositoryExtension, SqliteCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogRepositoryFacade, CatalogRepositoryFacade>();

        services.AddTransient<ICatalogNodeRepository, SqliteCatalogNodeRepository>();
        services.AddTransient<ICatalogNodeToCatalogRelationshipRepositoryExtension, SqliteCatalogNodeToCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeHierarchicalRelationshipRepositoryExtension, SqliteCatalogNodeHierarchicalRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeRelationshipRepositoryExtension, SqliteCatalogNodeRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogNodeRepositoryFacade, CatalogNodeRepositoryFacade>();

        services.AddTransient<ICatalogEntryRepository, SqliteCatalogEntryRepository>();
        services.AddTransient<ICatalogEntryToCatalogRelationshipRepositoryExtension, SqliteCatalogEntryToCatalogRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryToCatalogNodeRelationshipRepositoryExtension, SqliteCatalogEntryToCatalogNodeRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipRepositoryExtension, SqliteCatalogEntryHierarchicalRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension, SqliteCatalogEntryHierarchicalRelationshipTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryRelationshipRepositoryExtension, SqliteCatalogEntryRelationshipRepositoryExtension>();
        services.AddTransient<ICatalogEntryRelationshipTypeRepositoryExtension, SqliteCatalogEntryRelationshipTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryTypeRepositoryExtension, SqliteCatalogEntryTypeRepositoryExtension>();
        services.AddTransient<ICatalogEntryRepositoryFacade, CatalogEntryRepositoryFacade>();

        return services;
    }
}
