using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryToCatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryToCatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryToCatalogRelationship",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryToCatalogRelationship", columns: x => new { x.CatalogEntryId, x.CatalogId });
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogRelationship_CatalogEntryId",
            table: "CatalogEntryToCatalogRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogRelationship_CatalogId",
            table: "CatalogEntryToCatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogRelationship_CatalogEntryId_Order",
            table: "CatalogEntryToCatalogRelationship",
            columns: ["CatalogEntryId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogRelationship_CatalogId_ReferenceOrder",
            table: "CatalogEntryToCatalogRelationship",
            columns: ["CatalogId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryToCatalogRelationship");
    }
}
