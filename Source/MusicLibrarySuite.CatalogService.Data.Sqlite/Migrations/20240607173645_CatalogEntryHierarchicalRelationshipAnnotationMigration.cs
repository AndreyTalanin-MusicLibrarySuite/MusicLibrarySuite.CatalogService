using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationshipAnnotation" /> relationship.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipAnnotationMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationshipAnnotation",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                ParentCatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogEntryHierarchicalRelationshipTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "TEXT", maxLength: 32768, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationshipAnnotation", columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns",
                    columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId },
                    principalTable: "CatalogEntryHierarchicalRelationship",
                    principalColumns: ["CatalogEntryId", "ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId"],
                    onDelete: ReferentialAction.Cascade);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrNotEmpty", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrMaxLength", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryHierarchicalRelationshipAnnotation");
    }
}
