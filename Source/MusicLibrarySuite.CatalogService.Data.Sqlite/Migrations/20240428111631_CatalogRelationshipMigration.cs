using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogRelationship",
            columns: table => new
            {
                CatalogId = table.Column<Guid>(type: "TEXT", nullable: false),
                DependentCatalogId = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "TEXT", maxLength: 32768, nullable: true),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogRelationship", columns: x => new { x.CatalogId, x.DependentCatalogId });
                table.ForeignKey(
                    name: "FK_CatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogRelationship_Catalog_DependentCatalogId",
                    column: x => x.DependentCatalogId,
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogRelationship_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogRelationship_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogRelationship_Description_NullOrNotEmpty", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                table.CheckConstraint(name: "CK_CatalogRelationship_Description_NullOrMaxLength", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
                table.CheckConstraint(name: "CK_CatalogRelationship_CatalogIds_NotEqual", sql: "\"CatalogId\" <> \"DependentCatalogId\"");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogRelationship_CatalogId",
            table: "CatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogRelationship_DependentCatalogId",
            table: "CatalogRelationship",
            column: "DependentCatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogRelationship_CatalogId_Order",
            table: "CatalogRelationship",
            columns: ["CatalogId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder",
            table: "CatalogRelationship",
            columns: ["DependentCatalogId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogRelationship");
    }
}
