using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryRelationshipAnnotation" /> extension.
/// </summary>
public partial class CatalogEntryRelationshipAnnotationMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryRelationshipAnnotation",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                DependentCatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogEntryRelationshipTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "TEXT", maxLength: 32768, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryRelationshipAnnotation", columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns",
                    columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId },
                    principalTable: "CatalogEntryRelationship",
                    principalColumns: ["CatalogEntryId", "DependentCatalogEntryId", "CatalogEntryRelationshipTypeId"],
                    onDelete: ReferentialAction.Cascade);
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Description_NullOrNotEmpty", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryRelationshipAnnotation_Description_NullOrMaxLength", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryRelationshipAnnotation");
    }
}
