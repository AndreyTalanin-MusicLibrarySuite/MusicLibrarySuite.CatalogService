using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogNodeHierarchicalRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeHierarchicalRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeHierarchicalRelationship",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                ParentCatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeHierarchicalRelationship", columns: x => new { x.CatalogNodeId, x.ParentCatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId",
                    column: x => x.ParentCatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogNodeHierarchicalRelationship_Order_EqualsZero", sql: "\"Order\" = 0");
                table.CheckConstraint(name: "CK_CatalogNodeHierarchicalRelationship_CatalogNodeIds_NotEqual", sql: "\"CatalogNodeId\" <> \"ParentCatalogNodeId\"");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeHierarchicalRelationship_CatalogNodeId",
            table: "CatalogNodeHierarchicalRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId",
            table: "CatalogNodeHierarchicalRelationship",
            column: "ParentCatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeHierarchicalRelationship_CatalogNodeId_Order",
            table: "CatalogNodeHierarchicalRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId_ReferenceOrder",
            table: "CatalogNodeHierarchicalRelationship",
            columns: ["ParentCatalogNodeId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogNodeHierarchicalRelationship");
    }
}
