using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogNodeToCatalogRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeToCatalogRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeToCatalogRelationship",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeToCatalogRelationship", columns: x => new { x.CatalogNodeId, x.CatalogId });
                table.ForeignKey(
                    name: "FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId",
                    column: x => x.CatalogId,
                    principalTable: "Catalog",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeToCatalogRelationship_CatalogNodeId",
            table: "CatalogNodeToCatalogRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeToCatalogRelationship_CatalogId",
            table: "CatalogNodeToCatalogRelationship",
            column: "CatalogId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeToCatalogRelationship_CatalogNodeId_Order",
            table: "CatalogNodeToCatalogRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeToCatalogRelationship_CatalogId_ReferenceOrder",
            table: "CatalogNodeToCatalogRelationship",
            columns: ["CatalogId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogNodeToCatalogRelationship");
    }
}
