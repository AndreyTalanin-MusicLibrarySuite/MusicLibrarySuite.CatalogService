using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationshipType" /> entity.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipTypeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationshipType",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "TEXT", nullable: false),
                OwnerCatalogEntryTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                TargetCatalogEntryTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Code = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Annotated = table.Column<bool>(type: "INTEGER", nullable: false),
                OwnerUnique = table.Column<bool>(type: "INTEGER", nullable: false),
                TargetUnique = table.Column<bool>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationshipType", columns: x => x.Id);
                table.UniqueConstraint(name: "AK_CatalogEntryHierarchicalRelationshipType_Code", columns: x => x.Code);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId",
                    column: x => x.OwnerCatalogEntryTypeId,
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId",
                    column: x => x.TargetCatalogEntryTypeId,
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Code_NotEmpty", sql: "LENGTH(TRIM(\"Code\")) > 0");
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationshipType_Code_MaxLength", sql: "LENGTH(TRIM(\"Code\")) <= 256");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationshipType_OwnerCatalogEntryTypeId",
            table: "CatalogEntryHierarchicalRelationshipType",
            column: "OwnerCatalogEntryTypeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationshipType_TargetCatalogEntryTypeId",
            table: "CatalogEntryHierarchicalRelationshipType",
            column: "TargetCatalogEntryTypeId");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryHierarchicalRelationshipType");
    }
}
