using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogNodeRelationship" /> relationship.
/// </summary>
public partial class CatalogNodeRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNodeRelationship",
            columns: table => new
            {
                CatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                DependentCatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "TEXT", maxLength: 32768, nullable: true),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNodeRelationship", columns: x => new { x.CatalogNodeId, x.DependentCatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId",
                    column: x => x.DependentCatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Description_NullOrNotEmpty", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_Description_NullOrMaxLength", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
                table.CheckConstraint(name: "CK_CatalogNodeRelationship_CatalogNodeIds_NotEqual", sql: "\"CatalogNodeId\" <> \"DependentCatalogNodeId\"");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeRelationship_CatalogNodeId",
            table: "CatalogNodeRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogNodeRelationship_DependentCatalogNodeId",
            table: "CatalogNodeRelationship",
            column: "DependentCatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeRelationship_CatalogNodeId_Order",
            table: "CatalogNodeRelationship",
            columns: ["CatalogNodeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogNodeRelationship_DependentCatalogNodeId_ReferenceOrder",
            table: "CatalogNodeRelationship",
            columns: ["DependentCatalogNodeId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogNodeRelationship");
    }
}
