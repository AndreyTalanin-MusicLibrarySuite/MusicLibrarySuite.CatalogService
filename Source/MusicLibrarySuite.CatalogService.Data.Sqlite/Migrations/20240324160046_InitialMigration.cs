using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific initial empty database migration.
/// </summary>
public partial class InitialMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
    }
}
