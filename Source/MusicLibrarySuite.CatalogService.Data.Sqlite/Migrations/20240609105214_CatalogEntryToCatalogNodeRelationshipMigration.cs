using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryToCatalogNodeRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryToCatalogNodeRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryToCatalogNodeRelationship",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogNodeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryToCatalogNodeRelationship", columns: x => new { x.CatalogEntryId, x.CatalogNodeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId",
                    column: x => x.CatalogNodeId,
                    principalTable: "CatalogNode",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId",
            table: "CatalogEntryToCatalogNodeRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId",
            table: "CatalogEntryToCatalogNodeRelationship",
            column: "CatalogNodeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId_Order",
            table: "CatalogEntryToCatalogNodeRelationship",
            columns: ["CatalogEntryId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId_ReferenceOrder",
            table: "CatalogEntryToCatalogNodeRelationship",
            columns: ["CatalogNodeId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryToCatalogNodeRelationship");
    }
}
