using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryHierarchicalRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryHierarchicalRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryHierarchicalRelationship",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                ParentCatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogEntryHierarchicalRelationshipTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryHierarchicalRelationship", columns: x => new { x.CatalogEntryId, x.ParentCatalogEntryId, x.CatalogEntryHierarchicalRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId",
                    column: x => x.ParentCatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId",
                    column: x => x.CatalogEntryHierarchicalRelationshipTypeId,
                    principalTable: "CatalogEntryHierarchicalRelationshipType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryHierarchicalRelationship_CatalogEntryIds_NotEqual", sql: "\"CatalogEntryId\" <> \"ParentCatalogEntryId\"");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryId",
            table: "CatalogEntryHierarchicalRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId",
            table: "CatalogEntryHierarchicalRelationship",
            column: "ParentCatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipTypeId",
            table: "CatalogEntryHierarchicalRelationship",
            column: "CatalogEntryHierarchicalRelationshipTypeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryHierarchicalRelationship_CatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_Order",
            table: "CatalogEntryHierarchicalRelationship",
            columns: ["CatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId_CatalogEntryHierarchicalRelationshipTypeId_ReferenceOrder",
            table: "CatalogEntryHierarchicalRelationship",
            columns: ["ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryHierarchicalRelationship");
    }
}
