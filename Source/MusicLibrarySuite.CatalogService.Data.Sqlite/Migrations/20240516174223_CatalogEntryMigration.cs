using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntry" /> entity.
/// </summary>
public partial class CatalogEntryMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntry",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogEntryTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                SystemProtected = table.Column<bool>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntry", columns: x => x.Id);
                table.ForeignKey(
                    name: "FK_CatalogEntry_CatalogEntryType_CatalogEntryTypeId",
                    column: x => x.CatalogEntryTypeId,
                    principalTable: "CatalogEntryType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntry_CatalogEntryTypeId",
            table: "CatalogEntry",
            column: "CatalogEntryTypeId");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntry");
    }
}
