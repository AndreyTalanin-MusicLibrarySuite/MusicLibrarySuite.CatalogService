using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogNode" /> entity.
/// </summary>
public partial class CatalogNodeMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogNode",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "TEXT", nullable: false),
                Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                Description = table.Column<string>(type: "TEXT", maxLength: 32768, nullable: true),
                SystemProtected = table.Column<bool>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogNode", columns: x => x.Id);
                table.CheckConstraint(name: "CK_CatalogNode_Name_NotEmpty", sql: "LENGTH(TRIM(\"Name\")) > 0");
                table.CheckConstraint(name: "CK_CatalogNode_Name_MaxLength", sql: "LENGTH(TRIM(\"Name\")) <= 256");
                table.CheckConstraint(name: "CK_CatalogNode_Description_NullOrNotEmpty", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");
                table.CheckConstraint(name: "CK_CatalogNode_Description_NullOrMaxLength", sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");
            });
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogNode");
    }
}
