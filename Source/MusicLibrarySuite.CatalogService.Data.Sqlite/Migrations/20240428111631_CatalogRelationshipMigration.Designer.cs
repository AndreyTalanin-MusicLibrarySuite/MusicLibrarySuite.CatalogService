// <auto-generated />
using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using MusicLibrarySuite.CatalogService.Data.Sqlite.Contexts;

#nullable disable

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations
{
    [DbContext(typeof(SqliteCatalogServiceDbContext))]
    [Migration("20240428111631_CatalogRelationshipMigration")]
    partial class CatalogRelationshipMigration
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.28");

            modelBuilder.Entity("MusicLibrarySuite.CatalogService.Data.Entities.Catalog", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("TEXT");

                b.Property<DateTimeOffset>("CreatedOn")
                    .ValueGeneratedOnAddOrUpdate()
                    .HasColumnType("TEXT");

                b.Property<string>("Description")
                    .HasMaxLength(32768)
                    .HasColumnType("TEXT");

                b.Property<bool>("Enabled")
                    .HasColumnType("INTEGER");

                b.Property<string>("Name")
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnType("TEXT");

                b.Property<bool>("SystemProtected")
                    .HasColumnType("INTEGER");

                b.Property<DateTimeOffset>("UpdatedOn")
                    .ValueGeneratedOnAddOrUpdate()
                    .HasColumnType("TEXT");

                b.HasKey("Id");

                b.ToTable("Catalog", (string)null);

                b.HasCheckConstraint("CK_Catalog_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");

                b.HasCheckConstraint("CK_Catalog_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");

                b.HasCheckConstraint("CK_Catalog_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");

                b.HasCheckConstraint("CK_Catalog_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
            });

            modelBuilder.Entity("MusicLibrarySuite.CatalogService.Data.Entities.CatalogRelationship", b =>
            {
                b.Property<Guid>("CatalogId")
                    .HasColumnType("TEXT");

                b.Property<Guid>("DependentCatalogId")
                    .HasColumnType("TEXT");

                b.Property<DateTimeOffset>("CreatedOn")
                    .ValueGeneratedOnAddOrUpdate()
                    .HasColumnType("TEXT");

                b.Property<string>("Description")
                    .HasMaxLength(32768)
                    .HasColumnType("TEXT");

                b.Property<bool>("Enabled")
                    .HasColumnType("INTEGER");

                b.Property<string>("Name")
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnType("TEXT");

                b.Property<int>("Order")
                    .HasColumnType("INTEGER");

                b.Property<int>("ReferenceOrder")
                    .HasColumnType("INTEGER");

                b.Property<DateTimeOffset>("UpdatedOn")
                    .ValueGeneratedOnAddOrUpdate()
                    .HasColumnType("TEXT");

                b.HasKey("CatalogId", "DependentCatalogId");

                b.HasIndex("CatalogId")
                    .HasDatabaseName("IX_CatalogRelationship_CatalogId");

                b.HasIndex("DependentCatalogId")
                    .HasDatabaseName("IX_CatalogRelationship_DependentCatalogId");

                b.HasIndex("CatalogId", "Order")
                    .IsUnique()
                    .HasDatabaseName("UIX_CatalogRelationship_CatalogId_Order");

                b.HasIndex("DependentCatalogId", "ReferenceOrder")
                    .IsUnique()
                    .HasDatabaseName("UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder");

                b.ToTable("CatalogRelationship", (string)null);

                b.HasCheckConstraint("CK_CatalogRelationship_CatalogIds_NotEqual", "\"CatalogId\" <> \"DependentCatalogId\"");

                b.HasCheckConstraint("CK_CatalogRelationship_Description_NullOrMaxLength", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768");

                b.HasCheckConstraint("CK_CatalogRelationship_Description_NullOrNotEmpty", "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0");

                b.HasCheckConstraint("CK_CatalogRelationship_Name_MaxLength", "LENGTH(TRIM(\"Name\")) <= 256");

                b.HasCheckConstraint("CK_CatalogRelationship_Name_NotEmpty", "LENGTH(TRIM(\"Name\")) > 0");
            });

            modelBuilder.Entity("MusicLibrarySuite.CatalogService.Data.Entities.CatalogRelationship", b =>
            {
                b.HasOne("MusicLibrarySuite.CatalogService.Data.Entities.Catalog", "Catalog")
                    .WithMany("CatalogRelationships")
                    .HasForeignKey("CatalogId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();

                b.HasOne("MusicLibrarySuite.CatalogService.Data.Entities.Catalog", "DependentCatalog")
                    .WithMany()
                    .HasForeignKey("DependentCatalogId")
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

                b.Navigation("Catalog");

                b.Navigation("DependentCatalog");
            });

            modelBuilder.Entity("MusicLibrarySuite.CatalogService.Data.Entities.Catalog", b =>
            {
                b.Navigation("CatalogRelationships");
            });
#pragma warning restore 612, 618
        }
    }
}
