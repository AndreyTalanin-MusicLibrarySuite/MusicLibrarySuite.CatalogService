using System;

using Microsoft.EntityFrameworkCore.Migrations;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Migrations;

/// <summary>
/// Represents the SQLite-specific database migration adding the <see cref="CatalogEntryRelationship" /> relationship.
/// </summary>
public partial class CatalogEntryRelationshipMigration : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "CatalogEntryRelationship",
            columns: table => new
            {
                CatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                DependentCatalogEntryId = table.Column<Guid>(type: "TEXT", nullable: false),
                CatalogEntryRelationshipTypeId = table.Column<Guid>(type: "TEXT", nullable: false),
                Order = table.Column<int>(type: "INTEGER", nullable: false),
                ReferenceOrder = table.Column<int>(type: "INTEGER", nullable: false),
                Enabled = table.Column<bool>(type: "INTEGER", nullable: false),
                CreatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                UpdatedOn = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey(name: "PK_CatalogEntryRelationship", columns: x => new { x.CatalogEntryId, x.DependentCatalogEntryId, x.CatalogEntryRelationshipTypeId });
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId",
                    column: x => x.CatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId",
                    column: x => x.DependentCatalogEntryId,
                    principalTable: "CatalogEntry",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.ForeignKey(
                    name: "FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId",
                    column: x => x.CatalogEntryRelationshipTypeId,
                    principalTable: "CatalogEntryRelationshipType",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Restrict);
                table.CheckConstraint(name: "CK_CatalogEntryRelationship_CatalogEntryIds_NotEqual", sql: "\"CatalogEntryId\" <> \"DependentCatalogEntryId\"");
            });

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_CatalogEntryId",
            table: "CatalogEntryRelationship",
            column: "CatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_DependentCatalogEntryId",
            table: "CatalogEntryRelationship",
            column: "DependentCatalogEntryId");

        migrationBuilder.CreateIndex(
            name: "IX_CatalogEntryRelationship_CatalogEntryRelationshipTypeId",
            table: "CatalogEntryRelationship",
            column: "CatalogEntryRelationshipTypeId");

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryRelationship_CatalogEntryId_CatalogEntryRelationshipTypeId_Order",
            table: "CatalogEntryRelationship",
            columns: ["CatalogEntryId", "CatalogEntryRelationshipTypeId", "Order"],
            unique: true);

        migrationBuilder.CreateIndex(
            name: "UIX_CatalogEntryRelationship_DependentCatalogEntryId_CatalogEntryRelationshipTypeId_ReferenceOrder",
            table: "CatalogEntryRelationship",
            columns: ["DependentCatalogEntryId", "CatalogEntryRelationshipTypeId", "ReferenceOrder"],
            unique: true);
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(name: "CatalogEntryRelationship");
    }
}
