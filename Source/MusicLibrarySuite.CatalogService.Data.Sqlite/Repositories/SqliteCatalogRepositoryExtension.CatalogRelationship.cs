using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogRelationship" /> database entity.
/// </summary>
public class SqliteCatalogRelationshipRepositoryExtension : ICatalogRelationshipRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogRelationshipRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogRelationshipRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogRelationship[]> GetCatalogRelationshipsAsync(CatalogId catalogId, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogRelationship> catalogRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
