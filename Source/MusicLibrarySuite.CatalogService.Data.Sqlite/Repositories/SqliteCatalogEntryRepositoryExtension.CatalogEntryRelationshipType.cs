using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogEntryRelationshipType" /> database entity.
/// </summary>
public class SqliteCatalogEntryRelationshipTypeRepositoryExtension : ICatalogEntryRelationshipTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogEntryRelationshipTypeRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogEntryRelationshipTypeRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationshipType?> GetCatalogEntryRelationshipTypeAsync(CatalogEntryRelationshipTypeCode catalogEntryRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeId> catalogEntryRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(IEnumerable<CatalogEntryRelationshipTypeCode> catalogEntryRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationshipType[]> GetCatalogEntryRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
