using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogEntryType" /> database entity.
/// </summary>
public class SqliteCatalogEntryTypeRepositoryExtension : ICatalogEntryTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogEntryTypeRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogEntryTypeRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryType?> GetCatalogEntryTypeAsync(CatalogEntryTypeCode catalogEntryTypeCode, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeCode> catalogEntryTypeCodes, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryType[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
