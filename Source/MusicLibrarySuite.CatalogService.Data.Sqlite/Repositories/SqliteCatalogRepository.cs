using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository for the <see cref="Catalog" /> database entity.
/// </summary>
public class SqliteCatalogRepository : ICatalogRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogRepository" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogRepository()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog?> GetCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog[]> GetCatalogsAsync(IEnumerable<CatalogId> catalogIds, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog[]> GetCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog[]> GetCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog[]> GetCatalogsAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogsAsync(DbSetFilter<Catalog> filter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogsAsync(AsyncDbSetFilter<Catalog> asyncFilter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogsAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<Catalog> AddCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task UpdateCatalogAsync(Catalog catalog, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task RemoveCatalogAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
