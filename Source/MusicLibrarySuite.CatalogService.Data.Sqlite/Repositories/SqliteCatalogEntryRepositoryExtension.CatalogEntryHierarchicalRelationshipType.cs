using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogEntryHierarchicalRelationshipType" /> database entity.
/// </summary>
public class SqliteCatalogEntryHierarchicalRelationshipTypeRepositoryExtension : ICatalogEntryHierarchicalRelationshipTypeRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogEntryHierarchicalRelationshipTypeRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogEntryHierarchicalRelationshipTypeRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryHierarchicalRelationshipType?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeCode catalogEntryHierarchicalRelationshipTypeCode, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeCode> catalogEntryHierarchicalRelationshipTypeCodes, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryHierarchicalRelationshipType[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
