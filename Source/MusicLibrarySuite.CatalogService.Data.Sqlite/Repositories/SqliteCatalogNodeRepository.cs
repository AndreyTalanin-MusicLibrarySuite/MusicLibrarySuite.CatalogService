using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;
using MusicLibrarySuite.Infrastructure.Data;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository for the <see cref="CatalogNode" /> database entity.
/// </summary>
public class SqliteCatalogNodeRepository : ICatalogNodeRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogNodeRepository" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogNodeRepository()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode[]> GetCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode[]> GetCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogNodesAsync(DbSetFilter<CatalogNode> filter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogNodesAsync(AsyncDbSetFilter<CatalogNode> asyncFilter, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNode> AddCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task UpdateCatalogNodeAsync(CatalogNode catalogNode, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
