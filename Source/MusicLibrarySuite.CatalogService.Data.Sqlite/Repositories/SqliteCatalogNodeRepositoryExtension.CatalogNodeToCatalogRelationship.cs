using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogNodeToCatalogRelationship" /> database entity.
/// </summary>
public class SqliteCatalogNodeToCatalogRelationshipRepositoryExtension : ICatalogNodeToCatalogRelationshipRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogNodeToCatalogRelationshipRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogNodeToCatalogRelationshipRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNodeToCatalogRelationship[]> GetCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogNodeToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogNodeToCatalogRelationship> catalogNodeToCatalogRelationships, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
