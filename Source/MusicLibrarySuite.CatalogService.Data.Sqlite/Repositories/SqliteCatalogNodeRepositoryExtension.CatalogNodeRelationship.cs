using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogNodeRelationship" /> database entity.
/// </summary>
public class SqliteCatalogNodeRelationshipRepositoryExtension : ICatalogNodeRelationshipRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogNodeRelationshipRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogNodeRelationshipRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogNodeRelationship[]> GetCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogNodeRelationship> catalogNodeRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
