using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogEntryToCatalogNodeRelationship" /> database entity.
/// </summary>
public class SqliteCatalogEntryToCatalogNodeRelationshipRepositoryExtension : ICatalogEntryToCatalogNodeRelationshipRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogEntryToCatalogNodeRelationshipRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogEntryToCatalogNodeRelationshipRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryToCatalogNodeRelationship[]> GetCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogEntryToCatalogNodeRelationshipsAsync(CatalogNodeId catalogNodeId, IEnumerable<CatalogEntryToCatalogNodeRelationship> catalogEntryToCatalogNodeRelationships, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
