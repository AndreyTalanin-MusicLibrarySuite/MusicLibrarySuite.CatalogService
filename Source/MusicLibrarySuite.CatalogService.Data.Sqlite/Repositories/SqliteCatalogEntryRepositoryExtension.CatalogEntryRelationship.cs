using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Data.Entities;
using MusicLibrarySuite.CatalogService.Data.Repositories.Abstractions;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// Represents a SQLite-specific implementation of a repository extension for the <see cref="CatalogEntryRelationship" /> database entity.
/// </summary>
public class SqliteCatalogEntryRelationshipRepositoryExtension : ICatalogEntryRelationshipRepositoryExtension
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SqliteCatalogEntryRelationshipRepositoryExtension" /> type.
    /// </summary>
    /// <exception cref="NotImplementedException">Thrown if the constructor is called.</exception>
    public SqliteCatalogEntryRelationshipRepositoryExtension()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task<CatalogEntryRelationship[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationship> catalogEntryRelationships, bool byTarget, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }
}
