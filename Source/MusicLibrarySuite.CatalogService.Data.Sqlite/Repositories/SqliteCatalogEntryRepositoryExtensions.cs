using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Data.Entities;

namespace MusicLibrarySuite.CatalogService.Data.Sqlite.Repositories;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to repository extensions for the <see cref="CatalogEntry" /> database entity.
/// <para>
/// List of available repository extensions:
/// <list type="bullet">
/// <item><see cref="SqliteCatalogEntryToCatalogRelationshipRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryToCatalogNodeRelationshipRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryHierarchicalRelationshipRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryHierarchicalRelationshipTypeRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryRelationshipRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryRelationshipTypeRepositoryExtension" /></item>
/// <item><see cref="SqliteCatalogEntryTypeRepositoryExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete("Do not use the __SqliteCatalogEntryRepositoryExtensions class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __SqliteCatalogEntryRepositoryExtensions
{
}
