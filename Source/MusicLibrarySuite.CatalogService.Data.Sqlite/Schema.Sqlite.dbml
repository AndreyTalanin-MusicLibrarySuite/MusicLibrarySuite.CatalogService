// ================================================================================
//  Catalog
//
//  Tables:
//      Catalog - Entity
//      CatalogRelationship - Relationship, Annotated, Two-Directional Ordering
//
//  References:
//      FK_CatalogRelationship_Catalog_CatalogId
//      FK_CatalogRelationship_Catalog_DependentCatalogId
// ================================================================================

Table "Catalog" {
    "Id" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]
    "SystemProtected" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_Catalog" [primary key constraint]
    //      "CK_Catalog_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_Catalog_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_Catalog_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_Catalog_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //  }
}

Table "CatalogRelationship" {
    "CatalogId" text [primary key, not null]
    "DependentCatalogId" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogRelationship" [primary key constraint]
    //      "FK_CatalogRelationship_Catalog_CatalogId" [foreign key constraint]
    //      "FK_CatalogRelationship_Catalog_DependentCatalogId" [foreign key constraint]
    //      "CK_CatalogRelationship_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogRelationship_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogRelationship_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_CatalogRelationship_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //      "CK_CatalogRelationship_CatalogIds_NotEqual" [check constraint, sql: "\"CatalogId\" <> \"DependentCatalogId\""]
    //  }

    Indexes {
        "CatalogId" [name: "IX_CatalogRelationship_CatalogId"]
        "DependentCatalogId" [name: "IX_CatalogRelationship_DependentCatalogId"]
        ("CatalogId", "Order") [name: "UIX_CatalogRelationship_CatalogId_Order", unique]
        ("DependentCatalogId", "ReferenceOrder") [name: "UIX_CatalogRelationship_DependentCatalogId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogRelationship_Catalog_CatalogId" {
    "CatalogRelationship"."CatalogId" > "Catalog"."Id"
}

Ref "FK_CatalogRelationship_Catalog_DependentCatalogId" {
    "CatalogRelationship"."DependentCatalogId" > "Catalog"."Id"
}


// ================================================================================
//  CatalogNode
//
//  Tables:
//      CatalogNode - Entity
//      CatalogNodeToCatalogRelationship - Relationship, Two-Directional Ordering
//      CatalogNodeHierarchicalRelationship - Relationship, Owner-Unique, Two-Directional Ordering
//      CatalogNodeRelationship - Relationship, Annotated, Two-Directional Ordering
//
//  References:
//      FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId
//      FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId
//      FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId
//      FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId
//      FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId
//      FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId
// ================================================================================

Table "CatalogNode" {
    "Id" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]
    "SystemProtected" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogNode" [primary key constraint]
    //      "CK_CatalogNode_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogNode_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogNode_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_CatalogNode_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //  }
}

Table "CatalogNodeToCatalogRelationship" {
    "CatalogNodeId" text [primary key, not null]
    "CatalogId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogNodeToCatalogRelationship" [primary key constraint]
    //      "FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId" [foreign key constraint]
    //      "FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId" [foreign key constraint]
    //  }

    Indexes {
        "CatalogNodeId" [name: "IX_CatalogNodeToCatalogRelationship_CatalogNodeId"]
        "CatalogId" [name: "IX_CatalogNodeToCatalogRelationship_CatalogId"]
        ("CatalogNodeId", "Order") [name: "UIX_CatalogNodeToCatalogRelationship_CatalogNodeId_Order", unique]
        ("CatalogId", "ReferenceOrder") [name: "UIX_CatalogNodeToCatalogRelationship_CatalogId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogNodeToCatalogRelationship_CatalogNode_CatalogNodeId" {
    "CatalogNodeToCatalogRelationship"."CatalogNodeId" > "CatalogNode"."Id"
}

Ref "FK_CatalogNodeToCatalogRelationship_Catalog_CatalogId" {
    "CatalogNodeToCatalogRelationship"."CatalogId" > "Catalog"."Id"
}

Table "CatalogNodeHierarchicalRelationship" {
    "CatalogNodeId" text [primary key, not null]
    "ParentCatalogNodeId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogNodeHierarchicalRelationship" [primary key constraint]
    //      "FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId" [foreign key constraint]
    //      "FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId" [foreign key constraint]
    //      "CK_CatalogNodeHierarchicalRelationship_Order_EqualsZero" [check constraint, sql: "\"Order\" = 0"]
    //      "CK_CatalogNodeHierarchicalRelationship_CatalogNodeIds_NotEqual" [check constraint, sql: "\"CatalogNodeId\" <> \"ParentCatalogNodeId\""]
    //  }

    Indexes {
        "CatalogNodeId" [name: "IX_CatalogNodeHierarchicalRelationship_CatalogNodeId"]
        "ParentCatalogNodeId" [name: "IX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId"]
        ("CatalogNodeId", "Order") [name: "UIX_CatalogNodeHierarchicalRelationship_CatalogNodeId_Order", unique]
        ("ParentCatalogNodeId", "ReferenceOrder") [name: "UIX_CatalogNodeHierarchicalRelationship_ParentCatalogNodeId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogNodeHierarchicalRelationship_CatalogNode_CatalogNodeId" {
    "CatalogNodeHierarchicalRelationship"."CatalogNodeId" > "CatalogNode"."Id"
}

Ref "FK_CatalogNodeHierarchicalRelationship_CatalogNode_ParentCatalogNodeId" {
    "CatalogNodeHierarchicalRelationship"."ParentCatalogNodeId" > "CatalogNode"."Id"
}

Table "CatalogNodeRelationship" {
    "CatalogNodeId" text [primary key, not null]
    "DependentCatalogNodeId" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogNodeRelationship" [primary key constraint]
    //      "FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId" [foreign key constraint]
    //      "FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId" [foreign key constraint]
    //      "CK_CatalogNodeRelationship_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogNodeRelationship_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogNodeRelationship_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_CatalogNodeRelationship_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //      "CK_CatalogNodeRelationship_CatalogNodeIds_NotEqual" [check constraint, sql: "\"CatalogNodeId\" <> \"DependentCatalogNodeId\""]
    //  }

    Indexes {
        "CatalogNodeId" [name: "IX_CatalogNodeRelationship_CatalogNodeId"]
        "DependentCatalogNodeId" [name: "IX_CatalogNodeRelationship_DependentCatalogNodeId"]
        ("CatalogNodeId", "Order") [name: "UIX_CatalogNodeRelationship_CatalogNodeId_Order", unique]
        ("DependentCatalogNodeId", "ReferenceOrder") [name: "UIX_CatalogNodeRelationship_DependentCatalogNodeId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogNodeRelationship_CatalogNode_CatalogNodeId" {
    "CatalogNodeRelationship"."CatalogNodeId" > "CatalogNode"."Id"
}

Ref "FK_CatalogNodeRelationship_CatalogNode_DependentCatalogNodeId" {
    "CatalogNodeRelationship"."DependentCatalogNodeId" > "CatalogNode"."Id"
}


// ================================================================================
//  CatalogEntry
//
//  Tables:
//      CatalogEntry - Entity
//      CatalogEntryToCatalogRelationship - Relationship, Two-Directional Ordering
//      CatalogEntryToCatalogNodeRelationship - Relationship, Two-Directional Ordering
//      CatalogEntryHierarchicalRelationship - Relationship, Two-Directional Ordering
//      CatalogEntryHierarchicalRelationshipAnnotation - Extension, One-To-One
//      CatalogEntryHierarchicalRelationshipType - Entity
//      CatalogEntryRelationship - Relationship, Two-Directional Ordering
//      CatalogEntryRelationshipAnnotation - Extension, One-To-One
//      CatalogEntryRelationshipType - Entity
//      CatalogEntryType - Entity
//
//  References:
//      FK_CatalogEntry_CatalogEntryType_CatalogEntryTypeId
//      FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId
//      FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId
//      FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId
//      FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId
//      FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId
//      FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId
//      FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId
//      FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns
//      FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId
//      FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId
//      FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId
//      FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId
//      FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId
//      FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns
//      FK_CatalogEntryRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId
//      FK_CatalogEntryRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId
// ================================================================================

Table "CatalogEntry" {
    "Id" text [primary key, not null]
    "CatalogEntryTypeId" text [not null]
    "SystemProtected" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntry" [primary key constraint]
    //      "FK_CatalogEntry_CatalogEntryType_CatalogEntryTypeId" [foreign key constraint]
    //  }

    Indexes {
        "CatalogEntryTypeId" [name: "IX_CatalogEntry_CatalogEntryTypeId"]
    }
}

Ref "FK_CatalogEntry_CatalogEntryType_CatalogEntryTypeId" {
    "CatalogEntry"."CatalogEntryTypeId" > "CatalogEntryType"."Id"
}

Table "CatalogEntryToCatalogRelationship" {
    "CatalogEntryId" text [primary key, not null]
    "CatalogId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryToCatalogRelationship" [primary key constraint]
    //      "FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId" [foreign key constraint]
    //  }

    Indexes {
        "CatalogEntryId" [name: "IX_CatalogEntryToCatalogRelationship_CatalogEntryId"]
        "CatalogId" [name: "IX_CatalogEntryToCatalogRelationship_CatalogId"]
        ("CatalogEntryId", "Order") [name: "UIX_CatalogEntryToCatalogRelationship_CatalogEntryId_Order", unique]
        ("CatalogId", "ReferenceOrder") [name: "UIX_CatalogEntryToCatalogRelationship_CatalogId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogEntryToCatalogRelationship_CatalogEntry_CatalogEntryId" {
    "CatalogEntryToCatalogRelationship"."CatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryToCatalogRelationship_Catalog_CatalogId" {
    "CatalogEntryToCatalogRelationship"."CatalogId" > "Catalog"."Id"
}

Table "CatalogEntryToCatalogNodeRelationship" {
    "CatalogEntryId" text [primary key, not null]
    "CatalogNodeId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryToCatalogNodeRelationship" [primary key constraint]
    //      "FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId" [foreign key constraint]
    //  }

    Indexes {
        "CatalogEntryId" [name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId"]
        "CatalogNodeId" [name: "IX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId"]
        ("CatalogEntryId", "Order") [name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogEntryId_Order", unique]
        ("CatalogNodeId", "ReferenceOrder") [name: "UIX_CatalogEntryToCatalogNodeRelationship_CatalogNodeId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogEntryToCatalogNodeRelationship_CatalogEntry_CatalogEntryId" {
    "CatalogEntryToCatalogNodeRelationship"."CatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryToCatalogNodeRelationship_CatalogNode_CatalogNodeId" {
    "CatalogEntryToCatalogNodeRelationship"."CatalogNodeId" > "CatalogNode"."Id"
}

Table "CatalogEntryHierarchicalRelationship" {
    "CatalogEntryId" text [primary key, not null]
    "ParentCatalogEntryId" text [primary key, not null]
    "CatalogEntryHierarchicalRelationshipTypeId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryHierarchicalRelationship" [primary key constraint]
    //      "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId" [foreign key constraint]
    //      "CK_CatalogEntryHierarchicalRelationship_CatalogEntryIds_NotEqual" [check constraint, sql: "\"CatalogEntryId\" <> \"ParentCatalogEntryId\""]
    //  }

    Indexes {
        "CatalogEntryId" [name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryId"]
        "ParentCatalogEntryId" [name: "IX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId"]
        "CatalogEntryHierarchicalRelationshipTypeId" [name: "IX_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipTypeId"]
        ("CatalogEntryId", "Order") [name: "UIX_CatalogEntryHierarchicalRelationship_CatalogEntryId_Order", unique]
        ("ParentCatalogEntryId", "ReferenceOrder") [name: "UIX_CatalogEntryHierarchicalRelationship_ParentCatalogEntryId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_CatalogEntryId" {
    "CatalogEntryHierarchicalRelationship"."CatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryHierarchicalRelationship_CatalogEntry_ParentCatalogEntryId" {
    "CatalogEntryHierarchicalRelationship"."ParentCatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryHierarchicalRelationship_CatalogEntryHierarchicalRelationshipType_CatalogEntryHierarchicalRelationshipTypeId" {
    "CatalogEntryHierarchicalRelationship"."CatalogEntryHierarchicalRelationshipTypeId" > "CatalogEntryHierarchicalRelationshipType"."Id"
}

Table "CatalogEntryHierarchicalRelationshipAnnotation" {
    "CatalogEntryId" text [primary key, not null]
    "ParentCatalogEntryId" text [primary key, not null]
    "CatalogEntryHierarchicalRelationshipTypeId" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]

    //  Constraints {
    //      "PK_CatalogEntryHierarchicalRelationshipAnnotation" [primary key constraint]
    //      "FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns" [foreign key constraint]
    //      "CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogEntryHierarchicalRelationshipAnnotation_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_CatalogEntryHierarchicalRelationshipAnnotation_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //  }
}

Ref "FK_CatalogEntryHierarchicalRelationshipAnnotation_CatalogEntryHierarchicalRelationship_MultipleColumns" {
    "CatalogEntryHierarchicalRelationshipAnnotation".("CatalogEntryId", "ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId") >
        "CatalogEntryHierarchicalRelationship".("CatalogEntryId", "ParentCatalogEntryId", "CatalogEntryHierarchicalRelationshipTypeId")
}

Table "CatalogEntryHierarchicalRelationshipType" {
    "Id" text [primary key, not null]
    "OwnerCatalogEntryTypeId" text [not null]
    "TargetCatalogEntryTypeId" text [not null]
    "Name" text [not null]
    "Code" text [not null, unique]
    "Annotated" integer [not null]
    "OwnerUnique" integer [not null]
    "TargetUnique" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryHierarchicalRelationshipType" [primary key constraint]
    //      "AK_CatalogEntryHierarchicalRelationshipType_Code" [alternate key constraint]
    //      "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId" [foreign key constraint]
    //      "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId" [foreign key constraint]
    //      "CK_CatalogEntryHierarchicalRelationshipType_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogEntryHierarchicalRelationshipType_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogEntryHierarchicalRelationshipType_Code_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Code\")) > 0"]
    //      "CK_CatalogEntryHierarchicalRelationshipType_Code_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Code\")) <= 256"]
    //  }

    Indexes {
        "Code" [name: "AK_CatalogEntryHierarchicalRelationshipType_Code", unique]
        "OwnerCatalogEntryTypeId" [name: "IX_CatalogEntryHierarchicalRelationshipType_OwnerCatalogEntryTypeId"]
        "TargetCatalogEntryTypeId" [name: "IX_CatalogEntryHierarchicalRelationshipType_TargetCatalogEntryTypeId"]
    }
}

Ref "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId" {
    "CatalogEntryHierarchicalRelationshipType"."OwnerCatalogEntryTypeId" > "CatalogEntryType"."Id"
}

Ref "FK_CatalogEntryHierarchicalRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId" {
    "CatalogEntryHierarchicalRelationshipType"."TargetCatalogEntryTypeId" > "CatalogEntryType"."Id"
}

Table "CatalogEntryRelationship" {
    "CatalogEntryId" text [primary key, not null]
    "DependentCatalogEntryId" text [primary key, not null]
    "CatalogEntryRelationshipTypeId" text [primary key, not null]
    "Order" integer [not null]
    "ReferenceOrder" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryRelationship" [primary key constraint]
    //      "FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId" [foreign key constraint]
    //      "FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId" [foreign key constraint]
    //      "CK_CatalogEntryRelationship_CatalogEntryIds_NotEqual" [check constraint, sql: "\"CatalogEntryId\" <> \"DependentCatalogEntryId\""]
    //  }

    Indexes {
        "CatalogEntryId" [name: "IX_CatalogEntryRelationship_CatalogEntryId"]
        "DependentCatalogEntryId" [name: "IX_CatalogEntryRelationship_DependentCatalogEntryId"]
        "CatalogEntryRelationshipTypeId" [name: "IX_CatalogEntryRelationship_CatalogEntryRelationshipTypeId"]
        ("CatalogEntryId", "Order") [name: "UIX_CatalogEntryRelationship_CatalogEntryId_Order", unique]
        ("DependentCatalogEntryId", "ReferenceOrder") [name: "UIX_CatalogEntryRelationship_DependentCatalogEntryId_ReferenceOrder", unique]
    }
}

Ref "FK_CatalogEntryRelationship_CatalogEntry_CatalogEntryId" {
    "CatalogEntryRelationship"."CatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryRelationship_CatalogEntry_DependentCatalogEntryId" {
    "CatalogEntryRelationship"."DependentCatalogEntryId" > "CatalogEntry"."Id"
}

Ref "FK_CatalogEntryRelationship_CatalogEntryRelationshipType_CatalogEntryRelationshipTypeId" {
    "CatalogEntryRelationship"."CatalogEntryRelationshipTypeId" > "CatalogEntryRelationshipType"."Id"
}

Table "CatalogEntryRelationshipAnnotation" {
    "CatalogEntryId" text [primary key, not null]
    "DependentCatalogEntryId" text [primary key, not null]
    "CatalogEntryRelationshipTypeId" text [primary key, not null]
    "Name" text [not null]
    "Description" text [null]

    //  Constraints {
    //      "PK_CatalogEntryRelationshipAnnotation" [primary key constraint]
    //      "FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns" [foreign key constraint]
    //      "CK_CatalogEntryRelationshipAnnotation_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogEntryRelationshipAnnotation_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogEntryRelationshipAnnotation_Description_NullOrNotEmpty" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) > 0"]
    //      "CK_CatalogEntryRelationshipAnnotation_Description_NullOrMaxLength" [check constraint, sql: "\"Description\" IS NULL OR LENGTH(TRIM(\"Description\")) <= 32768"]
    //  }
}

Ref "FK_CatalogEntryRelationshipAnnotation_CatalogEntryRelationship_MultipleColumns" {
    "CatalogEntryRelationshipAnnotation".("CatalogEntryId", "DependentCatalogEntryId", "CatalogEntryRelationshipTypeId") >
        "CatalogEntryRelationship".("CatalogEntryId", "DependentCatalogEntryId", "CatalogEntryRelationshipTypeId")
}

Table "CatalogEntryRelationshipType" {
    "Id" text [primary key, not null]
    "OwnerCatalogEntryTypeId" text [not null]
    "TargetCatalogEntryTypeId" text [not null]
    "Name" text [not null]
    "Code" text [not null, unique]
    "Annotated" integer [not null]
    "OwnerUnique" integer [not null]
    "TargetUnique" integer [not null]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryRelationshipType" [primary key constraint]
    //      "AK_CatalogEntryRelationshipType_Code" [alternate key constraint]
    //      "FK_CatalogEntryRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId" [foreign key constraint]
    //      "FK_CatalogEntryRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId" [foreign key constraint]
    //      "CK_CatalogEntryRelationshipType_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogEntryRelationshipType_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogEntryRelationshipType_Code_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Code\")) > 0"]
    //      "CK_CatalogEntryRelationshipType_Code_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Code\")) <= 256"]
    //  }

    Indexes {
        "Code" [name: "AK_CatalogEntryRelationshipType_Code", unique]
        "OwnerCatalogEntryTypeId" [name: "IX_CatalogEntryRelationshipType_OwnerCatalogEntryTypeId"]
        "TargetCatalogEntryTypeId" [name: "IX_CatalogEntryRelationshipType_TargetCatalogEntryTypeId"]
    }
}

Ref "FK_CatalogEntryRelationshipType_CatalogEntryType_OwnerCatalogEntryTypeId" {
    "CatalogEntryRelationshipType"."OwnerCatalogEntryTypeId" > "CatalogEntryType"."Id"
}

Ref "FK_CatalogEntryRelationshipType_CatalogEntryType_TargetCatalogEntryTypeId" {
    "CatalogEntryRelationshipType"."TargetCatalogEntryTypeId" > "CatalogEntryType"."Id"
}

Table "CatalogEntryType" {
    "Id" text [primary key, not null]
    "Name" text [not null]
    "Code" text [not null, unique]
    "Enabled" integer [not null]
    "CreatedOn" text [not null]
    "UpdatedOn" text [not null]

    //  Constraints {
    //      "PK_CatalogEntryType" [primary key constraint]
    //      "AK_CatalogEntryType_Code" [alternate key constraint]
    //      "CK_CatalogEntryType_Name_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Name\")) > 0"]
    //      "CK_CatalogEntryType_Name_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Name\")) <= 256"]
    //      "CK_CatalogEntryType_Code_NotEmpty" [check constraint, sql: "LENGTH(TRIM(\"Code\")) > 0"]
    //      "CK_CatalogEntryType_Code_MaxLength" [check constraint, sql: "LENGTH(TRIM(\"Code\")) <= 256"]
    //  }

    Indexes {
        "Code" [name: "AK_CatalogEntryType_Code", unique]
    }
}
